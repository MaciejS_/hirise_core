from math import ceil, floor

from torch.nn import functional as tnnf


class Downsampler:
    pass

class AvgpoolDownsampler(Downsampler):

    @classmethod
    def downsample(cls, smap, target_s):
        size, stride, padding = zip(*(find_avgpool_params_brute_force(smap.shape[-1], target_s),) * 2)
        return tnnf.avg_pool2d(smap[None, None, ...],
                               kernel_size=size,
                               stride=stride,
                               padding=padding,
                               count_include_pad=False).squeeze()

    @classmethod
    def find_avgpool_params_brute_force(cls, input_size: int, output_size:int):
        min_kernel_size = floor(input_size / output_size)
        for kernel_size in range(min_kernel_size, input_size + 1):
            for stride_length in range(kernel_size, 0, -1):
                for padding_width in range(0, ceil(kernel_size / 2)):
                    n_kernels = 1 + (input_size + 2*padding_width - kernel_size) / stride_length
                    if n_kernels == output_size:
                        return [kernel_size, stride_length, padding_width]
        raise ValueError(f"Couldn't find AvgPool2D params for input_size: {input_size} and output_size:{output_size}")



def avgpool_downsample(smap, target_s):
    size, stride, padding = zip(*(find_avgpool_params_brute_force(smap.shape[-1], target_s),)*2)
    return tnnf.avg_pool2d(smap[None, None, ...],
                           kernel_size=size,
                           stride=stride,
                           padding=padding,
                           count_include_pad=False).squeeze()


def get_interpolation_upsampler(mode):
    return lambda smap, target_s: tnnf.interpolate(smap, (target_s, target_s), mode=mode)


def find_avgpool_params_brute_force(input_size: int, output_size:int):
    min_kernel_size = floor(input_size / output_size)
    for kernel_size in range(min_kernel_size, input_size + 1):
        for stride_length in range(kernel_size, 0, -1):
            for padding_width in range(0, ceil(kernel_size / 2)):
                n_kernels = 1 + (input_size + 2*padding_width - kernel_size) / stride_length
                if n_kernels == output_size:
                    return [kernel_size, stride_length, padding_width]
    raise ValueError(f"Couldn't find AvgPool2D params for input_size: {input_size} and output_size:{output_size}")