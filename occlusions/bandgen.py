import math
from typing import Union, List

import torch
from hirise.experiment.utils import torch_normalize
from hirise.occlusions.rescaling import get_interpolation_upsampler


class BandGen:

    def __init__(self, downsampler):
        self.downsampler = downsampler

    def generate_bands(self, saliency_map: torch.Tensor, saliency_bounds: Union[List[List[float]], torch.Tensor], s: int) -> torch.Tensor:
        """
        Generates a set of N ma

        :param saliency_map: torch.Tensor
        :param saliency_bounds: 2D float tensor of shape [N, 2], containing N pairs of (lower, upper) band value limits
        :param s: int, target
        :return: 3D Tensor of shape
        """
        saliency_bounds = torch.as_tensor(saliency_bounds)
        if not (saliency_bounds.ndim == 2 and saliency_bounds.shape[-1] == 2):
            raise ValueError(f"saliency_bounds must be a 2-dimensional tensor of shape [n, 2], got shape {saliency_bounds.shape}")

        downsampled_map = self.downsampler.downsample(saliency_map, s) if s is not None else saliency_map

        upper_edge_funcs = [torch.lt if math.isclose(next_lower, prev_upper) else torch.le
                               for (next_lower, prev_upper)
                               in zip(saliency_bounds[1:, 0], saliency_bounds[:, 1])] + [torch.le] # last bound always uses le

        bands = []
        for lower, upper, upper_func in zip(saliency_bounds[:, 0], saliency_bounds[:, 1], upper_edge_funcs):
            band = (torch.ge(downsampled_map, lower)
                    * upper_func(downsampled_map, upper)).to(torch.float)
            bands.append(band)

        return torch.stack(bands)

    def __str__(self):

        return f"{self.__class__.__name__}(downsampler={self.downsampler.__name__})"

class ZOrderBandGen(BandGen):

    def __init__(self, downsampler, upsampler=None):
        self.downsampler = downsampler
        self.upsampler = upsampler or get_interpolation_upsampler("nearest")

    def generate_bands(self, saliency_map: torch.Tensor, saliency_bounds:Union[list, torch.Tensor], s_levels: Union[list, torch.Tensor]):
        """
        Generates adaptive bands using the z-order approach (cookiecutters)
        All returned

        :param saliency_map: 2D tensor containing a saliency map
        :param saliency_bounds: list of N (lower, upper) pairs, or tensor of shape (N, 2), containing saliency values limiting the bands
        :param s_values: list of N `s` values, defining source
        :return: (N, max_s, max_s) tensor
        """
        proto_indices = list(range(len(s_levels)))
        s_val_indices = list(zip(proto_indices, proto_indices[1:] + [proto_indices[-1]]))

        #     downsampled_maps = [tnnf.interpolate(smap[None, None, ...], (s,s), mode="nearest").squeeze() for s in s_levels]
        downsampled_maps = [self.downsampler.downsample(saliency_map, s) for s in s_levels]
        downsampled_maps = list(map(torch_normalize, downsampled_maps))

        # generate unilateral bands
        unilateral_bands = [(torch.ge(downsampled_maps[n], saliency_bounds[n, 0]).to(torch.float),
                             torch.gt(downsampled_maps[n_next], saliency_bounds[n, 1]).to(torch.float))
                            for (n, n_next)
                            in s_val_indices]

        max_s = max(s_levels)
        common_upsampled = [list(
            map(lambda x: self.upsampler(x[None, None, ...], max_s).squeeze(),
                (ul_low, ul_high)))
                            for ul_low, ul_high
                            in unilateral_bands]

        t_common_upsampled = torch.stack([torch.stack(x) for x in common_upsampled])
        step_diffs = t_common_upsampled[:, 0] - t_common_upsampled[:, 1]
        cookiecutters = torch.clamp(torch.cumsum(reversed(t_common_upsampled[:, 0]), dim=0), 0, 1)
        bands_from_cookiecutters = torch.cat(
                (torch.clamp(step_diffs[:-1] - reversed(cookiecutters[:-1]), 0, 1),
                 cookiecutters[0].unsqueeze(0)),
                dim=0)

        return bands_from_cookiecutters
