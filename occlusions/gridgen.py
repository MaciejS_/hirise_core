import warnings

import more_itertools
import torch

from math import ceil, floor
from typing import Union, List, Collection


class GridGen:
    """
    Creates a grid of binary values, which serves as a base for masks tensor.
    This class is an abstract base for concrete implementations
    """
    # Number of extra dimensions prepended to the result.
    _N_EXTRA_DIMS = 2

    def __init__(self) -> None:
        super().__init__()

    def generate_grid(self, N: int, shape: Union[torch.Size, Collection[int]], p1: float):
        """
        Generates a (1+N)-D tensor of N-D binary grids.
        Output shape: (N, *shape)

        :param N: number of grids to generate
        :param shape:  desired shape of a single grid
        :param p1: occlusion probability for a single cell
        :return: (N+1)-D grid tensor
        """
        raise NotImplementedError("Must be overridden by a concrete implementation")

    def generate_grids_within_limits(self,
                                     Ns: Union[int, List[int], torch.Tensor],
                                     binary_maps: Union[torch.Tensor, List[torch.Tensor]],
                                     p1s: Union[float, List[float], torch.Tensor]):
        """
        Ns and p1s should either be single values, or have the same length as binary_maps.
        `binary_maps` can either be a ND tensor, if all maps have the same shape, or a list of 2D tensors if shapes vary.

        :param Ns: determines the number of grids to generate for each of `binary_maps`. if a single int is given,
        :param binary_maps: list
        :param p1s: fill_rate values.
        :return: list of tensors of shape (N[i], 1, *binary_map[i].shape)
        """
        if isinstance(binary_maps, list) or binary_maps.ndim == 3:
            Ns = [Ns] * len(binary_maps) if isinstance(Ns, int) else Ns
            p1s = [p1s] * len(binary_maps) if isinstance(p1s, (float, int)) else p1s
            return [self.grids_for_band(n, binary_map, p1) for (n, binary_map, p1) in zip(Ns, binary_maps, p1s)]
        elif binary_maps.ndim == 2:
            if not isinstance(Ns, int) or not isinstance(p1s, float):
                raise TypeError(
                    "if single `binary_map` is provided, `Ns` and `p1s` should also be single values, rather than collections")
            return [self.grids_for_band(Ns, binary_maps, p1s)]

    def grids_for_band(self, N: int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        """
        Generate a set of `N` binary grids of shape `binary_map.shape`, each of which has `p1` cells set to 1, however
        these "active" cells will only be found where `binary_map` has ones.

        :param N: number of grids to generate
        :param binary_map: binary map which determines the shape of a single grid. Cells set to zero will also be set to zero in output grids
        :param p1: target fill rate
        :return: float tensor of shape [N, *binary_map.shape]
        """
        raise NotImplementedError("Must be overridden by a concrete implementation")

    def __str__(self):
        return self.__class__.__name__


class CoordinateGridGen(GridGen):
    """
    Coordinate-based grid generators create grids by generating a list of coordinates, which indicate the grid cells to be kept visible
    This approach guarantees that no mask will be left fully occluded, even for low `p1` fill rate, but performs worse than threshold-based method.
    Performance decreases with the number of cells to occlude (it depends on both `p1` and `s`)
    All coordinates are generated independently and may overlap, producing grids with fill rate below `p1`
    """

    def __init__(self, device: torch.device) -> None:
        super().__init__()
        self.device = device

    def generate_grid(self, N: int, shape: Union[torch.Size, Collection[int]], p1: float) -> torch.Tensor:
        size = torch.as_tensor(shape).prod().item()
        ndim = len(shape)
        if size < 2:
            warnings.warn(f"shape == {shape} used in generate_grid. Experiment results may be unreliable.",
                          category=RuntimeWarning)

        grid = torch.zeros((N, *shape), device=self.device)
        cells_to_fill = ceil(p1 * size)

        full_ndidx = torch.nonzero(torch.ones(grid.shape[:-ndim])).chunk(grid.ndim - ndim, dim=1)
        n_idx = len(full_ndidx[0])
        rands_for_coords = torch.rand((ndim, n_idx, cells_to_fill), device=self.device)
        cell_coords = (rands_for_coords * torch.tensor(shape, device=self.device).reshape(
                (ndim, *((1,) * (rands_for_coords.ndim - 1))))) \
            .to(dtype=torch.long)

        grid[(*full_ndidx, *cell_coords)] = 1
        return grid

    def grids_for_band(self, N: int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        cell_coords = torch.nonzero(binary_map, as_tuple=False)
        n_picks = ceil(len(cell_coords) * p1)

        grids = torch.zeros(N, *binary_map.shape, device=self.device)
        grid_idx = torch.arange(N).repeat_interleave(n_picks).to(torch.long)
        random_picks = torch.floor(torch.rand((n_picks * N)) * len(cell_coords)).to(torch.long)
        picked_coords = cell_coords[random_picks]
        grids[(grid_idx, *picked_coords.T)] = 1

        return grids


class SmartCoordinateGridGen(CoordinateGridGen):
    """
    A smarter variant of CorrdinateGridGen, which tries to avoid the coupon collector problem, by generating (1-p1) 0's on grid of ones,
    rather than p1 1's on grid of zeros.

    It'll still exhibit worst fill-rate reliability issues around p1=0.5
    """

    def generate_grid(self, N: int, shape: Union[int, torch.Size, Collection[int]], p1: float) -> torch.Tensor:
        # risky s==0 warning issued by super().generate_grid()

        if p1 > 0.5:
            return (~(super().generate_grid(N, shape, 1 - p1).bool())).float()
        else:
            return super().generate_grid(N, shape, p1)


    def grids_for_band(self, N: int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        binary_map = binary_map.to(self.device)
        if p1 > 0.5:
            return (~(super().grids_for_band(N, binary_map, 1-p1)).bool()).float() * binary_map
        else:
            return super().grids_for_band(N, binary_map, p1)


class ThresholdGridGen(GridGen):
    """
    Threshold-based grid generators create a grid of random values and then use the occlusion probability value (`p1`)
    as a digitization threshold. Cells corresponding to values below that threshold are occluded.
    For low `s` and `p1` values, some grids might end up being empty. If `fix_empty` is True, empty masks will be amended by occluding p1*(s^2) randomly selected cells.
    Performance decreases with `s`
    """

    def __init__(self, device:torch.device) -> None:
        super().__init__()
        self.device = device

    def generate_grid(self, N: int, shape: Union[int, torch.Size, Collection[int]], p1: float) -> torch.Tensor:
        """
        Generate a 4-D tensor of binary grids of shape (N, s, s])

        :param N: number of masks to generate
        :param s: masks are square grids of (s,s) shape. value of this parameter determines the edge length
        :param p1: occlusion probability
        :param target_size: shape of the result mask
        :return: 4-D torch.tensor containing masks, located on the device provided on initialization
        """
        size = torch.prod(torch.as_tensor(shape)).item()
        if size < 2:
            warnings.warn(f"shape == {shape} used in generate_grid. Experiment results may be unreliable.",
                          category=RuntimeWarning)

        return (torch.rand((N, *shape), device=self.device) < p1).float()

    def grids_for_band(self, N: int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        band = binary_map.to(self.device)
        return self.generate_grid(N, binary_map.shape, p1) * band

    def __str__(self):
        return f"{self.__class__.__name__}"


class PermutationGridGen(GridGen):
    """
    Grid generator based on a random permutation of coordinates.
    Guarantees exact fill rate on each mask (rounded to nearest multiple of 1/prod(shape))
    """

    def __init__(self, device: torch.device) -> None:
        super().__init__()
        self.device = device

    def generate_grid(self, N: int, shape: Union[int, torch.Size, Collection[int]], p1: float) -> torch.Tensor:
        size = torch.as_tensor(shape).prod().item()
        if size < 2:
            warnings.warn(f"shape == {shape} used in generate_grid. Experiment results may be unreliable.",
                          category=RuntimeWarning)

        grid = torch.zeros((N, *shape), device=self.device)
        n_cells_float = p1 * size
        cells_to_fill = ceil(n_cells_float) if p1 < 0.5 else floor(n_cells_float)

        permutations = [torch.randperm(size, dtype=torch.long)[:cells_to_fill] for _ in range(N)]
        selected_indices = torch.cat(permutations, dim=0).to(self.device)
        grid_idx = torch.arange(N, dtype=torch.long, device=self.device).repeat_interleave(cells_to_fill)
        flat_view_of_single_map = grid.view(N, -1)
        flat_view_of_single_map[(grid_idx, selected_indices)] = 1

        return grid

    def grids_for_band(self, N:int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        map_on_device = binary_map.to(device=self.device)
        cell_coords = torch.nonzero(map_on_device, as_tuple=False)
        n_cells_float = len(cell_coords) * p1
        n_picks = ceil(n_cells_float) if p1 < 0.5 else floor(n_cells_float)

        grids = torch.zeros(N, *map_on_device.shape, device=self.device)
        grid_idx = torch.arange(N, device=self.device).repeat_interleave(n_picks).to(torch.long)
        selections = torch.cat([torch.randperm(len(cell_coords))[:n_picks] for _ in range(N)], dim=0).to(device=self.device)
        picked_coords = cell_coords[selections]
        grids[(grid_idx, *picked_coords.T)] = 1

        return grids


class CombinationGridGen(GridGen):
    """
    Grid generator, which picks random combinations of occlusions
    Each mask is based on an independently generated combination, so duplicates are possible.
    Performance decreases linearly with grid size
    """

    def __init__(self, device: torch.device) -> None:
        super().__init__()
        self.device = device

    def generate_grid(self, N: int, shape: Union[int, torch.Size, Collection[int]], p1: float) -> torch.Tensor:
        size = torch.as_tensor(shape).prod().item()
        if size < 2:
            warnings.warn(f"shape == {shape} used in generate_grid. Experiment results may be unreliable.",
                          category=RuntimeWarning)

        grid = torch.zeros((N, *shape), device=self.device)
        n_cells_float = p1 * size
        cells_to_fill = ceil(n_cells_float) if p1 < 0.5 else floor(n_cells_float)

        idxs = list(range(size))
        combinations = [more_itertools.random_combination(idxs, r=cells_to_fill) for _ in range(N)]
        selected_indices = torch.as_tensor(combinations, dtype=torch.long).flatten().to(self.device)
        grid_idx = torch.arange(N, dtype=torch.long, device=self.device).repeat_interleave(cells_to_fill)
        flat_view_of_grids = grid.view(N, -1)
        flat_view_of_grids[(grid_idx, selected_indices)] = 1

        return grid

    def grids_for_band(self, N:int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        map_on_device = binary_map.to(device=self.device)
        cell_coords = torch.nonzero(map_on_device, as_tuple=False)
        n_cells_float = len(cell_coords) * p1
        n_picks = ceil(n_cells_float) if p1 < 0.5 else floor(n_cells_float)

        grids = torch.zeros(N, *map_on_device.shape, device=self.device)

        if n_picks:
            grid_idx = torch.arange(N, device=self.device).repeat_interleave(n_picks).to(torch.long)
            selections = [torch.stack(more_itertools.random_combination(cell_coords, r=n_picks)) for _ in range(N)]
            picked_coords = torch.cat(selections, dim=0).to(device=self.device)
            grids[(grid_idx, *picked_coords.T)] = 1

        return grids


class FixingGridGen(GridGen):

    FIXING_THRESHOLD_ALWAYS_FIX = 0.0
    FIXING_THRESHOLD_NEVER_FIX = 1.0

    _INFINITE_LOOP_PRONE_FIXERS = (ThresholdGridGen, CoordinateGridGen)

    def __init__(self, grid_gen: GridGen, fixer_grid_gen: GridGen=None, fixing_threshold:float=FIXING_THRESHOLD_ALWAYS_FIX):
        """
        FixingGridGen creates masking grids and guarantees that all uninformative (empty of full) grids will be fixed.
        If p1 is exactly 0 or 1, then empty and full masks will be allowed.

        :param grid_gen: GridGen used to generate grids
        :param fixer_grid_gen: GridGen used to generate replacements for uninformative grids
        :param fixing_threshold: float value expressing the likelihood of occurrence of uninformative grid.
                                If this likelihood for given parameters (p1, s) is greater than this threshold, fixing will be attempted.
                                This option is meant to improve performance for large values of `s`, where uninformative grids are unlikely
                                and searching for uninformative masks becomes very costly.
        """
        super().__init__()
        self.base_grid_gen = grid_gen
        self.fixer = fixer_grid_gen or grid_gen
        if type(fixer_grid_gen) in FixingGridGen._INFINITE_LOOP_PRONE_FIXERS:
            warnings.warn(f"{type(fixer_grid_gen)} is prone to infinite grid fixing loops. "
                               f"Use PermutationGridGen or SmartCoordinateGridGen instead.",
                          category=RuntimeWarning)

        if not (0.0 <= fixing_threshold <= 1.0):
            raise ValueError("fixing threshold value must be in range [0, 1] - 0 means that fixing is never performed, 1 means that it's performed always")
        self.fixing_threshold = fixing_threshold

    def generate_grid(self, N: int, shape: Union[torch.Size, Collection[int]], p1: float) -> torch.Tensor:
        size = torch.as_tensor(shape).prod().item()
        if size < 2:
            warnings.warn(f"shape == {shape} used in generate_grid. Experiment results may be unreliable.",
                          category=RuntimeWarning)

        return self.fix_uninformative_grids(self.base_grid_gen.generate_grid(N, shape, p1), p1, single_grid_ndim=len(shape))

    def grids_for_band(self, N: int, binary_map: torch.Tensor, p1: float) -> torch.Tensor:
        mask_on_device = binary_map.to(self.base_grid_gen.device)
        return self.fix_uninformative_grids(self.base_grid_gen.grids_for_band(N, mask_on_device, p1), p1, binary_map)

    def fix_uninformative_grids(self, grids: torch.Tensor, p1:float, band: torch.Tensor=None, single_grid_ndim:int=None) -> torch.Tensor:
        """
        Locates both fully occluded and unoccluded grids, and replaces them with new grids.
        `self.fixer_grid_gen` is used to generate new grids, therefore the performance of this method
        and properties of replaced masks are dependent on chosen grid gen

        :param grids: N-dim tensor. Will be modified in place.
        :param p1: expected fill rate
        :param band: masking saliency band, which will be later combined with grids - fixing process needs to know which areas of the grid are relevant
        :return: `grids` after modification.
        """
        single_grid_ndim = single_grid_ndim or (band.ndim if band is not None else 2)
        single_grid_shape = grids.shape[-single_grid_ndim:]
        uninformative_grids_likely = self.uninformative_grid_likelihood(single_grid_shape, p1) >= self.fixing_threshold
        if uninformative_grids_likely and (0 < p1 < 1):
            while True:
                faulty_grids_tensor = FixingGridGen.needs_fixing(grids, band, single_grid_ndim) #~FixingGridGen.is_informative(grids, band)
                faulty_grids_idx = torch.nonzero(faulty_grids_tensor, as_tuple=True)# .chunk(faulty_grids_tensor.ndim, dim=1)
                n_faulty_grids = len(faulty_grids_idx[0])
                if n_faulty_grids > 0:
                    if band is not None:
                        if band.sum() <= 1:
                            return grids  # special case when band causes the resulting grid to be always uninformative
                        band = band.to(grids.device)
                        grids[faulty_grids_idx] = self.fixer.grids_for_band(n_faulty_grids, band, p1)
                    else:
                        grids[faulty_grids_idx] = self.fixer.generate_grid(n_faulty_grids, single_grid_shape, p1)
                else:
                    return grids
        else:
            return grids

    @staticmethod
    def is_empty(grids: torch.Tensor, single_grid_ndim: int) -> torch.Tensor:
        """
        Returns a tensor of boolean values, similar to `masks` but with last `single_grid_ndim` dimensions cut off.

        :param grids: N-dim mask tensor
        :param single_grid_ndim: K - determines the dimensionality of a single grid in `grids`.
        :return: (N-K)-dim boolean tensor
        """
        if not (0 < single_grid_ndim <= grids.ndim):
            raise ValueError(f"single_grid_ndim ({single_grid_ndim}) can't be greater than actual grid ndim ({grids.ndim})")

        return grids.sum(tuple(range(grids.ndim - single_grid_ndim, grids.ndim))) == 0

    @staticmethod
    def is_informative(grids: torch.Tensor, band: Union[None, torch.Tensor], single_grid_ndim:int) -> torch.Tensor:
        """
        Reduces `grids` to a (N-K)-dim boolean tensor, where each truth value determines
        whether the corresponding grid is informative (contains both occluded and visible fields)
        The shape of the returned tensor is `grids.shape[:-K]`

        K is given by single_grid_ndim
        
        :param grids: N-dim torch.Tensor
        :param band: M-dim torch.Tensor used to mask each grid - must be broadcastable to grids
        :param single_grid_ndim: declares the dimensionality of a single grid in grids.
        :return: (N-K)-dim torch.Tensor containing boolean values determining the informativeness of corresponding masks
        """
        band = band.to(grids.device) if band is not None else None
        is_empty = FixingGridGen.is_empty(grids * band if band is not None else grids, single_grid_ndim)
        inverted_grids = ~(grids.bool())
        is_full = FixingGridGen.is_empty(inverted_grids * band if band is not None else inverted_grids, single_grid_ndim)
        return ~(is_empty + is_full)

    @staticmethod
    def needs_fixing(grids: torch.Tensor, band: Union[None, torch.Tensor], single_grid_ndim:int) -> torch.Tensor:
        """
        Returns an (N-K)-dim ternsor of booleans, each correspondin to one grid, indicating whether
        This method is meant to prevent infinite fixing loops in these cases where a band includes
        only one cell, or no cells at all, and it becomes impossible to fix a mask.
        The shape of the returned tensor is `grids.shape[:-K]`

        K is given by single_grid_ndim

        :param grids: N-dim torch.Tensor, last K dimensions must match `band`
        :param band: M-dim torch.Tensor used to mask each grid - must be broadcastable to grids
        :param single_grid_ndim: declares the dimensionality of a single grid in grids.
        :return: (N-K)-dim torch.Tensor containing boolean values determining the informativeness of corresponding masks
        """
        if band is not None and band.sum() <= 1:
            return torch.zeros(grids.shape[:-single_grid_ndim])
        else:
            return ~FixingGridGen.is_informative(grids, band, single_grid_ndim)

    @staticmethod
    def uninformative_grid_likelihood(grid_shape: tuple, p1: float):
        grid_area = torch.as_tensor(grid_shape).prod().item()
        return (p1 ** grid_area) + ((1-p1) ** grid_area)

    def __str__(self):
        return f"{self.__class__.__name__}(grid_gen={str(self.base_grid_gen)}, fixer_grid_gen={str(self.fixer)})"


class SmartGridGen(GridGen):
    """
    A grid gen implementation which uses either CoordinateGridGen or ThresholdGridGen (with empty mask fixing), based on provided `s` and `p1` parameters
    """

    def __init__(self, threshold_grid_gen: ThresholdGridGen, coordinate_grid_gen: CoordinateGridGen) -> None:
        super().__init__()
        self.threshold_grid_gen = threshold_grid_gen
        self.coordinate_grid_gen = coordinate_grid_gen

    def generate_grid(self, N: int, shape: int, p1: float) -> torch.Tensor:
        # TODO: coord if condition(s,p1) else threshold
        # perhaps some self-tuning procedure on __init__?
        raise NotImplementedError