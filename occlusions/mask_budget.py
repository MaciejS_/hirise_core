import collections
import math
from typing import Sequence, Union, List
import numpy as np

class MaskBudget():

    def  __init__(self,
                  s_values: Union[Sequence[int], int],
                  p_values: Union[Sequence[float], float],
                  band_spans: Union[Sequence[float], np.ndarray]):
        """

        :param s_values: single s value (constant-s) or a collection of s values (variable-s)
        :param p_values: single p value (constant-s) or a collection of p values (variable-s)
        :param band_spans: list of saliency-wise band widths
        """
        self.s_values = s_values if isinstance(s_values, collections.Sequence) else [s_values]
        self.p_values = p_values if isinstance(p_values, collections.Sequence) else [p_values]
        self.band_spans = band_spans

    def split(self, N) -> List[int]:
        raise NotImplementedError('This method needs to be overridden by a concrete implementation')

    def __str__(self) -> str:
        return self.__class__.__name__

class EqualSplit(MaskBudget):
    """
    This MaskBudget splits `N` evenly among bands. However, to maintain equal split,
    it will round the sum(N_i) to the nearest multiple of `len(self.band_spans)`
    """
    def split(self, N: int) -> List[int]:
        if isinstance(N, int):
            return [max(math.floor(float(N) / len(self.band_spans)), 1) for _ in self.band_spans]
        else:
            raise TypeError(f"Unsupported N type: {type(N)} (got: {N})")


class SpanProportionalSplit(MaskBudget):
    """
    Splits the total number of masks `N` proportionally to `band_spans`
    """
    def split(self, N: int) -> List[int]:
        if isinstance(N, int):
            band_span_sum = sum(self.band_spans)
            normalized_band_spans = [float(x) / band_span_sum for x in self.band_spans]
            return [max(int(float(N) * span), 1) for span in normalized_band_spans]
        else:
            raise TypeError(f"Unsupported N type: {type(N)} (got: {N})")


class SProportionalSplit(MaskBudget):
    """
    Splits the total number of masks `N` proportionally to `s_values`
    """
    def split(self, N: int) -> List[int]:
        if isinstance(N, int):
            s_sum = sum(self.s_values)
            normalized_s_ratios = [float(x) / s_sum for x in self.s_values]
            return [max(int(float(N) * s_ratio), 1) for s_ratio in normalized_s_ratios]
        else:
            raise TypeError(f"Unsupported N type: {type(N)} (got: {N})")