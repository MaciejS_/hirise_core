from typing import Union

import torch
from hirise.experiment.utils import replace_initial_consecutive_zeros
from hirise.occlusions.distributions_torch import make_callable, make_dfn, invert, make_cdfn, interpolate, torch_normalize


def make_bounds(edge_values: list) -> torch.Tensor:
    """
    Generate a set of contiguous bins

    :param edge_values: list of all edge values (e.g. for 2 bins, 3 values are needed -
    :return: (2, n) tensor containing (lower_bound, upper_bound) pairs, when sliced along last axis
    """
    if len(edge_values) < 2:
        raise ValueError("`edge_values` must contain at least two elements")
    return torch.tensor([edge_values[:-1], edge_values[1:]]).T.contiguous()


def saliency_bands(smap: torch.Tensor, bounds: torch.Tensor) -> torch.Tensor:
    """
    Generate slices of `smap`, which contain values between boundary edges contained in `bounds`
    Lower boundary is included in band, upper is excluded
    
    :param smap: single saliency map in for of a 2D tensor
    :param bounds: tensor of shape (n, 2), containing pairs of (lower, upper) boundary values
    :return: 3D torch Tensor of shape (n, y, x), where (y, x) is the shape of `smap`
    """
    lower, upper = bounds.T[..., None, None]  # add two dims to broadcast over
    return (torch.ge(smap, lower) * torch.lt(smap, upper)).to(torch.float)


class ThresholdGen:

    @classmethod
    def make_contiguous_bounds(cls, saliency_map: torch.Tensor, band_edges: torch.Tensor) -> torch.Tensor:
        """

        :param self:
        :param band_edges: 1D tensor. numpy array or list
        :return:
        """
        return cls.make_bounds(saliency_map, ThresholdGen.as_bounds(band_edges))

    @classmethod
    def make_bounds(cls, saliency_map: torch.Tensor, bounds: torch.Tensor) -> torch.Tensor:
        raise NotImplementedError("Needs to be overridden by a concrete implementation")

    @staticmethod
    def as_bounds(edge_values: Union[list, torch.Tensor]) -> torch.Tensor:
        """
        Generate a set of contiguous bins

        :param edge_values: list of all edge values (e.g. for 2 bins, 3 values are needed -
        :return: (2, n) tensor containing (lower_bound, upper_bound) pairs, when sliced along last axis
        """
        if len(edge_values) < 2:
            raise ValueError("`edge_values` must contain at least two elements")
        if isinstance(edge_values, torch.Tensor) and edge_values.ndim > 1:
            raise ValueError(f"`band_edges` must be 1-dimensional, got {edge_values.ndim} dimensions")
        if not all(map(lambda x, y: x < y, edge_values, edge_values[1:])):
            raise ValueError(f"`edge_values` must be monotonically increasing ")

        return torch.as_tensor([edge_values[:-1], edge_values[1:]]).T.contiguous()

    @classmethod
    def validate_bounds(cls, bounds):
        tensor_bounds = torch.as_tensor(bounds)
        if tensor_bounds.ndim != 2 or tensor_bounds.shape[1] != 2:
            raise ValueError(f"Expected bounds to be 2-dimensional tensor of shape (N, 2), got shape {tensor_bounds.shape}")

    def __str__(self):
        return self.__class__.__name__


class SaliencyValueThresholdGen(ThresholdGen):

    domain_name = "Saliency value"

    @classmethod
    def make_bounds(cls, saliency_map: torch.Tensor, bounds: torch.Tensor):
        ThresholdGen.validate_bounds(bounds)
        return torch.as_tensor(bounds)


class PercentileThresholdGen(ThresholdGen):

    domain_name = "Percentile"

    @classmethod
    def make_bounds(cls, saliency_map: torch.Tensor, bounds: torch.Tensor) -> torch.Tensor:
        ThresholdGen.validate_bounds(bounds)
        callable_map_df = make_callable(make_dfn(saliency_map))
        return callable_map_df(bounds.flatten()).reshape(bounds.shape)


class EnergyThresholdGen(ThresholdGen):

    domain_name = "Energy"

    @classmethod
    def make_bounds(cls, saliency_map: torch.Tensor, bounds: torch.Tensor) -> torch.Tensor:
        return cls._make_bounds_interpolated(saliency_map, bounds)

    @staticmethod
    def _make_bounds_icdfn(saliency_map: torch.Tensor, bounds: torch.Tensor) -> torch.Tensor:
        """
        First of established energy -> saliency conversion schemes, using a two-step process:
        1. Convert energy threshold to area via icdfn()
        2. converts the output in area domain to saliency via dfn()
        """
        ThresholdGen.validate_bounds(bounds)

        dfn = make_callable(make_dfn(saliency_map))
        icdfn = make_callable(invert(replace_initial_consecutive_zeros(make_cdfn(saliency_map))))
        return dfn(icdfn(bounds.flatten())).reshape(bounds.shape)

    @staticmethod
    def _make_bounds_interpolated(saliency_map: torch.Tensor, bounds: torch.Tensor) -> torch.Tensor:
        """
        Alternative method which converts bounds from energy domain to saliency domain by
        interpolating `bounds` through a {x=cumsum(saliency), y=saliency} mapping.
        It's more direct (less intermediate operations) and achieves the exact same result

        """
        ThresholdGen.validate_bounds(bounds)

        normalized_saliency_values = make_dfn(saliency_map)
        cumulative_saliency = torch_normalize(torch.cumsum(normalized_saliency_values, 0))
        new_x = torch.linspace(0, 1, len(normalized_saliency_values))

        interpolated_values = interpolate(target_x=new_x,
                                          source_x=cumulative_saliency,
                                          source_y=normalized_saliency_values)
        energy_to_saliency_func = make_callable(interpolated_values.to(torch.float))
        return energy_to_saliency_func(bounds)
