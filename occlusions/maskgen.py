from typing import Union, Sequence, List

import torch
from hirise.occlusions.gridgen import GridGen
from hirise.occlusions.upsampling import Upsampler
from hirise.occlusions.rescaling import AvgpoolDownsampler
from hirise.structures import MaskAggregator


class MaskGen:

    def __init__(self, grid_gen: GridGen, upsampler: Upsampler):
        self.grid_gen = grid_gen
        self.upsampler = upsampler

    def generate_masks(self, N:int, s:int, p1:float, target_size:tuple, grid_retainers:List[MaskAggregator]=()):
        """
        Generate a 4-D tensor of masks of shape (N, 1, target_size[0], target_size[1])

        :param N: number of masks to generate
        :param s: masks are square grids of (s,s) shape. value of this parameter determines the edge length
        :param p1: occlusion probability
        :param target_size: shape of the result mask
        :return: 4-D mask tensor
        """
        grid = self.grid_gen.generate_grid(N, (s, s), p1)
        for retainer in grid_retainers:
            retainer.add_many(grid.squeeze().cpu(), None)
        return self.upsampler.upsample(grid.unsqueeze(1), target_size)

    def __str__(self):
        return f"{self.__class__.__name__}(grid_gen={str(self.grid_gen)})"

class BandedMaskGen:

    def __init__(self, grid_gen: GridGen, upsampler: Upsampler):
        self.grid_gen = grid_gen
        self.upsampler = upsampler

    def generate_masks(self,
                       N: Union[int, Sequence[int]],
                       p1: Union[float, Sequence[float]],
                       target_size: tuple,
                       bands: torch.Tensor) -> Sequence[torch.Tensor]:

        N, bands, p1 = self._broadcast_params(N, bands, p1)

        grids = [self.grid_gen.grids_for_band(n, binary_map, p)
                 for (n, binary_map, p)
                 in zip(N, bands, p1)]

        return [self.upsampler.upsample(g.unsqueeze(1), target_size) for g in grids]

    def _broadcast_params(self, N, bands, p1):
        if bands.ndim == 2:
            if not (isinstance(N, int) and isinstance(p1, float)):
                raise ValueError(
                        "if single `binary_map` is provided, `N` and `p1` should also be single values, rather than collections")
            bands = bands.unsqueeze(0)
        elif bands.ndim < 2 or bands.ndim > 3:
            raise ValueError("Unsupported bands tensor shape")

        N = [N] * len(bands) if isinstance(N, int) else N
        p1 = [p1] * len(bands) if isinstance(p1, (float, int)) else p1

        if not (len(N) == len(p1) == len(bands)):
            raise ValueError(f"Parameters need to be either single values, or collections of equal length. "
                             f"Got {len(N)} N, {len(p1)} p1, {len(bands)} bands")
        return N, bands, p1

    def __str__(self):
        return f"{self.__class__.__name__}(grid_gen={str(self.grid_gen)})"


class AdaptiveMaskGen:

    def __init__(self, grid_gen: GridGen, upsampler: Upsampler, direct_upsample: bool):
        """

        `direct_upsample`: if False, grids will be first upsampled to match `bands`, combined with them and then upsampled to `target_size`. Otherwise, both will be upsampled directly to `target_size` and then combined.
        Direct upsampling will produce different results than gradual. Especially in terms of amount of non-binary areas of the mask. Direct upsampling tends to produce smoother but more blurry results, while gradual upsampling results in more faithful but also more jagged shapes.
        This holds true for interpolation algorithms like `bicubic` and `bilinear`. `nearest-neighbor` won't produce blury results, but the results of direct and gradual upsampling may still differ due to discretization erorrs occurring when scaling factor is not an integer.


        :param grid_gen: 
        :param upsampler: 
        :param direct_upsample:
        """
        self.grid_gen = grid_gen
        self.upsampler = upsampler
        self.direct_upsample = direct_upsample

    def generate_masks(self,
                       N: Union[int, Sequence[int]],
                       occlusion_s: Sequence[int],
                       p1: Union[float, Sequence[float]],
                       target_size: tuple,
                       bands: torch.Tensor) -> Sequence[torch.Tensor]:

        N, bands, p1, s = self._validate_params(N, bands, p1, occlusion_s)

        grid_bases = [AvgpoolDownsampler.downsample(band, target_s)
                      for band, target_s
                      in zip(bands, s)]

        grid_sets = [self.grid_gen.grids_for_band(n, base, p).unsqueeze(1)
                     for (n, base, p)
                     in zip(N, grid_bases, p1)]

        if self.direct_upsample:
            upsampled_grid_sets = [self.upsampler.upsample(grids, target_size) for grids in grid_sets]
            upsampled_masking_bands = self.upsampler.upsample(bands.unsqueeze(1), target_size)
            final_masks = [grids * mask[None, ...] for grids, mask in zip(upsampled_grid_sets, upsampled_masking_bands)]
        else:
            mask_size = bands.shape[-2:]
            upsampled_grid_sets = [self.upsampler.upsample(grids, mask_size) for grids in grid_sets]
            masked_grids = [grids * mask[None, None, ...] for grids, mask in zip(upsampled_grid_sets, bands.to(self.upsampler.device))]
            final_masks = [self.upsampler.upsample(grids, target_size) for grids in masked_grids]

        return final_masks

    def _validate_params(self, N, bands, p1, s):
        if bands.ndim == 2:
            if not isinstance(N, int) or not isinstance(p1, float):
                raise ValueError(
                        "if single `binary_map` is provided, `N` and `p1` should also be single values, rather than collections")
            bands = bands.unsqueeze(0)
        elif bands.ndim < 2 or bands.ndim > 3:
            raise ValueError("Unsupported bands tensor shape")

        N = [N] * len(bands) if isinstance(N, int) else N
        p1 = [p1] * len(bands) if isinstance(p1, (float, int)) else p1
        s = [s] * len(bands) if isinstance(s, int) else s

        if not (len(N) == len(p1) == len(bands) == len(s)):
            raise ValueError(f"Parameters need to be either single values, or collections of equal length. "
                             f"Got {len(N)} N, {len(p1)} p1, {len(s)} s, {len(bands)} bands")

        return N, bands, p1, s

    def __str__(self):
        return f"{self.__class__.__name__}(grid_gen={str(self.grid_gen)}, direct_upsample={self.direct_upsample})"