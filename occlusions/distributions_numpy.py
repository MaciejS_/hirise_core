import numpy as np
from hirise.experiment.utils import normalize as numpy_normalize
from scipy.interpolate import interp1d


def invert(array: np.ndarray) -> np.ndarray:
    """
    Returns an array of X values corresponding to Y values in `array`
    :param array:
    :return:
    """
    length = len(array)
    normalized = numpy_normalize(array)
    target_values = np.arange(length) / (length-1)
    interpolator = interp1d(normalized, target_values)
    return interpolator(np.linspace(0, 1, length))


def interpolating_callable(array: np.ndarray):
    """
    Returns a callable which converts input 'indices' in range (0, 1) to interpolations of `array` values closest to these indices

    e.g. for 3-element array, indices [0, 0.5, 1] will return respectively 0-th, 1st and 2nd element. Outputs for intermediate values,
    like 0.33 will be obtained through linear interpolation of neighboring elements from `array`, in this case, 0th and 1st

    :param array:
    :return:
    """
    return interp1d(np.linspace(0, 1, len(array)), array)


def make_df(array: np.ndarray) -> np.ndarray:
    # DF - Distribution Function
    return np.sort(array, axis=None)


def make_dfn(array: np.ndarray) -> np.ndarray:
    # DFN - Distribution Function Normalized
    return numpy_normalize(make_df(array))


def make_cdf(array: np.ndarray) -> np.ndarray:
    # CDFN - Cumulative Distribution Function
    return np.cumsum(make_df(array))


def make_cdfn(array: np.ndarray) -> np.ndarray:
    # CDFN - Cumulative DFN
    return numpy_normalize(make_cdf(array))