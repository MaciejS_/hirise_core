import multiprocessing

import torch
import torch.nn.functional as tnnf
from joblib import Parallel, delayed
from tqdm.autonotebook import trange


class Upsampler():

    _UPSAMPLING_PARAMS = {"area":             {"mode": "area",     "align_corners": None},
                          "nearest":          {"mode": "nearest",  "align_corners": None},
                          "bilinear":         {"mode": "bilinear", "align_corners": False},
                          "bilinear-aligned": {"mode": "bilinear", "align_corners": True},
                          "bicubic":          {"mode": "bicubic",  "align_corners": False},
                          "bicubic-aligned":  {"mode": "bicubic",  "align_corners": True}}

    def __init__(self, device, upsampling_mode="bilinear"):
        self.device = device
        if upsampling_mode not in self._UPSAMPLING_PARAMS:
            raise ValueError(f"Upsampling mode ({upsampling_mode}) must be one of: {self._UPSAMPLING_PARAMS.keys()}")
        self.upsampling_mode = self._UPSAMPLING_PARAMS[upsampling_mode]["mode"]
        self.align_corners = self._UPSAMPLING_PARAMS[upsampling_mode]["align_corners"]

    def upsample(self, grid: torch.Tensor, target_size: tuple) -> torch.Tensor:
        N, _, s = grid.shape[:3]
        cell_size = (torch.tensor(target_size, dtype=torch.float) / s).ceil_().to(dtype=torch.int)

        shifts = (torch.rand(N, 2) * cell_size.reshape(1, 2)).to(dtype=torch.long)

        return self.make_filters(grid, shifts, target_size)

    def make_filters(self, grid: torch.Tensor, shifts, target_size: tuple) -> torch.Tensor:
        N, s = grid.shape[0], grid.shape[-1]
        cell_size = (torch.tensor(target_size, dtype=torch.float) / s).ceil_().to(dtype=torch.int)
        up_size = (s + 1) * cell_size

        upscaled = tnnf.interpolate(grid, (up_size[0], up_size[1]),
                                    mode=self.upsampling_mode,
                                    align_corners=self.align_corners)
        masks = torch.empty((N, 1, *target_size), device=self.device)
        for i in range(N):
            x_shift, y_shift = map(int, shifts[i])

            masks[i] = upscaled[i,
                       :,
                       x_shift:x_shift + target_size[0],
                       y_shift:y_shift + target_size[1]]

        return masks


class ParallelizedUpsampler(Upsampler):

    def __init__(self,
                 pool_size: int = 2,
                 use_tqdm: bool = False,
                 upsampling_mode: str = "bilinear") -> None:
        Upsampler.__init__(self, upsampling_mode=upsampling_mode, device=torch.device("cpu"))
        self._pool_size = pool_size if pool_size > 0 else multiprocessing.cpu_count() + (pool_size + 1)
        self._threadpool = Parallel(n_jobs=self._pool_size, backend="threading")
        self._use_tqdm = use_tqdm

    def upsample(self, grid: torch.Tensor, target_size: tuple) -> torch.Tensor:
        N, _, s = grid.shape[:3]
        cell_size = (torch.tensor(target_size, dtype=torch.float) / s).ceil_().to(dtype=torch.int)

        shifts = (torch.rand(N, 2) * cell_size.reshape(1, 2)).to(dtype=torch.int)

        grid_parts = torch.chunk(grid, self._pool_size)
        shifts_parts = torch.chunk(shifts, self._pool_size)

        mask_batches = self._threadpool(delayed(self.make_filters)(grid_parts[thread_idx],
                                                                   shifts_parts[thread_idx],
                                                                   target_size)
                                        for thread_idx
                                        in trange(min(self._pool_size, len(grid_parts)),
                                                  desc="Generating filters",
                                                  leave=False,
                                                  disable=not self._use_tqdm))

        return torch.cat(mask_batches)
