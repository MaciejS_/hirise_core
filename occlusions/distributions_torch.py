from typing import Callable, Any

import numpy as np
import torch
from hirise.experiment.utils import torch_normalize
from hirise.occlusions.distributions_numpy import invert as numpy_invert, interpolating_callable as numpy_ic



def make_callable(array: torch.Tensor) -> Callable[[Any], torch.Tensor]:
    """
    Returns a callable, which translates floats to valid indices of `array`, and retrieves their respective values from the array.
    Uses `prestep` scheme, as visualized by matplotlib's 'Step Demo', shifted by 1 and with additional call to torch.max_clamp(), to prevent IndexError on call with 1.0
    Might be slightly slower than make_callable_midstep

    >>> make_callable(torch.arange(4))(0.5)
    torch.tensor([2])

    >>> make_callable(torch.arange(4))([0, 0.3, 0.6, 0.8])
    torch.tensor([0, 1, 2, 3])

    >>> make_callable(torch.arange(4))([0, 0.24, 0.25, 0.49, 0.5, 0.74, 0.75], 1)
    torch.tensor([0, 0, 1, 1, 2, 2, 3, 3])

    >>> make_callable(torch.arange(4))([0, 0.8, 0.3, 0.6])
    torch.tensor([0, 3, 1, 2])

    :param array: torch.Tensor used for value retrieval lookup by the returned callable
    """
    return lambda x: array[(torch.as_tensor(x, dtype=torch.float) * len(array)).floor_().clamp_max_(len(array)-1).to(torch.long)]


def make_callable_midstep(array: torch.Tensor) -> Callable[[Any], torch.Tensor]:
    """
    Returns a callable, which indexes `array` using floats from (0, 1) range.
    Uses `midstep` scheme, as visualized by matplotlib's 'Step Demo'.
    Causes the first and last entries in the array to be underrepresented by uniform sampling.

    >>> make_callable(torch.arange(4))(0.5)
    torch.tensor([2])

    >>> make_callable(torch.arange(4))([0, 0.16, 0.17, 0.49, 0.5, 0.83, 0.84, 1])
    torch.tensor([0, 0, 1, 1, 2, 2, 3, 3])

    >>> make_callable(torch.arange(4))([0, 0.85, 0.2, 0.6])
    torch.tensor([0, 3, 1, 2])

    :param array: torch.Tensor used for value retrieval by the returned callable
    """
    return lambda x: array[(torch.as_tensor(x, dtype=torch.float) * (array.shape[0] - 1)).round_().to(torch.long)]


def make_interpolating_callable(tensor: torch.Tensor):
    array = tensor.cpu().numpy()
    numpy_callable = numpy_ic(array)
    return lambda x: torch.as_tensor(numpy_callable(x), device=tensor.device, dtype=tensor.dtype)

def interpolate(target_x: torch.Tensor, source_x: torch.Tensor, source_y: torch.Tensor) -> torch.Tensor:
    """
    Wrapper for np.interpolate(). Until torch provides a proper interp1d function, this will have to do.
    interpolate returns `target_y` as source_function(target_x), where source_function is generated from `source_x` and `source_y`
    """
    return torch.as_tensor(np.interp(target_x.numpy(), source_x.numpy(), source_y.numpy()))

def invert(tensor: torch.Tensor) -> torch.Tensor:
    array = tensor.cpu().numpy()
    inverted_array = numpy_invert(array)
    return torch.as_tensor(inverted_array, device=tensor.device)


def make_df(tensor: torch.Tensor) -> torch.Tensor:
    # DF - Distribution Function
    return torch.sort(tensor.flatten())[0]


def make_dfn(tensor: torch.Tensor) -> torch.Tensor:
    # DFN - Distribution Function Normalized
    return torch_normalize(make_df(tensor))


def make_cdf(tensor: torch.Tensor) -> torch.Tensor:
    # CDF - Cumulative Distribution Function
    return torch.cumsum(make_df(tensor), dim=0)


def make_cdfn(tensor: torch.Tensor) -> torch.Tensor:
    # CDFN - Cumulative DFN
    return torch_normalize(make_cdf(tensor))
