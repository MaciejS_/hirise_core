import numpy as np
import torch
import torch.nn as nn
from hirise.device import device_from_torch_model

class RISE(nn.Module):
    def __init__(self, model, input_size, mask_gen):
        super(RISE, self).__init__()
        self.model = model
        self.input_size = input_size

        # Torch-specific device selector
        self.device = device_from_torch_model(model)
        self.mask_gen = mask_gen

    def generate_masks(self, N, s, p1, savepath='masks.npy'):
        self.masks = self.mask_gen.generate_masks(N, s, p1, self.input_size)

        self.N = N
        self.p1 = p1

    def save_masks(self, savepath):
        np.save(savepath, self.masks)
        
    def load_masks(self, filepath):
        with np.load(filepath) as masks_file:
            self.masks = torch.from_numpy(masks_file).float()
        self.N = self.masks.shape[0]

    def forward(self, x):
        class_scores = self.evaluate_masks(x)
        sal = self.class_saliency(x, class_scores)
        return sal / self.N / self.p1
    
    def evaluate_masks(self, image, masks=None) -> torch.Tensor:
        """
        Returns a (num_masks, num_classes)-shaped matrix
        containing confidence scores for each of the occluded images  
        """
        if masks is None:
            masks = self.masks

        if isinstance(masks, np.ndarray):
            masks = torch.from_numpy(masks).to(self.device, dtype=torch.float)

        return self.model(torch.mul(masks, image.data))

    def class_saliency(self, image, mask_evaluations) -> torch.Tensor:
        """
        Applies classification scores to mask pixels, resulting in tensor containing saliency maps for all classes
        
        :param mask_evaluations: 
        """
        _, _, H, W = image.size()
        p = mask_evaluations.to(self.device)
        # Number of classes recognized by the classifier
        CL = p.size(1)
        # This matrix multiplication combines all masks
        sal = torch.matmul(p.data.transpose(0, 1), self.masks.to(self.device).view(self.N, H * W))
        # The results is salience of each pixel for each of supported classes
        return sal.cpu().view((CL, H, W))


class RISEBatch(RISE):
    def forward(self, x):
        # Apply array of filters to the image
        N = self.N
        B, C, H, W = x.size()
        stack = torch.mul(self.masks.view(N, 1, H, W), x.data.view(B * C, H, W))
        stack = stack.view(B * N, C, H, W)
        stack = stack

        #p = nn.Softmax(dim=1)(model(stack)) in batches
        p = []
        for i in range(0, N*B, self.gpu_batch):
            p.append(self.model(stack[i:min(i + self.gpu_batch, N*B)]))
        p = torch.cat(p)
        CL = p.size(1)
        p = p.view(N, B, CL)
        sal = torch.matmul(p.permute(1, 2, 0), self.masks.view(N, H * W))
        sal = sal.view(B, CL, H, W)
        return sal

# To process in batches
# def explain_all_batch(data_loader, explainer):
#     n_batch = len(data_loader)
#     b_size = data_loader.batch_size
#     total = n_batch * b_size
#     # Get all predicted labels first
#     target = np.empty(total, 'int64')
#     for i, (imgs, _) in enumerate(tqdm(data_loader, total=n_batch, desc='Predicting labels')):
#         p, c = torch.max(nn.Softmax(1)(explainer.model(imgs.cuda())), dim=1)
#         target[i * b_size:(i + 1) * b_size] = c
#     image_size = imgs.shape[-2:]
#
#     # Get saliency maps for all images in val loader
#     explanations = np.empty((total, *image_size))
#     for i, (imgs, _) in enumerate(tqdm(data_loader, total=n_batch, desc='Explaining images')):
#         saliency_maps = explainer(imgs.cuda())
#         explanations[i * b_size:(i + 1) * b_size] = saliency_maps[
#             range(b_size), target[i * b_size:(i + 1) * b_size]].data.cpu().numpy()
#     return explanations


