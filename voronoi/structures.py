import typing as t

import numpy as np
from scipy.spatial.qhull import Voronoi
from shapely.geometry import Polygon

from hirise.voronoi.utils import assemble_polygons

class FiniteVoronoi(Voronoi):

    def __init__(
        self,
        points: np.array,
        incremental: bool = True,
        coord_range: tuple = (0, 1),
        *args,
        **kwargs
    ):
        """
        Creates a Voronoi Diagram using scipy.spatial.Voronoi and creates a set of finite polygons out of obtained
        voronoi regions.
        Ordering of `points` is maintained.

        :param points: np.array of shape [N, 2], points used to generate the Voronoi diagram.
        :param incremental: bool, if True, it will be possible to incrementally add more points to this Voronoi diagram
        :param radius: (deprecated)
        :param args: additional args passed to scipy.spatial.Voronoi
        :param kwargs: additional kwargs passed to scipy.spatial.Voronoi
        """
        self._coord_range = coord_range
        self._fenceposts = FiniteVoronoi.make_distant_fenceposts(coord_range)
        self._n_fenceposts = len(self._fenceposts)

        # TODO: this could be generalized to any size and location of the bounding box, but isn't required right now
        self._validate_point_coords(points)
        _, unique_point_idx = np.unique(points, return_index=True, axis=0)
        self._seeds = points[np.sort(unique_point_idx)]
        points = np.concatenate((self._fenceposts, self._seeds), axis=0)

        Voronoi.__init__(self,
            points,
            *args,
            incremental=incremental,
            **kwargs
        )

    def add_points(self, points: np.ndarray, restart: bool = False) -> None:
        # this selector compares each point to all of the existing points in voronoi diagram and selects only these
        # of the new points, that didn't match any of the existing.
        # duplicates in `points` set are removed as well
        self._validate_point_coords(points)
        # unique_points = np.unique(points, axis=0)
        _, unique_point_idx = np.unique(points, return_index=True, axis=0)
        unique_points = points[np.sort(unique_point_idx)]
        new_points = unique_points[
            ~np.isclose(
                np.expand_dims(unique_points, 1),
                self.points
            ).all(axis=-1).any(axis=-1)
        ]
        self._seeds = np.concatenate((self._seeds, new_points), axis=0)

        Voronoi.add_points(self, new_points, restart=restart)

        if hasattr(self, '_polygons'):
            del self._polygons

        if hasattr(self, '_polygon_coords'):
            del self._polygon_coords


    @property
    def polygons(self) -> t.Sequence[Polygon]:
        """
        Returns polygons representing Voronoi regions originating from `points`.
        """
        if not hasattr(self, '_polygon'):
            polygon_indices = [self.regions[idx] for idx in self.point_region[self._n_fenceposts:]]
            self._polygons = assemble_polygons(polygon_indices, self.vertices)
        return self._polygons

    @property
    def polygon_coords(self) -> t.Sequence[np.ndarray]:
        """
        Returns coordinates of vertices of Voronoi regions originating from `points`.
        Vertex coordinates for each region are ordered clockwise/counterclockwise and can
        be readily used for drawing.
        Shape: [N, 2], where each row contains (X, Y) coordinate pairs.
        (opposite of numpy's X-last convention, but compliant with expectations of PIL)
        """
        if not hasattr(self, '_polygon_coords'):
            polygon_indices = [self.regions[idx] for idx in self.point_region[self._n_fenceposts:]]
            self._polygon_coords = [self.vertices[vertex_idxs] for vertex_idxs in polygon_indices]
        return self._polygon_coords

    @property
    def seeds(self) -> np.ndarray:
        """
        Returns coordinates of all added voronoi seeds.
        As opposed to the `self.points` inherited from Voronoi, `self.seeds` doesn't include fencepost points
        """
        return self._seeds

    def _create_polygons(self) -> None:
        """
        Create voronoi polygons (Shapely.geometry.polygon) from the voronoi diagram.
        Regions originating from fenceposts are ignored
        """
        polygon_indices = [self.regions[idx] for idx in self.point_region[self._n_fenceposts:]]
        self._raw_polygon_coords = [self.vertices[vertex_idxs] for vertex_idxs in polygon_indices]

    def _validate_point_coords(self, point_coords) -> None:
        """
        Validates points and raises ValueError if validation fails
        """
        if np.any(point_coords < min(self._coord_range)) or np.any(point_coords > max(self._coord_range)):
            raise ValueError(f"All points must be in coord_range: {self._coord_range}")

    @staticmethod
    def make_distant_fenceposts(coord_range):
        """
        Returns an array of fencepost point pairs located in the corners of the (0, 1) unit box,
        which will spawn four voronoi ridges crossing corners at exactly 45 degrees.

        This set of fenceposts is guaranteed to not interfere with the voronoi regions originating
        from seeds, because the fenceposts are located one diagonal's length (+eps) away from the corners,
        guaranteeing that any point within the unit box will be closer to any other point within the unit box,
        than to any of the fenceposts.

        Within a pair of fenceposts, the 'farther' is located additional 2*`eps` away, while the 'closer'
        is located one `eps` away, to make sure that the above constraint is always satisfied
        (with a tiny bit of extra distance)
        """

        # NOTE: It shouldn't be too hard to generalize this to any convex polygon, if needed.
        #   all I need to do is to find the geometric center of the polygon, and then set the fenceposts
        #   on the extension of center-vertex lines, at distance equal to the greatest of all
        #   center-vertex distances in this polygon.
        #   But since I won't be using anything other than a square unit box for the foreseeable future,
        #   let's leave it at that.

        eps = 1e-3
        vmin, vmax = coord_range
        d = vmax - vmin
        return np.array([
            [vmin-d - 2*eps, vmin-d - 2*eps], [vmin-d - eps, vmin-d - eps], # upper-left pair
            [vmin-d - 2*eps, vmax+d + 2*eps], [vmin-d - eps, vmax+d + eps], # upper-right pair

            [vmax+d + 2*eps, vmin-d - 2*eps], [vmax+d + eps, vmin-d - eps], # lower-left pair
            [vmax+d + 2*eps, vmax+d + 2*eps], [vmax+d + eps, vmax+d + eps], # lower-right pair
        ])