from typing import *

import random
import numpy as np
import torch
from shapely.geometry import Point, Polygon

from hirise.voronoi.structures import FiniteVoronoi

__all__ = [
    "SeedCoordinateProvider",
    "UniformRandomSeeds",
    "CheckerboardPatternSeeds",
    "MeshGen",
    "VoronoiMeshGen"
]

class SeedCoordinateProvider:

    def __init__(self, ndim: int = 2):
        """
        Creates a callable which takes in integer N and returns an [N, ndim] numpy array of Voronoi mesh seed coordinates.
        This is an abstract base class which has to be overridden by a concrete implementation

        :param ndim: int, dimensionality of a single seed coordinate
        """
        self.ndim = ndim

    def __call__(self, n_seeds: int) -> np.ndarray:
        raise NotImplementedError("Must be overridden by a concrete implementation")


class UniformRandomSeeds(SeedCoordinateProvider):

    def __init__(self, ndim: int = 2):
        """
        Creates a callable which takes in integer N and returns an [N, ndim] numpy array of random floats,
        which can be used as seed coordinates for Voronoi mesh.
        :param ndim: int, dimensionality of a single seed coordinate
        """
        super().__init__(ndim=ndim)

    def __call__(self, n_seeds: int) -> np.ndarray:
        return np.random.rand(n_seeds, self.ndim)


class CheckerboardPatternSeeds(SeedCoordinateProvider):

    def __init__(self, ndim: int = 2):
        """
        Creates a callable which takes in integer N and returns an [N, ndim] array of evenly spaced point coords
        which can be used as seed coordinates for Voronoi mesh.
        :param ndim: int, dimensionality of a single seed coordinate
        """
        super().__init__(ndim=ndim)

    def __call__(self, n_seeds: int) -> np.ndarray:
        N = round(n_seeds ** (1 / self.ndim))

        _ix = slice(1/(N*2), 1, 1/N)
        return np.mgrid[(_ix,) * self.ndim].T.reshape(-1, self.ndim)


class MeshGen:

    def __init__(self, seed_coordinate_provider):
        self.coord_provider = seed_coordinate_provider

    def create_mesh(self, N: int) -> FiniteVoronoi:
        """
        Creates a Bounded Voronoi diagram, consisting of N random polygons.
        :param N: number of polygons in the returned FiniteVoronoi
        :return:
        """
        raise NotImplementedError("Must be overridden by a concrete implementation")


class VoronoiMeshGen(MeshGen):

    def __init__(self,
                 seed_coordinate_provider=UniformRandomSeeds(ndim=2),
                 incremental_mesh=True):
        """
        VoronoiMeshGen creates sets of Voronoi polygons

        :param seed_coordinate_provider: a callable that takes an integer N and returns an (N, 2) torch.Tensor of point coordinates
        :param incremental_mesh: if True, created meshes will support addition of new points
        """
        MeshGen.__init__(self, seed_coordinate_provider)
        self.incremental_mesh = incremental_mesh

    def create_mesh(self, N: int) -> FiniteVoronoi:
        points = self.coord_provider(N)
        return self.create_mesh_from_points(points)

    def create_mesh_from_points(self, points: Union[np.array, torch.Tensor]) -> FiniteVoronoi:
        """
        Creates a Bounded Voronoi diagram, consisting of N random polygons.
        :param N: number of polygons in the returned FiniteVoronoi
        :return:
        """
        return FiniteVoronoi(points, incremental=self.incremental_mesh)