import itertools
from typing import *

import more_itertools
import numpy as np
import torch

from hirise.occlusions.gridgen import GridGen
from hirise.voronoi.structures import FiniteVoronoi
from hirise.voronoi.meshgen import MeshGen
from hirise.voronoi.renderer import OcclusionRenderer


class VoronoiMaskGen:

    def __init__(self, mesh_gen: MeshGen, occlusion_selector: GridGen, renderer: OcclusionRenderer):
        self.mesh_gen = mesh_gen
        self.grid_gen = occlusion_selector
        self.renderer = renderer

    def generate_masks(self, N: int, M: int, s:int, p1:float, target_shape: Tuple[int, int]) -> torch.Tensor:
        """
        Generate `N` masks from `M` meshes. Each mesh will split the image into `s` polygonal regions,
        `p1`% of which will be left uncovered by the generated masks.
            
        :param N: total number of masks to generate, split evenly among meshes
        :param M: number of meshes to use
        :param s: number of polygons in each mesh
        :param p1: float, percentage of polygons to use in mask
        :param target_shape: shape of a single mask
        :return: torch.Tensor of shape (N, *target_shape)
        """
        masks_per_mesh = (
                np.ones(M) * (N // M)
                + np.array(list(more_itertools.padded([1] * (N%M), fillvalue=0, n=M)))
        ).astype(int)

        mask_tensors = []
        for n in masks_per_mesh:
            mesh = self.mesh_gen.create_mesh(s)
            occlusion_selectors = self.grid_gen.generate_grid(n, (s,), p1).to(torch.bool)
            masks = self.renderer.batch_render(mesh.polygon_coords, occlusion_selectors, target_shape)
            mask_tensors.append(masks)

        return torch.cat(mask_tensors)

    def yield_masks(self, M: int, s:int, p1:float, target_shape: Tuple[int, int]) -> Iterable[torch.Tensor]:
        """
        Creates an infinite generator of 2D masks generated from M meshes used in a round-robin fashion.

        :param M: number of meshes to use
        :param s: number of polygons in each mesh
        :param p1: float, percentage of polygons to use in mask
        :param target_shape: shape of a single mask
        :return: torch.Tensor of shape (N, *target_shape)
        """
        meshes = [self.mesh_gen.create_mesh(s) for _ in range(M)]

        for mesh in itertools.cycle(meshes):
            occlusion_selector = self.grid_gen.generate_grid(1, (s,), p1).to(torch.bool).squeeze(0)
            polygons = self.renderer._one_hot_select(mesh.polygon_coords, occlusion_selector)
            yield self.renderer.render(polygons, target_shape)

    def masks_from_mesh(self, N: int, voronoi: FiniteVoronoi, p1: float, target_shape: Tuple[int, int]) -> torch.Tensor:
        occlusion_selectors = self.grid_gen.generate_grid(N, shape=(len(voronoi.polygons),), p1=p1).to(torch.bool)
        return self.renderer.batch_render(voronoi.polygon_coords, occlusion_selectors, target_shape)
