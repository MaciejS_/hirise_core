from typing import Sequence, Tuple, Union

import torch
import numpy as np

import skimage.filters
import shapely.affinity
from shapely.geometry import Polygon
from torchvision.transforms.functional import gaussian_blur

from hirise.voronoi.utils import polymask
import warnings

__all__ = ["OcclusionRenderer", # generic
           "PillowOcclusionRenderer",

           "OcclusionRendererDecorator", # generic

           "RandomRenderShift", # generic
           "FixedRandomRenderShift",
           "OcclusionDependentRandomRenderShift",

           "GaussianBlur", # generic
           "FixedGaussianBlur",
           "OcclusionDependentGaussianBlur",
]



class OcclusionRenderer:

    def render(self, occlusions: Sequence[np.ndarray], target_shape: Tuple[int, int]):
        raise NotImplementedError("Has to be overridden by a concrete implementation")

    def batch_render(self, occlusion_set: Sequence[np.ndarray],
                     occlusion_selectors: Union[np.ndarray, torch.Tensor],
                     target_shape: Tuple[int, int]):
        raise NotImplementedError("Has to be overridden by a concrete implementation")

    @staticmethod
    def _one_hot_select(occlusion_set, one_hot_selector) -> Sequence[np.ndarray]:
        occlusion_indices = (
            one_hot_selector.nonzero(as_tuple=True)[0]
            if isinstance(one_hot_selector, torch.Tensor)
            else one_hot_selector.nonzero()[0]
        )
        return [occlusion_set[x] for x in occlusion_indices]

    @staticmethod
    def _validate_selectors(occlusion_selectors, occlusion_set):
        if len(occlusion_set) < 1:
            raise ValueError("Occlusion set cannot be empty!")
        if not occlusion_selectors.shape[-1] == len(occlusion_set):
            raise ValueError(
                f"The size of the last dimension of occlusion selectors (shape {occlusion_selectors.shape} "
                f"must match the size of occlusion_set ({len(occlusion_set)})"
            )
        if occlusion_selectors.ndim < 2:
            raise ValueError(f"occlusion_selectors must be at least 2D, got {occlusion_selectors.ndim}D")
        if occlusion_selectors.dtype not in (bool, torch.bool):
            raise TypeError(
                f"occlusion_selectors dtype bust be `bool` or `torch.bool`. Got `{occlusion_selectors.dtype}`"
            )


class PillowOcclusionRenderer(OcclusionRenderer):

    MASK_MODE_BINARY = '1'
    MASK_MODE_MONOCHROME = 'L'

    OUTLINE_NONE = "none"
    OUTLINE_HALFTONE = "halftone"
    OUTLINE_SOLID = "solid"

    _OUTLINE_VALUE_MAP = {
        "none": 0,
        "halftone": 1,
        "solid": 2,
    }

    _FILL_VALUE = _OUTLINE_VALUE_MAP["solid"]

    def __init__(self,
                 device: torch.device,
                 fill_polygon: bool = True,
                 outline_type: str = OUTLINE_SOLID,
                 mask_mode=MASK_MODE_MONOCHROME):
        """
        Creates a MaskRenderer which uses Pillow as the rendering backend.
        MaskRenderer can only render shapes passed as `occlusion_shapes`, to improve performance. If you need a generalized rendering utility,
        see `hirise.voronoi.utils.polymask`

        :param fill_polygon: bool, if True, polygons will be rendered with the value of 1
        :param target_shape: (Y, X) tuple containing the desired resolution of the rendered masks
        """
        self._polygon_fill = self._FILL_VALUE if fill_polygon else 0

        try:
            self._outline_fill = self._OUTLINE_VALUE_MAP[outline_type.lower()]
        except (KeyError, AttributeError):
            raise ValueError(f"outline_type has to be one of {tuple(self._OUTLINE_VALUE_MAP)} "
                             f"(got: f{outline_type})")

        allowed_mask_modes = (PillowOcclusionRenderer.MASK_MODE_MONOCHROME,
                              PillowOcclusionRenderer.MASK_MODE_BINARY)
        if mask_mode not in allowed_mask_modes:
            raise ValueError(f"mask mode must be one of {allowed_mask_modes}, got \'{mask_mode}\'")
        self._mask_mode = mask_mode

        self._polygon_coord_range = (0, 1)
        self.device = device


    def render(
        self,
        occlusions: Union[np.ndarray, Sequence[np.ndarray]],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        """
        Renders all `occlusions` to a 2D torch.Tensor
        """
        if isinstance(occlusions, (np.ndarray)):
            occlusions = (occlusions, )

        mask = polymask(
            polygon_vertices=occlusions,
            target_shape=target_shape,
            fill_value=self._polygon_fill,
            outline_value=self._outline_fill,
            mask_mode=self._mask_mode
        )

        return torch.as_tensor(mask, device=self.device) / self._FILL_VALUE

    def batch_render(
        self,
        occlusion_set: Sequence[np.ndarray],
        occlusion_selectors: Union[np.ndarray, torch.Tensor],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        """
        Renders a set of masks from given occlusions.
        1D slices along the last axis of `occlusion_selectors` determine which of `the polygons in `occlusion_set`
        will be used for a given mask.
        """
        self._validate_selectors(occlusion_selectors, occlusion_set)
        hypershape = occlusion_selectors.shape[:-1]
        masks = torch.zeros(*hypershape, *target_shape, device=self.device)
        for ndidx in np.ndindex(*hypershape):
            selector = occlusion_selectors[ndidx]
            occlusions = self._one_hot_select(occlusion_set, one_hot_selector=selector)
            masks[ndidx] = self.render(occlusions, target_shape=target_shape)

        return masks


#region [decorators]
class OcclusionRendererDecorator(OcclusionRenderer):

    def __init__(self, renderer: OcclusionRenderer):
        self.renderer = renderer

#region [shift decorators]
class RandomRenderShift(OcclusionRendererDecorator):

    def _shift_crop_one(self, mask: torch.Tensor, shift_radius: float, target_shape: Tuple[int, int]) -> torch.Tensor:
        """
        Performs a shift-crop of a single mask. The initial dimensions of the mask must be at least (target_shape + shift_radius).
        The mask is then cropped to `target_shape`, however the origin point of the crop is chosen randomly within range (0, shift_radius)
        """
        if any(np.array(target_shape) > mask.shape[-len(target_shape):]):
            raise ValueError(f"Not possible to make a {target_shape} mask out of a {mask.shape[-2:]} tensor")

        shift_vector = np.round(np.random.rand(2) * shift_radius).astype(int)
        y_shift, x_shift = shift_vector
        return mask[y_shift:y_shift + target_shape[0],
                    x_shift:x_shift + target_shape[1]]

    def _shift_crop_many(self, masks: torch.Tensor, shift_radius: float, target_shape: Tuple[int, int]):
        """
        Performs a shift-crop of multiple masks. The initial dimensions of each mask must be at least (target_shape + shift_radius).
        Each mask is then cropped to `target_shape`, however the origin point of the crop is chosen randomly within range (0, shift_radius)
        """
        single_mask_shape = masks.shape[-len(target_shape):]

        if any(np.array(target_shape) > single_mask_shape):
            raise ValueError(f"Not possible to make a {target_shape} mask out of a {single_mask_shape} tensor")

        hyperdims = masks.shape[:-len(target_shape)]
        n_masks = np.prod(hyperdims).item()
        shift_vectors = np.round(np.random.rand(n_masks, 2) * shift_radius).astype(int)
        raveled_masks = masks.reshape(-1, *single_mask_shape)

        shifted_masks = torch.zeros(n_masks, *target_shape)
        for i, (y_shift, x_shift) in enumerate(shift_vectors):
            shifted_masks[i] = raveled_masks[i,
                                     y_shift:y_shift + target_shape[0],
                                     x_shift:x_shift + target_shape[1]]
        return shifted_masks.reshape(*hyperdims, *target_shape)


class FixedRandomRenderShift(RandomRenderShift):
    """
    Applies a random shift to each mask, in both axes.
    The range of the shift is not greater than `shift_radius` in each dimension.
    Each mask is rendered in higher resolution than given by `target_shape` and then cropped to
    """

    def __init__(self, renderer: OcclusionRenderer, shift_radius: float):
        super().__init__(renderer)
        self.shift_radius = shift_radius

    def render(self, occlusions: Sequence[np.ndarray], target_shape: Tuple[int, int]) -> torch.Tensor:
        up_size = np.array(target_shape) + self.shift_radius
        raw_mask = self.renderer.render(occlusions, up_size)
        return self._shift_crop_one(raw_mask, self.shift_radius, target_shape)

    def batch_render(
        self,
        occlusion_set: Sequence[np.ndarray],
        occlusion_selectors: Union[np.ndarray, torch.Tensor],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        up_size = np.array(target_shape) + self.shift_radius
        raw_masks = self.renderer.batch_render(occlusion_set, occlusion_selectors, up_size)
        return self._shift_crop_many(raw_masks, self.shift_radius, target_shape)


class RandomPolygonShift(OcclusionRendererDecorator):

    def __init__(self, renderer: OcclusionRenderer, shift_radii:Union[Tuple[float], np.ndarray, torch.Tensor]):
        """

        :param renderer: decorated renderer
        :param shift_radii: shift radii in XY(Z) order
        """
        super().__init__(renderer)
        self.shift_radii = np.array(shift_radii)

    def render(self, occlusions: Sequence[np.ndarray], target_shape: Tuple[int, int]) -> torch.Tensor:
        occlusions = [shapely.affinity.translate(polygon, *self.shift_radii[::-1]) for polygon in occlusions]
        return self.renderer.render(occlusions, target_shape)

    def batch_render(
        self,
        occlusion_set: Sequence[np.ndarray],
        occlusion_selectors: Union[np.ndarray, torch.Tensor],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        raise NotImplementedError


class OcclusionDependentRandomRenderShift(RandomRenderShift):
    """
    Reflects the original RISE approach to shifts - forces the renderer to render in higher resolution and then crops the result
    `shift_radius` isn't strictly a "radius" - it's rather the maximum shift distance in both Y and X dimensions,
    meaning that a diagonal shift may displace the mask up to sqrt(2) * radius. The goal is to prevent is to maintain overlap
    between the original occlusion and its shifted render.
    """

    def render(self, occlusions: Sequence[np.ndarray], target_shape: Tuple[int, int]) -> torch.Tensor:
        """
        Applies a random shift to the mask rendered by the decorated renderer.
        The maximum shift distance is dependent on the dimensions of the smallest occlusion
        """
        # TODO: This will be more reliable if polygons area cropped to the bounding box
        #  (otherwise this approach may yield massive shifts, if polygons stretch far beyond the redering viewport)
        shift_radius = self._infer_shift_range(occlusions, target_shape)

        up_size = np.array(target_shape) + shift_radius
        raw_mask = self.renderer.render(occlusions, up_size)
        return self._shift_crop_one(raw_mask, shift_radius, target_shape)

    def batch_render(
        self,
        occlusion_set: Sequence[np.ndarray],
        occlusion_selectors: Union[np.ndarray, torch.Tensor],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        """
        Applies a random shift to each mask rendered by the decorated renderer.
        The maximum shift distance is dependent on the dimensions of the smallest occlusion in the entire `occlusion_set`
        Each mask is shifted by a different random vector.
        """
        shift_radius = self._infer_shift_range(occlusion_set, target_shape)

        up_size = np.array(target_shape) + shift_radius
        raw_masks = self.renderer.batch_render(occlusion_set, occlusion_selectors, up_size)
        return self._shift_crop_many(raw_masks, shift_radius, target_shape)

    def _infer_shift_range(self, polygons: Sequence[np.ndarray], target_shape: Tuple[int, int]) -> float:
        """
        Calculates the appropriate shift_radius for given set of polygons
        """
        polygons = [Polygon(polygons)] if isinstance(polygons, np.ndarray) else list(map(Polygon, polygons))

        polygon_bounds = np.stack([p.bounds for p in polygons]).reshape((-1, 2, 2))
        bound_dims = np.abs(polygon_bounds[:, 1, :] - polygon_bounds[:, 0, :])
        shift_radius = np.round(bound_dims.min(0).mean() * (sum(target_shape) / len(target_shape))).astype(int)
        return shift_radius

#endregion [shift decorators]

#region [blur decorators]
class GaussianBlur(OcclusionRendererDecorator):

    def _blur_one(self, mask: torch.Tensor, sigma: float) -> torch.Tensor:
        sigma = float(sigma)
        if sigma == 0:
            return mask
        # kernel size formula via https://github.com/scipy/scipy/blob/701ffcc8a6f04509d115aac5e5681c538b5265a2/scipy/ndimage/filters.py#L257
        _scaled_sigma = 8 * round(sigma)
        kernel_size = _scaled_sigma + (0 if _scaled_sigma % 2 else 1)
        return gaussian_blur(mask.unsqueeze(0), kernel_size=kernel_size, sigma=sigma).squeeze()

    def _blur_many(self, masks: torch.Tensor, sigma: float) -> torch.Tensor:
        sigma = float(sigma)
        if sigma == 0:
            return masks
        original_shape = masks.shape
        # kernel size formula via https://github.com/scipy/scipy/blob/701ffcc8a6f04509d115aac5e5681c538b5265a2/scipy/ndimage/filters.py#L257
        _scaled_sigma = 8 * round(sigma)
        kernel_size = _scaled_sigma + (0 if _scaled_sigma % 2 else 1)
        blurred = gaussian_blur(masks.reshape(-1, *original_shape[-2:]), kernel_size=kernel_size, sigma=sigma)
        return blurred.reshape(*original_shape)


class FixedGaussianBlur(GaussianBlur):

    def __init__(self, renderer: OcclusionRenderer, sigma: float):
        super().__init__(renderer)
        self.sigma = sigma

    def render(self, occlusions: Sequence[np.ndarray], target_shape: Tuple[int, int]) -> torch.Tensor:
        mask = self.renderer.render(occlusions, target_shape)
        return self._blur_one(mask, self.sigma)

    def batch_render(
        self,
        occlusion_set: Sequence[np.ndarray],
        occlusion_selectors: Union[np.ndarray, torch.Tensor],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        masks = self.renderer.batch_render(occlusion_set, occlusion_selectors, target_shape)
        return self._blur_many(masks, sigma=self.sigma)


class OcclusionDependentGaussianBlur(GaussianBlur):

    def __init__(self, renderer: OcclusionRenderer, min_sigma, max_sigma):
        super().__init__(renderer)
        self.min_sigma = min_sigma
        self.max_sigma = max_sigma

    def render(self, occlusions: Sequence[np.ndarray], target_shape: Tuple[int, int]) -> torch.Tensor:
        _max_allowed_sigma = min(target_shape) / 4 # assuming that `kernel_size == 8*sigma`
        sigma = min(self._infer_sigma(occlusions), _max_allowed_sigma)
        mask = self.renderer.render(occlusions, target_shape)
        return self._blur_one(mask, sigma)

    def batch_render(
        self,
        occlusion_set: Sequence[np.ndarray],
        occlusion_selectors: Union[np.ndarray, torch.Tensor],
        target_shape: Tuple[int, int]
    ) -> torch.Tensor:
        sigma = self._infer_sigma(occlusion_set)
        raw_masks = self.renderer.batch_render(occlusion_set, occlusion_selectors, target_shape)
        return self._blur_many(raw_masks, sigma)

    def _infer_sigma(self, polygons: Sequence[np.ndarray]) -> float:
        # TODO: This implementation is a basic placeholder and need to be re-evaluated
        #       1. try to find sigma values that resemble blur introduced by interpolation
        #       2. establish what are the most effective values of min and max blur radius
        warnings.warn("Blur radius inference hasn't been tested or evaluated in terms of suitability yet")
        polygons = [Polygon(polygons)] if isinstance(polygons, np.ndarray) else list(map(Polygon, polygons))

        smin, smax = self.min_sigma, self.max_sigma
        min_polygon_area = min(p.area for p in polygons)
        return smin + (smax - smin) * (min_polygon_area ** (1/4))

#endregion [blur decorators]

#endregion [decorators]
