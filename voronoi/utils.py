from typing import *

import numpy as np
import torch
from PIL import Image, ImageDraw
from shapely.geometry import Polygon

UNIT_BOUNDING_BOX = Polygon([[0, 0], [1, 0], [1, 1], [0, 1]])


def assemble_polygons(regions: Sequence[Sequence[int]], vertices: np.ndarray) -> Tuple[Polygon]:
    """
    Builds a list of Polygon objects from a 2D array of vertex coordinates (`vertices`) and a list of  vertex indices for each polygon

    :param regions: list of lists of indices for `vertices`, where each inner list contains a set of vertices for a single polygon
    :param vertices: 2D array of vertices used by `regions`
    :return: List of Polygons
    """
    return tuple(map(lambda region: Polygon(vertices[region]), regions))


def polymask(
    polygon_vertices: Union[Sequence[np.ndarray]],
    target_shape: Tuple[int, int],
    fill_value: int = 1,
    outline_value: int = 1,
    mask_mode: str = 'L',
    scale_to_target: bool = True,
) -> np.ndarray:
    """
    PIL-based polygon renderer. Returns a 2D bitmap stored in an ndarray,

    adapted from: https://stackoverflow.com/a/64876117/11048027
    :param polygon_vertices: list of 2D np.ndarray of shape [Ni, 2] where Ni is the number of vertices of i-th polygon. Each row contains coords in (X, Y) order
    :param target_shape: target shape of the mask (Y, X)
    :param polygons: list of polygons to render on the mask
    :param fill_value: value used to fill the interior of the polygon
    :param outline_value: value used to draw the edge of the polygon with
    :param mask_mode: PIL.Image mode used for drawing
    :param scale_to_target: if true, `polygon_vertices` will be assumed to be in range (0,1) (no checks performed) and scaled to `target_shape`, otherwise coords will be used as-is
    :return: (ndarray) the rendered mask
    """

    if isinstance(polygon_vertices, (np.ndarray, torch.Tensor)):
        polygon_vertices = (polygon_vertices, )

    # create a binary image
    height, width = target_shape
    img = Image.new(mode=mask_mode, size=(width, height), color=0)
    draw = ImageDraw.Draw(img)

    # coords are provided in XY order but target_shape in YX,
    # so we have to adjust by reversing the order of scaling factors
    if scale_to_target:
        scale = np.flip(np.array(target_shape))
        polygon_vertices = [verts * scale for verts in polygon_vertices]

    for verts in polygon_vertices:
        draw.polygon(verts.ravel().tolist(), outline=outline_value, fill=fill_value)

    return np.frombuffer(img.tobytes(), dtype=np.uint8).reshape(target_shape).astype(np.float32)
