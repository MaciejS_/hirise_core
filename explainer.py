from typing import List, Union, Sequence

import torch
from tqdm.autonotebook import tqdm

from hirise.mask_evaluator import MaskEvaluator
from hirise.occlusions.bandgen import BandGen
from hirise.occlusions.bands import ThresholdGen
from hirise.occlusions.maskgen import MaskGen
from hirise.structures import CombinedMask, MaskAggregator, BatchingCombinedMask


class RISE():

    def __init__(self,
                 mask_gen: MaskGen,
                 mask_evaluator: MaskEvaluator,
                 mask_batch_size:int=1000,
                 use_tqdm=False):
        self.mask_gen = mask_gen
        self.mask_evaluator = mask_evaluator
        self.mask_batch_size = mask_batch_size
        self.use_tqdm = use_tqdm

    def explain(self,
                img: torch.Tensor,
                N:int,
                s:int,
                p1:float,
                explained_class=0,
                mask_retainers:List[MaskAggregator]=()) -> (torch.Tensor, torch.Tensor):

        map_builder = CombinedMask(img.shape[-2:], p1)
        for batch_start, batch_end in tqdm(MaskEvaluator.batch_boundaries(N, self.mask_batch_size),
                                             desc="Mask batch",
                                             leave=False,
                                             disable=not self.use_tqdm):

            n_masks = min(batch_end - batch_start, self.mask_batch_size)
            masks = self.mask_gen.generate_masks(n_masks, p1, img.shape[-2:], torch.ones(s,s))[0]
            mask_scores = self.mask_evaluator.evaluate(img, masks, MaskEvaluator.ALL_CLASSES)

            map_builder.add_many(masks.squeeze().cpu(), mask_scores[:, explained_class].cpu())
            for retainer in mask_retainers:
                retainer.add_many(masks.squeeze().cpu(), mask_scores.cpu())

        return map_builder.get_mask()


class HiRISE():


    def __init__(self,
                 band_gen: BandGen,
                 mask_gen: MaskGen,
                 threshold_gen: ThresholdGen,
                 mask_evaluator: MaskEvaluator,
                 op_size_limit:int = 1000*(224*224),
                 single_mask_ndim:int = 2):
        self.band_gen = band_gen
        self.mask_gen = mask_gen
        self.threshold_gen = threshold_gen
        self.evaluator = mask_evaluator
        self.op_size_limit = op_size_limit
        self.single_mask_ndim = single_mask_ndim

    def explain_iteratively(self,
                            img: torch.Tensor,
                            band_edges: Union[list],
                            mask_budgets: Union[int, Sequence[int]],
                            n_iterations: int,
                            observed_class: int,
                            ):
        # I might need a specialized mask gen here - to optimally batch mask evaluations, or extend current evaluator to support tensor lists


        if (len(band_spec.band_ratios) != len(mask_budgets)) and mask_budget_ratios != 1:
            raise ValueError("Number of bands inferred from mask_budget don't ")



        # first iteration is plain RISE
        # then use

        raise NotImplementedError

    def explain(self,
                img: torch.Tensor,
                bands: Union[List[torch.Tensor], torch.Tensor],
                mask_budgets_per_band: List[int],
                p1_values: List[float],
                observed_classes:Union[int, Sequence[int]]=None,
                mask_retainers:Sequence[MaskAggregator]=()) -> List[torch.Tensor]:

        if len(bands) != len(mask_budgets_per_band):
            # TODO: test this
            raise ValueError(f"Number of bands ({len(bands)}) doesn't match the number of entries in mask_budgets ({len(mask_budgets_per_band)})")

        if not all(map(lambda band: band.shape[-1] == band.shape[-2], bands)):
            raise("All band tensors must be rectangular")

        if len(mask_retainers) != len(bands):
            raise

        combined_bands = []
        for mask_count, band, p1 in zip(mask_budgets_per_band, bands, p1_values):
            masks = self.mask_gen.generate_masks(mask_count, band.shape[0], p1, img.shape)
            scores = self.evaluator.evaluate(img, masks, observed_classes=observed_classes)
            combined_masks = BatchingCombinedMask(CombinedMask(masks.shape[-2:], p1),
                                                  self.op_size_limit,
                                                  single_mask_ndim=2)
            combined_masks.add_many(masks, scores)
            combined_bands.append(combined_masks)

        return [cm.retainer() for cm in combined_bands]

