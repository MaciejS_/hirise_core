import math
import heapq
from typing import List, Tuple, Union, Sequence, Dict, Any, Iterable

import numpy as np
import torch
import hirise.structures_utils as utils
from hirise.mask_evaluator import MaskEvaluator
from tqdm.autonotebook import tqdm


class MaskAggregator:

    def add_one(self, masks, scores) -> None:
        raise NotImplementedError

    def add_many(self, masks, scores) -> None:
        raise NotImplementedError

    @property
    def mask_count(self):
        raise NotImplementedError

class NoopMaskRetainer(MaskAggregator):
    """
    An "dud" version of MaskAggregator. Does nothing except fit into MaskAggregator-shaped holes.
    """

    def add_one(self, masks, scores) -> None:
        pass

    def add_many(self, masks, scores) -> None:
        pass

    @property
    def mask_count(self):
        return 0


class CombinedMask(MaskAggregator):
    """
    Basic mask aggreagtor - creates a score-weighted average of all received masks.
    """
    def __init__(self, shape, mask_occlusion_prob):
        self.combined_mask = torch.zeros(shape)
        self.mask_counter = 0
        # TODO: (note) `p1` should stay constant for now i.e. no support for aggregation of variable-p1 masks by one aggregator
        self.mask_occlusion_prob = mask_occlusion_prob

    @classmethod
    def from_map(cls, saliency_map: torch.Tensor, n_masks: int, occlusion_probability: float) -> "CombinedMask":
        """
        Restore a combined mask from a saliency map and the information about the number of masks it consists of
        :param saliency_map: a 2D saliency map 
        :param n_masks: number of masks tha that were combined to produce `mask` given as first parameter
        :param occlusion_probability: float, pixel occlusion probability of the masks in mask
        :return: CombinedMask
        """
        if len(saliency_map.shape) != 2:
            raise ValueError("Mask shape must be 2-dimensional, got %d (%s)" % (len(saliency_map.shape), saliency_map.shape))
        combined_mask = cls(saliency_map.shape[:-2], occlusion_probability)
        combined_mask.mask_counter = n_masks
        combined_mask.combined_mask = saliency_map
        return combined_mask

    @classmethod
    def from_masks(cls, masks: torch.Tensor, scores: torch.Tensor, occlusion_probability: float) -> "CombinedMask":
        if not 2 <= len(masks.shape) < 4:
            raise ValueError("masks tensor should have 2 (if one 2D mask) or 3 dimensions (if multiple 2D masks), got %d " % len(masks.shape) )
        stack = cls(masks.shape[-2:], occlusion_probability)
        stack.add_many(masks, scores)
        return stack

    def add_one(self, mask: torch.Tensor, score: Union[float, torch.Tensor]) -> None:
        if mask.ndim != 2:
            raise ValueError("mask has to be 2D, got %d" % mask.ndim)

        self.combined_mask += mask * score
        self.mask_counter += 1


    def _add_many(self, masks: torch.Tensor, scores: torch.Tensor) -> None:
        self.combined_mask += (masks * scores.reshape(-1, 1, 1)).sum(dim=0)
        self.mask_counter += len(masks)

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor) -> None:
        if masks.ndim == 3:
            self._add_many(masks, scores)
        elif masks.ndim == 2:
            self._add_many(masks[None, ...], scores)
        else:
            raise ValueError(f"Invalid masks shape - masks: {masks.shape}, scores: {scores.shape}")

    def get_mask(self):
        if self.mask_counter == 0:
            return self.combined_mask
        else:
            return self.combined_mask / self.mask_counter / self.mask_occlusion_prob

    @property
    def mask_count(self):
        return self.mask_counter


class VariableFillCombinedMask(CombinedMask):

    def __init__(self, shape):
        """
        :param shape:
        VariableFillCombinedMask automatically calculates the mask fill rate of added masks and accounts for it in mask aggregation
        """
        super().__init__(shape, mask_occlusion_prob=1) # use occlusion_prob == 1 to turn the final division in get_mask into a no-op

    @classmethod
    def from_map(cls, saliency_map: torch.Tensor, n_masks: int, occlusion_probability: float) -> "CombinedMask":
        return super().from_map(saliency_map, n_masks, occlusion_probability=1)

    @classmethod
    def from_masks(cls, masks: torch.Tensor, scores: torch.Tensor, occlusion_probability: float) -> "CombinedMask":
        return super().from_masks(masks, scores, occlusion_probability=1)

    def add_one(self, mask: torch.Tensor, score: Union[float, torch.Tensor]) -> None:
        fill_rate = mask.mean()
        super().add_one(mask, (score / fill_rate) if fill_rate else 0)

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor) -> None:
        hyperdims = masks.shape[:-2]
        fill_rates = masks.view(*hyperdims, -1).mean(dim=len(hyperdims))
        weighted_scores = torch.where(
            fill_rates == 0, # replace all weighted scores where (fillrate == 0 would cause NaN result) with zeros
            torch.zeros_like(fill_rates),
            scores / fill_rates
        )
        super().add_many(masks, weighted_scores)

    def get_mask(self):
        return super().get_mask()


class CombinedMaskWithCheckpoints(MaskAggregator):

    def __init__(self, combined_mask: MaskAggregator):
        """
        This is an extension of CombinedMask which allows for manual creation of mask state snapshots
        via `save_checkpoint` method and later retrieval of stored checkpoints by `get_checkpoints`
        :param combined_mask: underlying mask aggreagator
        """
        self.mask = combined_mask
        self.checkpoints = []

    def add_one(self, mask: torch.Tensor, score: float) -> None:
        """
        Adds a single scored mask to underlying CombinedMasks.
        No additional action is performed.
        :return:
        """
        self.mask.add_one(mask, score)

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor) -> None:
        """
        Adds a batch of scored masks to the underlying CombinedMask
        """
        self.mask.add_many(masks, scores)

    def save_checkpoint(self) -> None:
        """
        Makes a copy of the current state of the combined mask and adds it to the internal list of checkpoints.
        """
        self.checkpoints.append((self.mask.mask_count, torch.clone(self.mask.get_mask())))

    def get_checkpoints(self) -> List[Tuple[int, torch.Tensor]]:
        """
        Returns all stored checkpoints as (mask_idx, mask_state) tuples
        """
        return self.checkpoints

    @property
    def mask_count(self):
        return self.mask.mask_count


class LambdaCheckpoints(CombinedMaskWithCheckpoints):

    def __init__(self, combined_mask: MaskAggregator, checkpoint_lambda: callable):
        """

        :param combined_mask:
        :param checkpoint_lambda: callable taking a (index, mask, score) triple, returning True if it should be checkpointed
        """
        super().__init__(combined_mask)
        self.mask = combined_mask
        self.checkpoint_lambda = checkpoint_lambda


    def add_one(self, mask: torch.Tensor, score: float) -> None:
        """
        Adds the provided mask to underlying combined mask and saves a checkpoint if lambda
        :param mask: 2D numpy.ndarray
        :param score:
        :return:
        """
        super().add_one(mask, score)
        # the mask has already been added, so the counter shows i+1. To capture state at i-th mask subtraction of 1 is required
        if self.checkpoint_lambda(self.mask.mask_count, mask, score):
            self.save_checkpoint()

    def add_many(self, masks, scores):
        """

        :param masks:
        :param scores:
        :return:
        """
        aggregated_mask_indices = range(self.mask.mask_count + 1, self.mask.mask_count + 1 + len(masks))
        checkpoints = map(lambda x: self.checkpoint_lambda(*x), zip(aggregated_mask_indices, masks, scores))
        checkpoint_indices = [idx + 1
                              for idx, is_checkpoint
                              in enumerate(checkpoints)
                              if is_checkpoint]

        if not checkpoint_indices:
            super().add_many(masks, scores)
        else:
            mask_ranges = zip([0] + checkpoint_indices,
                              checkpoint_indices)

            for start, end in mask_ranges:
                super().add_many(masks[start:end], scores[start:end])
                self.save_checkpoint()

            if checkpoint_indices[-1] < len(masks):
                i = checkpoint_indices[-1]
                super().add_many(masks[i:], scores[i:])

    def get_mask(self):
        return self.mask.get_mask()


class ArbitraryCheckpoints(CombinedMaskWithCheckpoints):

    def __init__(self, combined_mask: MaskAggregator, checkpoint_idxs):
        super().__init__(combined_mask)
        self.checkpoint_idxs = checkpoint_idxs
        self.i = 0

    def add_one(self, mask, score):
        super().add_one(mask, score)

        if self.mask.mask_count in self.checkpoint_idxs:
            self.save_checkpoint()

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor):
        boundaries = self.calculate_insertion_boundaries(self.mask.mask_count, len(masks), self.checkpoint_idxs)
        for start, end in boundaries:
            super().add_many(masks[start:end], scores[start:end])
            if self.mask.mask_count in self.checkpoint_idxs:
                self.save_checkpoint()

    @staticmethod
    def calculate_insertion_boundaries(existing_masks: int, new_masks: int, checkpoint_idxs: Sequence):
        checkpoints_crossed = list(filter(lambda x: existing_masks < x < existing_masks + new_masks, checkpoint_idxs))
        offsets = list(map(lambda x: x - existing_masks, checkpoints_crossed))

        return list(zip([0] + offsets, offsets + [new_masks]))

    def get_mask(self):
        return self.mask.get_mask()

class IntervalCheckpoints(CombinedMaskWithCheckpoints):

    def __init__(self, combined_mask: MaskAggregator, interval):
        super().__init__(combined_mask)
        self.interval = interval

    def add_one(self, mask, score):
        super().add_one(mask, score)

        if self.mask.mask_count % self.interval == 0:
            self.save_checkpoint()

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor):
        boundaries = self.calculate_insertion_boundaries(self.mask.mask_count, len(masks), self.interval)
        for start, end in boundaries:
            super().add_many(masks[start:end], scores[start:end])
            if end % self.interval == 0:
                self.save_checkpoint()

    @staticmethod
    def calculate_insertion_boundaries(existing_masks: int, new_masks: int, interval: int):
        checkpoints_so_far = math.floor(existing_masks / interval)
        checkpoints_after_addition = math.floor((existing_masks + new_masks) / interval)

        masks_until_first_next_interval = interval - (existing_masks % interval)
        upper_boundaries = [masks_until_first_next_interval + (i * interval)
                            for i
                            in range(checkpoints_after_addition - checkpoints_so_far)]

        boundaries = list(zip((0, *upper_boundaries), (*upper_boundaries, new_masks)))
        if upper_boundaries:
            last_mask_falls_on_checkpoint_boundary = (existing_masks + new_masks) % interval == 0
            return boundaries[:-1] if last_mask_falls_on_checkpoint_boundary else boundaries
        else:
            return [(0, new_masks)]


class TopMasksRetainer(MaskAggregator):

    def __init__(self, mask: MaskAggregator, max_n_masks: int) -> None:
        self.decorated_mask_aggregator = mask
        self.masks = []
        self.max_n_masks = max_n_masks

    def add_one(self, mask: torch.Tensor, score: float):
        self._add_one(mask, score, pass_down=True)

    def _add_one(self, mask, score, pass_down=True):
        item_to_insert = (score, mask)
        min_score = heapq.nsmallest(1, self.masks, TopMasksRetainer.__by_score)[0][0] if self.masks else 0

        if len(self.masks) < self.max_n_masks:
            heapq.heappush(self.masks, item_to_insert)
        elif score > min_score:
            heapq.heapreplace(self.masks, item_to_insert)
        else:
            pass  # the heap is already full and this mask has a smaller score than any other

        if pass_down:
            self.decorated_mask_aggregator.add_one(mask, score)

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor):
        for score, mask in zip(scores, masks):
            self._add_one(mask, score, pass_down=False)
        self.decorated_mask_aggregator.add_many(masks, scores)

    def get_entries(self, n:int=None):
        if n and n < 0:
            raise ValueError("`n` must be positive")
        return heapq.nlargest(n or min(self.max_n_masks, len(self.masks)),
                              self.masks,
                              lambda x: x[0])

    def get_masks(self, top_n:int=None):
        return [mask for score, mask in self.get_entries(top_n)]

    def get_scores(self, top_n:int=None):
        return [score for score, mask in self.get_entries(top_n)]

    @staticmethod
    def __by_score(score_mask_pair):
        score, mask = score_mask_pair
        return score


class BatchingCombinedMask:
    """
    This CombinedMask prevents out-of-memory errors during large mask combination operations (e.g. when calculating saliency maps for a large number of classes)
    BatchingCombinedMask will split operations which are likely to exceed `single_op_size_limit` into smaller, sequential mini-batch operations.
    """

    def __init__(self, retainer: Union[CombinedMask, CombinedMaskWithCheckpoints], single_op_size_limit: int, single_mask_ndim:int, use_tqdm=False):
        """
        :param retainer: mask aggregator used to combine masks and retain results
        :param single_op_size_limit: maximum size of the result matrix of a single mini-batch operation
        :param single_mask_ndim: int, dimensionality of a single mask (2 for 2D masks)
        :param use_tqdm: optional, if True, tqdm progress bar will be shown during mask combination process
        """
        self.retainer = retainer
        self.single_op_size_limit = single_op_size_limit
        self.single_mask_ndim = single_mask_ndim
        self.use_tqdm = use_tqdm

    def add_one(self, mask, scores):
        self.retainer.add_one(mask, scores)

    def add_many(self, masks: torch.Tensor, scores: torch.Tensor) -> None:
        total_N = masks.shape[-3] if masks.ndim == 3 else 1
        required_batches = self.calculate_op_size(masks, scores) / self.single_op_size_limit
        single_batch_size = math.floor(total_N / required_batches)

        if single_batch_size < 1:
            raise RuntimeError("Mask")

        for batch_start, batch_end in tqdm(MaskEvaluator.batch_boundaries(total_N, single_batch_size),
                                             desc="Mask combination batches",
                                             leave=False,
                                             disable=not self.use_tqdm):
            self.retainer.add_many(masks[batch_start:batch_end], scores[batch_start:batch_end])

    def calculate_op_size(self, masks: torch.Tensor, scores: torch.Tensor) -> int:
        """
        Calculates the product of shape of (mask * scores).
        """
        masks_hypershape = masks.shape[:-self.single_mask_ndim]
        mask_shape = masks.shape[-self.single_mask_ndim:]
        scores_hypershape = scores.shape

        if masks.ndim == self.single_mask_ndim or (masks.shape[0] == 1):
            op_shape = torch.cat(list(map(lambda x: torch.as_tensor(x, dtype=torch.long),
                                      [scores_hypershape, mask_shape])))
        elif masks.ndim > self.single_mask_ndim:
            op_shape = torch.cat(list(map(lambda x: torch.as_tensor(x, dtype=torch.long),
                                      [scores_hypershape, masks_hypershape[1:], mask_shape])))
        else:
            raise ValueError(f"Masks tensor dimensionality ({masks.ndim}) must be >= than that of declared base mask shape ({self.single_mask_ndim})")

        return torch.prod(op_shape, dim=0).item()

    def get_mask(self):
        return self.retainer.get_mask()

class ScoresRetainer(MaskAggregator):

    def __init__(self):
        MaskAggregator.__init__(self)
        self.scores = None

    def add_one(self, masks, scores) -> None:
        stackable_scores = scores.unsqueeze(0)
        if self.scores is not None:
            self.scores = torch.cat([self.scores, stackable_scores])
        else:
            self.scores = stackable_scores

    def add_many(self, masks, scores) -> None:
        if self.scores is not None:
            self.scores = torch.cat([self.scores, scores])
        else:
            self.scores = scores

    def get(self):
        return self.scores


class ClassFilter(MaskAggregator):
    """
    Prevents scores for classes other than `observed_class` from being passed to `retainer`
    Modifications introduced by this component affect all downstream aggregators.
    """
    def __init__(self, retainer: MaskAggregator, observed_class: int):
        MaskAggregator.__init__(self)
        self.retainer = retainer
        self.observed_class = observed_class

    def add_one(self, masks, scores) -> None:
        self.retainer.add_one(masks, scores[:, self.observed_class])

    def add_many(self, masks, scores) -> None:
        self.retainer.add_many(masks, scores[:, self.observed_class])


class UniqueMaskCollector(MaskAggregator):
    """
    Collects unique masks as {hash: mask} mappings.
    """

    def __init__(self, retainer: MaskAggregator = None, mask_ndim: int = 2, mask_hash_function=utils.hash_tensor):
        """
        :param mask_ndim: optional int, dimensionality of a single mask.
        """
        MaskAggregator.__init__(self)
        self._nested_retainer = retainer or NoopMaskRetainer()
        self._mask_ndim = mask_ndim
        self._encountered_masks = {}
        self._hashing_function = mask_hash_function

    def add_one(self, mask, scores) -> None:
        if mask.ndim != self._mask_ndim:
            raise ValueError(f"masks ndim should be equal to {self._mask_ndim}, got {mask.ndim} instead")
        self._add_one(mask)
        self._nested_retainer.add_one(mask, scores)

    def _add_one(self, masks) -> None:
        hash = self._hashing_function(masks)
        self._encountered_masks[hash] = masks

    def add_many(self, masks, scores) -> None:
        if masks.ndim < self._mask_ndim + 1:
            raise ValueError(
                    f"masks should have at least one more dimension than a single mask (expected ndim>={self._mask_ndim + 1}, got {masks.ndim}")
        ravelled_masks = masks.reshape(-1, *masks.shape[-self._mask_ndim:])
        for mask in ravelled_masks:
            self._add_one(mask)
        self._nested_retainer.add_many(masks, scores)

    @property
    def masks(self):
        return self._encountered_masks.values()


class StackingRetainer(MaskAggregator):
    """
    Simply stacks all received masks.
    """

    def __init__(self, shape):
        self.shape = shape
        self.retained_masks = None
        self.retained_scores = None

    def add_one(self, masks, scores) -> None:
        expanded_mask = masks.unsqueeze(0)
        expanded_scores = scores.unsqueeze(0)
        self.add_many(expanded_mask, expanded_scores)

    def add_many(self, masks, scores) -> None:
        if tuple(masks.shape[1:]) != self.shape:
            raise ValueError(f"Shape of the masks tensor ({masks.shape[:-len(self.shape)]}){masks.shape[-len(self.shape)]} has to be equal to shape expected by this retainer {self.shape}")

        self._add_masks(masks)

        if scores is not None:
            self._add_scores(scores)

    def _add_masks(self, masks):
        if self.retained_masks is None:
            self.retained_masks = masks  # future stacking will be performed alongside the first dim
        else:
            self.retained_masks = torch.cat((self.retained_masks, masks), dim=0)

    def _add_scores(self, scores):
        scores_tensor = torch.as_tensor([[scores]]) if isinstance(scores, (int, float)) else scores
        if self.retained_scores is None:
            self.retained_scores = scores_tensor
        else:
            self.retained_scores = torch.cat((self.retained_scores, scores_tensor), dim=0)

    @property
    def masks(self):
        return self.retained_masks

    @property
    def scores(self):
        return self.retained_scores

    @property
    def mask_count(self):
        return len(self.retained_masks)