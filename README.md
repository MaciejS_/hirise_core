# HiRISE

An attempt to improve [RISE](https://arxiv.org/abs/1806.07421v3)

# Contents
This repository contains code of two attempts at improving RISE

* **HiRISE** - an abandoned attempt based on iterative reduction of evaluated area (successfully implemented by [HiPe](https://arxiv.org/abs/2103.05108), published in 2021)
* **VRISE** - Voronoi-RISE, which relaxes constraints placed on occlusion shapes and samples occlusions from Voronoi meshes instead of square grids.


# Setup:
1. Create a new Python virtual environment and activate it
```shell
virtualenv --python=python3.8 <path_to_env>
source <path_to_env>/bin/activate
```
1. While in the virtualenv, install prerequisites:
    * PyTorch >= 1.7.1 built with CUDA version approriate for your GPU [[current builds](https://pytorch.org/get-started/locally/)][[legacy builds](https://pytorch.org/get-started/previous-versions/)]
    * Pillow or Pillow-SIMD. Plain Pillow can be installed from pip (`pip3 install Pillow`). To install Pillow-SIMD follow the [instruction](https://pillow.readthedocs.io/en/stable/installation.html#building-from-source)
1. Install remaining requirements
```shell
pip3 install -r ./requirements.txt
```
1. (Optional) install the venv kernel in jupyter
```shell
# while the env is active
ipython kernel install --user --name=<friendly_env_name>
```
  > In case of import errors (e.g. ImportErrors on packages included in requirements), check the kernel config (`~/.local/share/jupyter/kernels/<venv_name>/kernel.json`) and make sure that the path to `python3` points to the virtual environment, rather than the systemwide interpreter

1. (Optional) Set up a PyCharm project
    1. Create a project from existing sources and select the entire repo as sources root
    1. Select the virtualenv interpreter as project interpreter
    1. Fix the `cannot find module hirise` issue
        1. Go to `Settings -> Project -> Interpreter`
        2. Click on the cog next to the list of interpreters and choose `Show All`
        3. Select the project's interpreter and click on the icon depicting a tree of folders
        4. Add the parent directory of hirise to interpreter's paths
        * this is a not-so-temporary issue with project structure which should be fixed in the future
    1. (Optional) Add other hirise repositories to the project
        * None of the top level folders should be marked as sources root
    1. Create the test suite run configuration (and run it to make sure everything works)
        1. Use `unittest` configuration
        1. Select `hirise/tests` as the module to run
        1. Set the parent dir of hirise as the 'Working directory'
    1. Run the test suite - all tests should pass successfully

# Structure:
```
* evaluation/ - Insertion/Deletion game implementation
* experiment/ - some experiment-related utilities
* occlusions/ - implementaation of the first HiRISE approach (mostly abandoned)
    * common
        * gridgen.py - occlusion selector (also used by Voronoi)
        * maskgen.py - mask generator
    * specific to iterative appraoch
        * bandgen.py/bands.py - saliency map segmentation logic for iterative approach
        * mask_budget.py - mask budgeting logic variants for iterative approach
        * distributions_*.py - implementations of saliency distribution transformation
* voronoi/ - second approach based on image segmentation using voronoi diagrams (VRISE code)
* structures.py - mask aggregators
```
The structure of this repository could use some improvement, yes.


# Entry points:
* `explainer.RISE` - ready to use implementation of RISE
* `explanations.RISE` - original RISE implementation by Vitali Petsiuk
* `voronoi.maskgen.VoronoiMaskGen` integrates all of the Voronoi components

