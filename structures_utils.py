from typing import Union
import numpy as np
import torch

def hash_tensor(tensor: Union[np.ndarray, torch.Tensor]) -> int:
    """
    Calculates a hash of the entirety of `tensor`'s data.
    Expect poor performance for large tensors.
    """
    if isinstance(tensor, torch.Tensor):
        tensor = tensor.cpu().numpy()
    return hash(tensor.data.tobytes())