from builtins import ValueError

import torch
import math
import numpy as np
import scipy.stats
from joblib import Parallel, delayed
from tqdm.autonotebook import tqdm

from hirise_nbutils.imagenet_labels import labels as imgnet_classes


def top_k_classes(model, img, k):
    """
    Returns top K classes detected on `img` by `model` as (top_confidences, top_class_idx) pair
    Sorted in descending order 
    """
    return map(lambda x: x.cpu().numpy().squeeze(), torch.topk(model(img), k))


def show_top_k_classes(top_class_scores, top_class_idx):
    """
    Prints out the information about top K classes in received data
    The ndarrays are assumed to be sorted in descending order
    :param top_class_scores:
    :param top_class_idx:
    :return:
    """
    for i in range(len(top_class_scores)):
        template = "{i}: class {idx:3} ({score:5.4f}) - {name} "
        print(template.format(i=i+1, 
                              name=imgnet_classes[top_class_idx[i]], 
                              idx=top_class_idx[i], 
                              score=top_class_scores[i]))

def sequential_confusion_triu(maps, metric, op_dims, k=1):
    """
    Calculates distances between all unique pairs of `maps`, using the given `metric`.
    The result is an upper triangle of a confusion matrix, raveled to a 1-D vector.
    `np.triu_indices` can be used to rebuild a confusion matrix from the returned triu.

    :param maps: N-dim tensor of 2D maps. Must be at least 3-dimensional
    :param metric: callable taking two maps and returning a single scalar value.
    :param op_dims: int: dimensionality of a single input element.
    :param k: value of `k` parameter passed to np.triu_indices
    :return: 1-D np.ndarray containing the distance values. A raveled upper triangle of a confusion matrix.
    """
    base_dim = (op_dims + 1)

    n_maps = maps.shape[-base_dim]
    triu_indices = list(zip(*np.triu_indices(n_maps, k=k)))

    result_tensor = np.zeros((*maps.shape[:-base_dim], len(triu_indices)))
    for ndidx in list(np.ndindex(maps.shape[:-base_dim])):
        result_tensor[ndidx] = np.array([metric(maps[(*ndidx, y)],
                                                maps[(*ndidx, x)])
                                       for y, x
                                       in triu_indices])

    return result_tensor

def parallel_confusion_triu(maps, metric, op_dims, n_jobs=-2, backend="threading", k:int=1):
    """
    Calculates distances between all unique pairs of `maps`, using the given `metric`.
    The result is an upper triangle of a confusion matrix, raveled to a 1-D vector.
    `np.triu_indices` can be used to rebuild a confusion matrix from the returned triu.

    The confusion matrix is calculated for map pairs along the `-(op_dim+1)`-th dimension.
    The shape of the output is equal to `[*shape[0:-(op_dims+1)], sum(range(1, original_shape[-(op_dims+1)] + 1)]`
    For example, for a tensor of shape [3, 5, 7, 10, 10] and op_dims == 2, the result's shape will be equal to [3, 5, 21]

    :param maps: N-dim tensor of 2D maps. Must be at least 3-dimensional
    :param metric: callable taking two maps and returning a single scalar value.
    :param op_dims: int: dimensionality of a single input element.
    :param k: value of `k` parameter passed to np.triu_indices
    :return: (N-op_dims) np.ndarray containing the distance values. The last dimension will be a raveled upper triangle of a confusion matrix.
    """
    def calc_metric(ndidx):
        result = np.array([metric(maps[(*ndidx, y)],
                                  maps[(*ndidx, x)])
                         for y, x
                         in triu_indices])

        return ndidx, result

    base_dim = (op_dims + 1)

    n_maps = maps.shape[-base_dim]
    triu_indices = list(zip(*np.triu_indices(n_maps, k=k)))

    result_tensor = np.zeros((*maps.shape[:-base_dim], len(triu_indices)))
    results = Parallel(n_jobs=n_jobs, backend=backend)([delayed(calc_metric)(ndidx)
                                                        for ndidx
                                                        in list(np.ndindex(maps.shape[:-base_dim]))])
    for ndidx, result in results:
        result_tensor[ndidx] = result

    return result_tensor

def confusion_triu(maps, metric, op_dims, k=1):
    """
    Wrapper over `parallel_confusion_triu` function from the same package.
    Check out its docs for more details.

    :param maps:
    :param metric:
    :param op_dims:
    :return:
    """
    return parallel_confusion_triu(maps, metric, op_dims, k=k)

def parallel_confusion_matrix(a, b, metric, op_dims, n_jobs=-2, backend="threading"):
    """
    Calculates distances between all pairs of elements `a` and `b`, using given `metric`.
    The result is a full confusion matrix, raveled to a 1-D vector.
    `np.triu_indices` can be used to rebuild a confusion matrix from the returned triu.

    The confusion matrix is calculated for map pairs along the `-(op_dim+1)`-th dimension.
    The shape of the output is equal to `[*shape[0:-(op_dims+1)], sum(range(1, original_shape[-(op_dims+1)] + 1)]`
    For example, for a tensor of shape [3, 5, 7, 10, 10] and op_dims == 2, the result's shape will be equal to [3, 5, 21]

    :param maps: N-dim tensor of 2D maps. Must be at least 3-dimensional
    :param metric: callable taking two maps and returning a single scalar value.
    :param op_dims: int: dimensionality of a single input element.
    :return: (N-op_dims) np.ndarray containing the distance values. The last dimension will be a raveled upper triangle of a confusion matrix.
    """

    def calc_metric(ndidx):
        result = np.array([metric(a[(*ndidx, y)],
                                  b[(*ndidx, x)])
                         for y, x
                         in np.ndindex(*operation_shape)])
        return ndidx, result

    base_dim = (op_dims + 1)

    hypershapes = [x.shape[:-base_dim] for x in (a, b)]
    if hypershapes[0] != hypershapes[1]:
        raise ValueError(f"Hypershapes of a ({hypershapes[0]}) and b ({hypershapes[1]}) must match")

    operation_shape = [x.shape[-base_dim] for x in (a,b)]
    result_tensor = np.zeros((*hypershapes[0], operation_shape[0] * operation_shape[1]))
    results = Parallel(n_jobs=n_jobs, backend=backend)([delayed(calc_metric)(ndidx)
                                                        for ndidx
                                                        in list(np.ndindex(*hypershapes[0]))])
    for ndidx, result in results:
        result_tensor[ndidx] = result

    return result_tensor

def confusion_matrix(a, b, metric, op_dims):
    return parallel_confusion_matrix(a, b, metric, op_dims)

def spearman_upper_matrix(maps, op_dims=2):
    """
    This function returns Spearman Rank coefficients between each pair of maps in `saliency_maps`
    The result is a raveled upper half of a matrix, in form of a vector.
    `op_dims` determines the dimensionality of a single comparison element. E.g. 0 for scalars, 1 for vectors, 2 for matrices. It's required for proper iteration over n-dim map tensors

    """
    return sequential_confusion_triu(maps, lambda m1, m2: scipy.stats.spearmanr(m1, m2, axis=None), op_dims=op_dims)

def triu_to_matrix(upper_triangular_vector, k=1):
    """
    Creates a rectangular matrix from given `upper_triangular_vector`, which is interpreted as a raveled upper triangle, excluding the diagonal
    """
    n = int((1+math.sqrt(1+(8*len(upper_triangular_vector))))/2) # this is series sum formula solved for N
    upper_triangle_indices = np.triu_indices(n, k)

    distance_matrix = np.zeros((n,n))
    for idx, coords in enumerate(zip(*upper_triangle_indices)):
        y, x = coords
        distance_matrix[y][x] = distance_matrix[x][y] = upper_triangular_vector[idx]
        
    return distance_matrix


def normalize(ndarray:np.ndarray, axes=None, new_range:tuple=(0, 1), original_range:tuple=None):
    """
    Normalize ndarray to given new range of values.
    e.g. to separately normalize each 2D image stored in the last two dims of a n-dim tensor, use axes=(-2. -1)
    By default, the min/max values in input will be mapped to 0 and 1, and other values will be interpolated accordingly.
    `new_range` determines the range of output values
    `original_range` prevents inference of min/max values from input array and is especially useful when converting output of rand to a different range.
    In other words, if `original_range` is not specified, then extremes of `ndarray` will be mapped to extremes specified in new_range

    :param axes: limits the scope of min/max operation. E.g. when (-1,-2) is provided for 3D array, each 2D slice along the first axis will be normalized independently
    :param new_range: target range of values
    :param original_range: specifies the range of values `ndarray` was sampled from. When None, this range will be inferred from ndarray.
    """
    new_min, new_max = new_range
    new_amplitude = new_max - new_min

    if original_range is not None:
        amin, amax = original_range
    else:
        amin, amax = map(lambda func: func(ndarray, axis=axes, keepdims=True), (np.amin, np.amax))
    old_amplitude = amax - amin

    return (((ndarray - amin) / old_amplitude) * new_amplitude) + new_min

def torch_normalize(tensor:torch.Tensor, new_range:tuple=(0,1), original_range:tuple=None):
    """
    Normalize tensor to given new range of values.
    By default, the min/max values in input will be mapped to 0 and 1, and other values will be interpolated accordingly.
    `new_range` determines the range of output values
    `original_range` prevents inference of min/max values from input array and is especially useful when converting output of rand to a different range.
    In other words, if `original_range` is not specified, then extremes of `ndarray` will be mapped to extremes specified in new_range

    :param new_range: target range of values
    :param original_range: specifies the range of values `ndarray` was sampled from. When None, this range will be inferred from ndarray.
   """
    new_min, new_max = new_range
    new_amplitude = new_max - new_min

    amin, amax = original_range or (tensor.min(), tensor.max())
    old_amplitude = amax - amin

    return (((tensor - amin) / old_amplitude) * new_amplitude) + new_min

def similarity_to_img(maps_tensor, img, metric):
    """
    Calculate similarity of each 2D element of ND `maps_tensor` to 2D `img` using `metric`
    The elements are assumed to constitute the last two axes of `maps_tensor`
    """
    if len(maps_tensor.shape) > 3:
        return np.stack([similarity_to_img(d, img, metric)
                         for d
                         in tqdm(maps_tensor, leave=False, desc=f"dim {len(maps_tensor.shape)}")])
    else:
        return np.array([metric(_map, img) for _map in maps_tensor])


def replace_initial_consecutive_zeros(monotonic_increasing_array):
    """
    Replaces a sequence of zeros at the beginning of the array with results of linear interpolation
    of zero and the first non-zero value.

    All-zero arrays are returned as-is

    :param array:
    :return:
    """
    array = monotonic_increasing_array
    if array.min() == array.max() == 0:
        return array

    first_value = array[0]
    length = len(array)
    i = 1
    while array[i] == first_value and i < length:
        i += 1

    filler_base = np.arange(i, dtype=float) if isinstance(array, np.ndarray) else torch.arange(i, dtype=torch.float)
    array[:i] = (filler_base / i) * array[i]
    return array


