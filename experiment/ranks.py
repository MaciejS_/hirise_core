import torch

def average_of_ranks(mask_scores: torch.Tensor) -> torch.Tensor:
    """
    Calculates an average rank of each class, as mean of ranks attained on evaluations of all masks

    :param mask_scores: 2D float torch.Tensor of shape (n_masks, n_classes), containing confidence scores
    :return: 1D float torch.Tensor of shape (n_classes), contaning average rank for each class
    """
    mask_ranks = torch.argsort(mask_scores, dim=-1).to(torch.float)
    return torch.mean(mask_ranks, dim=0)


def from_avg_score(mask_scores: torch.Tensor) -> torch.Tensor:
    """
    Calculates the rank of each class, based on averages scores

    :param mask_scores: 2D float torch.Tensor of shape (n_masks, n_classes), containing confidence scores
    :return: 1D int torch.Tensor of shape (n_classes), contaning class rank based on averaged scores
    """
    return torch.argsort(mask_scores.mean(dim=0)).to(torch.float)
