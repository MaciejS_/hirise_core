import numpy as np
import torch

# mask budgets
def s_proportional(img, total_N, s_steps, p1, bound_gen, bound_edges) -> np.ndarray:
    return np.ceil(total_N * np.array(s_steps) / sum(s_steps)).astype(int)

def equal_split(img, total_N, s_steps, p1, bound_gen, bound_edges) -> np.ndarray:
    n_per_iteration = float(total_N) / len(s_steps)
    shape = s_steps.shape if isinstance(s_steps, (np.ndarray, torch.Tensor)) else (len(s_steps), )
    return np.full(shape, n_per_iteration, dtype=int)