import math

import numpy as np
from typing import Union


def top_quantile(heatmap: np.ndarray, q: Union[float, list, tuple]):
    """
    Returns a modified input map, with all cells below given quantile zeroed out.
    `q` may be either float or a collection of floats

    >>> top_quantile(np.arange(10), 0.85)
    [0 0 0 0 0 0 0 0 0 9]

    >>> top_quantile(np.arange(10), (0.15, 0.85))
    [[0 0 0 0 0 0 0 0 0 9]
     [0 1 2 3 4 5 6 7 8 9]]
    """
    if isinstance(q, float):
        q = [q]

    if not all(0 <= quantile <= 1 for quantile in q):
        raise ValueError("All quantiles must be in (0; 1) range, got %s" % q)

    sorted_indices = np.argsort(heatmap, axis=None, kind='stable')
    N = len(sorted_indices)

    result = np.tile(heatmap.flatten(), (len(q), 1))

    for idx, threshold in enumerate(q):
        to_remove = sorted_indices[:math.floor(N * threshold)]
        result[idx, to_remove] = 0
    return result.reshape(len(q), *heatmap.shape).squeeze()

def top_energy(heatmap: np.ndarray, threshold:Union[float, list, tuple]):
    """
    Leaves only the pixels which contribute to the top `threshold` percent of total image energy

    """
    if isinstance(threshold, float):
        threshold = [threshold]

    if not all(0 <= thr <= 1 for thr in threshold):
        raise ValueError("Energy thresholds must be in (0; 1) range, got %s" % threshold)

    sorted_indices = np.argsort(heatmap, axis=None, kind='stable')
    sorted_values = heatmap.flatten()[sorted_indices]
    cumulated_energy = np.cumsum(sorted_values)
    cumulated_energy = cumulated_energy / cumulated_energy[-1]

    result = np.tile(heatmap.flatten(), (len(threshold), 1))

    for idx, thr in enumerate(threshold):
        to_remove = sorted_indices[cumulated_energy <= (1-thr)]
        result[idx, to_remove] = 0
    return result.reshape(len(threshold), *heatmap.shape).squeeze()