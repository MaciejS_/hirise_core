import numpy as np
import torch

from PIL import Image
from matplotlib import pyplot as plt
from torch.utils.data.sampler import Sampler
from torchvision import transforms


# Dummy class to store arguments
class Dummy():
    pass


# Function that opens image from disk, normalizes it and converts to tensor
read_tensor = transforms.Compose([
    lambda img_file: Image.open(img_file),
    lambda img: img.convert(mode="RGB"),
    transforms.Resize((224, 224)),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                          std=[0.229, 0.224, 0.225]),
    lambda x: torch.unsqueeze(x, 0)
])


# Plots image from tensor
def tensor_imshow(inp, title=None, **kwargs):
    """Imshow for Tensor."""
    inp = make_displayable(inp)
    plt.imshow(inp, **kwargs)
    if title is not None:
        plt.title(title)


def make_displayable(inp):
    inp = inp.numpy() if isinstance(inp, torch.Tensor) else inp

    inp = inp.squeeze().transpose((1, 2, 0))
    if len(inp.shape) != 3:
        raise ValueError("inp should be 3-dimensional, got shape %s instead" % inp.shape)

    # Mean and std for ImageNet
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    inp = np.clip(inp, 0, 1)
    return inp


# Image preprocessing function
preprocess = transforms.Compose([
                transforms.Resize((224, 224)),
                transforms.ToTensor(),
                # Normalization for ImageNet
                transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225]),
            ])


# Sampler for pytorch loader. Given range r loader will only
# return dataset[r] instead of whole dataset.
class RangeSampler(Sampler):
    def __init__(self, r):
        self.r = r

    def __iter__(self):
        return iter(self.r)

    def __len__(self):
        return len(self.r)
