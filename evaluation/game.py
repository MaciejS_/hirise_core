from typing import *

import torch
import numpy as np
from torch import nn

from hirise.device import device_from_torch_model
from hirise.evaluation.utils import gkern


class AlterationGame:

    def __init__(self,
                 model: nn.Module,
                 initial_state: torch.Tensor,
                 final_state: torch.Tensor,
                 explanation: torch.Tensor,
                 step_size: int,
                 observed_classes: Union[int, np.ndarray] = None) -> None:
        self.device = device_from_torch_model(model)
        self.model = model

        self.step_size = step_size
        self.img_px_count = np.prod(initial_state.shape[-2:])
        self.n_steps = (self.img_px_count + self.step_size - 1) // self.step_size

        self.salient_order = self._calculate_saliency_order(explanation)
        img = self._to_tensor(initial_state)
        self.observed_classes = self._interpret_observed_classes_value(img, observed_classes)

        # Game state. This should be initialized by concrete game implementations.
        self.current_step = 0
        self._initial_img = img
        self._current_img = self._initial_img.clone().detach()
        self._final_img = final_state
        self.current_predictions = None

        self.precision = next(model.parameters()).dtype

        self._initialize_scores_list()

    def _initialize_scores_list(self):
        self.scores_list = [self._calculate_current_score()]

    def _interpret_observed_classes_value(self, img, observed_classes):
        if observed_classes is None:
            observed_classes = -1

        if isinstance(observed_classes, int) and observed_classes < 0:
            pre_game_scores, top_class_idxs = torch.sort(self.model(img), descending=True)
            return top_class_idxs.cpu().numpy().squeeze()[:-observed_classes]
        elif observed_classes is Ellipsis:
            return observed_classes
        else:
            return np.array(observed_classes)

    def _calculate_saliency_order(self, explanation):
        explanation = self._to_tensor(explanation)
        return torch.argsort(explanation.reshape(self.img_px_count), descending=True)

    @property
    def current_state(self) -> torch.Tensor:
        """
        Returns the current state of image used to instantiate this instance of AlterationGame
        :return: torch.Tensor
        """
        return self._current_img.cpu()

    @property
    def final_state(self) -> torch.Tensor:
        """
        Returns the target state of the image used to instantiate this instance of AlterationGame
        :return: torch.Tensor
        """
        return self._final_img.cpu()

    @property
    def initial_state(self) -> torch.Tensor:
        """
        Returns a copy of the image used to instantiate this instance of AlterationGame
        :return: torch.Tensor
        """
        return self._initial_img.cpu()

    @property
    def scores(self) -> np.ndarray:
        return np.array(self.scores_list)

    def step(self) -> float:
        """
        Performs the next step of alteration game.

        :return: float, confidence scores in for observed classes
        """

        if not self.is_over():
            self.__insert_from_final()
            self.current_step += 1
            current_scores = self._calculate_current_score()
            self.scores_list.append(current_scores)
        else:
            raise StopIteration

        return current_scores

    def reset(self) -> None:
        """
        Resets the game to initial state
        :return: None
        """
        self.current_step = 0
        self._current_img = self._initial_img.clone().detach()

        self._initialize_scores_list()

    def is_over(self):
        return self.current_step >= self.n_steps

    def _calculate_current_score(self):
        self.current_predictions = self.model(self._current_img.to(self.device, self.precision)).float().squeeze()
        return self.current_predictions.cpu().numpy()[self.observed_classes]

    def __insert_from_final(self):
        """
        Copies the next batch of most salient pixels from the final image to the current image
        :return:
        """
        i = self.current_step
        n = self.step_size
        coords = self.salient_order[i * n: (i + 1) * n]

        current_img, final_img = map(lambda img: img.cpu().numpy().reshape(1, 3, self.img_px_count),
                                     (self._current_img, self._final_img))
        current_img[0, :, coords] = final_img[0, :, coords]  # this is an in-place operation on underlying numpy arrays

    def _to_tensor(self, array) -> torch.Tensor:
        """
        Converts given array to torch.Tensor. If torch.Tensor is passed, then it's a no-op
        :param array: np.ndarray or torch.Tensor
        :return:
        """
        return (torch.from_numpy(array) if isinstance(array, np.ndarray) else array.clone().detach()).to(self.device)


class DeletionGame(AlterationGame):

    def __init__(self,
                 model: nn.Module,
                 image: torch.Tensor,
                 explanation: torch.Tensor,
                 step_size: int,
                 substrate_fn: callable,
                 observed_classes: int=None,
                 reverse: bool=False) -> None:
        """

        :param model: torch.nn.Module used to evaluate images
        :param image: the image to evaluate the saliency map against
        :param explanation: the evaluated saliency map
        :param step_size: int, number of pixels to alter in each step of the game
        :param substrate_fn: a callable such that: substrate_fn(initial_img) -> final_img
        :param observed_classes:
        :param reverse: if True, the image will be drawn onto an empty canvas, starting with the most salient areas
        """
        device = device_from_torch_model(model)
        initial_image = torch.from_numpy(image).to(device=device) if isinstance(image, np.ndarray) else image.clone()
        final_image = torch.as_tensor(substrate_fn(initial_image), device=device)

        super().__init__(model,
                         initial_state=initial_image if not reverse else final_image,
                         final_state=final_image if not reverse else initial_image,
                         explanation=explanation,
                         step_size=step_size,
                         observed_classes=observed_classes)

        self.reversed = reverse

    @staticmethod
    def default_substrate() -> callable:
        """
        Substrate as used in orignal RISE article
        :return: numpy.zeros_like
        """
        return np.zeros_like


class InsertionGame(AlterationGame):

    def __init__(self,
                 model: nn.Module,
                 image: torch.Tensor,
                 explanation: torch.Tensor,
                 step_size: int,
                 substrate_fn: callable,
                 observed_classes: int=None,
                 reverse:bool=False) -> None:
        """
        Insertion game, on each step, inserts `step_size` most-salient pixels back into a distorted version of the original image
        After each step, the confidence score for the original class is measured.
        If `reverse` is true, the initial image is used as the final state and the would-be final state is used as the initial

        :param model: nn.Module,
        :param image: torch.Tensor: normalized image tensor
        :param num_steps: int, number of pixels affected in each step of the game
        """
        device = device_from_torch_model(model)
        final_image = torch.from_numpy(image).to(device) if isinstance(image, np.ndarray) else image.clone()
        initial_image = torch.as_tensor(substrate_fn(final_image), device=device)

        super().__init__(model,
                         initial_state=initial_image if not reverse else final_image,
                         final_state=final_image if not reverse else initial_image,
                         explanation=explanation,
                         step_size=step_size,
                         observed_classes=observed_classes)

        self.reversed = reverse

    @staticmethod
    def default_substrate():
        """
        The substrate function used in the original RISE article
        :return:
        """
        return default_blur_kernel()


class BatchedGame(AlterationGame):
    """
    Alternative implementation of the game, which batches subsequent game steps to process them in parallel on GPU.
    :return:
    """

    def __init__(self,
                 model,
                 initial_state: torch.Tensor,
                 final_state: torch.Tensor,
                 explanation,
                 batch_size,
                 step_size,
                 observed_classes):
        super().__init__(model,
                         initial_state=initial_state,
                         final_state=final_state,
                         explanation=explanation,
                         step_size=step_size,
                         observed_classes=observed_classes)
        self.batch_size = batch_size

    def step(self) -> torch.Tensor:
        """
        Performs the next step of alteration game.

        :return: float, confidence scores in for observed classes
        """

        current_batch_idx = int(self.current_step / self.batch_size)
        next_batch_size = max(0, min(self.batch_size, self.n_steps - (current_batch_idx * self.batch_size)))
        if not self.is_over():
            self.batch = self._prepare_next_batch(next_batch_size)
            self.current_step += self.batch_size
            current_scores = self._calculate_current_score()
            self.scores_list.append(current_scores)
            self._current_img = self.batch[-1].unsqueeze(0)
        else:
            raise StopIteration

        return current_scores

    def is_over(self):
        return self.current_step >= self.n_steps

    @AlterationGame.scores.getter
    def scores(self) -> np.ndarray:
        return torch.cat(self.scores_list).cpu().numpy()

    def _initialize_scores_list(self):
        self.scores_list = [torch.Tensor([AlterationGame._calculate_current_score(self)]).float().to(self.device)]

    def _calculate_current_score(self):
        self.current_predictions = self.model(self.batch.to(self.precision)).float()
        return self.current_predictions[:, self.observed_classes].float()

    def _prepare_next_batch(self, batch_size):
        """
        Creates a set of progressively more occluded versions of the input image.
        Occlusions
        :return:
        """
        i = self.current_step
        n = self.step_size

        current_img_flat = self._current_img.view([3, self.img_px_count])
        final_img_flat = self._final_img.view([3, self.img_px_count])

        next_batch = current_img_flat.repeat(batch_size, 1, 1)

        for idx in range(batch_size):
            coords = self.salient_order[i * n: (i + idx + 1) * n]
            next_batch[idx, :, coords] = final_img_flat[:, coords]  # this is an in-place operation on underlying numpy arrays

        return next_batch.reshape([batch_size, 3, *self._current_img.shape[-2:]])


class BatchedDeletionGame(BatchedGame):

    def __init__(self,
                 model: nn.Module,
                 image: torch.Tensor,
                 explanation: torch.Tensor,
                 batch_size: int,
                 step_size: int,
                 substrate_fn: callable,
                 observed_classes: int=None,
                 reverse:bool=False) -> None:
        """
        Deletion game, on each step, removes `step_size` most-salient pixels from the image.
        After each step, the confidence score for the original class is measured.
        If `reverse` is true, the initial image is used as the final state and the would-be final state is used as the initial
        """
        device = device_from_torch_model(model)
        initial_image = torch.from_numpy(image).to(device=device) if isinstance(image, np.ndarray) else image.clone()
        final_image = torch.as_tensor(substrate_fn(initial_image), device=device)

        super().__init__(model,
                         initial_state=initial_image if not reverse else final_image,
                         final_state=final_image if not reverse else initial_image,
                         explanation=explanation,
                         step_size=step_size,
                         batch_size=batch_size,
                         observed_classes=observed_classes)

        self.reversed = reverse

    @staticmethod
    def default_substrate() -> callable:
        """
        Substrate as used in orignal RISE article
        :return: numpy.zeros_like
        """
        return torch.zeros_like


class BatchedInsertionGame(BatchedGame):
    def __init__(self,
                 model: nn.Module,
                 image: torch.Tensor,
                 explanation: torch.Tensor,
                 batch_size: int,
                 step_size: int,
                 substrate_fn: callable,
                 observed_classes: int = None,
                 reverse: bool = None) -> None:
        """
        Insertion game, on each step, inserts `step_size` most-salient pixels back into a distorted version of the original image
        After each step, the confidence score for the original class is measured.
        If `reverse` is true, the initial image is used as the final state and the would-be final state is used as the initial
        """
        device = device_from_torch_model(model)
        final_image = torch.from_numpy(image).to(device) if isinstance(image, np.ndarray) else image.clone()
        initial_image = torch.as_tensor(substrate_fn(final_image), device=device)

        super().__init__(model,
                         initial_state=initial_image if not reverse else final_image,
                         final_state=final_image if not reverse else initial_image,
                         explanation=explanation,
                         step_size=step_size,
                         batch_size=batch_size,
                         observed_classes=observed_classes)

        self.reversed = reverse

    def get_current_state(self):
        raise NotImplementedError

    @staticmethod
    def default_substrate():
        """
        The substrate function used in the original RISE article
        :return:
        """
        return default_blur_kernel()


#region kernels
def default_deletion_kernel() -> Callable[[torch.Tensor], torch.Tensor]:
    return lambda t: torch.zeros(t.shape)


def custom_valued_deletion_kernel(channel_values: Union[List[float], np.ndarray, torch.Tensor]) -> Callable[[torch.Tensor], torch.Tensor]:
    """
    Returns callable which takes a tensor representing an image and returns a tensor of exact same shape filled with `channel_values`
    The number of `channel_values` must match the first dimension of the provided tensor.

    :param channel_values: each value in this tensor will be used to fill
    """
    channels_tensor = torch.as_tensor(channel_values)
    return lambda t: torch.ones_like(t) * channels_tensor.reshape(t.shape[0], *(1,)*(len(t.shape)-1))


def imgnet_neutral_deletion_kernel():
    return custom_valued_deletion_kernel(channel_values=[0.485, 0.456, 0.406])


def default_blur_kernel():
    klen = 11
    kern = gkern(klen, nsig=5)

    def blur(tensor: Union[np.ndarray, torch.Tensor]):
        original_shape = tensor.shape

        tensor = torch.as_tensor(tensor, dtype=torch.float)
        if tensor.ndim == 2:
            tensor = tensor.reshape(1, 1, *tensor.shape)
        elif tensor.ndim == 3:
            tensor = tensor.reshape(1, *tensor.shape)

        return nn.functional.conv2d(tensor, kern.to(tensor.device), padding=klen // 2).reshape(original_shape)

    # Function that blurs input image
    return blur

#endregion kernels