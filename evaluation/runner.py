import numpy as np
import torch
from matplotlib import pyplot as plt

from hirise.evaluation.game import AlterationGame, BatchedGame
from hirise.evaluation.visualization import Visualizer
from tqdm.autonotebook import tqdm


class GameRunner:

    def __init__(self):
        pass

    def run(self, game:AlterationGame) -> np.ndarray:
        """
        Runs all steps of the game and returns the array fo scores
        :return:
        """
        if game.is_over():
            game.reset()

        scores = list(game.scores)
        while not game.is_over():
            scores.append(game.step())

        return np.array(scores)

class TqdmRunner(GameRunner):

    def __init__(self):
        super().__init__()

    def run(self, game:AlterationGame, leave_bar=False):

        if game.is_over():
            game.reset()

        scores = list(game.scores)

        with tqdm(total=game.n_steps, initial=game.current_step, leave=leave_bar) as progress_bar:
            while not game.is_over():
                scores.append(game.step())
                progress_bar.update(1)

        return np.array(scores)

class BatchedGameRunner(GameRunner):
    def __init__(self):
        super().__init__()

    def run(self, game:BatchedGame, leave_bar=False, use_tqdm=False):

        if game.is_over():
            game.reset()

        with tqdm(total=game.n_steps, initial=game.current_step, leave=leave_bar, disable=not use_tqdm) as progress_bar:
            while not game.is_over():
                step_scores = game.step()
                progress_bar.update(len(step_scores))

        return game.scores


class VisualizingRunner(GameRunner):

    def __init__(self, visualizer: Visualizer):
        super().__init__()
        self.visualizer = visualizer

    def run(self, game, show_every_nth_step=1) -> np.ndarray:
        n = show_every_nth_step
        counter = 0

        if game.is_over():
            game.reset()

        scores = list(game.scores)
        while not game.is_over():
            score = game.step()
            scores.append(score)

            if counter % n == 0:
                figure = plt.figure(figsize=(20, 5))
                self.visualizer.visualize(game, figure)
                plt.show(figure)
                plt.close(figure)
            counter += 1
        return np.array(scores)