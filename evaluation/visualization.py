import os

import matplotlib.figure
import matplotlib.gridspec
import numpy as np
import torch
from hirise.device import cuda_otherwise_cpu
from hirise.evaluation.game import AlterationGame
from hirise.explanations import RISE
from hirise.utils import make_displayable
from matplotlib import gridspec


class Visualizer:
    def visualize(self,
                  game: AlterationGame,
                  fig: matplotlib.figure.Figure,
                  grid: matplotlib.gridspec.GridSpec=None) -> matplotlib.figure.Figure:
        """

        :param game:
        :param fig: axes to draw on
        :return:
        """
        raise NotImplementedError

    def _create_one_element_grid(self, fig):
        return gridspec.GridSpec(1, 1, figure=fig)

    @property
    def grid_size(self):
        raise NotImplementedError


class VisualizerDecorator(Visualizer):

    def __init__(self, visualizer: Visualizer) -> None:
        super().__init__()
        self.visualizer = visualizer

    def visualize(self, game: AlterationGame, fig: matplotlib.figure.Figure,
                  grid: matplotlib.gridspec.GridSpec = None) -> matplotlib.figure.Figure:
        raise NotImplementedError


class SaveToDisk(VisualizerDecorator):

    def __init__(self, visualizer, savepath, base_name) -> None:
        """
        SaveToDisk writes the figure returned by `visualizer` to a file on local disk
        :param visualizer:
        :param savepath:
        :param base_name:
        :param display: if False. the
        """
        super().__init__(visualizer)
        self.savepath = savepath
        self.base_name = base_name
        self.counter = 0

    def visualize(self,
                  game: AlterationGame,
                  fig: matplotlib.figure.Figure,
                  grid: matplotlib.gridspec.GridSpec=None) -> matplotlib.figure.Figure:
        """
        Passes the args to wrapped Visualizer and stores the resulting figure on disk
        :param game:
        :param fig:
        :param grid:
        :return:
        """
        filename = "{base_name}{i:03d}.png".format(base_name=self.base_name, i=self.counter)
        figure = self.visualizer.visualize(game, fig, grid)
        figure.savefig(fname = os.path.join(self.savepath, filename))
        return figure


class CompositeVisualizer(Visualizer):

    def __init__(self):
        super().__init__()
        self.visualizers = []

    def of_visualizers(self, visualizers: dict, grid_spec=None):
        raise NotImplementedError

    def add_visualizer(self, visualizer: Visualizer) -> None:
        self.visualizers.append(visualizer)

    def visualize(self,
                  game: AlterationGame,
                  fig: matplotlib.figure.Figure=None,
                  grid: matplotlib.gridspec.GridSpec=None) -> matplotlib.figure.Figure:
        """
        Executes the visualize method of all embedded visualizers.

        :param game: AlterationGame to visualize
        :param fig: figure onto which the visualization will be drawn. If None, a new figure will be created
        :param grid: grid to use for embedded visualizers. if None, a new grid will be created, with one cell per visualizer
        :return: final state of the figure
        """
        grid_sizes = [v.grid_size for v in self.visualizers]

        fig = fig or matplotlib.figure.Figure(constrained_layout=True)
        grid = grid or gridspec.GridSpec(1, sum(x for y, x in grid_sizes), figure=fig)

        for idx, v in enumerate(self.visualizers):
            grid_start_x = sum(x for y, x in grid_sizes[:idx])
            grid_end_x = grid_start_x + grid_sizes[idx][1]
            v.visualize(game, fig, grid[grid_start_x:grid_end_x])

        return fig

class CurrentStateVisualizer(Visualizer):
    """
    The basic evaluation visualization as implemented in the github:eclique/RISE repo
    """

    def __init__(self, title, y_axis_label, explained_class_name) -> None:
        super().__init__()
        self.title = title
        self.y_label = y_axis_label
        self.explained_class_name = explained_class_name

    @property
    def grid_size(self):
        return (1,1)

    def visualize(self, game, fig, grid:gridspec.GridSpec=None):
        # alias commonly used variables
        i = game.current_step - 1
        n = game.n_steps

        grid = grid or self._create_one_element_grid(fig)
        # plot image with insertions/deletions
        state_img_ax = fig.add_subplot(grid)
        state_img_ax.imshow(make_displayable(game.current_state))
        state_img_ax.set_title('{label} {progress:.1f}%, P={score:.4f}'.format(label=self.y_label,
                                                                               progress=100 * i / n,
                                                                               score=game.scores[i]))
        state_img_ax.axis('off')


class AreaUnderCurveVisualizer(Visualizer):

    def __init__(self, title, y_axis_label, explained_class_name) -> None:
        super().__init__()
        self.title = title
        self.y_label = y_axis_label
        self.explained_class_name = explained_class_name

    @property
    def grid_size(self):
        return (1,1)

    def visualize(self,
                  game: AlterationGame,
                  fig: matplotlib.figure.Figure,
                  grid: matplotlib.gridspec.GridSpec = None) -> matplotlib.figure.Figure:
        grid = grid or self._create_one_element_grid(fig)

        # alias commonly used variables
        i = game.current_step
        n = game.n_steps

        # plot AUC
        auc_ax = fig.add_subplot(grid)
        auc_ax.plot(np.arange(i + 1) / n, game.scores[:i + 1])
        auc_ax.set_xlim(-0.1, 1.1)
        auc_ax.set_ylim(0, 1.05)
        auc_ax.fill_between(np.arange(i + 1) / n, 0, game.scores[:i + 1], alpha=0.4)
        auc_ax.set_title(self.title)
        auc_ax.set_xlabel(self.y_label)
        auc_ax.set_ylabel(self.explained_class_name)

        return fig


class TopKClassesVisualization(Visualizer):
    """
    Visualize saliency maps for Top K classes recognized at current step

    During the alteration game, classes not present in the initial image might rise to the top of the predictions list.
    This visualizer explains why does the classifier detect these unexpected object in the partially destroyed image
    """

    def __init__(self, top_k, explainer:RISE, class_names):
        super().__init__()
        self.top_k = top_k
        self.explainer = explainer
        self.class_names = class_names

        self.device = cuda_otherwise_cpu()

    @property
    def grid_size(self):
        return (1, self.top_k)

    def visualize(self,
                  game: AlterationGame,
                  fig: matplotlib.figure.Figure,
                  grid: matplotlib.gridspec.GridSpec=None) -> matplotlib.figure.Figure:

        top_scores, top_classes = torch.topk(game.current_predictions.squeeze(), self.top_k)
        saliency_scores = self.explainer.evaluate_masks(game.current_state.to(self.device))
        selected_class_scores = saliency_scores[:, top_classes.squeeze()]
        raveled_saliency_maps = selected_class_scores.T.to(self.device).matmul(
            self.explainer.masks.view(self.explainer.N, -1).to(self.device)).cpu()
        saliency_maps = raveled_saliency_maps.view(self.top_k, *game.initial_state.shape[-2:])

        grid = grid or self._create_one_element_grid(fig)
        subgrid = grid.subgridspec(1, self.top_k)

        for i in range(self.top_k):
            top_class, prob = int(top_classes[i]), float(top_scores[i])
            # plot saliency map for this class
            ax = fig.add_subplot(subgrid[i])
            ax.imshow(make_displayable(game.current_state))
            ax.imshow(saliency_maps[i], cmap='jet', alpha=0.5)
            ax.set_title('{class_name}\n (p={prob:.3f})'.format(class_name=self.class_names[top_class],
                                                                          prob=prob))
            ax.axis('off')

        return fig
