import numpy as np

def pointing_game(smaps: np.ndarray, bboxes: np.ndarray, single_map_ndim: int) -> np.ndarray:
    """
    Performs pointing game on `smaps` and bboxes.
    Bboxes are matched to smaps along the `N` dim and broadcast to all remaining leading dimensions.
    Therefore, if there's multiple maps to evaluate against a single bbox, the evaluation can be carried
    out in a single step.

    smaps: (*hypershape, N, y_dim, x_dim) tensor of saliency maps
    bboxes: (N, 2 (y.x), 2 (min.max)) tensor of bounding boxes

    returns: (*hypershape, N,) binary vector containing 1 if the max of smap at (*hS, N) lies within N-th bbox, 0 otherwise
    """

    single_map_shape = smaps.shape[-single_map_ndim:]
    hypershape = smaps.shape[:-single_map_ndim]

    # flatten each map
    raveled_smaps = smaps.reshape(*smaps.shape[:-single_map_ndim], -1)

    # calculate argmax for each map
    raveled_argmax = raveled_smaps.argmax(axis=-1).reshape(*hypershape, 1)

    # convert the single-int idx returned by argmax to 2-dim index
    maxpoint_indices = np.apply_along_axis(
        func1d=np.unravel_index,
        axis=-1,
        arr=raveled_argmax,
        shape=single_map_shape
    )

    # apply the boundary-checking logic

    # first, broadcast bounding boxes and points to a common shape, and subtract them, so that
    # the resulting array contains (ymin - y), (ymax - y), (xmin - x), (xmax - x)
    # where min/max values occupy the last dimension
    # and the yx values occupy the second-to-last dim
    # result shape: (N, 2(y, x), 2(min, max))
    _diffs = bboxes - maxpoint_indices

    # then, reduce the last dimension by multiplying the (Xmin - X) with (Xmax - X)
    # because coordinates are always positive, and (Xmin < X < Xmax), then ((Xmin - X) * (Xmax - X) < 0)
    # if X lays on a boundary, then the product will be == 0.
    # if X is smaller than Xmin, then it's also smaller than Xmax and so both differences will be > 1, producing a value > 1
    # if X is greater than Xmax, then both diffs will be < 1, producing a value > 1
    # if X is between Xmin and Xmax, then (Xmin - X < 1) but (Xmax - X > 1), producing a value < 1
    # result shape (N, 2(y,x))
    _coincides_along_axis = np.prod(_diffs, -1) < 0

    # now apply logical over Y and X info to make sure that the point lays inside the bounding box and not beside it
    # each value in the result in array contains bools for overlap of each (point, bbox pair)
    #
    # result shape: (N)
    _is_within_bbox_bounds = np.prod(_coincides_along_axis, -1)

    return _is_within_bbox_bounds