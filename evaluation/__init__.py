import numpy as np
import torch
from matplotlib import pyplot as plt
from tqdm.autonotebook import trange, tqdm

from hirise.evaluation.utils import auc
from hirise.explanations import RISE
from hirise.utils import tensor_imshow
from hirise.device import cuda_otherwise_cpu


class CausalMetric():

    def __init__(self, model, mode, step, substrate_fn, class_names, device=None):
        r"""Create deletion/insertion metric instance.

        Args:
            model (nn.Module): Black-box model being explained.
            mode (str): 'del' or 'ins'.
            step (int): number of pixels modified per one iteration.
            substrate_fn (func): a mapping from old pixels to new pixels.
            class_dictionary: a mapping of output class indices to their names
        """
        assert mode in ['del', 'ins']
        self.model = model
        self.mode = mode
        self.step = step
        self.substrate_fn = substrate_fn
        self.class_names = class_names

        self.HW = 224 * 224
        self.n_classes = 1000

        self.device = device or cuda_otherwise_cpu()

    def single_run(self, img_tensor, explanation, verbose=0, save_to=None, explainer: RISE = None):
        r"""Run metric on one image-saliency pair.

        Args:
            img_tensor (Tensor): normalized image tensor.
            explanation (np.ndarray): saliency map.
            verbose (int): in [0, 1, 2].
                0 - return list of scores.
                1 - also plot final step.
                2 - also plot every step and print 2 top classes.
            save_to (str): directory to save every step plots to.

        Return:
            scores (nd.array): Array containing scores at every step.
        """
        pred = self.model(img_tensor.to(self.device))  # initial prediction on unaltered image
        _, top_class = torch.max(pred, 1)  # best class and its confidence score
        top_class = top_class.cpu().numpy()[0]
        n_steps = (self.HW + self.step - 1) // self.step
        n_plot_cols = 4 if explainer else 2

        if self.mode == 'del':
            title = 'Deletion game'
            ylabel = 'Pixels deleted'
            start = img_tensor.clone()
            finish = self.substrate_fn(img_tensor)
        elif self.mode == 'ins':
            title = 'Insertion game'
            ylabel = 'Pixels inserted'
            start = self.substrate_fn(img_tensor)
            finish = img_tensor.clone()

        scores = np.empty(n_steps + 1)
        # Coordinates of pixels in order of decreasing saliency
        salient_order = np.flip(np.argsort(explanation.reshape(-1, self.HW), axis=1), axis=-1)

        for i in trange(n_steps + 1, desc=title, disable=(True if verbose == 0 else None)):
            pred = self.model(start.to(self.device))
            scores[i] = pred[0, top_class]
            # Render image if verbose, if it's the last step or if save is required.
            if ((verbose > 2) and (i % 10 == 0)) or (verbose == 1 and i == n_steps) or save_to:
                if verbose > 2:
                    # if verbose == 2, also print out class probabilities
                    classes_to_show = 2
                    pr, cl = torch.topk(pred, classes_to_show)
                    for j in range(classes_to_show):
                        class_idx, prob = map(float, (cl[0][j], pr[0][j]))
                        print('{}: {:.3f}'.format(self.class_names[class_idx], prob))

                # plot image with insertions/deletions
                plt.figure(figsize=(20, 5))
                plt.subplot(1, n_plot_cols, 1)
                plt.title('{} {:.1f}%, P={:.4f}'.format(ylabel, 100 * i / n_steps, scores[i]))
                plt.axis('off')
                tensor_imshow(start[0])

                # plot AUC
                plt.subplot(1, n_plot_cols, 2)
                plt.plot(np.arange(i + 1) / n_steps, scores[:i + 1])
                plt.xlim(-0.1, 1.1)
                plt.ylim(0, 1.05)
                plt.fill_between(np.arange(i + 1) / n_steps, 0, scores[:i + 1], alpha=0.4)
                plt.title(title)
                plt.xlabel(ylabel)
                plt.ylabel(self.class_names[top_class])

                # if verbose 3, also visualize saliency for these two mis-identified classes
                if verbose == 3:
                    pr, cl = torch.topk(pred, classes_to_show)
                    saliency_scores = explainer.evaluate_masks(start.to(self.device))
                    selected_class_scores = saliency_scores[:, [int(cls) for cls in cl[0]]]
                    raveled_saliency_maps = selected_class_scores.T.to(self.device).matmul(
                        explainer.masks.view(explainer.N, self.HW).to(self.device))
                    saliency_maps = raveled_saliency_maps.cpu().view(classes_to_show, 224, 224)

                    for j in range(classes_to_show):
                        class_idx, prob = int(cl[0][j]), float(pr[0][j])
                        # plot saliency map for this class
                        plt.subplot(1, n_plot_cols, 3 + j)
                        plt.title('Saliency of {class_name} {prob:.3f}'.format(class_name=self.class_names[class_idx],
                                                                               prob=prob))
                        tensor_imshow(start[0])
                        plt.imshow(saliency_maps[j], cmap='jet', alpha=0.5)

                if save_to:
                    plt.savefig(save_to + '/{:03d}.png'.format(i))
                    plt.close()
                else:
                    plt.show()
            if i < n_steps:
                coords = salient_order[:, self.step * i:self.step * (i + 1)]
                start.cpu().numpy().reshape(1, 3, self.HW)[0, :, coords] = finish.cpu().numpy().reshape(1, 3, self.HW)[
                                                                           0, :, coords]
        return scores

    def evaluate(self, img_batch, exp_batch, batch_size):
        r"""Efficiently evaluate big batch of images.

        Args:
            img_batch (Tensor): batch of images.
            exp_batch (np.ndarray): batch of explanations.
            batch_size (int): number of images for one small batch.

        Returns:
            scores (nd.array): Array containing scores at every step for every image.
        """
        n_samples = img_batch.shape[0]
        predictions = torch.FloatTensor(n_samples, self.n_classes)
        assert n_samples % batch_size == 0
        for i in tqdm(range(n_samples // batch_size), desc='Predicting labels'):
            preds = self.model(img_batch[i * batch_size:(i + 1) * batch_size].to(self.device)).cpu()
            predictions[i * batch_size:(i + 1) * batch_size] = preds
        top = np.argmax(predictions, -1)
        n_steps = (self.HW + self.step - 1) // self.step
        scores = np.empty((n_steps + 1, n_samples))
        salient_order = np.flip(np.argsort(exp_batch.reshape(-1, self.HW), axis=1), axis=-1)
        r = np.arange(n_samples).reshape(n_samples, 1)

        substrate = torch.zeros_like(img_batch)
        for j in tqdm(range(n_samples // batch_size), desc='Substrate'):
            substrate[j * batch_size:(j + 1) * batch_size] = self.substrate_fn(
                img_batch[j * batch_size:(j + 1) * batch_size])

        if self.mode == 'del':
            caption = 'Deleting  '
            start = img_batch.clone()
            finish = substrate
        elif self.mode == 'ins':
            caption = 'Inserting '
            start = substrate
            finish = img_batch.clone()

        # While not all pixels are changed
        for i in tqdm(range(n_steps + 1), desc=caption + 'pixels'):
            # Iterate over batches
            for j in range(n_samples // batch_size):
                # Compute new scores
                preds = self.model(start[j * batch_size:(j + 1) * batch_size].to(self.device))
                preds = preds.cpu().numpy()[range(batch_size), top[j * batch_size:(j + 1) * batch_size]]
                scores[i, j * batch_size:(j + 1) * batch_size] = preds
            # Change specified number of most salient pixels to substrate pixels
            coords = salient_order[:, self.step * i:self.step * (i + 1)]
            start.cpu().numpy().reshape(n_samples, 3, self.HW)[r, :, coords] = finish.cpu().numpy().reshape(n_samples,
                                                                                                            3, self.HW)[
                                                                               r, :, coords]
        print('AUC: {}'.format(auc(scores.mean(1))))
        return scores