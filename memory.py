from typing import Union

import torch
import numpy as np

import math

class VRAMBatchEstimator:

    def __init__(self):
        import pycuda.driver
        import torch.cuda

        self.mem_info_provider = pycuda.driver.mem_get_info

    def max_batch_size(self, mask_shape: tuple, model) -> int:
        """
        Estimate the maximum number of simultaneously processed masks
        :param mask_shape: tuple, shape of a single masl
        :param model: Torch callable, the model used to process the masks
        :return: int, number of masks in a single batch
        """
        previous_guess = 0
        guess = self.__max_gpu_batch_size(self.mem_overhead_for_n_masks(1, mask_shape, model))
        while previous_guess < guess:
            previous_guess = guess
            n = previous_guess // 2
            guess = self.__max_gpu_batch_size(self.mem_overhead_for_n_masks(n, mask_shape, model) / n)
        return guess

    def mem_overhead_for_n_masks(self, n: int, mask_shape: tuple, model) -> int:
        """
        Returns the amount of VRAM required to evaluate `n` masks with shape `mask_shape` by `model`.
        This value is determined through an attempt to pass n masks through the model.
        A Runtime Error might be raised if requested `n` is too large for the device

        :param n: int, number of masks
        :param mask_shape: tuple: shape of a single mask
        :param model: Torch model
        :return: int, amount of memory required to process n masks
        """
        torch.cuda.empty_cache()
        initial_free, _ = self.mem_info_provider()

        try:
            t = torch.rand(n, *mask_shape).to(self.device)
            r = model(t)
        except Exception as e:
            del t
            del r
            raise e

        free_afterwards, _ = self.mem_info_provider()
        torch.cuda.empty_cache()
        return initial_free - free_afterwards

    def __max_gpu_batch_size(self, mem_use_per_mask):
        torch.cuda.empty_cache()
        initial_free, _ = self.mem_info_provider()
        return math.floor(initial_free / mem_use_per_mask)


class RAMBatchEstimator():

    def __init__(self) -> None:
        super().__init__()
        import psutil

        self.mem_info_provider = psutil.virtual_memory()

    def max_batch_size(self, mask_shape, available_mem):
        a = np.random.rand(*mask_shape)
        mask_size = a.nbytes

        return available_mem // mask_size