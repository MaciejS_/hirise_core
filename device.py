import torch

def cuda_otherwise_cpu() -> torch.device:
    """
    Returns a handle to CUDA device if available. Otherwise, handle to CPU is returned.

    :return: torch.device
    """
    return torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

def device_from_torch_model(model) -> torch.device:
    """
    Returns the PyTorch device the `model` has been loaded to
    :param model: PyTorch model
    :return: torch.device
    """
    return next(model.parameters()).device