import typing as t
import math
from typing import List, Union

import torch
from tqdm.autonotebook import tqdm


class MaskEvaluator:

    ALL_CLASSES = Ellipsis

    def __init__(
        self,
        model: torch.nn.Sequential,
        device: torch.device = None,
        batch_size: int = 100,
        use_tqdm: bool = False,
        model_name: t.Optional[str] = None,
        use_fp16: bool = False,
    ) -> None:
        super().__init__()
        # TODO: Add automatic model cast to FP16 if `fp16 == True`?
        self.model = model
        self.batch_size = batch_size
        self.use_tqdm = use_tqdm
        self.device = device
        self.use_fp16 = use_fp16

        if self.use_fp16:
            if self.device.type != 'cuda':
                raise RuntimeError(f"FP16 can be used only with a 'cuda' device (got {self.device.type})")
            self.model = self.model.half()
        else:
            self.model = self.model.float()

    def evaluate(self, img:torch.Tensor, masks: torch.Tensor, observed_classes:Union[int, List[int]]=None) -> torch.Tensor:
        """
        Evaluates how each mask in `masks` affects the model's output on `img` and returns a (N, C) tensor of results,
        where N is the number of masks and C is the number of classes.
        Caveat - if `model` used by this evaluator doesn't use softmax, resulting scores won't sum to 1 or fit in (0, 1) range

        :param img: 2D torch.Tensor of shape (y, x) containing the evaluated image of size
        :param masks: 3D torch.Tensor of shape (N, y, x) containig masks to evaluate
        :param observed_classes: optional list of class IDX, for which to evaluate masks. By default evaluated masks for all classes are returned
        :return: (N, C)-shaped torch tensor, where
        """
        N = len(masks)

        if not observed_classes:
            observed_classes = MaskEvaluator.ALL_CLASSES

        on_device_img = img.to(self.device)

        results = []
        for batch_start, batch_end in tqdm(self.batch_boundaries(N, self.batch_size),
                                             desc=f"Batch ({self.batch_size} msk)",
                                             leave=False,
                                             disable=not self.use_tqdm):
            batch = masks[batch_start:batch_end].to(self.device)
            if self.use_fp16:
                masked_imgs = torch.mul(on_device_img.half(), batch.half())
                scores = self.model(masked_imgs)
            else:
                masked_imgs = torch.mul(on_device_img, batch)
                scores = self.model(masked_imgs)
            results.append(scores[:, observed_classes].float())
        return torch.cat(results)

    def evaluate_banded(self, img:torch.Tensor, masks: List[torch.Tensor], observed_classes:Union[int, List[int]]=None) -> List[torch.Tensor]:
        """
        Evaluates how each mask in `masks` affects the model's output on `img` and returns a (N, C)-shaped 2D tensor of results,
        where N is the number of masks and C is the number of classes supported by the evaluator.
        Caveat - if `model` used by this evaluator doesn't use softmax, resulting scores won't sum to 1 or fit in (0, 1) range

        :param img: 2D torch.Tensor of shape (y, x) containing the evaluated image of size
        :param masks: 3D torch.Tensor of shape (N, y, x) containig masks to evaluate
        :param observed_classes: optional list of class IDX, for which to evaluate masks. By default evaluated masks for all classes are returned
        :return: 2D torch.Tensor of shape (N, C), where N is the number of masks and C is the number of classes supported by `model` used byt his evaluator.
        """
        raveled_masks = torch.cat(masks)
        raveled_scores = self.evaluate(img, raveled_masks, observed_classes)
        unraveled_scores = raveled_scores.split([m.shape[0] for m in masks], dim=0)
        return unraveled_scores


    @staticmethod
    def batch_boundaries(N: int, batch_size: int):
        if N < 1 or batch_size < 1:
            raise ValueError("N and batch_size must be > 1")

        bounds = [min(x * batch_size, N) for x in range(math.ceil(N / batch_size) + 1)]
        return list(zip(bounds[:-1], bounds[1:]))

    def __str__(self):
        return self.model_name