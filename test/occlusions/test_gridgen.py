import itertools
import warnings
from unittest.mock import Mock

import numpy as np
import torch
from hirise.occlusions.gridgen import *

from unittest import TestCase, skipIf, skip, mock

from parameterized import parameterized, parameterized_class

# base grid dimensionality is the number of dimensions of a single grid.
# RISE uses 2D natively (though it also could use 1D and unravel it to (s, s) shape).
# Voronoi uses 1D grids.
TEST_GRID_NDIMS = (1, 2, 3)
BASIC_GRID_GEN_TYPES = [ThresholdGridGen, CoordinateGridGen,
                        SmartCoordinateGridGen, PermutationGridGen,
                        CombinationGridGen]

TESTED_BASE_GRID_DIMENSIONALITIES = [
        {"NDIM": n} for n in TEST_GRID_NDIMS
]

FIXING_GRID_GEN_CLASS_VARIANTS = [
        {"fixing_grid_gen": g, "NDIM": n}
        for g, n
        in itertools.product(BASIC_GRID_GEN_TYPES, TEST_GRID_NDIMS)
]

BANDED_GRID_TEST_PARAMETERS = [
        (10, [(torch.rand(10,10) < 0.5).to(torch.float)], 0.5, "single map, single param values"),
        (10, [(torch.rand(s, s) < 0.5).to(torch.float) for s in [3, 5, 8]], 0.5, "multiple maps with common params"),
        ([10, 20, 30], [(torch.rand(s, s) < 0.5).to(torch.float) for s in [3, 5, 8]], [0.2, 0.5, 0.7], "multiple maps with multiple parameters")
]

DECLARED_GRID_NDIM_DEFAULTING_LOGIC_TEST_CASES = [
        (torch.rand(10, 3, 3), None, None, 2, "defaults to 2 when not explicitly provided and band is not given"),
        (torch.rand(10, 3, 3), torch.rand(3), None, 1, "defaults to band_ndim if only band is provided"),
]

DECLARED_GRID_NDIM_LOGIC_TEST_CASES = [
        (torch.rand(10, 3, 3), torch.rand(3, 3), 1, 1, "provided value overrides band_ndim"),
        (torch.rand(10, 2, 6, 3), None, 3, 3, "provided value is used"),
]


def DEFAULT_PARAMETERIZED_CLASSNAME_FUNC(c, n, p):
    dict_as_pairs = ', '.join(f"{k.lower()}={v}" for k,v in p.items())
    return f"{c.__name__}.{dict_as_pairs}"

class CommonGridGenTests:

    def test_fill_rate_0_results_in_empty_masks(self):
        requested_fill_rate = 0
        grids = self.generate_grids_with_custom_fill_rate(requested_fill_rate)
        actual_fill_rates = self.calculate_fill_rate(grids, self.NDIM)
        self.assertTrue(np.all(actual_fill_rates == 0))

    def test_total_fill_rate_is_close_to_p1(self):
        fill_rate = self.calculate_fill_rate(self.grid, self.NDIM).mean(axis=0)
        self.assertTrue(np.all(np.isclose(fill_rate, self.p1, atol=1)))

    @parameterized.expand([
        (0, "empty"),
        (1, "full")
    ],
    testcase_func_name=lambda f, n, p: "test_typical_fill_rate.no_mask_is_%s_at_typical_fill_rate" % p.args[-1])
    def test_no_mask_is_empty_at_average_fill_rate(self, prohibited_fill_rate, desc):
        fill_rates = self.calculate_fill_rate(self.grid, self.NDIM)
        self.assertFalse(np.any(fill_rates == prohibited_fill_rate), msg=(prohibited_fill_rate, fill_rates))

    def test_string_representation_startswith_classname(self):
        self.assertTrue(str(self.grid_gen).startswith(self.grid_gen_class.__name__))

    def generate_grids_with_custom_fill_rate(self, fill_rate):
        """
        Generates a set of test grids using default parameters set by setUp method, except for fill_rate, which is takes form
        :return:
        """
        raise NotImplementedError

    @parameterized.expand([
        (1, (3,), (1, 3)),
        (4, (3,), (4, 3)),
        (4, (3, 5), (4, 3, 5)),
        (2, (3,2,1,3,2,1), (2,3,2,1,3,2,1)),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.N={p.args[-3]} and shape={p.args[-2]} "
                                     f"result in selector of shape {p.args[-1]}")
    def test_requested_shape(self, N, shape, expected_shape):
        self.assertTupleEqual(self.grid_gen.generate_grid(N, shape, .5).shape, expected_shape)


class WeakGuaranteeGridGenTests:

    @parameterized.expand([
        (0.001, 0, "is rounded up when fill rate ~= 0"),
        (0.999, 1, "is rounded down when fill rate ~= 1")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_fill_rate_rounding(self, requested_fill_rate, prohibited_actual_fill_rate, desc):
        grid = self.generate_grids_with_custom_fill_rate(requested_fill_rate)
        fill_rates = self.calculate_fill_rate(grid, self.NDIM)
        self.assertFalse(np.any(fill_rates == prohibited_actual_fill_rate), msg=fill_rates)


class StrongGuaranteeGridGenTests:

    def test_masks_are_guaranteed_to_have_at_least_one_cell_filled(self):
        self.assertFalse(np.any(self.calculate_fill_rate(self.grid, self.NDIM) == 0))

    def test_fill_rate_of_each_mask_is_as_close_to_p1_as_possible(self):
        expected_fill_rate = np.ceil(self.p1 * self.mask_area) / self.mask_area
        fill_rate = self.calculate_fill_rate(self.grid, self.NDIM)
        self.assertTrue(np.allclose(fill_rate, expected_fill_rate, atol=2/self.mask_area),
                        msg=f"Expected {expected_fill_rate} for each mask, got {fill_rate}")

    def test_all_masks_with_same_params_have_the_exact_same_fill_rate(self):
        fill_rate = self.calculate_fill_rate(self.grid, self.NDIM)
        fill_rate_stddev = fill_rate.std(axis=0)
        self.assertTrue(np.allclose(fill_rate_stddev, 0),
                        msg=f"Expected no fill rate variability between masks, got {fill_rate_stddev} "
                            f"(actual fill rates: {fill_rate})")

    def test_fill_rate_1_results_in_full_masks(self):
        requested_fill_rate = 1
        # grids = self.grid_gen.generate_grid(self.N, self.s, requested_fill_rate)
        grids = self.generate_grids_with_custom_fill_rate(requested_fill_rate)
        actual_fill_rates = self.calculate_fill_rate(grids, self.NDIM)
        self.assertTrue(np.all(actual_fill_rates == 1))


class ThresholdGridGenTest():

    @classmethod
    def setUpClass(cls) -> None:
        cls.grid_gen_class = ThresholdGridGen
        cls.grid_gen = cls.grid_gen_class(torch.device("cpu"))

    def test_fill_rate_1_results_in_full_masks(self):
        requested_fill_rate = 1
        grids = self.generate_grids_with_custom_fill_rate(requested_fill_rate)
        actual_fill_rates = self.calculate_fill_rate(grids, self.NDIM)
        self.assertTrue(np.all(actual_fill_rates == 1))

    def test_some_masks_at_low_p1_may_be_empty(self):
        grid = self.generate_grids_with_custom_fill_rate(0.001)
        fill_rates = self.calculate_fill_rate(grid, self.NDIM)
        some_masks_were_empty = np.any(fill_rates == 0)
        self.assertEqual(some_masks_were_empty, True)


class CommonCoordinateGridGenTests(WeakGuaranteeGridGenTests):

    def test_fill_rate_decay_occurs_at_half_fill(self):
        requested_fill_rate = 0.5

        grid = self.generate_grids_with_custom_fill_rate(requested_fill_rate)
        actual_fill_rate = self.calculate_fill_rate(grid, self.NDIM)

        self.assertTrue(actual_fill_rate.mean() < requested_fill_rate - 0.05)


class CoordinateGridGenTest(CommonCoordinateGridGenTests):

    @classmethod
    def setUpClass(cls) -> None:
        cls.grid_gen_class = CoordinateGridGen
        cls.grid_gen = cls.grid_gen_class(torch.device("cpu"))

    def test_high_p1_is_unlikely_to_be_achieved(self):
        requested_fill_rate = 1
        grid = self.generate_grids_with_custom_fill_rate(requested_fill_rate)
        actual_fill_rate = self.calculate_fill_rate(grid, self.NDIM)

        self.assertFalse(np.isclose(actual_fill_rate.mean(), requested_fill_rate, atol=0.1))


class SmartCoordinateGridGenTest(CommonCoordinateGridGenTests):

    @classmethod
    def setUpClass(cls) -> None:
        cls.grid_gen_class = SmartCoordinateGridGen
        cls.grid_gen = cls.grid_gen_class(torch.device("cpu"))

    def test_high_p1_is_achieved(self):
        grid = self.generate_grids_with_custom_fill_rate(0.999)
        fill_rates = self.calculate_fill_rate(grid, self.NDIM)
        self.assertFalse(np.all(fill_rates > 0.995))

    @parameterized.expand([
        (0.15,),
        (0.25,),
        (0.75,),
        (0.85,)
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_fill_rate_at(self, fill_rate):
        grid = self.generate_grids_with_custom_fill_rate(fill_rate)
        fill_rates = self.calculate_fill_rate(grid, self.NDIM)
        self.assertAlmostEqual(fill_rates.mean(), fill_rate, places=2)


class PermutationGridGenTest(WeakGuaranteeGridGenTests,
                             StrongGuaranteeGridGenTests):
    @classmethod
    def setUpClass(cls) -> None:
        cls.grid_gen_class = PermutationGridGen
        cls.grid_gen = cls.grid_gen_class(torch.device("cpu"))


class CombinationGridGenTests(WeakGuaranteeGridGenTests,
                              StrongGuaranteeGridGenTests):
    @classmethod
    def setUpClass(cls) -> None:
        cls.grid_gen_class = CombinationGridGen
        cls.grid_gen = cls.grid_gen_class(torch.device("cpu"))


class PlainGridGenTest(CommonGridGenTests):

    def setUp(self):
        self.N = 12

        # ndim will be injected when testcases are parameterized.
        # Default not provided to avoid scenario where everything is tested on that default
        self.shape = (2 ** (6 // self.NDIM),) * self.NDIM
        self.p1 = 0.2
        self.low_p1 = 0.001

        self.grid = self.grid_gen.generate_grid(self.N, self.shape, self.p1)
        self.mask_area = np.prod(self.shape)

    @parameterized.expand([
            ((8,), "1D grid"),
            ((7, 7), "2D square grid"),
            ((5, 8), "2D rectangular grid"),
            ((3, 3, 3), "3D cubic grid"),
            ((3, 4, 5), "3D cuboid grid"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape_of_result_is_maintained(self, shape, desc):
        grid = self.grid_gen.generate_grid(self.N, shape, self.p1)
        self.assertTupleEqual(grid.shape, (self.N, *shape))

    @parameterized.expand([
            (torch.Size,),
            (tuple,),
            (list,),
            (torch.tensor,),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape_param_is_accepted_when_instance_of(self, tested_type):
        shape = tested_type(self.shape)
        self.grid_gen.generate_grid(self.N, shape, self.p1) # assert doesn't raise

    def test_result_is_of_expected_type(self):
        self.assertIsInstance(self.grid, torch.Tensor)

    def test_uniform_distribution_of_occlusions(self):
        grid = self.grid_gen.generate_grid(1000, self.shape, 0.2)
        cell_occurence_frequency = grid.reshape(-1, *grid.shape[-2:]).mean(0).numpy()
        average_frequency = cell_occurence_frequency.mean(None)
        stddev = cell_occurence_frequency.std(None)

        self.assertTrue(np.all(np.abs(cell_occurence_frequency - average_frequency) < 4 * stddev),
                        msg=cell_occurence_frequency)

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_results_are_generated_on_provided_device(self):
        device = torch.device("cuda:0")
        grid_gen = self.grid_gen_class(device)

        result = grid_gen.generate_grid(self.N, self.shape, self.p1)

        self.assertEqual(result.device, device)

    @staticmethod
    def calculate_fill_rate(grid, base_shape_ndim):
        fill_rates = grid.reshape(*grid.shape[:-base_shape_ndim], -1).mean(-1)
        return fill_rates.numpy() if isinstance(fill_rates, torch.Tensor) else fill_rates


    def generate_grids_with_custom_fill_rate(self, fill_rate):
        """
        Generates a set of test grids using default parameters set by setUp method, except for fill_rate, which is takes form
        :return:
        """
        return self.grid_gen.generate_grid(self.N, self.shape, fill_rate)


class CommonBandedGridGenTests(CommonGridGenTests):

    def setUp(self) -> None:
        self.device = torch.device("cpu")

        self.N = np.array([6, 9, 12])
        self.p1 = np.array([0.5, 0.75, 0.8])

        # s is only used here to increase the exponent and the total size depends on NDIM.
        # Minimal grid size is 64, to prevent test failure cause by excessive grid coarseness.
        # `s` of each band is different to test both banded grid generation and, at the same time, support for bands of varying coarseness
        # ([s_of_first_band, s_of_second_band, s_of_third_band])
        # NDIM == 1 will yield [(2^6)^1, (2^(6+1))^1, (2^(6+2))^1] = [2^6, 2^7, 2^8]    = [64, 128, 256]
        # NDIM == 2 will yield [(2^3)^2, (2^(3+1))^2, (2^(3+2))^2] = [8^2, 16^2, 32^2]  = [64, 256, 1024]
        # NDIM == 3 will yield [(2^2)^3, (2^(2+1))^3, (2^(2+2))^3] = [4^3, 8^3,  16^3]  = [64, 512, 4096]
        self.dummy_bands = [(torch.rand(*(2 ** ((6 // self.NDIM) + i),) * self.NDIM) < p1).to(torch.float)
                            for i, p1
                            in enumerate(self.p1)]

        self.grid = self.grid_gen.generate_grids_within_limits(self.N, self.dummy_bands, self.p1)
        self.mask_area = np.array([band.sum() for band in self.dummy_bands])

    @parameterized.expand(
            BANDED_GRID_TEST_PARAMETERS,
            testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1])
    )
    def test_result_is_of_expected_type(self, Ns, bands, p1s, description):
        grids = self.grid_gen.generate_grids_within_limits(Ns, bands, p1s)

        self.assertIsInstance(grids, list)
        self.assertTrue(all(map(lambda x: isinstance(x, torch.Tensor), grids)))

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_results_are_generated_on_provided_device(self):
        expected_device = torch.device("cuda:0")
        grid_gen = self.grid_gen_class(expected_device)
        grids = grid_gen.generate_grids_within_limits(10, self.dummy_bands, 0.2)
        self.assertTrue(all(r.device == expected_device for r in grids))

    def test_only_cells_from_within_band_are_picked(self):
        inverse_bands = [-(band-1) for band in self.dummy_bands]
        max_fill_rate_for_each_s = map(np.amax, self.calculate_fill_rate(self.grid, self.NDIM, inverse_bands))
        self.assertEqual(max(max_fill_rate_for_each_s), 0)

    @parameterized.expand(
            BANDED_GRID_TEST_PARAMETERS,
            testcase_func_name=lambda f, n, p: "%s.for %s" % (f.__name__, p.args[-1])
    )
    def test_grid_shape(self, Ns, bands, p1s, description):
        grids = self.grid_gen.generate_grids_within_limits(Ns, bands, p1s)
        shapes, band_shapes = map(lambda grid_set: torch.stack([torch.as_tensor(grid.shape) for grid in grid_set]),
                                  (grids, bands))

        self.assertTrue(torch.all(shapes[:, 0] == torch.as_tensor(Ns)),
                         msg=f"Sizes of first dimension should be equal to Ns, expected {shapes[:, 0]} got {Ns}")
        self.assertTrue(torch.all(shapes[:, 1:] == band_shapes),
                         msg=f"Trailing dimensions should be equal to shapes of given bands, expected {band_shapes} got {shapes[:, 1:]}")

    @staticmethod
    def calculate_masked_fill_rate(limited_grids, bands, grid_ndim):
        grids_masked_with_bands = (np.ma.masked_where(np.broadcast_to(band, g.shape) == 0, g)
                                   for g, band
                                   in zip(limited_grids, bands))
        return list(map(lambda x: PlainGridGenTest.calculate_fill_rate(x, grid_ndim), grids_masked_with_bands))

    def calculate_fill_rate(self, limited_grids, grid_ndim, bands=None):
        fill_rates = self.calculate_masked_fill_rate(limited_grids,
                                                     self.dummy_bands if bands is None else bands,
                                                     grid_ndim)
        common_shape = (max(self.N), len(self.N))
        result_does_not_exist = np.ones(common_shape)
        stacked_fill_rates = np.empty(common_shape)

        for x_idx, fill_rate_values in enumerate(fill_rates):
            result_does_not_exist[:len(fill_rate_values), x_idx] = 0
            stacked_fill_rates[:len(fill_rate_values), x_idx] = fill_rate_values.ravel()

        masked_fill_rates = np.ma.masked_where(result_does_not_exist == 1, stacked_fill_rates)

        return masked_fill_rates

    def generate_grids_with_custom_fill_rate(self, fill_rate):
        """
        Generates a set of test grids using default parameters set by setUp method, except for fill_rate, which is takes form
        :return:
        """
        return self.grid_gen.generate_grids_within_limits(self.N, self.dummy_bands, fill_rate)


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class PlainCoordinateGridGenTest(PlainGridGenTest,
                                  CoordinateGridGenTest,
                                  TestCase):
    def setUp(self):
        PlainGridGenTest.setUp(self)


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class PlainSmartCoordinateGridGenTest(PlainGridGenTest,
                                      SmartCoordinateGridGenTest,
                                      TestCase):
    def setUp(self):
        PlainGridGenTest.setUp(self)

    @parameterized.expand([
        (0.15,),
        (0.25,),
        (0.75,),
        (0.85,)
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_fill_rate_at(self, expected_fill_rate):
        grid = self.generate_grids_with_custom_fill_rate(expected_fill_rate)
        fill_rates = self.calculate_fill_rate(grid, self.NDIM).T
        self.assertTrue(np.allclose(fill_rates, expected_fill_rate, atol=0.1), msg=(expected_fill_rate, fill_rates))


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class PlainThresholdGridGenTest(PlainGridGenTest,
                                ThresholdGridGenTest,
                                TestCase):
    def setUp(self):
        PlainGridGenTest.setUp(self)


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class PlainPermutationGridGenTest(PlainGridGenTest,
                                  PermutationGridGenTest,
                                  TestCase):
    def setUp(self):
        PlainGridGenTest.setUp(self)


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class PlainCombinationGridGenTests(PlainGridGenTest,
                                   CombinationGridGenTests,
                                   TestCase):
    def setUp(self):
        PlainGridGenTest.setUp(self)

        
@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class BandedCoordinateGridGenTest(CommonBandedGridGenTests,
                                  CoordinateGridGenTest,
                                  TestCase):
    def setUp(self) -> None:
        CommonBandedGridGenTests.setUp(self)


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class BandedSmartCoordinateGridGenTest(CommonBandedGridGenTests,
                                       SmartCoordinateGridGenTest,
                                       TestCase):
    def setUp(self) -> None:
        CommonBandedGridGenTests.setUp(self)

    @parameterized.expand([
        (0.15,),
        (0.25,),
        (0.75,),
        (0.85,)
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_fill_rate_at(self, expected_fill_rate):
        grid = self.generate_grids_with_custom_fill_rate(expected_fill_rate)
        fill_rates = self.calculate_fill_rate(grid, self.NDIM).T
        for fr in fill_rates:
            self.assertTrue(np.allclose(fr, expected_fill_rate, atol=0.1), msg=(expected_fill_rate, fr))


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class BandedPermutationGridGenTest(CommonBandedGridGenTests,
                                   PermutationGridGenTest,
                                   TestCase):

    def setUp(self) -> None:
        CommonBandedGridGenTests.setUp(self)


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class BandedThresholdGridGenTest(CommonBandedGridGenTests,
                                 ThresholdGridGenTest,
                                 TestCase):
    def setUp(self) -> None:
        CommonBandedGridGenTests.setUp(self)


@skip  # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(TESTED_BASE_GRID_DIMENSIONALITIES, class_name_func=DEFAULT_PARAMETERIZED_CLASSNAME_FUNC)
class BandedCombinationGridGenTests(CommonBandedGridGenTests,
                                    CombinationGridGenTests,
                                    TestCase):
    def setUp(self) -> None:
        CommonBandedGridGenTests.setUp(self)


class FixingGridGenTests(WeakGuaranteeGridGenTests):

    @classmethod
    def setUpClass(cls):
        cls.grid_gen_class = FixingGridGen

    def test_fixing_threshold_is_set_to_always_fix_by_default(self):
        self.assertEqual(self.grid_gen.fixing_threshold, FixingGridGen.FIXING_THRESHOLD_ALWAYS_FIX)

    @mock.patch("hirise.occlusions.gridgen.warnings.warn")
    def test_issues_a_warning_if_this_fixer_grid_gen_may_cause_infinite_loops(self, warn_mock):
        self.grid_gen_class(self.grid_gen.base_grid_gen, self.grid_gen.fixer)

        if type(self.grid_gen.fixer) in (ThresholdGridGen, CoordinateGridGen):
            warn_mock.assert_called_once_with(mock.ANY, category=RuntimeWarning)
        else:
            warn_mock.assert_not_called()


class StandaloneFixingGridGenTests(TestCase):

    def setUp(self):
        self.mock_base_grid_gen = mock.Mock(spec=ThresholdGridGen)
        self.expected_base_result = torch.rand(100, 20, 20)
        self.mock_base_grid_gen.generate_grid = mock.Mock(side_effect=[self.expected_base_result])
        self.mock_base_grid_gen.device = torch.device("cpu")

        self.mock_fixer = mock.Mock(spec=SmartCoordinateGridGen)
        self.expected_fixing_result = torch.rand(100, 20, 20)
        self.mock_fixer.generate_grid = mock.Mock(side_effect=[self.expected_fixing_result])
        self.mock_fixer.device = torch.device("cpu")

        # is_informative will call for replacement of all masks to make these test simpler
        # integration of this method with fixing is tested in PlainFixingGG and BandedFixingGG tests
        self.is_informative_mock = mock.Mock(side_effect=[lambda x, y: torch.zeros(x.shape[0], 1).to(torch.bool),
                                                          lambda x, y: torch.ones(x.shape[0], 1).to(torch.bool),
                                                          lambda x, y: torch.ones(x.shape[0], 1).to(torch.bool)])

    @parameterized.expand([
        (-0.01, False),
        (0, True),
        (0.0, True),
        (0.01, True),
        (0.99, True),
        (1, True),
        (1.0, True),
        (1.01, False),
    ],
    testcase_func_name=lambda f, n, p: "%s.%.2f %s" % (f.__name__, p.args[-2], "valid" if p.args[-1] else "invalid"))
    def test_allowed_fixing_threshold_values(self, threshold, is_allowed):
        base_grid_gen = ThresholdGridGen(torch.device("cpu"))
        if not is_allowed:
            with self.assertRaises(ValueError):
                FixingGridGen(grid_gen=base_grid_gen, fixing_threshold=threshold)
        else:
            FixingGridGen(grid_gen=base_grid_gen, fixing_threshold=threshold)

    def test_grids_not_altered_when_fixing_is_not_performed(self):
        self.grid_gen = FixingGridGen(grid_gen=self.mock_base_grid_gen,
                                      fixer_grid_gen=self.mock_fixer,
                                      fixing_threshold=FixingGridGen.FIXING_THRESHOLD_NEVER_FIX)
        self.grid_gen.is_informative = self.is_informative_mock

        grids = self.grid_gen.generate_grid(N=100, shape=(3, 3), p1=0.1)

        self.mock_base_grid_gen.generate_grid.assert_called_once()
        self.mock_fixer.generate_grid.assert_not_called()

        self.assertTrue(torch.allclose(grids, self.expected_base_result))
        self.grid_gen.is_informative.assert_not_called()

    @mock.patch("hirise.occlusions.gridgen.FixingGridGen.is_informative")
    def test_behaviour_when_fixing_is_performed(self, is_informative_mock):
        N = 100
        s = 3
        p1 = 0.1
        thr = 0.001
        self.grid_gen = FixingGridGen(grid_gen=self.mock_base_grid_gen,
                                      fixer_grid_gen=self.mock_fixer,
                                      fixing_threshold=thr)
        self.mock_base_grid_gen.generate_grid.side_effect = [(torch.rand(N, s, s) < 0.5).to(torch.float)]
        expected_result = (torch.rand(N, s, s) < 0.5).to(torch.float)
        self.mock_fixer.generate_grid.side_effect = [expected_result]
        # set up the responses from is_informative so that all of initial masks are replaced with masks returned by fixer
        is_informative_mock.side_effect = [torch.zeros(N).to(torch.bool),
                                           torch.ones(N).to(torch.bool)]

        # act
        grids = self.grid_gen.generate_grid(N=N, shape=(s, s), p1=p1)
        is_informative_mock.assert_called()

        # assert
        self.assertTrue(torch.allclose(grids, expected_result), msg=f"{grids.shape}, {expected_result.shape}")


    @parameterized.expand([
        (0.1, {"s": 2, "p1": 0.8},   True,  "fixes when likelihood greater than threshold"),
        (0,   {"s": 100, "p1": 0.5}, True,  "fixes when threshold=0 (always fix) and likelihood too small for float (both equal to 0)"),
        (0.1, {"s": 10, "p1": 0.1},  False, "doesn't fix when likelihood less than threshold"),
        (1,   {"s": 2, "p1": 1},     False, "doesn't fix when threshold and likelihood == 1 (because values equal)"),
        (0,   {"s": 2, "p1": 0},     False, "doesn't fix when likelihood == 0 (because values equal)"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    @mock.patch("hirise.occlusions.gridgen.FixingGridGen.is_informative")
    def test_checks_when(self, fixing_threshold, params, should_fix, desc, is_informative_mock):
        # arrange
        N = 100
        s = params["s"]
        self.grid_gen = FixingGridGen(grid_gen=self.mock_base_grid_gen,
                                      fixer_grid_gen=self.mock_fixer,
                                      fixing_threshold=fixing_threshold)
        self.mock_base_grid_gen.generate_grid.side_effect = [(torch.rand(N, s, s))]
        self.mock_fixer.generate_grid.side_effect = [torch.rand(N, s, s)]

        is_informative_mock.side_effect = [torch.zeros(N).to(torch.bool),
                                           torch.ones(N).to(torch.bool)]

        # act
        self.grid_gen.generate_grid(N=N, shape=(s, s), p1=params["p1"])

        # assert
        self.mock_base_grid_gen.generate_grid.assert_called_once()
        if should_fix:
            is_informative_mock.assert_called()
            self.mock_fixer.generate_grid.assert_called_once()
        else:
            is_informative_mock.assert_not_called()
            self.mock_fixer.generate_grid.assert_not_called()

    @parameterized.expand([
            *DECLARED_GRID_NDIM_LOGIC_TEST_CASES,
            *DECLARED_GRID_NDIM_DEFAULTING_LOGIC_TEST_CASES,
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    @mock.patch("hirise.occlusions.gridgen.FixingGridGen.needs_fixing")
    def test_fix_uninformative_ndim_arg_logic(self, grids, band, ndim, expected_ndim, desc, needs_fixing_mock):
        p1 = .2
        fixing_threshold = 0
        needs_fixing_mock.return_value = torch.zeros(grids.shape[:-expected_ndim])

        self.grid_gen = FixingGridGen(grid_gen=ThresholdGridGen(torch.device("cpu")),
                                      fixer_grid_gen=PermutationGridGen(torch.device("cpu")),
                                      fixing_threshold=fixing_threshold)

        # act
        self.grid_gen.fix_uninformative_grids(grids, p1, band, ndim)

        # assert
        needs_fixing_mock.assert_called_once_with(mock.ANY, mock.ANY, expected_ndim)


class FixingGridGenStaticFunctionTests(TestCase):

    @parameterized.expand([
            ((8,), "1D grid"),
            ((7, 7), "2D square grid"),
            ((5, 8), "2D rectangular grid"),
            ((3, 3, 3), "3D cubic grid"),
            ((3, 4, 5), "3D cuboid grid"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_is_empty_works_with(self, grid_shape, desc):
        shape_size = np.prod(grid_shape).item()
        expected_empty_grids_tensor = torch.as_tensor([False, True, False])
        sample_grids_tensor = torch.stack([torch.ones(grid_shape, dtype=torch.float),
                                           torch.zeros(grid_shape, dtype=torch.float),
                                           torch.as_tensor(torch.arange(shape_size).reshape(grid_shape) == 1, dtype=torch.float)])

        result = FixingGridGen.is_empty(sample_grids_tensor, single_grid_ndim=len(grid_shape))
        self.assertTrue(torch.equal(result, expected_empty_grids_tensor),
                        msg=f"Expected: {expected_empty_grids_tensor}, got {result}")

    @parameterized.expand([
            ((12, 2, 3, 7), i) for i in range(1, 4)
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.when grid_ndim equals {p.args[-1]}")
    def test_is_empty_return_shape(self, shape, grid_ndim):
        result = FixingGridGen.is_empty(torch.rand(shape), grid_ndim)
        self.assertTupleEqual(result.shape, shape[:-grid_ndim])

    @parameterized.expand([
        (torch.rand(4, 3, 3), 4, "declared grid_ndim is greater than actual grids's ndim"),
        (torch.rand(4, 3, 3), 0, "declared grid_ndim is zero"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_is_empty_raises_when(self, grids, grid_ndim, desc):
        with self.assertRaises(ValueError):
            FixingGridGen.is_empty(grids, grid_ndim)

    @parameterized.expand([
        ((1, 2, 2), 2,
         [[[0, 1], [0, 0]]],
         [False],
         "squashable hyperdim"),
        ((3, 2, 2), 2,
         [[[0, 1], [0, 0]],
          [[0, 0], [0, 0]],
          [[0, 0], [0, 1]]],
         [False, True, False],
         "actual hyperdim")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_is_empty_for_hyperdimensional_grids(self, grid_shape, grid_ndim, custom_grid, expected_result_for_custom, desc):
        n_samples = 3
        expected_empty_masks_tensor = torch.stack([torch.full(grid_shape[:-2], False, dtype=torch.bool),
                                                   torch.full(grid_shape[:-2], True, dtype=torch.bool),
                                                   torch.as_tensor(expected_result_for_custom, dtype=torch.bool)])
        sample_masks_tensor = torch.stack([torch.ones(grid_shape, dtype=torch.float),
                                           torch.zeros(grid_shape, dtype=torch.float),
                                           torch.as_tensor(custom_grid, dtype=torch.float)])

        result = FixingGridGen.is_empty(sample_masks_tensor, grid_ndim)
        self.assertSequenceEqual(result.shape, (n_samples, *grid_shape[:-2]))
        self.assertTrue(torch.equal(result, expected_empty_masks_tensor))

    @parameterized.expand([
        (torch.ones(2, 3, 3), torch.as_tensor([False, False]), "full grids are not informative"),
        (torch.zeros(2, 3, 3), torch.as_tensor([False, False]), "empty grids are not informative"),
        (torch.as_tensor([[[0, 1], [0, 0]],
                          [[1, 1], [1, 0]]]), torch.as_tensor([True, True]), "mixed grids are informative"),
        (torch.as_tensor([[[0, 1], [0, 0]],
                          [[1, 1], [1, 1]]]), torch.as_tensor([True, False]), "sanity check on tensor containing both kinds of grids")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_is_informative_not_banded(self, grids, expected_result, desc):
        result = FixingGridGen.is_informative(grids, band=None, single_grid_ndim=2)
        self.assertTrue(all(result == expected_result))

    @parameterized.expand([
        (torch.as_tensor([[[1,0], [0,1]]]), torch.as_tensor([[1,0], [0,1]]), torch.as_tensor([False]), "fully occluded within mask bounds are not informative"),
        (torch.as_tensor([[[1,0], [0,1]]]), torch.as_tensor([[0,1], [1,0]]), torch.as_tensor([False]), "entirely empty within mask bounds are not informative"),
        (torch.as_tensor([[[1,0], [0,1]]]), torch.as_tensor([[1,0], [1,0]]), torch.as_tensor([True]), "mixed within mask bounds is informative"),

        (torch.as_tensor([[[1, 0], [0, 1]]]), torch.as_tensor([[0, 0], [0, 0]]), torch.as_tensor([False]), "when band is empty, mask is never informative"),
        (torch.as_tensor([[[1, 0], [0, 1]]]), torch.as_tensor([[1, 0], [0, 0]]), torch.as_tensor([False]), "when band includes only a single filled cell, mask is not informative (100% visible)"),
        (torch.as_tensor([[[0, 0], [0, 1]]]), torch.as_tensor([[1, 0], [0, 0]]), torch.as_tensor([False]), "when band includes only a single empty cell, mask is not informative (100% occluded)"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_is_informative_banded(self, grids, band, expected_result, desc):
        result = FixingGridGen.is_informative(grids, band, band.ndim)
        self.assertTrue(all(result == expected_result))

    @parameterized.expand([
        (torch.rand(4, 3, 3), None, 4, "declared grid_ndim is greater than actual grids's ndim"),
        (torch.rand(4, 3, 3), None, 0, "declared grid_ndim is zero"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_is_informative_raises_when(self, grids, band, grid_ndim, desc):
        with self.assertRaises(ValueError):
            FixingGridGen.is_informative(grids, band, grid_ndim)

    @parameterized.expand(
            DECLARED_GRID_NDIM_LOGIC_TEST_CASES,
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    @mock.patch("hirise.occlusions.gridgen.FixingGridGen.is_empty")
    def test_is_informative_grid_ndim_logic(self, grids, band, declared_ndim, expected_ndim, desc, is_empty_mock):
        FixingGridGen.is_informative(grids, band, declared_ndim)
        expected_call = mock.call(mock.ANY, expected_ndim)

        is_empty_mock.assert_has_calls((expected_call,) * 2)

    @parameterized.expand([
        ((2, 2), 0.1, 0.6562),
        ((3, 3), 0.01, 0.9135),
        ((3, 3), 0.98, 0.8337),
    ],
    testcase_func_name=lambda f, n, p: "%s.mask shape %s, p1=%.3f" % (f.__name__, p.args[-3], p.args[-2]))
    def test_uninformative_grid_likelihood(self, grid_shape, p1, expected_result):
        self.assertAlmostEqual(FixingGridGen.uninformative_grid_likelihood(grid_shape, p1),
                               expected_result,
                               places=3)

    @parameterized.expand([
        (np.zeros((5, 5)), "empty band"),
        (np.arange(25).reshape((5,5)) < 1, "band consists of a single cell"),
    ], testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_needs_fixing_returns_false_for_each_mask_when(self, band, desc):
        shape = (3, 10, 5, 5)
        fake_masks = torch.rand(*shape)

        verdict = FixingGridGen.needs_fixing(fake_masks, band, band.ndim)

        self.assertEqual(verdict.shape, shape[:-2])
        self.assertTrue((verdict.numpy() == False).all()) # all cells are false so none of the masks requires fixing

    @parameterized.expand([
        (None, 2, "band is None (for not-banded gridgen)"),
        (np.arange(25).reshape(5, 5) < 2, 2, "band consists of at least two cells"),
    ], testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    @mock.patch.object(FixingGridGen, "is_informative")
    def test_needs_fixing_delegates_to_is_informative_when(self, band, ndim, desc, is_informative_mock):
        expected_result = Mock()
        inverted_expected_result = Mock()
        expected_result.__invert__ = Mock(return_value=inverted_expected_result)
        is_informative_mock.return_value = expected_result
        fake_masks = torch.rand(*(3, 10, 5, 5))

        # Act
        result = FixingGridGen.needs_fixing(fake_masks, band, ndim)

        # Assert
        is_informative_mock.assert_called_once()
        expected_result.__invert__.assert_called_once()

        self.assertEqual(result, inverted_expected_result)
        intercepted_masks, intercepted_band, single_grid_ndim = is_informative_mock.call_args[0]
        self.assertTrue(np.allclose(fake_masks, intercepted_masks))

        if band is None:
            self.assertIsNone(intercepted_band)
        else:
            self.assertTrue(np.allclose(band, intercepted_band))

    @parameterized.expand(
            DECLARED_GRID_NDIM_LOGIC_TEST_CASES,
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_needs_fixing_grid_ndim_logic(self, grids, band, declared_ndim, expected_ndim, desc):
        result = FixingGridGen.needs_fixing(grids, band, declared_ndim)
        self.assertEqual(result.ndim, grids.ndim - expected_ndim)
        self.assertTupleEqual(result.shape, grids.shape[:-expected_ndim])


@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(
        FIXING_GRID_GEN_CLASS_VARIANTS,
        class_name_func=lambda c, n, p: f"{c.__name__}.fixing={p['fixing_grid_gen'].__name__}, ndim={p['NDIM']}")
class PlainFixingGridGenTest(PlainGridGenTest,
                             FixingGridGenTests,
                             TestCase):

    @classmethod
    def setUpClass(cls):
        FixingGridGenTests.setUpClass()

    def setUp(self):
        cpu = torch.device("cpu")
        self.grid_gen = self.grid_gen_class(grid_gen=ThresholdGridGen(cpu), fixer_grid_gen=self.fixing_grid_gen(device=cpu))
        PlainGridGenTest.setUp(self)

        self.shape = np.random.randint(7, 10, size=self.NDIM)

        self.empty_grids_idx, self.good_grids_idx, self.full_grids_idx = np.random.permutation(self.N).reshape(3, -1)
        self.grids_tensor = self.grid_gen.generate_grid(self.N, self.shape, self.p1)
        self.grids_tensor[self.empty_grids_idx] = 0
        self.grids_tensor[self.full_grids_idx] = 1

        self.fixed_tensor = self.grid_gen.fix_uninformative_grids(self.grids_tensor, self.p1, single_grid_ndim=self.NDIM)
        self.fixed_grids_fill_rate = self.calculate_fill_rate(self.fixed_tensor[self.empty_grids_idx], self.NDIM)

    def test_fixing_does_not_alter_already_informative_masks(self):
        self.assertTrue(np.allclose(self.fixed_tensor[self.good_grids_idx],
                                    self.grids_tensor[self.good_grids_idx]))

    def test_all_empty_masks_have_been_fixed(self):
        result = np.all(self.calculate_fill_rate(self.fixed_tensor[self.empty_grids_idx], self.NDIM) > 0)
        self.assertTrue(result)

    def test_all_full_masks_have_been_fixed(self):
        result = np.all(self.calculate_fill_rate(self.fixed_tensor[self.empty_grids_idx], self.NDIM) < 1)
        self.assertTrue(result)

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_results_are_generated_on_provided_device(self):
        # NOTE: This is an override of test from `CommonGridGenTests`, because FixingGridGen's constructor differs from
        #   constructors of other gridgens
        device = torch.device("cuda:0")
        grid_gen = self.grid_gen_class(grid_gen=ThresholdGridGen(device), fixer_grid_gen=self.fixing_grid_gen(device=device))

        result = grid_gen.generate_grid(self.N, self.shape, self.p1)

        self.assertEqual(result.device, device)

@skip # this skip prevents the unparameterized version of this class from being executed
@parameterized_class(
        FIXING_GRID_GEN_CLASS_VARIANTS,
        class_name_func=lambda c, n, p: f"{c.__name__}.fixing={p['fixing_grid_gen'].__name__}, ndim={p['NDIM']}")
class BandedFixingGridGenTest(CommonBandedGridGenTests,
                              FixingGridGenTests,
                              TestCase):

    @classmethod
    def setUpClass(cls):
        FixingGridGenTests.setUpClass()

    def setUp(self) -> None:
        cpu = torch.device("cpu")
        self.grid_gen = self.grid_gen_class(grid_gen=ThresholdGridGen(cpu), fixer_grid_gen=self.fixing_grid_gen(device=cpu))
        CommonBandedGridGenTests.setUp(self)

        self.empty_grids_idx, self.good_grids_idx, self.full_grids_idx = zip(*[np.random.permutation(N).reshape(3, -1) for N in self.N])

        self.grids_tensor = self.grid_gen.generate_grids_within_limits(self.N, self.dummy_bands, self.p1)
        for grids_tensor, empty_grids_idx, full_grids_idx in zip(self.grids_tensor, self.empty_grids_idx, self.full_grids_idx):
            grids_tensor[empty_grids_idx] = 0
            grids_tensor[full_grids_idx] = 1

        self.fixed_tensors = [self.grid_gen.fix_uninformative_grids(grid, p1, band)
                              for grid, p1, band
                              in zip(self.grids_tensor, self.p1, self.dummy_bands)]

    def test_fixing_does_not_alter_already_informative_masks(self):
        for fixed_tensors, broken_tensors, idxs in zip(self.fixed_tensors, self.grids_tensor, self.good_grids_idx):
            self.assertTrue(np.allclose(fixed_tensors[idxs], broken_tensors[idxs]))

    def test_all_empty_masks_have_been_fixed(self):
        fill_rates = self.calculate_fill_rate([ft[idxs] for ft, idxs in zip(self.fixed_tensors, self.empty_grids_idx)],
                                              self.NDIM,
                                              self.dummy_bands)
        self.assertTrue(all(np.all(fr > 0) for fr in fill_rates.T))

    def test_all_full_masks_have_been_fixed(self):
        fill_rates = self.calculate_fill_rate([ft[idxs] for ft, idxs in zip(self.fixed_tensors, self.full_grids_idx)],
                                              self.NDIM,
                                              self.dummy_bands)
        self.assertTrue(all(np.all(fr < 1) for fr in fill_rates.T))

    def test_empty_band_will_cause_fixing_to_be_skipped(self):
        self.empty_bands = [torch.zeros_like(band) for band in self.dummy_bands]

        not_really_fixed_tensors = [self.grid_gen.fix_uninformative_grids(grid, p1, band)
                                      for grid, p1, band
                                      in zip(self.grids_tensor, self.p1, self.empty_bands)]

        for fixed_tensor, initial_tensor in zip(not_really_fixed_tensors, self.grids_tensor):
            self.assertTrue(np.allclose(fixed_tensor, initial_tensor))

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_results_are_generated_on_provided_device(self):
        # NOTE: This is an override of test from `CommonGridGenTests`, because FixingGridGen's constructor differs from
        #   constructors of other gridgens
        device = torch.device("cuda:0")
        grid_gen = self.grid_gen_class(grid_gen=ThresholdGridGen(device), fixer_grid_gen=self.fixing_grid_gen(device=device))

        result = grid_gen.generate_grids_within_limits(self.N, self.dummy_bands, self.p1)

        self.assertTrue(all(r.device == device for r in result))