from unittest import TestCase, skipIf, skip, mock
from unittest.mock import Mock

import torch
from hirise.occlusions.gridgen import CoordinateGridGen, PermutationGridGen
from hirise.occlusions.upsampling import Upsampler, ParallelizedUpsampler
from parameterized import parameterized

UPSAMPLING_MODES_TESTCASES = [
        ("area", "area", None),
        ("nearest", "nearest", None),
        ("bilinear", "bilinear", False),
        ("bilinear-aligned", "bilinear", True),
        ("bicubic", "bicubic", False),
        ("bicubic-aligned", "bicubic", True),
]


class CommonUpsamplerTests:

    def setUp(self) -> None:
        self.device_cpu = torch.device("cpu")
        self.upsampler = Upsampler(self.device_cpu)

        self.N = 10
        self.grid_s = 4
        self.grid_p1 = 0.2
        self.grid = PermutationGridGen(self.device_cpu).generate_grid(self.N, (self.grid_s, self.grid_s), self.grid_p1).unsqueeze(1)

        self.target_size = (30, 30)

        self.upsampled = self.upsampler.upsample(self.grid, self.target_size)

    def test_result_size_is_correct(self):
        expected_shape = (self.N, 1, *self.target_size)
        self.assertTupleEqual(self.upsampled.shape, expected_shape)


    @parameterized.expand(
            UPSAMPLING_MODES_TESTCASES,
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[0]}")
    def test_interpolator_params_for_mode(self, mode_name, expected_mode, expected_align_value):
        upsampler = Upsampler(device=self.device_cpu, upsampling_mode=mode_name)

        self.assertEqual(upsampler.upsampling_mode, expected_mode)
        self.assertEqual(upsampler.align_corners, expected_align_value)

    @parameterized.expand(
            UPSAMPLING_MODES_TESTCASES,
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[0]}")
    def test_result_shape_for_mode(self, mode_name, expected_mode, expected_align_value):
        expexcted_shape = (*self.grid.shape[:-len(self.target_size)], *self.target_size)
        upsampler = Upsampler(device=self.device_cpu, upsampling_mode=mode_name)

        # act
        result = upsampler.upsample(self.grid, self.target_size)

        self.assertTupleEqual(result.shape, expexcted_shape)


class UpsamplerTest(CommonUpsamplerTests, TestCase):

    def setUp(self) -> None:
        CommonUpsamplerTests.setUp(self)

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_upsampler_places_results_on_provided_device(self):
        origin_device = torch.device("cpu")
        upsampler_device = torch.device("cuda:0")
        upsampler = Upsampler(upsampler_device)
        result = upsampler.upsample(self.grid.to(origin_device), self.target_size)

        self.assertTrue(result.device, upsampler_device)

    def test_default_mode_is_bilinear(self):
        upsampler = Upsampler(self.device_cpu) # upsampling mode left unspecified on purpose
        self.assertTrue(upsampler.upsampling_mode, "bilinear")

    @mock.patch("hirise.occlusions.upsampling.tnnf.interpolate")
    def test_upsampling_parameters_are_passed_to_the_interpolator(self, interpolate_mock):
        upsampler = Upsampler(self.device_cpu)
        upsampler.upsampling_mode = Mock()
        upsampler.align_corners = Mock()
        interpolate_mock.return_value = torch.rand(*self.grid.shape[:-2], 5*13, 5*13)

        upsampler.upsample(self.grid.to(self.device_cpu), self.target_size)

        interpolate_mock.assert_called_once()
        _, kwargs = interpolate_mock.call_args_list[0]
        self.assertEqual(upsampler.upsampling_mode, kwargs["mode"])
        self.assertEqual(upsampler.align_corners, kwargs["align_corners"])


class ParallelizedUpsamplerTest(UpsamplerTest):

    def setUp(self) -> None:
        UpsamplerTest.setUp(self)
        self.pool_size = 2
        self.upsampler = ParallelizedUpsampler(self.pool_size)
        self.result = self.upsampler.upsample(self.grid, self.target_size)

    def test_parallelized_always_uses_cpu(self):
        self.assertTrue(self.result.device, torch.device("cpu"))

    @skip("Not implemented")
    def test_threadpool_is_used(self, threadpool_mock):
        self.fail()