from unittest import TestCase

import torch
from hirise.occlusions.rescaling import avgpool_downsample, find_avgpool_params_brute_force, AvgpoolDownsampler
from parameterized import parameterized


class TestAvgpoolDownsample(TestCase):

    def setUp(self) -> None:
        self.input_s = 224
        self.map_with_one_cell = torch.zeros(self.input_s, self.input_s)
        self.map_with_one_cell[221, 221] = 1

    @parameterized.expand([
        (112,),
        (56,),
        (28,),
        (14,),
        (7,),
        (5,),
        (3,),
        (2,),
    ],
    testcase_func_name=lambda f, n, p: "%s.%d -> %d" % (f.__name__, 224, p.args[-1]))
    def test_preserves_information_about_marked_cells(self, output_s):
        downsampled = AvgpoolDownsampler.downsample(self.map_with_one_cell, output_s)
        self.assertGreater(downsampled.max(), 0)


    @parameterized.expand([
            (224, 112),
            (224, 56),
            (224, 28),
            (224, 14),
            (224, 7),
            (224, 5),
            (224, 3),
            (224, 2)
    ],
    testcase_func_name=lambda f, n, p: "%s.%d -> %d" % (f.__name__, p.args[-2], p.args[-1]))
    def test_output_size_is_equal_to_target_s_for_factors(self, input_s, output_s):
        expected_shape = (output_s, output_s)
        input_map = torch.rand(input_s, input_s)
        downsampled = AvgpoolDownsampler.downsample(input_map, output_s)

        self.assertTrue(downsampled.shape == expected_shape,
                        msg=f"Expected size {expected_shape}, got {downsampled.shape}")


class TestAvgPoolParamsFinder(TestCase):

    @parameterized.expand([
        (224, 112),
        (224, 56),
        (224, 28),
        (224, 14),
        (224, 7),
        (224, 5),
        (224, 3),
        (224, 2),
        (3, 2)
    ],
    testcase_func_name=lambda f, n, p: "%s.%d -> %d" % (f.__name__, p.args[-2], p.args[-1]))
    def test_finds_parameters_for_integer_scaling_factor(self, input_s, output_s):
        results = find_avgpool_params_brute_force(input_s, output_s)
        self.assertGreater(len(results), 0)

    @parameterized.expand(
        list(map(lambda x: (37, x), range(1, 37))),
            testcase_func_name=lambda f, n, p: "%s.%d -> %d" % (f.__name__, p.args[-2], p.args[-1]))
    def test_finds_parameters_for_prime_input_size(self, input_s, output_s):
        results = find_avgpool_params_brute_force(input_s, output_s)
        self.assertGreater(len(results), 0)


