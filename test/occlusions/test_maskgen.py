from unittest import TestCase, skip, skipIf
from unittest.mock import Mock, MagicMock, call

import torch
from hirise.occlusions.gridgen import GridGen, PermutationGridGen
from hirise.occlusions.maskgen import MaskGen, BandedMaskGen, AdaptiveMaskGen
from hirise.occlusions.upsampling import Upsampler
from parameterized import parameterized, parameterized_class
from hirise.structures import MaskAggregator


def parameter_broadcasting_funcname(function, n_params, params):
    param_values = params.args[:-1] # expected results is the last parameter and it's not meant to be included in the name
    param_names = ["N", "bands", "p1", "s_levels"]

    def quantifier_word_for_funcname(object):
        if isinstance(object, list) or (isinstance(object, torch.Tensor) and object.ndim == 3):
            return "multiple"
        else:
            return "single"

    description_string = ', '.join([f"{quantifier_word_for_funcname(o)} {o_name}"
                                    for o, o_name
                                    in zip(param_values, param_names)])

    return f"{function.__name__}.{description_string}"


class CommonMaskGenFixture():

    def setUp(self):
        self.N = 10
        self.s = 3
        self.p1 = 0.2

        self.target_shape = (7,7)

        self.color_channel_dim_idx = 1

        self.grid_mock = MagicMock(spec=torch.Tensor)
        self.grid_gen_mock = Mock(spec=GridGen)
        self.grid_gen_mock.generate_grid = Mock(return_value=self.grid_mock)

        self.unsqueezed_grid_mock = MagicMock(spec=torch.Tensor)
        self.grid_mock.unsqueeze = Mock(return_value=self.unsqueezed_grid_mock)

        self.upsampled_grid_mock = MagicMock(spec=torch.Tensor)
        self.upsampler_mock = Mock(spec=Upsampler)
        self.upsampler_mock.device = torch.device("cpu")
        self.upsampler_mock.upsample = Mock(return_value=self.upsampled_grid_mock)


class MaskGenTestFixture(CommonMaskGenFixture):

    def setUp(self):
        CommonMaskGenFixture.setUp(self)
        self.mask_gen = MaskGen(self.grid_gen_mock, self.upsampler_mock)
        self.sample_masks = self.mask_gen.generate_masks(self.N, self.s, self.p1, self.target_shape)


class PlainMaskGenTests(MaskGenTestFixture,
                        TestCase):

    def setUp(self):
        MaskGenTestFixture.setUp(self)

    def test_sequence_of_operations(self):
        self.grid_gen_mock.generate_grid.assert_called_once_with(self.N, (self.s, self.s), self.p1)
        self.grid_mock.unsqueeze.assert_called_once_with(self.color_channel_dim_idx)
        self.upsampler_mock.upsample.assert_called_once_with(self.unsqueezed_grid_mock, self.target_shape)


class MaskGenGridRetainerInteractionTests(MaskGenTestFixture,
                                          TestCase):

    def setUp(self):
        MaskGenTestFixture.setUp(self)

        self.grid_mock.squeeze = Mock(return_value=self.grid_mock)
        self.grid_mock.cpu = Mock(return_value=self.grid_mock)

        self.retainer_mock_1 = MagicMock(spec=MaskAggregator)
        self.retainer_mock_2 = MagicMock(spec=MaskAggregator)
        self.retainers = [self.retainer_mock_1, self.retainer_mock_2]

        self.mask_gen.generate_masks(self.N, self.s, self.p1, self.target_shape, self.retainers)

    def test_each_retainer_is_called_exactly_once(self):
        for i, retainer_mock in enumerate(self.retainers):
            retainer_mock.add_many.assert_called_once()

    def test_exact_grid_gen_output_is_passed_to_retainers(self):
        for i, retainer_mock in enumerate(self.retainers):
            call_args = retainer_mock.add_many.call_args_list[0]
            grids, _ = call_args[0]

            self.assertEqual(grids, self.grid_gen_mock.generate_grid.return_value)

    def test_none_is_passed_instead_of_scores(self):
        for i, retainer_mock in enumerate(self.retainers):
            call_args = retainer_mock.add_many.call_args_list[0]
            _, scores = call_args[0]

            self.assertIsNone(scores, msg=f"Expected `scores` to be None, got {type(scores)}")


class CommonBandedMaskGenFixture(CommonMaskGenFixture):

    def setUp(self):
        CommonMaskGenFixture.setUp(self)
        self.banded_grid_mock = MagicMock(spec=torch.Tensor)
        self.grid_gen_mock.grids_for_band = Mock(return_value=self.banded_grid_mock)

        self.unsqueezed_banded_grids_mock = MagicMock(spec=torch.Tensor)
        self.banded_grid_mock.unsqueeze = Mock(return_value=self.unsqueezed_banded_grids_mock)

        self.dummy_band = (torch.rand(8, 8) < 0.5).to(torch.float)
        self.multiple_dummy_bands = (torch.rand(3, 8, 8) < 0.5).to(torch.float)

        self.Ns = [3, 5, 8]
        self.p1s = [0.3, 0.5, 0.8]


class CommonBandedMaskGenTests():

    def generate_banded_masks(self, N, bands, p1, target_shape):
        raise NotImplementedError

    def test_raises_when_bands_are_given_as_list_of_tensors(self):
        with self.assertRaises(AttributeError):
            list_of_bands = [torch.rand(5,5) for _ in range(3)]
            self.generate_banded_masks(self.Ns, list_of_bands, self.p1s, self.target_shape)

    @parameterized.expand([
        ([5, 7, 11], torch.rand(4, 4), [0.3, 0.5, 0.7], "multiple parameters but single band"),
        ([5, 7, 11], torch.rand(5, 4, 4), [0.3, 0.5, 0.7], "number of bands does not broadcast to number of other params"),
        ([5, 7, 11], torch.rand(3, 4, 4), [0.3, 0.5, 0.7, 0.9], "number of p1 values does not broadcast to number of bands"),
        ([5, 7, 11, 13], torch.rand(3, 4, 4), [0.3, 0.5, 0.7], "number of N values does not broadcast to number of bands"),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_when_param_shapes_mismatch(self, Ns, bands, p1s, desc):
        with self.assertRaises(ValueError):
            self.generate_banded_masks(Ns, bands, p1s, self.target_shape)


class TestPlainBandedMaskGen(CommonBandedMaskGenFixture,
                             CommonBandedMaskGenTests,
                             TestCase):

    def setUp(self):
        CommonBandedMaskGenFixture.setUp(self)

        self.mask_gen = BandedMaskGen(self.grid_gen_mock, self.upsampler_mock)

    @parameterized.expand([
        (5,             torch.rand(4, 4),       0.3,             1),
        (5,             torch.rand(3, 4, 4),    0.3,             3),
        ([5, 7, 11],    torch.rand(3, 4, 4),    0.3,             3),
        (5,             torch.rand(3, 4, 4),    [0.3, 0.5, 0.7], 3),
        ([5, 7, 11],    torch.rand(3, 4, 4),    [0.3, 0.5, 0.7], 3),
    ],
    testcase_func_name=parameter_broadcasting_funcname)
    def test_parameter_broadcasting_behaviour(self, Ns, bands, p1s, expected_len):
        masks = self.mask_gen.generate_masks(Ns, p1s, self.target_shape, bands)
        self.assertIsInstance(masks, list)
        self.assertEqual(len(masks), expected_len,
                         msg=f"Expected {expected_len} tensors in result, got {len(masks)}")

    def test_grid_gen_called_once_when_single_parameters_are_used(self):
        self.mask_gen.generate_masks(self.N, self.p1, self.target_shape, self.dummy_band)
        call_args = self.grid_gen_mock.grids_for_band.call_args_list
        N, band, p1 = call_args[0][0]
        self.assertEqual(N, self.N, msg=f"N value differs. Expected {self.N}, got {N}")
        self.assertEqual(p1, self.p1, msg=f"p1 value differs. Expected {self.p1}, got {p1}")
        self.assertTrue(torch.allclose(band, self.dummy_band), msg=f"Band differs from expected. Use debugger to inspect values")

        self.upsampler_mock.upsample.assert_called_once_with(self.unsqueezed_banded_grids_mock, self.target_shape)

    def test_grid_gen_called_several_times_when_multiple_parameter_values_used(self):
        # N is given as a single value on purpose - to test parameter broadcasting
        self.mask_gen.generate_masks(self.N, self.p1s, self.target_shape, self.multiple_dummy_bands)
        calls_to_grid_gen = self.grid_gen_mock.grids_for_band.call_args_list
        calls_to_upsampler = self.upsampler_mock.upsample.call_args_list

        self.assertEqual(len(calls_to_grid_gen), len(self.multiple_dummy_bands))
        self.assertEqual(len(calls_to_upsampler), len(self.multiple_dummy_bands))
        for idx, (grid_gen_call_args, upsample_call_args) in enumerate(zip(calls_to_grid_gen, calls_to_upsampler)):
            N, band, p1 = grid_gen_call_args[0]
            grid, target_shape = upsample_call_args[0]
            self.assertEqual(N, self.N, msg=f"N values of call {idx} differ. Expected {self.N}, got {N}")
            self.assertEqual(p1, self.p1s[idx], msg=f"p1 values of call {idx} differ. Expected {self.p1s[idx]}, got {p1}")
            self.assertTrue(torch.allclose(band, self.multiple_dummy_bands[idx]), msg=f"Band used for call {idx} differs from expected. Use debugger to inspect values")

            self.assertEqual(grid, self.unsqueezed_banded_grids_mock)
            self.assertEqual(target_shape, self.target_shape)

    def test_grids_are_upsampled_before_upsampling(self):
        expected_n_calls = len(self.multiple_dummy_bands)
        expected_unsqueeze_calls = (call(self.color_channel_dim_idx),) * expected_n_calls
        expected_upsampler_calls = (call(self.unsqueezed_banded_grids_mock, self.target_shape),) * expected_n_calls

        # act
        self.mask_gen.generate_masks(self.N, self.p1s, self.target_shape, self.multiple_dummy_bands)

        # assert
        self.upsampler_mock.upsample.assert_has_calls(expected_upsampler_calls)
        self.banded_grid_mock.unsqueeze.assert_has_calls(expected_unsqueeze_calls)


    def generate_banded_masks(self, N, bands, p1, target_shape):
        return self.mask_gen.generate_masks(N, p1, target_shape, bands)

@skip
@parameterized_class(
        ("direct_upsample", "test_name"),
         [
             (True, "DirectUpsampling"),
             (False, "MergeBeforeUpsampling"),
         ],
        class_name_func=lambda c, n, p: f"{c.__name__}.{p['test_name']}")
class TestAdaptiveMaskGen(CommonBandedMaskGenFixture,
                          CommonBandedMaskGenTests,
                          TestCase):

    def setUp(self):
        CommonBandedMaskGenFixture.setUp(self)
        self.occlusion_s = [3, 4, 7]

        self.device = torch.device('cpu')
        self.mask_gen = AdaptiveMaskGen(self.grid_gen_mock,
                                        upsampler=self.upsampler_mock,
                                        direct_upsample=self.direct_upsample)

        self.combined_grid_mock = MagicMock(spec=torch.Tensor)
        self.grid_mock.__mul__ = Mock(return_value=self.combined_grid_mock)
        self.banded_grid_mock.__mul__ = Mock(return_value=self.combined_grid_mock)
        self.upsampled_grid_mock.__mul__ = Mock(return_value=self.combined_grid_mock)

    def test_grid_gen_is_called_with_downsampled_bands(self):
        self.mask_gen.generate_masks(self.N, self.occlusion_s, self.p1s, self.target_shape, self.multiple_dummy_bands)
        gridgen_call_args = self.grid_gen_mock.grids_for_band.call_args_list

        for idx, (expected_s, call_args) in enumerate(zip(self.occlusion_s, gridgen_call_args)):
            N, band, p = call_args[0]
            self.assertEqual(band.shape[-1], band.shape[-2])
            self.assertEqual(band.shape[-1], expected_s)

    def test_recombination_behaviour(self):
        self.grid_gen_mock.grids_for_band = Mock(side_effect=lambda N, base, p: torch.rand(N, *base.shape))
        provided_masking_bands = self.multiple_dummy_bands
        self.generate_banded_masks(self.Ns, provided_masking_bands, self.p1s, self.target_shape, self.occlusion_s)

        upsampler_calls = self.upsampler_mock.upsample.call_args_list
        mask_multiplication_calls = self.upsampled_grid_mock.__mul__.call_args_list

        if self.direct_upsample:
            for call_args in mask_multiplication_calls:
                other = call_args[0]
                self.assertEqual(other, self.upsampled_grid_mock,
                                 msg="Expected both the grid and masking band to be upsampled upon merge.")

            self.assertEqual(len(upsampler_calls), 1 + len(provided_masking_bands))
            for upsample_call_args in upsampler_calls:
                upsampled_tensor, target_shape = upsample_call_args[0]
                self.assertTupleEqual(target_shape, self.target_shape,
                                    msg=f"Expected all upsampling calls for direct_upsample to request resize to final upsampled_tensor shape {self.target_shape}, instead found {target_shape}")

        else:
            first_set_of_calls = upsampler_calls[:len(provided_masking_bands)]
            second_set_of_calls = upsampler_calls[len(provided_masking_bands):]

            for call_args, expected_band in zip(mask_multiplication_calls, provided_masking_bands):
                other = call_args[0][0]
                self.assertTrue(torch.allclose(other, expected_band),
                                msg="Expected merge of upsampled grid and not-yet-upsampled masking band.")

            for upsample_call_args, masking_band, N in zip(first_set_of_calls, provided_masking_bands, self.Ns):
                upsampled_tensor, target_shape = upsample_call_args[0]
                self.assertEqual(upsampled_tensor.ndim, 4)
                self.assertEqual(upsampled_tensor.shape[0], N)
                self.assertTupleEqual(target_shape, masking_band.shape,
                                    msg=f"Expected all upsampling calls for direct_upsample to request resize to final upsampled_tensor shape {self.target_shape}, instead found {target_shape}")

            for upsample_call_args in second_set_of_calls:
                upsampled_tensor, target_shape = upsample_call_args[0]
                self.assertEqual(upsampled_tensor, self.combined_grid_mock, msg="Second upsampling should have been performed on grid merged with masking band")
                self.assertTupleEqual(target_shape, self.target_shape,
                                    msg=f"Expected all upsampling calls for direct_upsample to request resize to final upsampled_tensor shape {self.target_shape}, instead found {target_shape}")


    @parameterized.expand([
        ([5, 7, 11], [4, 5, 6, 8], torch.rand(3, 10, 10), [0.3, 0.5, 0.7], "number_of_s_levels_does_not_broadcast_to_number_of_bands"),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_when_param_shapes_mismatch(self, Ns, s_levels, bands, p1s, desc):
        with self.assertRaises(ValueError):
            self.generate_banded_masks(Ns, bands, p1s, self.target_shape, s_levels)

    @parameterized.expand([
        (5,          torch.rand(10, 10),    0.3,             4,         1),
        (5,          torch.rand(3, 10, 10), 0.3,             4,         3),
        ([5, 7, 11], torch.rand(3, 10, 10), 0.3,             4,         3),
        (5,          torch.rand(3, 10, 10), 0.3,             [4, 5, 6], 3),
        (5,          torch.rand(3, 10, 10), [0.3, 0.5, 0.7], 4,         3),
        ([5, 7, 11], torch.rand(3, 10, 10), 0.3,             [4, 5, 6], 3),
        ([5, 7, 11], torch.rand(3, 10, 10), [0.3, 0.5, 0.7], 4,         3),
        ([5, 7, 11], torch.rand(3, 10, 10), [0.3, 0.5, 0.7], [4, 5, 6], 3),
    ],
    testcase_func_name=parameter_broadcasting_funcname)
    def test_parameter_broadcasting_behaviour(self, Ns, bands, p1s, s_levels, expected_len):
        # not having an exception raised is enough to pass
        self.mask_gen.generate_masks(Ns, s_levels, p1s, self.target_shape, bands)


    def generate_banded_masks(self, N, bands, p1, target_shape, s_levels=None):
        """
        Params other than those common to all BandedMaskGens are provided by default. Hence t
        """
        s = s_levels if s_levels is not None else self.occlusion_s
        return self.mask_gen.generate_masks(N, s, p1, target_shape, bands)


class CommonIntegrationTestFixture:

    def setUp(self):
        self.device = torch.device('cpu')
        self.live_grid_gen = PermutationGridGen(device=self.device)
        self.live_upsampler = Upsampler(self.device)


class PlainMaskGenIntegrationTest(MaskGenTestFixture,
                                  CommonIntegrationTestFixture,
                                  TestCase):
    def setUp(self):
        MaskGenTestFixture.setUp(self)
        CommonIntegrationTestFixture.setUp(self)

        self.live_mask_gen = MaskGen(grid_gen=self.live_grid_gen, upsampler=self.live_upsampler)

    def test_result_shape(self):
        expected_shape = (self.N, 1, *self.target_shape)
        result = self.live_mask_gen.generate_masks(self.N, self.s, self.p1, self.target_shape)

        self.assertTupleEqual(result.shape, expected_shape)


class CommonBandedMaskGenIntegrationTests(CommonBandedMaskGenFixture,
                                          CommonIntegrationTestFixture):

    def setUp(self):
        CommonBandedMaskGenFixture.setUp(self)
        CommonIntegrationTestFixture.setUp(self)

    @parameterized.expand([
        (12, "single N"),
        ([12, 17, 28], "different N for each grid")
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_result_shape(self, N, desc):
        # Act
        result = self.generate_banded_masks(N, self.multiple_dummy_bands, self.p1s, self.target_shape)

        # Assert
        self.assertIsInstance(result, list)
        self.assertTrue(all(map(lambda x: isinstance(x, torch.Tensor), result)))
        mask_tensor_shapes = torch.stack(list(map(lambda x: torch.as_tensor(x.shape), result)))

        self.assertTrue(torch.allclose(mask_tensor_shapes[:, 0], torch.as_tensor(N)))
        self.assertTrue(torch.allclose(mask_tensor_shapes[:, 1], torch.as_tensor(1)))
        self.assertTrue(torch.allclose(mask_tensor_shapes[:, 2:], torch.as_tensor(self.target_shape)))

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_result_is_placed_on_same_device_as_upsampler(self):
        gpu = torch.device('cuda:0')
        self.live_upsampler.device = gpu

        mask_sets = self.generate_banded_masks(self.Ns, self.multiple_dummy_bands, self.p1s, self.target_shape)

        self.assertTrue(all(map(lambda x: x.device == gpu, mask_sets)))

    @skip("Not sure yet if I can make this guarantee")
    def test_masks_are_not_empty(self):
        raise NotImplementedError


@skip
@parameterized_class(
        ("direct_upsample", "test_name"),
         [
             (True, "DirectUpsampling"),
             (False, "MergeBeforeUpsampling"),
         ],
        class_name_func=lambda c, n, p: f"{c.__name__}.{p['test_name']}")
class AdaptiveMaskGenIntegrationTest(CommonBandedMaskGenIntegrationTests,
                                     CommonIntegrationTestFixture,
                                     TestCase):

    def setUp(self):
        CommonBandedMaskGenIntegrationTests.setUp(self)
        CommonIntegrationTestFixture.setUp(self)
        self.occlusion_s = [3, 4, 7]

        self.mask_gen = AdaptiveMaskGen(self.live_grid_gen, self.live_upsampler, self.direct_upsample)

    def test_raises_when_any_occlusion_s_greater_than_target_size(self):
        masking_bands = torch.rand(3, 5, 5)
        occlusion_s = [2, 4, 8]

        with self.assertRaises(ValueError):
            self.mask_gen.generate_masks(self.Ns, occlusion_s, self.p1s, self.target_shape, masking_bands)


    def generate_banded_masks(self, N, bands, p1, target_shape, s_levels=None):
        """
        Params other than those common to all BandedMaskGens are provided by default. Hence t
        """
        s = s_levels if s_levels is not None else self.occlusion_s
        return self.mask_gen.generate_masks(N, s, p1, target_shape, bands)


class BandedMaskGenIntegrationTest(CommonBandedMaskGenIntegrationTests,
                                   TestCase):

    def setUp(self):
        CommonBandedMaskGenIntegrationTests.setUp(self)

        self.mask_gen = BandedMaskGen(self.live_grid_gen, self.live_upsampler)

    def generate_banded_masks(self, N, bands, p1, target_shape):
        return self.mask_gen.generate_masks(N, p1, target_shape, bands)