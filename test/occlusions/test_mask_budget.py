import numpy as np
from unittest import TestCase
from parameterized import parameterized

from hirise.occlusions.mask_budget import EqualSplit, SpanProportionalSplit, SProportionalSplit

class MaskBudgetCommonTests():

    def test_returned_value(self):
        self.assertIsInstance(self.result, list)
        self.assertEqual(len(self.result), len(self.band_spans))


class TestEqualSplit(MaskBudgetCommonTests,
                     TestCase):
    def setUp(self):
        self.tested_class = EqualSplit
        self.band_spans = [0.1, 0.3, 0.6]
        self.budget = self.tested_class(None, None, self.band_spans)
        self.example_N = 1000

        self.result = self.budget.split(self.example_N)

    @parameterized.expand([
        (333 * 3 + 1, np.random.rand(3), [333, 333, 333], "splits rounded down where normal rounding would round down"),
        (333 * 3 + 2, np.random.rand(3), [333, 333, 333], "splits rounded down where normal rounding would round up"),
        (1200,        np.random.rand(3), [400, 400, 400], "splits split equally"),
        (1200,        np.random.rand(2), [600, 600], "splits split equally, 2 bands"),
        (1200,        np.random.rand(4), [300, 300, 300, 300], "splits split equally, 4 bands"),
        (2,           np.random.rand(3), [1, 1, 1], "edge case - more bands than masks in budget will return one for each"),
        (2,           [],                [], "edge case - no spans result in empty allocation list"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_split(self, N_budget, spans, expected_allocation, desc):
        budget = self.tested_class(None, None, spans)
        result = budget.split(N_budget)
        self.assertSequenceEqual(result, expected_allocation)

    def test_str_representation(self):
        self.assertEqual(str(self.budget), "EqualSplit")


class TestSpanProportionalSplit(MaskBudgetCommonTests,
                                TestCase):
    def setUp(self):
        self.tested_class = SpanProportionalSplit
        self.band_spans = [0.1, 0.3, 0.5]
        self.budget = self.tested_class(None, None, self.band_spans)

        self.example_N = 1000
        self.result = self.budget.split(self.example_N)

    @parameterized.expand([
        (1000, [0.6, 0.1, 0.3],          [600, 100, 300], "typical case"),
        (1000, [0.3339, 0.333, 0.3321],  [334, 333, 332], "mask counts are rounded to the nearest integer"),
        (1000, [0.3333, 0.3333, 0.3333], [333, 333, 333], "each band is rounded separately, the budget might sum to less than N"),
        (1000, [0.4, 0.5],               [444, 555], "when spans sum to less than 1"),
        (1000, [0.8, 0.3, 0.2, 0.7],     [400, 150, 100, 350], "when spans sum to more than 1"),
        (1000, [0.3, 0.3, 0.3, 0.3],     [250, 250, 250, 250], "equal spans result in equal split"),
        (3,    [0.7, 0.2, 0.1],          [2, 1, 1], "edge case - span gets one mask if it were to get zero from proportional split"),
        (2,    [],                       [], "edge case - no spans result in empty allocation list"),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_split(self, N_budget, spans, expected_allocation, desc):
        budget = self.tested_class(None, None, spans)
        result = budget.split(N_budget)
        self.assertSequenceEqual(result, expected_allocation)

    def test_str_representation(self):
        self.assertEqual(str(self.budget), "SpanProportionalSplit")

class TestSProportionalSplit(TestCase):
    def setUp(self):
        self.tested_class = SProportionalSplit
        self.s_values = [3, 8, 13]
        self.budget = self.tested_class(self.s_values, None, None)

        self.example_N = 1000
        self.result = self.budget.split(self.example_N)

    @parameterized.expand([
        (1000, [2, 5, 3],    [200, 500, 300], "typical case"),
        (1000, [3, 8, 11],   [136, 363, 500], "mask counts are rounded to the nearest integer"),
        (1000, [3, 3, 3],    [333, 333, 333], "each band is rounded separately, the budget might sum to less than N"),
        (1000, [2, 6],       [250, 750], "when spans sum to less than 1"),
        (1000, [3, 3, 3, 3], [250, 250, 250, 250], "equal spans result in equal split"),
        (3,    [10, 2],      [2, 1], "edge case - band receives one mask if proportional split would give zero"),
        (2,    [],           [], "edge case - no s_values result in empty allocation list"),
        (1000, 3,            [1000], "edge case - single s value"),

    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_split(self, N_budget, s_values, expected_allocation, desc):
        budget = self.tested_class(s_values, None, None)
        result = budget.split(N_budget)
        self.assertSequenceEqual(result, expected_allocation)

    def test_str_representation(self):
        self.assertEqual(str(self.budget), "SProportionalSplit")

