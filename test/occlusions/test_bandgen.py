from unittest import TestCase, skip
from unittest.mock import Mock

import numpy as np
import torch
from hirise.occlusions.bandgen import ZOrderBandGen, BandGen
from hirise.occlusions.rescaling import get_interpolation_upsampler, AvgpoolDownsampler
from hirise.experiment.utils import torch_normalize
from hirise.occlusions.bands import make_bounds
from parameterized import parameterized


class TestPlainBandGen:

    def setUp(self) -> None:
        self.downsampler = AvgpoolDownsampler
        self.bandgen = BandGen(self.downsampler)

        self.dummy_smap = torch.arange(49).view(7, 7).to(torch.float) / 49
        self.dummy_smap[6,6] = 1


    @parameterized.expand([
        (None, "if s is None"),
        (3, "if s is given")
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_output_shape_and_type(self, s, desc):
        bands = self.bandgen.generate_bands(self.dummy_smap, self.bounds, s)

        self.assertIsInstance(bands, torch.Tensor)
        self.assertEqual(bands.ndim, 3)

        expected_s = self.dummy_smap.shape[-1] if s is None else s
        self.assertSequenceEqual(bands.shape[-2:], (expected_s, expected_s))

    def test_upsampler_not_called_if_s_not_provided(self):
        downsampler = Mock(spec=AvgpoolDownsampler)
        downsample = Mock()
        downsampler.downsample = downsample
        bandgen = BandGen(downsampler)

        bandgen.generate_bands(self.dummy_smap, self.bounds, s=None)

        downsample.assert_not_called()

    @parameterized.expand([
        ([1,2], "1D list"),
        ([[1,2, 3], [3,4, 5]], "nested lists with > 2 elements"),
        ([[1], [3]], "nested lists with 1 element"),
        (torch.as_tensor([1,2]), "1D tensor"),
        (torch.as_tensor([[1,2,3], [2,3,4]]), "2D tensor with > 2-element rows"),
        (torch.as_tensor([[1], [2]]), "2D tensor with > 2-element rows"),

    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_bound_list_validation_fails_on(self, bounds, desc):
        with self.assertRaises(ValueError):
            self.bandgen.generate_bands(self.dummy_smap, bounds, None)

    @parameterized.expand([
        ([[1,2], [2,3]], "nested lists with 2 elements each"),
        (torch.as_tensor([[1,2], [2,3]]), "2D tensor with 2-element rows"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_bound_list_validation_succeeds_on(self, bounds, desc):
        self.bandgen.generate_bands(self.dummy_smap, bounds, None)

    def test_list_of_lists_is_accepted_as_bounds_param(self):
        bounds_as_list = [[0, 1/3], [1/3, 2/3], [2/3, 1]]
        bounds_as_tensor = torch.as_tensor(bounds_as_list)

        bands_from_list = self.bandgen.generate_bands(self.dummy_smap, bounds_as_list, None)
        bands_from_tensor = self.bandgen.generate_bands(self.dummy_smap, bounds_as_tensor, None)

        self.assertTrue(torch.allclose(bands_from_list, bands_from_tensor))

    def test_lower_bound_is_always_included_in_band(self):
        for idx, (lower_bound, band) in enumerate(zip(self.bounds[:, 0], self.bands)):
            masked_map = band * self.dummy_smap
            self.assertTrue(torch.any(torch.eq(masked_map, lower_bound).flatten()),
                            msg=f"Expected lower bound: {lower_bound:.2f} to be included in band {idx}")


class TestPlainBandGenOnContiguousBounds(TestPlainBandGen, TestCase):

    def setUp(self) -> None:
        TestPlainBandGen.setUp(self)

        self.bounds = torch.as_tensor([[  0, 1/7],
                                        [1/7, 3/7],
                                        [3/7, 4/7],
                                        [4/7, 1]])

        self.bands = self.bandgen.generate_bands(self.dummy_smap, self.bounds, s=None)


    def test_contiguous_bands_have_no_overlaps(self):
        combined_bands = self.bands.prod(dim=0)
        self.assertEqual(combined_bands.max(), 0)

    def test_contiguous_bands_leave_no_blank_spaces(self):
        combined_bands = self.bands.sum(dim=0)
        self.assertTrue(combined_bands.min(), 1)

    def test_upper_bound_is_not_included_in_band(self):
        for idx, (upper_bound, band) in enumerate(zip(self.bounds[:-1, 1], self.bands[:-1])):
            self.assertFalse(torch.any(torch.eq(band * self.dummy_smap, upper_bound).flatten()),
                            msg=f"Expected upper bound {upper_bound:.2f} to be included in band {idx}")

    def test_upper_bound_of_last_band_is_included_in_band(self):
        self.assertTrue(torch.any(torch.eq(self.bands[-1] * self.dummy_smap, self.bounds[-1, 1]).flatten()),
                        msg=f"Expected upper bound 1.00 to be included in last band")

class TestPlainBandGenOnNonContiguousBounds(TestPlainBandGen, TestCase):

    def setUp(self) -> None:
        TestPlainBandGen.setUp(self)

        self.bounds = torch.as_tensor([[  0, 1/7],
                                        [2/7, 3/7],
                                        [5/7,   1]])

        self.not_in_bounds = torch.as_tensor([[1/7, 2/7],
                                              [3/7, 5/7]])

        self.areas_not_in_bounds = (torch.gt(self.dummy_smap, self.not_in_bounds[:, 0, None, None])
                                    * torch.lt(self.dummy_smap, self.not_in_bounds[:, 1, None, None])).to(torch.float)

        self.bands = self.bandgen.generate_bands(self.dummy_smap, self.bounds, s=None)

    def test_values_not_in_bounds_are_not_included_in_any_of_bands(self):
        self.assertEqual((self.areas_not_in_bounds.sum(dim=0) * self.bands.sum(dim=0)).max(), 0)

    def test_upper_bound_of_each_band_is_included_in_band(self):
        for idx, (upper_bound, band) in enumerate(zip(self.bounds[:, 1], self.bands)):
            self.assertTrue(torch.any(torch.eq(band * self.dummy_smap, upper_bound).flatten()),
                            msg=f"Expected upper bound {upper_bound:.2f} to be included in band {idx}")


class TestZOrderBandGen(TestCase):

    def setUp(self) -> None:
        self.dummy_smap = torch.rand(50, 50)

        self.upsampler = get_interpolation_upsampler("nearest")
        self.downsampler = AvgpoolDownsampler

        self.n_bounds = 10
        self.dummy_bounds = np.array(list(zip(range(0, self.n_bounds), range(1, self.n_bounds+1)))) / self.n_bounds
        self.s_levels = list(range(3, 3 + self.n_bounds))

        self.bandgen = ZOrderBandGen(self.downsampler, self.upsampler)

        self.bands = self.bandgen.generate_bands(self.dummy_smap, self.dummy_bounds, self.s_levels)

    def test_bands_are_returned_as_3d_tensor(self):
        self.assertIsInstance(self.bands, torch.Tensor)
        self.assertEqual(self.bands.shape[0], self.n_bounds)

    def test_all_bands_use_highest_provided_s(self):
        levels_ = torch.all(torch.tensor(self.bands.shape[-2:]) == max(self.s_levels))
        self.assertTrue(levels_)

    def test_bands_do_not_overlap(self):
        combined_bands = self.bands.sum(dim=0)
        self.assertEqual(combined_bands.max(), 1)

    def test_bands_cover_the_entire_area_without_gaps(self):
        combined_bands = self.bands.sum(dim=0)
        self.assertTrue(combined_bands.min(), 1)

    @skip("No such guarantee at this point")
    def test_extreme_values_are_included(self):
        bounds = make_bounds([0, 0.3, 0.6, 1])
        s_values = [5, 10, 25]

        bands = self.bandgen.generate_bands(self.dummy_smap, bounds, s_values)
        downsampled_smap = torch_normalize(AvgpoolDownsampler.downsample(self.dummy_smap, max(s_values)))
        bands_combined_with_smap = bands.unsqueeze(1) * downsampled_smap

        self.assertEqual(bands_combined_with_smap[0].min(), downsampled_smap.min())
        self.assertEqual(bands_combined_with_smap[1].max(),  downsampled_smap.max())

    @skip("This property is not satisfied because of up/downsampling applied by this method")
    def test_band_cell_values_fit_within_given_boundaries(self):
        raise NotImplementedError