import unittest
from unittest import mock, skip

import numpy as np
import torch
from hirise.occlusions.distributions_torch import make_callable, make_callable_midstep, invert, make_df, make_dfn, make_cdf, \
    make_cdfn, make_interpolating_callable
from parameterized import parameterized

class CommonMakeCallableTests():

    def setUp(self):
        self.np_array = np.arange(9)
        self.torch_array = torch.from_numpy(self.np_array)

    @parameterized.expand([
        (np.array([0.2, 0.4, 0.6]), np.arange(50), "numpy array", "numpy array"),
        (torch.as_tensor([0.2, 0.4, 0.6]), np.arange(50), "torch tensor", "numpy array"),
        (np.array([0.2, 0.4, 0.6]), torch.arange(50), "numpy array", "torch tensor"),
        (torch.as_tensor([0.2, 0.4, 0.6]), torch.arange(50), "torch tensor", "torch tensor"),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]} as source and {p.args[-2]} as selector"
    )
    def test_type_support(self, selector, source_array, desc, desc2):
        expected_result = torch.as_tensor([10, 20, 30])
        callable_array = make_callable(source_array)
        result = callable_array(selector)

        self.assertIsInstance(result, source_array.__class__)
        self.assertTupleEqual(tuple(result.shape), (3,))
        self.assertTrue(torch.allclose(torch.as_tensor(result), expected_result))


class TestMakeCallableTorch(CommonMakeCallableTests,
                            unittest.TestCase):

    def setUp(self):
        CommonMakeCallableTests.setUp(self)

    @parameterized.expand([
        (torch.arange(4), 0,    0),
        (torch.arange(4), 0.24, 0),
        (torch.arange(4), 0.25, 1),
        (torch.arange(4), 0.26, 1),
        (torch.arange(4), 0.49, 1),
        (torch.arange(4), 0.5,  2),
        (torch.arange(4), 0.51, 2),
        (torch.arange(4), 0.75, 3),
        (torch.arange(4), 0.99, 3),
        (torch.arange(4), 1,    3),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, f"In 4-value array, indexing with 0_{int(100*p.args[-2])} will yield value at index {p.args[-1]}"))
    def test_selection_by_float_index(self, array, indexing_float, expected_value_index):
        selector = make_callable(torch.tensor(array))
        result = selector(indexing_float)

        self.assertEqual(result.item(), array[expected_value_index])

    def test_selection_by_array(self):
        array = torch.tensor([0, 3, 5, 9])
        indexing_array = torch.tensor([0.56, 0.66, 0.11, 0.99])
        expected_result = torch.tensor([5, 5, 0, 9])

        selector = make_callable(array)
        result = selector(indexing_array)

        self.assertTrue(torch.allclose(result, expected_result), msg=f"Expected {expected_result}, got {result}")

    @parameterized.expand([
        (0.12, "single float"),
        ((0.11, 0.34), "tuple"),
        ([0.11, 0.34], "list"),
        (np.array([0.11, 0.34]), "numpy array"),
        (torch.tensor([0.11, 0.34]), "torch tensor"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, f"when indexed with {p.args[-1]}"))
    def test_returns_torch_tensor(self, indexing_value, description):
        selector = make_callable(self.torch_array)
        result = selector(indexing_value)

        self.assertIsInstance(result, torch.Tensor)


class TestMakeCallableMidstep(CommonMakeCallableTests,
                              unittest.TestCase):

    def setUp(self):
        CommonMakeCallableTests.setUp(self)

    @parameterized.expand([
        (torch.arange(4), 0,    0),
        (torch.arange(4), 0.16, 0),
        (torch.arange(4), 0.17, 1),
        (torch.arange(4), 0.49, 1),
        (torch.arange(4), 0.5,  2),
        (torch.arange(4), 0.83, 2),
        (torch.arange(4), 0.84, 3),
        (torch.arange(4), 1,    3),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, f"In 4-value array, midpoint indexing with 0_{int(100*p.args[-2])} will yield value at index {p.args[-1]}"))
    def test_selection_by_float_index(self, array, indexing_float, expected_value_index):
        selector = make_callable_midstep(torch.tensor(array))
        result = selector(indexing_float)

        self.assertEqual(result.item(), array[expected_value_index])

    def test_selection_by_array(self):
        array = torch.tensor([0, 3, 5, 9])
        indexing_array = torch.tensor([0.56, 0.66, 0.11, 0.99])
        expected_result = torch.tensor([5, 5, 0, 9])

        selector = make_callable_midstep(array)
        result = selector(indexing_array)

        self.assertTrue(torch.allclose(result, expected_result), msg=f"Expected {expected_result}, got {result}")

    @parameterized.expand([
        (0.12, "single float"),
        ((0.11, 0.34), "tuple"),
        ([0.11, 0.34], "list"),
        (np.array([0.11, 0.34]), "numpy array"),
        (torch.tensor([0.11, 0.34]), "torch tensor"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, f"when indexed with {p.args[-1]}"))
    def test_returns_torch_tensor(self, indexing_value, description):
        selector = make_callable_midstep(self.torch_array)
        result = selector(indexing_value)

        self.assertIsInstance(result, torch.Tensor)


class TestInterpolatingCallable(unittest.TestCase):

    def setUp(self) -> None:
        self.tensor = torch.as_tensor([10, 30, 80])
        self.callable_tensor = make_interpolating_callable(self.tensor)

    @mock.patch("hirise.occlusions.distributions_torch.numpy_ic")
    def test_each_call_uses_numpy_interpolation_under_the_hood(self, numpy_ic_mock):
        tensor = torch.rand(100)
        callable = make_interpolating_callable(tensor)

        numpy_ic_mock.assert_called_once()

    @parameterized.expand([
        ([0, 0.5, 1], [10, 30, 80], "original elements"),
        ([0.25, 0.75], [20, 55], "interpolated elements")
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_fetching_elements_from_input(self, float_indices, expected_outputs, desc):
        indices = torch.as_tensor(float_indices)
        expected_outputs = torch.as_tensor(expected_outputs)

        self.assertTrue(torch.allclose(self.callable_tensor(indices), expected_outputs))


class TestInvert(unittest.TestCase):

    def setUp(self):
        self.original_array = torch.arange(100, dtype=torch.float) / 100
        self.inverted_array = invert(self.original_array)

    @mock.patch("hirise.occlusions.distributions_torch.numpy_invert", return_value=np.arange(100))
    def test_invert_uses_numpy_invert_under_the_hood(self, numpy_invert_mock):
        invert(self.original_array)
        numpy_invert_mock.assert_called_once()

    def test_invert_result_is_always_in_0_1_range(self):
        original_range = torch.arange(100)
        inverted = invert(original_range).numpy()
        self.assertEqual(inverted.min(), 0)
        self.assertEqual(inverted.max(), 1)

    def test_inversion_of_linear_function_changes_nothing(self):
        inverted_array = invert(self.original_array)

        self.assertEqual(len(inverted_array), len(self.original_array))
        self.assertTrue(np.allclose(inverted_array, self.original_array / self.original_array.max()))

    def test_double_inversion_returns_array_close_to_original(self):
        # it's not identical due to errors introduced by linear interpolation
        original_y = np.array([1, 2, 4, 8, 16])

        inverted = invert(torch.as_tensor(original_y))
        original_from_inverted = invert(inverted)

        self.assertTrue(np.allclose(original_from_inverted, inverted, atol=1))


class TorchDistributionTestsFixture:

    def setUp(self):
        self.random_input = (torch.rand(10, 10) * 4)
        self.distribution = self.__class__.df_func(self.random_input)

    def test_make_df_flattens_the_input_tensor(self):
        self.assertEqual(self.distribution.ndim, 1,
                         msg=f"Expected the distribution array to be 1-dimensional, got {self.distribution.ndim}")

    def test_raises_when_given_a_numpy_array(self):
        with self.assertRaises(Exception):
            self.__class__.df_func(np.arange(25))


class NormalizedDistributionTest:

    def test_df_is_normalized(self):
        self.assertEqual(self.distribution.min(), 0,
                         msg="Expected min value in distribution to be equal to 0")
        self.assertEqual(self.distribution.max(), 1,
                         msg="Expected max value in distribution to be equal to 1")


class TestMakeDF(TorchDistributionTestsFixture,
                 unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.df_func = make_df

    def setUp(self):
        TorchDistributionTestsFixture.setUp(self)

    def test_distribution_is_simply_sorted_input_tensor(self):
        # And this means it's not a *real* distribution, just a sorted input, but it does its job
        self.assertTrue(np.allclose(self.distribution, np.sort(self.random_input, axis=None)))

    def test_df_is_not_normalized(self):
        self.assertEqual(self.distribution.min(), self.random_input.min(),
                         msg=f"Expected min value in distribution to be equal to min of input")
        self.assertEqual(self.distribution.max(), self.random_input.max(),
                         msg=f"Expected max value in distribution to be equal to max of input")


class TestMakeDFN(TorchDistributionTestsFixture,
                  NormalizedDistributionTest,
                  unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.df_func = make_dfn

    def setUp(self) -> None:
        TorchDistributionTestsFixture.setUp(self)


class TestMakeCDF(TorchDistributionTestsFixture,
                  unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.df_func = make_cdf

    def setUp(self) -> None:
        TorchDistributionTestsFixture.setUp(self)

    def test_is_monotonic(self):
        self.assertTrue(all(map(lambda x, y: x < y, self.distribution, self.distribution[1:])))

    def test_minimum_is_the_smallest_value_in_input(self):
        self.assertAlmostEqual(self.distribution.min().item(), self.random_input.min().item())

    def test_maximum_is_the_sum_of_all_values_in_input(self):
        self.assertAlmostEqual(self.distribution.max().item(),
                               torch.sum(self.random_input.flatten()).item(),
                               places=3)


class TestMakeCDFN(TorchDistributionTestsFixture,
                   NormalizedDistributionTest,
                   unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.df_func = make_cdfn

    def setUp(self) -> None:
        TorchDistributionTestsFixture.setUp(self)

    def test_is_monotonic(self):
        self.assertTrue(all(map(lambda x, y: x < y, self.distribution, self.distribution[1:])))

