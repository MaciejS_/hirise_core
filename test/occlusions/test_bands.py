from unittest import TestCase, skip

import torch
import numpy as np
from hirise.occlusions.bands import  saliency_bands, ThresholdGen, SaliencyValueThresholdGen, \
    PercentileThresholdGen, EnergyThresholdGen
from parameterized import parameterized


class TestMakeBounds(TestCase):

    def setUp(self):
        self.edges = [0.2, 0.5, 0.6, 0.75]
        self.expected_bounds = torch.tensor([[0.2, 0.5],
                                             [0.5, 0.6],
                                             [0.6, 0.75]])

        self.bounds = ThresholdGen.as_bounds(self.edges)

    def test_result_is_torch_tensor(self):
        self.assertIsInstance(self.bounds, torch.Tensor)

    def test_result_shape(self):
        self.assertTupleEqual(self.bounds.shape, (len(self.edges) - 1, 2))

    def test_result_is_as_expected(self):
        self.assertTrue(torch.all(self.bounds == self.expected_bounds))

    @parameterized.expand([
        ([], "empty lists"),
        ([0.33], "one-element list"),
        (0.33, "single float"),
        (torch.as_tensor([[0, 1],[1, 2]]), "2d input"),
        ([0, 0.33, 0.66, 0.5], "non-monotonic list"),
        (torch.as_tensor([0, 0.33, 0.66, 0.5]), "non-monotonic tensor")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_make_bounds_throws_on(self, edges, testcase_title):
        with self.assertRaises((ValueError, TypeError)):
            ThresholdGen.as_bounds(edges)

    def test_with_single_pair(self):
        lower_edge = 0.2
        upper_edge = 0.4
        edges = [lower_edge, upper_edge]
        ThresholdGen.as_bounds(edges)


class CommonThresholdGenTest():

    def setUp(self):
        self.dummy_map_shape = (25, 25)
        self.dummy_saliency_map = torch.rand(self.dummy_map_shape)
        self.dummy_band_edges = [0, 0.1, 0.5, 0.6, 1]

    def test_return_shape_of_make_bounds(self):
        bounds = self.threshold_gen_class.as_bounds(self.dummy_band_edges)
        bound_limits = self.threshold_gen_class.make_bounds(self.dummy_saliency_map, bounds)

        self.assertSequenceEqual(bound_limits.shape, bounds.shape)

    def test_return_shape_of_make_contiguous_bounds(self):
        bound_limits = self.threshold_gen_class.make_contiguous_bounds(self.dummy_saliency_map, self.dummy_band_edges)

        self.assertSequenceEqual(bound_limits.shape, (len(self.dummy_band_edges)-1, 2))


class SaliencyValueThresholdGenTest(CommonThresholdGenTest,
                                    TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.threshold_gen_class = SaliencyValueThresholdGen

    def setUp(self):
        CommonThresholdGenTest.setUp(self)

    def test_this_generator_simply_returns_input_values(self):
        domainless_bounds = ThresholdGen.as_bounds(self.dummy_band_edges)
        bounds = self.threshold_gen_class.make_bounds(self.dummy_saliency_map, domainless_bounds)
        self.assertTrue(torch.allclose(bounds, domainless_bounds))

class AreaThresholdGenTest(CommonThresholdGenTest,
                                    TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.threshold_gen_class = PercentileThresholdGen

    def setUp(self):
        CommonThresholdGenTest.setUp(self)
        self.dummy_map_area = np.prod(self.dummy_map_shape)

    def test_band_areas_are_equal_to_band_width_value(self):
        domainless_bounds = ThresholdGen.as_bounds(self.dummy_band_edges)
        band_widths = domainless_bounds[:, 1] - domainless_bounds[:, 0]

        area_bounds = self.threshold_gen_class.make_bounds(self.dummy_saliency_map, domainless_bounds)

        for band_idx, (lower_bound, upper_bound) in enumerate(area_bounds):
            cells_above_lower_band = torch.ge(self.dummy_saliency_map, lower_bound)
            cells_below_upper_band = torch.lt(self.dummy_saliency_map, upper_bound)
            band_area = torch.sum(cells_above_lower_band * cells_below_upper_band).to(torch.float) / self.dummy_map_area
            self.assertTrue(torch.allclose(band_area, band_widths[band_idx], atol=0.05),
                            msg=f"Area doesn't match for band {band_idx}, expected {band_widths[band_idx]}, got {band_area}")


class EnergyThresholdGenTest(CommonThresholdGenTest,
                                    TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.threshold_gen_class = EnergyThresholdGen

    def setUp(self):
        CommonThresholdGenTest.setUp(self)

    def test_total_value_within_band_is_equal_to_requested_in_input(self):
        domainless_bounds = ThresholdGen.as_bounds(self.dummy_band_edges)
        band_widths = domainless_bounds[:, 1] - domainless_bounds[:, 0]

        energy_bounds = self.threshold_gen_class.make_bounds(self.dummy_saliency_map, domainless_bounds)

        for band_idx, (lower_bound, upper_bound) in enumerate(energy_bounds):
            cells_above_lower_band = torch.ge(self.dummy_saliency_map, lower_bound)
            cells_below_upper_band = torch.lt(self.dummy_saliency_map, upper_bound)
            sorted_smap, argsort = torch.sort(torch.abs(self.dummy_saliency_map.flatten()))
            atol = torch.max(sorted_smap[1:] - sorted_smap[:-1])

            band_energy = torch.sum(self.dummy_saliency_map * cells_above_lower_band * cells_below_upper_band) / torch.sum(self.dummy_saliency_map)
            self.assertTrue(torch.allclose(band_energy, band_widths[band_idx], atol=2*atol),
                            msg=f"Energy doesn't match for band {band_idx}, expected {band_widths[band_idx]:.5f}, got {band_energy:.5f}, tolerance: {atol:.5f} ")

    @skip("Valid only if the icdfn method is used as converter")
    def test_fails_on_all_zeros_saliency_map(self):
        dummy_map = torch.zeros(100, 100)

        domainless_bounds = ThresholdGen.as_bounds(self.dummy_band_edges)
        with self.assertRaises(IndexError):
             self.threshold_gen_class.make_bounds(dummy_map, domainless_bounds)

    @skip("Only true when icdfn method is used")
    def test_almost_all_zero_map_will_cause_the_last_band_to_cover_the_entire_map(self):
        dummy_map = torch.zeros(100, 100)
        dummy_map[0, 10] = 1

        domainless_bounds = ThresholdGen.as_bounds(self.dummy_band_edges)
        bounds = self.threshold_gen_class.make_bounds(dummy_map, domainless_bounds)

        self.assertEqual(bounds[:-1].max(), 0)
        self.assertTrue(np.allclose(bounds[-1], torch.as_tensor([0, 1])))

    def test_both_interpolation_methods_are_equivalent(self):
        test_band_edges = ThresholdGen.as_bounds(np.sort(np.random.rand(100)))

        icdfn_method_values = EnergyThresholdGen._make_bounds_icdfn(self.dummy_saliency_map, test_band_edges)
        interpolation_method_values = EnergyThresholdGen._make_bounds_interpolated(self.dummy_saliency_map,
                                                                                   test_band_edges)

        self.assertTrue(torch.allclose(icdfn_method_values, interpolation_method_values, rtol=2*1e-01),
                        msg=f"Values don't match:\n icdfn: {icdfn_method_values}\n interpolated: {interpolation_method_values}")



class TestGenerateBands(TestCase):

    def setUp(self) -> None:
        self.dummy_input_size = (20, 20)
        self.dummy_smap = torch.arange(0, 1, step=1/np.product(self.dummy_input_size)).reshape(*self.dummy_input_size)
        self.dummy_numpy_smap = self.dummy_smap.cpu().numpy()
        self.bounds = torch.tensor([[0.2, 0.5],
                                    [0.5, 0.6],
                                    [0.6, 0.75]])


        self.bands = saliency_bands(self.dummy_smap, self.bounds)

    def test_output_shape(self):
        n_bands, map_shape = self.bands.shape[0], self.bands.shape[1:]
        self.assertEqual(n_bands, len(self.bounds), msg=f"Expected {len(self.bounds)} bands, got {n_bands}")
        self.assertTupleEqual(map_shape, self.dummy_input_size,
                              msg=f"Expected each map to be of shape {self.dummy_input_size}, got {map_shape}")

    def test_raises_on_misshaped_bounds_definition(self):
        misshaped_bounds = self.bounds.T
        with self.assertRaises(ValueError):
            saliency_bands(self.dummy_smap, misshaped_bounds)

    def test_on_single_band(self):
        bounds = [0.2, 0.6]
        band = saliency_bands(self.dummy_smap, torch.tensor(bounds)).cpu().numpy()
        lower_limit, upper_limit = bounds

        self.assertTupleEqual(band.shape, self.dummy_input_size)

        smap_within_band = np.ma.masked_where(band == 0, self.dummy_numpy_smap)
        smap_outside_of_band = np.ma.masked_where(band == 1, self.dummy_numpy_smap)

        self.assertTrue(np.all(smap_within_band < upper_limit), msg="Some values above upper limit were included in band")
        self.assertTrue(np.all(smap_within_band >= lower_limit), msg="Some values below lower limit were included in band")
        self.assertTrue(np.all(np.logical_or(smap_outside_of_band < lower_limit, smap_outside_of_band >= upper_limit)),
                    "Not all values within limits were captured by the band")

    def test_all_values_in_range(self):
        bands = saliency_bands(self.dummy_smap, self.bounds).cpu().numpy()

        for idx, (band, (lower_limit, upper_limit)) in enumerate(zip(bands, self.bounds)):
            smap_within_band = np.ma.masked_where(band == 0, self.dummy_numpy_smap)
            smap_outside_of_band = np.ma.masked_where(band == 1, self.dummy_numpy_smap)

            self.assertTrue(np.all(smap_within_band < upper_limit.item()), msg="Some values above upper limit were included in band")
            self.assertTrue(np.all(smap_within_band >= lower_limit.item()), msg="Some values below lower limit were included in band")
            self.assertTrue(np.all(np.logical_or(smap_outside_of_band < lower_limit.item(),
                                                 smap_outside_of_band >= upper_limit.item())),
                            "Not all values within limits were captured by the band")

