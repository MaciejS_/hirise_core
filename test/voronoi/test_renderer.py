import random

import numpy as np
import torch
import itertools

import unittest
from unittest import mock, skip
from unittest.case import skipIf
from parameterized import parameterized, parameterized_class

from shapely.affinity import scale
from shapely.geometry import Polygon

from hirise.voronoi.renderer import *
from hirise.voronoi.structures import FiniteVoronoi
from hirise.voronoi.utils import polymask

UNIT_BOX = Polygon([[0, 0], [1, 0], [1, 1], [0, 1]])
UNIT_RHOMBUS = Polygon([[.5, 0], [1, .5], [.5, 1], [0, .5]])


class NDArrayMatcher:

    def __init__(self, array):
        self.array = array

    def __eq__(self, other):
        return np.allclose(self.array, other)


class NDArrayPredicate:

    def __init__(self, predicate):
        self.predicate = predicate

    def __eq__(self, other):
        return self.predicate(other)


class CommonRendererTests():

    @parameterized.expand([
        (np.random.rand(12, 2), 50, (130, 130), "underscaled inputs"),
        (np.random.rand(12, 2), 130, (130, 130), "native inputs"),
        (np.random.rand(12, 2), 210, (130, 130), "overscaled inputs"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_render_shape_is_as_requested(self, seeds, scaling_factor, target_shape, desc):
        vor = FiniteVoronoi(seeds * scaling_factor, coord_range=(0, scaling_factor))

        # act
        result = self.renderer.render(vor.polygon_coords, target_shape=target_shape)

        # assert
        self.assertSequenceEqual(result.shape, target_shape)

    @parameterized.expand([
            (FiniteVoronoi(np.random.rand(12, 2)).polygons, "large set of polygons"),
            ([UNIT_BOX, UNIT_RHOMBUS], "multiple polygons"),
            ([UNIT_RHOMBUS], "single polygon as sequence")
    ], 
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_batch_render_shape_is_as_requested(self, polygons, desc):
        hypershape = (3, 4, 2)
        mask_shape = (100, 100)
        selectors = torch.randint(1, (*hypershape, len(polygons))).to(torch.bool)

        # act
        result = self.renderer.batch_render(polygons, selectors, mask_shape)

        # assert
        self.assertTupleEqual(result.shape, (*hypershape, *mask_shape))


class RendererTestFixture():

    def setUp(self):
        self.default_target_shape = (224, 224)
        voronoi = FiniteVoronoi(np.random.rand(10, 2), coord_range=(0, 224))

        outline_value = PillowOcclusionRenderer._OUTLINE_VALUE_MAP[PillowOcclusionRenderer.OUTLINE_HALFTONE]
        self.occlusion_vertices = voronoi.polygon_coords
        self.default_params_dict = {"fill_polygon": True,
                                    "outline_type": PillowOcclusionRenderer.OUTLINE_HALFTONE,
                                    "mask_mode":    PillowOcclusionRenderer.MASK_MODE_MONOCHROME,
                                    "device":       torch.device("cpu")}

        self.renderer = PillowOcclusionRenderer(**self.default_params_dict)
        self.polygon_renders = [
            polymask(
                [poly],
                target_shape=self.default_target_shape,
                fill_value=PillowOcclusionRenderer._FILL_VALUE,
                outline_value=outline_value,
                mask_mode=self.default_params_dict["mask_mode"]
            )
            for poly
            in self.occlusion_vertices
        ]


class TestPillowOcclusionRenderer(RendererTestFixture,
                                  CommonRendererTests,
                                  unittest.TestCase):

    def setUp(self):
        RendererTestFixture.setUp(self)

    @parameterized.expand([
            (False, 0),
            (True, PillowOcclusionRenderer._FILL_VALUE),
            (0, 0),
            (1, PillowOcclusionRenderer._FILL_VALUE),
            (2, PillowOcclusionRenderer._FILL_VALUE)],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.results in {p.args[-1]} when given as {p.args[-2]}")
    def test_init_interpretation_of_fill_value(self, provided_value, expected_value):
        self.default_params_dict["fill_polygon"] = provided_value

        # act
        renderer = PillowOcclusionRenderer(**self.default_params_dict)

        # assert
        self.assertEqual(renderer._polygon_fill, expected_value)

    @parameterized.expand([
            (PillowOcclusionRenderer.OUTLINE_NONE, 0, True),
            (PillowOcclusionRenderer.OUTLINE_HALFTONE, 1, True),
            (PillowOcclusionRenderer.OUTLINE_SOLID, 2, True),
            (-1, None, False),
            (.5, None, False),
            (3, None, False),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-3]} {'is' if p.args[-1] else 'is not'} accepted")
    def test_init_of_outline_fill_value(self, tested_value, expected_value, is_value_accepted):
        self.default_params_dict["outline_type"] = tested_value

        # act & assert
        if is_value_accepted:
            renderer = PillowOcclusionRenderer(**self.default_params_dict) # should not raise
            self.assertEqual(renderer._outline_fill, expected_value)
        else:
            with self.assertRaises(ValueError):
                PillowOcclusionRenderer(**self.default_params_dict)  # should raise

    @parameterized.expand([
            (True, PillowOcclusionRenderer.OUTLINE_HALFTONE, "polygon is filled and halftone outline is used"),
            (True, PillowOcclusionRenderer.OUTLINE_SOLID, "polygon is filled and solid outline is used"),
            (True, PillowOcclusionRenderer.OUTLINE_NONE, "polygon is filled and no outline"),

            (False, PillowOcclusionRenderer.OUTLINE_HALFTONE, "only a halftone outline is drawn"),
            (False, PillowOcclusionRenderer.OUTLINE_SOLID, "only a solid outline is drawn"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_mask_values_are_always_normalized(self, fill_polygon, outline_type, desc):
        self.default_params_dict["fill_polygon"] = fill_polygon
        self.default_params_dict["outline_type"] = outline_type

        renderer = PillowOcclusionRenderer(**self.default_params_dict)

        result = renderer.render(
            [np.asarray(scale(UNIT_BOX, .5, .5).exterior.coords)],
            target_shape=(100, 100)
        )

        self.assertTrue(torch.all(result <= 1))
        self.assertTrue(torch.any(result >= 0))


    def test_renderer_can_be_configured_to_output_empty_canvas(self):
        self.default_params_dict["fill_polygon"] = False
        self.default_params_dict["outline_type"] = PillowOcclusionRenderer.OUTLINE_NONE

        # act
        renderer = PillowOcclusionRenderer(**self.default_params_dict) # should not raise
        result = renderer.render(
            [np.asarray(scale(UNIT_BOX, .5, .5).exterior.coords)],
            target_shape=(100, 100)
        )

        # assert
        self.assertTrue(torch.all(result == 0))

    @parameterized.expand([
            (PillowOcclusionRenderer.MASK_MODE_MONOCHROME, False),
            (PillowOcclusionRenderer.MASK_MODE_BINARY, False),
            ("RGB", True),
            (None, True),
            ("YCbCr", True)
    ],
            testcase_func_name=lambda f, n,
                                      p: f"test_init_mask.{'does not raise' if p.args[-1] else 'raises'} when mask_mode is {p.args[-2]}")
    def test_init_mask_mode(self, mask_mode_value, should_raise):
        self.default_params_dict["mask_mode"] = mask_mode_value

        if should_raise:
            with self.assertRaises(ValueError):
                PillowOcclusionRenderer(**self.default_params_dict)
        else:
            PillowOcclusionRenderer(**self.default_params_dict)  # should not raise

    def test_init_params_are_used_when_rendering(self):
        self.renderer._polygon_fill = mock.Mock()
        self.renderer._outline_fill = mock.Mock()
        self.renderer._mask_mode = mock.Mock()
        self.renderer._polygon_coord_range = mock.Mock()
        test_shape = self.default_target_shape

        common_call_args = {"fill_value":           self.renderer._polygon_fill,
                            "outline_value":        self.renderer._outline_fill,
                            "mask_mode":            self.renderer._mask_mode}

        test_polygons = self.occlusion_vertices
        expected_polymask_call = mock.call(polygon_vertices=test_polygons, target_shape=test_shape, **common_call_args)

        with mock.patch('hirise.voronoi.renderer.polymask',
                        side_effect=lambda *args, **kwargs: np.random.rand(*kwargs['target_shape'])) as polymask_mock:
            # act
            self.renderer.render(test_polygons, self.default_target_shape)

            # assert
            polymask_mock.assert_called_once()
            polymask_mock.assert_has_calls([expected_polymask_call])

    @skipIf(torch.cuda.device_count() == 0, "CUDA devices not available")
    def test_result_is_placed_on_requested_device(self):
        test_device = torch.device("cuda:0")
        self.default_params_dict["device"] = test_device
        renderer = PillowOcclusionRenderer(**self.default_params_dict)

        # act
        result = renderer.render(self.occlusion_vertices, self.default_target_shape)

        # assert
        self.assertEqual(result.device, test_device)

    @parameterized.expand([
            (np.array([[False, False, False], [True, False, False], [True, False, True]]),
             [[], [0], [0, 2]],
             "2D numpy selector"),

            (torch.as_tensor([[False, False, False], [True, False, False], [True, False, True]]),
             [[], [0], [0, 2]],
             "2D torch selector"),

            (np.array([[[False, False, True], [True, True, False], [True, False, True]],
                       [[True, False, False], [False, False, False], [True, True, True]]]),
             [[2], [0, 1], [0, 2], [0], [], [0, 1, 2]],
             "3D numpy selector"),

            (torch.as_tensor([[[False, False, True], [True, True, False], [True, False, True]],
                              [[True, False, False], [False, False, False], [True, True, True]]]),
             [[2], [0, 1], [0, 2], [0], [], [0, 1, 2]],
             "3D torch selector"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    @mock.patch("hirise.voronoi.renderer.PillowOcclusionRenderer.render",
                side_effect=lambda occs, target_shape: torch.rand(*target_shape))
    def test_batch_render_selects_proper_polygons(self, test_selector, raveled_selections, desc, render_mock):
        all_test_polygons = self.occlusion_vertices[:3]

        target_shape = self.default_target_shape
        expected_polysets = [[all_test_polygons[x] for x in s] for s in raveled_selections]
        expected_calls = [mock.call(polyset, target_shape=target_shape) for polyset in expected_polysets]

        # act
        self.renderer.batch_render(all_test_polygons, test_selector, self.default_target_shape)

        # assert
        render_mock.assert_has_calls(expected_calls)

    @parameterized.expand([
            (TypeError, 3, np.array([[0, 1, 2], [0, 2, 5]], dtype=int), "numpy selector dtype is not bool"),
            (TypeError, 3, torch.as_tensor([[0, 1, 2], [0, 2, 5]], dtype=torch.int32),
             "torch selector dtype is not bool"),
            (ValueError, 3, np.array([1, 0, 0], dtype=bool), "selector array is 1-dim"),
            (ValueError, 4, np.array([[1, 0, 0], [0, 1, 1]], dtype=bool),
             "last dim of selector array doesn't match the occlusion set size"),
            (ValueError, 0, np.array([[], []], dtype=bool), "empty set of occlusions with matching selector"),
            (ValueError, 0, np.array([[1, 0, 0], [0, 1, 1]], dtype=bool),
             "empty set of occlusions with mismatched selector")
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_batch_render_raises_on(self, expected_exception, occl_set_size, selector_array, desc):
        test_polygons = self.occlusion_vertices[:occl_set_size]
        with self.assertRaises(expected_exception):
            self.renderer.batch_render(test_polygons, selector_array, self.default_target_shape)

    @parameterized.expand([
            ((224, 224), "default test shape"),
            ((50, 50), "custom test shape"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_run_render(self, test_shape, desc):

        # act
        result = self.renderer.render(self.occlusion_vertices, test_shape)

        # assert
        self.assertIsInstance(result, torch.Tensor)
        self.assertSequenceEqual(result.shape, test_shape)

    @parameterized.expand([
            (np.random.rand, (3,), "2D selector"),
            (torch.rand, (3,), "2D selector"),

            (np.random.rand, (2, 3,), "3D selector"),
            (torch.rand, (2, 3,), "3D selector"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_run_batch_render(self, selector_provider, hyperdim, desc):
        random_selector = selector_provider(*hyperdim, len(self.occlusion_vertices)) < .5
        expected_shape = (*hyperdim, *self.default_target_shape)

        # act & assert - shouldn't raise
        result = self.renderer.batch_render(self.occlusion_vertices,
                                            occlusion_selectors=random_selector,
                                            target_shape=self.default_target_shape)

        self.assertIsInstance(result, torch.Tensor)
        self.assertSequenceEqual(result.shape, expected_shape)


class CommonRendererDecoratorTestFixture:

    def setUp(self):
        fv = FiniteVoronoi(np.random.rand(20, 2))
        self.polygons = fv.polygons

        mock_config = {"render":       mock.Mock(side_effect=lambda ocl, shape: torch.rand(*shape)),
                       "batch_render": mock.Mock(
                           side_effect=lambda ocl, selectors, shape: torch.rand(*selectors.shape[:-1], *shape))}
        self.renderer_mock = mock.Mock(spec=PillowOcclusionRenderer, **mock_config)

class CommonRendererDecoratorTests():
    pass

class CommonBlurDecoratorTests:

    def test_mask_tensor_raveling_works_in_blur_many(self):
        hypershape = (3, 4, 2)
        mask_shape = (100, 100)
        random_masks = torch.rand(*hypershape, *mask_shape)
        ravelled_masks = random_masks.reshape(-1, *mask_shape)
        sigma = 4

        # act
        ravelled_masks_result = self.tested_decorator._blur_many(ravelled_masks, sigma)
        unraveled_masks_result = self.tested_decorator._blur_many(random_masks, sigma)

        # assert
        self.assertTrue(
            torch.allclose(unraveled_masks_result, ravelled_masks_result.reshape(unraveled_masks_result.shape)))

    def test_render_calls_decorated_method(self):
        target_shape = (100, 100)

        # act
        self.tested_decorator.render(self.polygons, target_shape)

        # assert
        self.renderer_mock.render.assert_called_once_with(self.polygons, target_shape)

    def test_batch_render_calls_decorated_method(self):
        target_shape = (100, 100)
        selectors = torch.randint(1, (3, len(self.polygons))).to(torch.bool)

        # act
        self.tested_decorator.batch_render(self.polygons, selectors, target_shape)

        # assert
        self.renderer_mock.batch_render.assert_called_once_with(self.polygons, selectors, target_shape)


class TestFixedGaussianBlur(CommonRendererDecoratorTestFixture,
                            CommonRendererDecoratorTests,
                            CommonBlurDecoratorTests,
                            unittest.TestCase):

    def setUp(self):
        CommonRendererDecoratorTestFixture.setUp(self)

        self.sigma = 4
        self.tested_decorator = FixedGaussianBlur(self.renderer_mock, self.sigma)

    def test_blur_many_equivalent_to_calling_blur_one_for_each_mask(self):
        random_masks = torch.rand(6, 100, 100)
        sigma = 4

        # act
        blur_one_results = torch.stack([self.tested_decorator._blur_one(m, sigma) for m in random_masks])
        blur_many_results = self.tested_decorator._blur_many(random_masks, 4)

        # assert
        self.assertTrue(torch.allclose(blur_one_results, blur_many_results))


class OcclusionDependentGaussianBlurTests(CommonRendererDecoratorTestFixture,
                                         CommonRendererDecoratorTests,
                                         CommonBlurDecoratorTests,
                                         unittest.TestCase):

    def setUp(self):
        CommonRendererDecoratorTestFixture.setUp(self)

        self.min_sigma = 1
        self.max_sigma = 8
        self.tested_decorator = OcclusionDependentGaussianBlur(self.renderer_mock, self.min_sigma, self.max_sigma)



class CommonRenderShiftsDecoratorTests():

    def test_requests_a_render_in_higher_resolution(self):
        mask_shape = (100, 100)

        # act
        result = self.tested_decorator.render(self.polygons, mask_shape)

        # assert
        shape_matcher = NDArrayPredicate(lambda other: np.all(other > mask_shape))
        self.renderer_mock.render.assert_called_once_with(self.polygons, shape_matcher)

    def test_requests_batch_render_in_higher_resolution(self):
        hypershape = (3, 4, 2)
        mask_shape = (100, 100)
        selectors = torch.randint(1, (*hypershape, len(self.polygons))).to(torch.bool)

        # act
        result = self.tested_decorator.batch_render(self.polygons, selectors, mask_shape)

        # assert
        shape_matcher = NDArrayPredicate(lambda other: np.all(other > mask_shape))
        self.renderer_mock.batch_render.assert_called_once_with(self.polygons, selectors, shape_matcher)

    @parameterized.expand([
            (RandomRenderShift._shift_crop_one, (100, 100), (100, 100), "single mask - zero shift and same target shape"),
            (RandomRenderShift._shift_crop_one, (100, 100), (90, 90), "single mask - zero shift and smaller output shape, fixed aspect ratio"),
            (RandomRenderShift._shift_crop_one, (100, 100), (80, 60), "single mask - zero shift and smaller output shape, altered aspect ratio"),

            (RandomRenderShift._shift_crop_many, (3, 100, 100), (100, 100), "many masks - zero shift and same target shape"),
            (RandomRenderShift._shift_crop_many, (3, 100, 100), (90, 90), "many masks - zero shift and smaller target shape, fixed aspect ratio"),
            (RandomRenderShift._shift_crop_many, (3, 100, 100), (80, 60), "many masks - zero shift and smaller target shape, altered aspect ratio")
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shift_by_zero_returns_original_mask(self, method, initial_mask_size, output_shape, desc):
        shift_radius = 0
        initial_mask = torch.rand(*initial_mask_size)
        expected_shape = (*initial_mask_size[:-2], *output_shape)
        expected_result = initial_mask[..., 0:expected_shape[-2], 0:expected_shape[-1]]

        # act
        result = method(self.tested_decorator, initial_mask, shift_radius, output_shape)

        # assert
        self.assertTupleEqual(result.shape, expected_shape)
        self.assertTrue(torch.allclose(result, expected_result))


    @parameterized.expand([
            (RandomRenderShift._shift_crop_one, (100, 100), 2, (90, 90), "single mask - smaller output shape, fixed aspect ratio"),
            (RandomRenderShift._shift_crop_one, (100, 100), 2, (80, 60), "single mask - smaller output shape, altered aspect ratio"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shifted_mask_matches_one_of_possible_shifts_in_this_range(self, method, initial_mask_size, shift_range, output_shape, desc):
        initial_mask = torch.rand(*initial_mask_size)

        possible_shifts = itertools.product(range(shift_range+1), repeat=2)
        possible_slices = [(slice(y,y+output_shape[0]), slice(x,x+output_shape[1])) for y, x in possible_shifts]
        possible_results = [initial_mask[yslice, xslice] for yslice, xslice in possible_slices]

        # act
        result = method(self.tested_decorator, initial_mask, shift_range, output_shape)

        # assert
        self.assertTupleEqual(result.shape, output_shape)
        self.assertTrue(any(torch.allclose(result, possible_result) for possible_result in possible_results))


    @parameterized.expand([
            (RandomRenderShift._shift_crop_many, (3, 100, 100), 2, (90, 90), "many masks - smaller target shape, fixed aspect ratio"),
            (RandomRenderShift._shift_crop_many, (3, 100, 100), 2, (80, 60), "many masks - smaller target shape, altered aspect ratio")
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shifted_mask_matches_one_of_possible_shifts_in_this_range(self, method, initial_mask_size, shift_range, output_shape, desc):
        initial_masks = torch.rand(*initial_mask_size)
        expected_shape = (*initial_mask_size[:-2], *output_shape)

        possible_shifts = itertools.product(range(shift_range+1), repeat=2)
        possible_slices = [(slice(y,y+output_shape[0]), slice(x,x+output_shape[1])) for y, x in possible_shifts]
        possible_results = [[mask[yslice, xslice] for yslice, xslice in possible_slices] for mask in initial_masks]

        # act
        result = method(self.tested_decorator, initial_masks, shift_range, output_shape)

        # assert
        self.assertTupleEqual(result.shape, expected_shape)
        self.assertTrue(all(any(torch.allclose(mask, possible_result)
                                for possible_result
                                in possible_results_for_mask)
                            for mask, possible_results_for_mask
                            in zip(result, possible_results)))


    @parameterized.expand([
            (RandomRenderShift._shift_crop_one, (100, 100), 0, (120, 120), "crop single - output shape is larger than input shape"),
            (RandomRenderShift._shift_crop_many, (3, 100, 100), 0, (120, 120), "crop many - output shape is larger than input shape"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_on(self, method, initial_mask_size, shift_range, output_shape, desc):
        initial_mask = torch.rand(*initial_mask_size)

        with self.assertRaises(ValueError):
            method(self.tested_decorator, initial_mask, shift_range, output_shape)



class FixedRandomRenderShiftDecoratorTests(CommonRendererDecoratorTestFixture,
                                            CommonRendererDecoratorTests,
                                           CommonRenderShiftsDecoratorTests,
                                           unittest.TestCase):
    def setUp(self):
        CommonRendererDecoratorTestFixture.setUp(self)

        self.shift_radius = 3
        self.tested_decorator = FixedRandomRenderShift(self.renderer_mock, self.shift_radius)

    # is a more specific override
    def test_requests_a_render_in_higher_resolution(self):
        mask_shape = (100, 100)
        expected_raw_render_shape = np.array(mask_shape) + self.shift_radius

        # act
        result = self.tested_decorator.render(self.polygons, mask_shape)

        # assert
        shape_matcher = NDArrayMatcher(expected_raw_render_shape)
        self.renderer_mock.render.assert_called_once_with(self.polygons, shape_matcher)

    # is a more specific override
    def test_requests_batch_render_in_higher_resolution(self):
        hypershape = (3, 4, 2)
        mask_shape = (100, 100)
        expected_raw_render_shape = np.array(mask_shape) + self.shift_radius
        selectors = torch.randint(1, (*hypershape, len(self.polygons))).to(torch.bool)

        # act
        result = self.tested_decorator.batch_render(self.polygons, selectors, mask_shape)

        # assert
        shape_matcher = NDArrayMatcher(expected_raw_render_shape)
        self.renderer_mock.batch_render.assert_called_once_with(self.polygons, selectors, shape_matcher)


class OcclusionDependentRandomRenderShiftTests(CommonRendererDecoratorTestFixture,
                                                CommonRendererDecoratorTests,
                                               CommonRenderShiftsDecoratorTests,
                                               unittest.TestCase):
    def setUp(self):
        CommonRendererDecoratorTestFixture.setUp(self)

        self.tested_decorator = OcclusionDependentRandomRenderShift(self.renderer_mock)

    @parameterized.expand([
            ([UNIT_BOX], (100, 100), 100, "single rectangle the size of viewport"),
            ([UNIT_RHOMBUS], (100, 100), 100, "single rhombus with bounding box the size of the viewport"),
            ([scale(UNIT_RHOMBUS, 1 / 2, 1 / 2)], (100, 100), 50, "smaller rectangle shrinks the shift distance"),

            ([scale(UNIT_RHOMBUS, 1 / 2, 1 / 2),
              scale(UNIT_RHOMBUS, 1 / 4, 1 / 4),
              scale(UNIT_RHOMBUS, 1 / 3, 1 / 3)],
             (100, 100),
             25,
             "shift is adjusted to the smallest polygon in the set"),
    ],
            testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_shift_range_inference(self, occlusions, target_shape, expected_shift_range, desc):
        result = self.tested_decorator._infer_shift_range(occlusions, target_shape)

        self.assertEqual(result, expected_shift_range)


@skip # prevent unparameterized instance from being ran
@parameterized_class(("decorator", "desc"),
    [
        (lambda self, r: FixedGaussianBlur(r, sigma=4), "FixedGaussianBlur"),
        (lambda self, r: OcclusionDependentGaussianBlur(r, min_sigma=1, max_sigma=8), "OcclusionDependentGaussianBlur"),
        (lambda self, r: FixedRandomRenderShift(r, shift_radius=30), "FixedRandomRenderShift"),
        (lambda self, r: OcclusionDependentRandomRenderShift(r), "OcclusionDependentRandomRenderShift"),
    ],
class_name_func=lambda c,n,d: f"{c.__name__}.{d['desc']}")
class TestDecoratorInteg(RendererTestFixture,
                         CommonRendererTests,
                         unittest.TestCase):
    def setUp(self):
        RendererTestFixture.setUp(self)
        self.renderer = self.decorator(self.renderer)