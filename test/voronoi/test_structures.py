import unittest
from unittest import mock
from unittest.mock import Mock

import numpy as np
from parameterized import parameterized
from shapely.geometry import Polygon, Point

from hirise.voronoi.structures import FiniteVoronoi

from hirise.voronoi.structures import assemble_polygons

UNIT_BOX = Polygon([[0, 0], [1, 0], [1, 1], [0, 1]])
CROPPED_POLYGONS_MOCK = Mock()

class TestFiniteVoronoi(unittest.TestCase):

    def setUp(self) -> None:
        self.random_points = np.random.rand(10, 2)
        self.other_random_points = np.random.rand(3, 2)

        self.finite_voronoi = FiniteVoronoi(self.random_points, incremental=True)
        self.test_bounding_poly = UNIT_BOX

    def test_coord_range_defaults_to_unit_box(self):
        expected_fenceposts = FiniteVoronoi.make_distant_fenceposts((0, 1))

        # act
        vor = FiniteVoronoi(np.random.rand(3, 2))

        # assert
        self.assertTrue(
            np.allclose(vor.points[:len(expected_fenceposts)], expected_fenceposts),
            msg=f"Got: {vor.points[:len(expected_fenceposts)]}, expected: {expected_fenceposts}"
        )

    @parameterized.expand([
        ((0, 1), "(0, 1) range is used when coord_range is None"),
        ((-1, 2), "arbitrary range"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_fencepost_creation(self, coord_range, desc):
        expected_fenceposts = FiniteVoronoi.make_distant_fenceposts(coord_range)

        # act
        vor = FiniteVoronoi(np.random.rand(3, 2), coord_range=coord_range)

        # assert
        self.assertTrue(
            np.allclose(vor.points[:len(expected_fenceposts)], expected_fenceposts),
            msg=f"Got: {vor.points[:len(expected_fenceposts)]}, expected: {expected_fenceposts}"
        )

    @parameterized.expand([
        ((0, 1), "unit box"),
        ((0, 5), "all-positive, larger than unit box"),
        ((-1, 3), "mixed"),
        ((-5, -4.5), "all-negative, smaller then unit box"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_any_coord_range_is_accepted(self, coord_range, dest):
        scaled_points = np.random.rand(10, 2) * (coord_range[1] - coord_range[0]) + coord_range[0]
        initial_points, extra_points = scaled_points[:5], scaled_points[5:]

        # act & assert - neither of these can raise
        fvor = FiniteVoronoi(initial_points, coord_range=coord_range, incremental=True)
        fvor.add_points(extra_points)


    def test_even_a_single_seed_diagram_will_be_made_finite(self):
        single_point = np.random.rand(1, 2)

        # act
        vor = FiniteVoronoi(single_point)

        # assert
        self.assertEqual(len(vor.polygons), 1)
        self.assertTrue(vor.polygons[0].contains(Point(*single_point[:, ::-1])))


    def test_all_initial_points_must_fit_in_the_coord_range(self):
        points = np.array([[1, 1], [1, 3], [1, 5], [3, 1], [5, 1], [5, 5]])
        coord_range = (2, 4)

        with self.assertRaises(ValueError):
            FiniteVoronoi(points, coord_range, coord_range=coord_range)

    def test_added_points_must_fit_in_the_coord_range(self):
        points = np.array([[1, 1], [1, 3], [1, 5], [3, 1], [5, 1], [5, 5]])
        initial_points = np.array([[3, 3]]) # fits in the initial range
        coord_range = (2, 4)
        fvor = FiniteVoronoi(initial_points, coord_range=coord_range, incremental=False)

        # act & assert
        with self.assertRaises(ValueError):
            fvor.add_points(points)


    def test_list_of_polygons_includes_only_seed_regions(self):
        seeds = self.random_points
        vor = FiniteVoronoi(seeds)

        self.assertEqual(len(vor.polygons), len(seeds))
        self.assertFalse(
            any(
                any(polygon.contains(point) for polygon in vor.polygons)
                for point
                in map(Point, vor._fenceposts[:, ::-1])
            ),
            msg="Expected no polygon to overlap with any of the fenceposts"
        )
        self.assertTrue(
            all(
                sum(1 for polygon in vor.polygons if polygon.contains(point)) == 1
                for point
                in map(Point, seeds[:, ::-1])
            ),
            msg="Expected all polygons to overlap with exactly one voronoi seed"
        )

    def test_seeds_list_contains_all_points_provided_by_user(self):
        initial_points = self.random_points
        extra_points = self.other_random_points

        vor = FiniteVoronoi(initial_points, incremental=True)
        self.assertTrue(
            np.allclose(vor.seeds, initial_points),
            msg=f"""
            Expected seeds list to be equal to list of seeds, got: 
            provided points: {initial_points} 
            returned seeds: {vor.seeds}
            """
        )

        vor.add_points(extra_points)
        self.assertTrue(
            np.allclose(vor.seeds, np.concatenate((initial_points, extra_points))),
            msg="Expected seeds list to be equal to the list of all added points"
        )

    @mock.patch('hirise.voronoi.structures.assemble_polygons', side_effect=lambda *args: assemble_polygons(*args))
    def test_list_of_polygons_is_not_generated_until_first_access(self, assemble_polygons_mock):
        # arrange and pre-conditions
        points = self.random_points
        vor = FiniteVoronoi(points)
        assemble_polygons_mock.assert_not_called()

        # act
        polygons = vor.polygons

        # assert
        assemble_polygons_mock.assert_called_once()
        self.assertEqual(len(polygons), len(points))

    @mock.patch('hirise.voronoi.structures.assemble_polygons', side_effect=lambda *args: assemble_polygons(*args))
    def test_polygon_list_is_reassembled_on_first_access_after_adding_new_points(self, assemble_polygons_mock):
        # arrange and pre-conditions
        points = self.random_points
        more_points = self.other_random_points

        vor = FiniteVoronoi(points)
        polygons = vor.polygons
        assemble_polygons_mock.reset_mock()
        vor.add_points(more_points)
        assemble_polygons_mock.assert_not_called()

        # act
        polygons = vor.polygons

        # assert
        assemble_polygons_mock.assert_called_once()
        self.assertEqual(len(polygons), len(np.concatenate((points, more_points))))


    def test_raises_on_attempt_to_add_points_if_incremental_is_false(self):
        final_voronoi = FiniteVoronoi(self.random_points, incremental=False)
        with self.assertRaisesRegex(RuntimeError, 'incremental mode not enabled'):
            final_voronoi.add_points(self.other_random_points)

    @parameterized.expand([
        (np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         "works fine for minimal non-cocircular set of points"),

        (np.array([[0,1], [1,0], [1,1], [.1,.1], [1,0]]),
         np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         "duplicates points in the original set are removed"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_create_voronoi(self, initial_points, expected_points, desc):
        expected_points.sort(axis=0)

        # act
        fv = FiniteVoronoi(initial_points)

        # assert
        result = np.copy(fv.seeds)
        result.sort(axis=0)
        self.assertTrue(np.allclose(result, expected_points),
                        msg=f"Expected:\n{expected_points}\ngot:\n{result}")


    @parameterized.expand([
        (np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         np.array([[0,1], [1,0]]),
         np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         "nothing changes when all added points already exist in the diagram"),

        (np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         np.array([[0,1], [.5, .5], [.1, .9]]),
         np.array([[0,1], [1,0], [1,1], [.1,.1], [.5, .5], [.1, .9]]),
         "only new points are added from a mix of new and existing"),

        (np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         np.array([[.3, .2], [.5, .8]]),
         np.array([[0,1], [1,0], [1,1], [.1,.1], [.3, .2], [.5, .8]]),
         "all points are added if all are new"),

        (np.array([[0,1], [1,0], [1,1], [.1,.1]]),
         np.array([[.3, .2], [.5, .8], [.5, .8]]),
         np.array([[0,1], [1,0], [1,1], [.1,.1], [.3, .2], [.5, .8]]),
         "duplicates within the set of added points are discarded before adding to the diagram"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_existing_points_are_skipped_on_add_points(self, initial_points, points_to_add, expected_state, desc):
        fv = FiniteVoronoi(initial_points)
        expected_state.sort(axis=0)

        # act
        fv.add_points(points_to_add)

        # assert
        state_after = np.copy(fv.seeds)
        state_after.sort(axis=0) # sort values to make sure that points match
        self.assertTrue(np.allclose(state_after, expected_state),
                        msg=f"Expected:\n{expected_state}\ngot:\n{state_after}")

    def test_polygon_coords_form_valid_polygons(self):
        fvor = FiniteVoronoi(np.random.rand(5, 2))
        polygons_from_coords = list(map(Polygon, fvor.polygon_coords))

        self.assertTrue(all(polygon.symmetric_difference(polygon.convex_hull).is_empty for polygon in polygons_from_coords))
