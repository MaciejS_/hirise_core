import unittest
from unittest import mock

import more_itertools
import torch
from parameterized import parameterized

import itertools
import numpy as np

from shapely.affinity import translate, scale
from shapely.geometry import Polygon, Point

from hirise.voronoi.utils import polymask, assemble_polygons

UNIT_BOX = Polygon([[0, 0], [1, 0], [1, 1], [0, 1]])
UNIT_RHOMBUS = Polygon([[.5, 0], [1, .5], [.5, 1], [0, .5]])
UNIT_OCTAGON = Polygon([[0, .33], [0, .66], [.33, 1], [.66, 1], [1, .66], [1, .33], [.66, 0], [.33, 0]])

DEFAULT_SHAPE = (100, 100)
DEF_RANGE = (0, 100)

def _make_square(coord_range: tuple):
    vmin, vmax = coord_range
    return np.asarray([
        [vmin, vmin],
        [vmin, vmax],
        [vmax, vmax],
        [vmax, vmin]
    ])

def _make_hex(coord_range: tuple):
    vmin, vmax = coord_range
    d = vmax - vmin
    return np.asarray([
        [vmin,           vmin + d * 1/4],
        [vmin,           vmin + d * 3/4],
        [vmin + d * 1/2, vmax],
        [vmax,           vmax - d * 1/4],
        [vmax,           vmax - d * 3/4],
        [vmax - d * 1/2, vmin],
    ])

def _make_octagon(coord_range: tuple):
    vmin, vmax = coord_range
    d = vmax - vmin
    return np.asarray([
        [vmin,           vmin + d * 1/3],
        [vmin,           vmin + d * 2/3],
        [vmin + d * 1/3, vmax],
        [vmin + d * 2/3, vmax],
        [vmax,           vmax - d * 1/3],
        [vmax,           vmax - d * 2/3],
        [vmax - d * 1/3, vmin],
        [vmax - d * 2/3, vmax],
    ])

def poly2coords(polygon: Polygon) -> np.ndarray:
    return np.asarray(polygon.exterior.coords)

def split_by_selector(mask:np.ndarray, xsel:slice, ysel:slice):
    """
    "Splits" the `mask` into "positive" and "negative" selections.
    Positive selection is a slice performed with xsel and ysel (applied along their respective axes)
    Negative selection is the result of masking out the values captured by the selector.
    """
    positive_selection = mask[ysel, xsel]
    _neg_selector = np.ones_like(mask)
    _neg_selector[ysel, xsel] = 0
    negative_selection = mask * _neg_selector
    return positive_selection, negative_selection

class TestAssemblePolygons(unittest.TestCase):

    def setUp(self) -> None:
        N_vertices = 12
        N_polygons = 5
        self.random_vertices = np.random.rand(N_vertices * 2).reshape(-1, 2)
        self.idx_lists = [list(more_itertools.random_combination(range(N_vertices), r=r))
                          for r
                          in np.random.randint(3, N_vertices, N_polygons)]
        self.regions = self.idx_lists

        self.expected_vertex_sets = [self.random_vertices[idxs] for idxs in self.regions]

        self.result = assemble_polygons(self.regions, self.random_vertices)

    @mock.patch("hirise.voronoi.utils.Polygon")
    def test_polygons_are_assembled_from_right_vertices(self, polygon_mock):
        assemble_polygons(self.regions, self.random_vertices)

        self.assertEqual(len(polygon_mock.call_args_list), len(self.regions))
        self.assertTrue(all(np.allclose(args[0], expected_vertices)
                            for (args, kwargs), expected_vertices
                            in zip(polygon_mock.call_args_list, self.expected_vertex_sets)))

    def test_result_is_a_tuple_of_polygons(self):
        self.assertIsInstance(self.result, tuple)
        self.assertTrue(all(isinstance(elem, Polygon) for elem in self.result))


class TestPolyMask(unittest.TestCase):

    def setUp(self) -> None:
        self.default_params = {"fill_value":    1,
                               "outline_value": 1,
                               "mask_mode":     'L'}

        random_polys = tuple(itertools.chain.from_iterable(itertools.repeat([UNIT_BOX, UNIT_RHOMBUS], 3)))
        # random_shifts don't shift further than .5 in any direction, guaranteeing intersection with the default bounding box
        random_shifts = np.random.rand(len(random_polys) * 3).reshape(-1, 2) - .5
        self.random_intersecting_polygons = tuple(map(lambda p, s: translate(p, *s), random_polys, random_shifts))

    @parameterized.expand([
            (0, 0),
            (1, .11)
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{str(p.args[-1]).replace('.', ',')}")
    def test_edge_pixel_value_when_outline_is(self, outline_value, expected_edge_fill_rate):
        small_square = _make_square((0, 10))
        target_shape = (100, 100)

        self.default_params["outline_value"] = outline_value

        result_array = polymask(
            small_square,
            target_shape=target_shape,
            scale_to_target=False,
            **self.default_params
        )

        # test edges are drawn with outline_value
        self.assertAlmostEqual(result_array[0,  :].mean(), expected_edge_fill_rate)
        self.assertAlmostEqual(result_array[:,  0].mean(), expected_edge_fill_rate)
        self.assertAlmostEqual(result_array[10, :].mean(), expected_edge_fill_rate)
        self.assertAlmostEqual(result_array[:, 10].mean(), expected_edge_fill_rate)

    @parameterized.expand([
            (0, .25 - .01, "slightly less than expected"),
            (1, .25 + .01, "slightly more than expected")
    ],
    testcase_func_name=lambda f,n,p: f"test_when_outline_is.{p.args[0]}, then rendered area is {p.args[-1]}")
    def test_area_when_outline_is(self, outline_value, expected_fill_rate, desc):
        centered_square = _make_square((25, 75))

        result = polymask(
            centered_square,
            target_shape=(100, 100),
            outline_value=outline_value,
            scale_to_target=False,
        )

        self.assertAlmostEqual(result.mean(), expected_fill_rate, places=3)

    def test_drawing_multiple_polygons(self):
        target_shape = DEFAULT_SHAPE
        polygon_a = translate(scale(UNIT_BOX, .2, .2, origin=Point([0, 0])), .6, .6)
        polygon_b = translate(scale(UNIT_BOX, .2, .3, origin=Point([0, 0])), .2, .2)
        polygons = [polygon_a, polygon_b]

        poly_a_selector = (slice(60, 80), ) * 2
        poly_b_selector = (slice(20, 50), slice(20, 40))

        result_array = polymask(list(map(poly2coords, polygons)),
                                target_shape=target_shape,
                                **self.default_params)

        self.assertEqual(result_array[poly_a_selector].mean(), self.default_params["fill_value"])
        self.assertEqual(result_array[poly_b_selector].mean(), self.default_params["fill_value"])

        expected_fill_area = (polygon_a.area + polygon_b.area)
        self.assertAlmostEqual(result_array.mean(), expected_fill_area, delta=0.05)

    def test_drawing_polygons_extending_past_the_plane(self):
        test_polygon = translate(UNIT_BOX, -.5, -.5)
        overlap_area = .25 # the test_polygon overlaps only a quarter of the UNIT_BOX now

        result_array = polymask(poly2coords(test_polygon), target_shape=(100, 100), outline_value=0)

        # assert
        self.assertAlmostEqual(result_array.mean(), overlap_area, delta=.02)

        # because the polygon extends past the edges, top row and leftmost column aren't all empty
        self.assertEqual(result_array[0,  :].mean(), 0.5)
        self.assertEqual(result_array[:,  0].mean(), 0.5)

    @parameterized.expand([
        (_make_square((0, 1)), slice(0, 2), "a (0,1) square will render as one pixel + one pixel of outline"),
        (_make_square((0, 50)), slice(0, 51), "a (0, 50) square will cover 2500 pixels"),
        (_make_square((25, 50)), slice(25, 51), "a (25, 50) square covers central 625px"),
        (_make_square((-25, 25)), slice(0, 26), "a (-25, 25) square will only be visible in range (0, 25)"),
    ], testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_drawing_of_rectangles(self, polygon, selector, desc):
        mask = polymask(polygon, outline_value=1, target_shape=DEFAULT_SHAPE, scale_to_target=False)

        positive_selection, negative_selection = split_by_selector(mask, selector, selector)

        # assert
        self.assertTrue(np.all(positive_selection == 1))
        self.assertTrue(np.all(negative_selection == 0))


    @parameterized.expand([
            ((200, 100),),
            ((50, 50),),
            ((100, 200),),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.for {p.args[-1]}")
    def test_result_shape_is_equal_to_target_shape(self, mask_shape):
        result = polymask(_make_square((0, max(mask_shape))), target_shape=mask_shape)
        self.assertTupleEqual(result.shape, mask_shape)

    @parameterized.expand([
        ([], "empty sequence"),
        (_make_square((0, 100)), "numpy. array of vertex coords"),
        ([_make_square((0, 100))], "numpy. sequence containing a single coord ndarray"),
        ([_make_square((0, 100)), _make_hex((0, 100))], "numpy. sequence of coord ndarrays"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_accepts_polygons_as(self, polygon_set, desc):
        # act & assert - should not raise
        polymask(polygon_set, target_shape=DEFAULT_SHAPE)

    @parameterized.expand([
        (UNIT_BOX, "shapely: standalone polygon, not packed in a sequence"),
        ([UNIT_BOX], "shapely: sequence containing a single polygon"),
        ([UNIT_BOX, UNIT_RHOMBUS], "shapely: sequence of polygons"),

        (torch.as_tensor(_make_square((0, 30))), "torch. standalone coord set"),
        ([torch.as_tensor(_make_square((0, 30)))], "torch. single coord set in a list"),
        (
            list(map(torch.as_tensor, (_make_square((0, 30)), _make_hex((0, 20))))),
            "torch. multiple coord sets in a list"
        ),
    ], testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_if_polygon_is_passed_as(self, polygon_set, desc):
        # act & assert
        with self.assertRaises((AttributeError, TypeError)):
            polymask(polygon_set, target_shape=DEFAULT_SHAPE)


    @parameterized.expand([
        (UNIT_BOX, (10, 10), True, slice(None, None), "scaling on - unit box covers the entire canvas"),
        (
            scale(UNIT_BOX, .5, .5, origin=Point([0,0])),
            (10, 10),
            True,
            slice(0, 6),
            "scaling on - shrunk unit box covers only a part of the entire canvas"
        ),
        (UNIT_BOX, (10, 10), False, slice(0, 2), "scaling off - unit box will occupies 4% of a 10x10 canvas (outline gives extra 3 pixels)"),
        (
            scale(UNIT_BOX, 10, 10, origin=Point([0,0])),
            (10, 10),
            False,
            slice(None, None),
            "scaling off - 10x scaled unit box covers entire 10x10 canvas"
        )
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_scaling_mode(self, polygon, target_shape, do_scaling, selector, desc):
        mask = polymask(
            np.asarray(polygon.exterior.coords),
            target_shape,
            scale_to_target=do_scaling
        )

        positive_selection, negative_selection = split_by_selector(mask, selector, selector)

        # assert
        self.assertTrue(np.all(positive_selection == 1))
        self.assertTrue(np.all(negative_selection == 0))

    def test_input_axes_arent_swapped(self):
        polygon = scale(UNIT_BOX, yfact=.5, origin=Point([0,0]))
        mask = polymask(poly2coords(polygon), DEFAULT_SHAPE)
        ysel, xsel = slice(0, 51), slice(None, None)

        positive_selection, negative_selection = split_by_selector(mask, xsel, ysel)

        # assert
        self.assertTrue(np.all(positive_selection == 1))
        self.assertTrue(np.all(negative_selection == 0))


    def test_scaling_factor_axes_arent_swapped(self):
        shape = (50, 100)
        polygon = scale(UNIT_BOX, yfact=.5, xfact=.5, origin=Point([0,0]))
        mask = polymask(poly2coords(polygon), shape)
        ysel, xsel = slice(0, 26), slice(0, 51)

        positive_selection, negative_selection = split_by_selector(mask, xsel, ysel)

        # assert
        self.assertTrue(np.all(positive_selection == 1))
        self.assertTrue(np.all(negative_selection == 0))
