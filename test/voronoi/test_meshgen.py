import unittest
import numpy as np

from unittest import mock

from parameterized import parameterized

from itertools import product

from hirise.voronoi.meshgen import *
from hirise.voronoi.structures import FiniteVoronoi
from shapely.geometry import Polygon, Point

from scipy.spatial.qhull import QhullError


class TestUniformRandomSeeds(unittest.TestCase):

    @parameterized.expand([
            (ndim, n_seeds) for ndim in (1, 2, 3) for n_seeds in (1, 3, 8)
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.ndim={p.args[-2]}, n_seeds={p.args[-1]}")
    def test_result_shape(self, ndim, n_seeds):
        provider = UniformRandomSeeds(ndim)
        expected_shape = (n_seeds, ndim)

        # act
        result = provider(n_seeds)

        # assert
        self.assertTupleEqual(result.shape, expected_shape)


class TestCheckerboardPatternSeeds(unittest.TestCase):

    @parameterized.expand([
            (1, 2**1, product((.25, .75), repeat=1)),
            (1, 3**1, product((1/6, 3/6, 5/6), repeat=1)),
            (1, 4**1, product((1/8, 3/8, 5/8, 7/8), repeat=1)),

            (2, 2**2, product((.25, .75), repeat=2)),
            (2, 3**2, product((1/6, 3/6, 5/6), repeat=2)),
            (2, 4**2, product((1/8, 3/8, 5/8, 7/8), repeat=2)),

            (3, 2**3, product((.25, .75), repeat=3)),
            (3, 3**3, product((1/6, 3/6, 5/6), repeat=3)),
            (3, 4**3, product((1/8, 3/8, 5/8, 7/8), repeat=3)),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.for ndim={p.args[-3]} and n_seeds={p.args[-2]}")
    def test_result_values(self, ndim, n_seeds, expected_coords):
        provider = CheckerboardPatternSeeds(ndim)
        expected_coords = np.array(tuple(expected_coords))[..., ::-1] # mgrid values change most frequently in the first column

        # act
        result = provider(n_seeds)

        # assert
        expected_shape = (n_seeds, ndim)
        self.assertTupleEqual(result.shape, expected_shape, msg=f"Expected shape {expected_shape}, got {result.shape}")
        self.assertTrue(np.allclose(expected_coords, result), msg=f"Expected\n{expected_coords}, got\n{result}")

    @parameterized.expand([
            (2, 6, 4),
            (2, 7, 9),
            (2, 12, 9),
            (2, 13, 16)
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.ndim={p.args[-3]}, n_seeds={p.args[-2]} is corrected to {p.args[-1]}")
    def test_number_of_seeds_is_rounded_to_nearest_power(self, ndim, n_seeds, expected_seed_count):
        provider = CheckerboardPatternSeeds(ndim)

        result = provider(n_seeds)

        self.assertEqual(len(result), expected_seed_count)


class CommonVoronoiMeshGenTests:

    def test_raises_for_negative_N(self):
        with self.assertRaises(Exception):
            self.live_meshgen.create_mesh(-1)

    @parameterized.expand([
            (4, ), (9, ), (15, )
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_works_for_N_equal(self, N):
        result = self.live_meshgen.create_mesh(N) # shouldn't raise
        self.assertIsInstance(result, FiniteVoronoi)

    @parameterized.expand([
            (np.random.rand(4, 2), "4 points"),
            (np.random.rand(9, 2), "5 points"),
            (np.random.rand(15, 2), "8 points"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_for_N_points(self, test_points, desc):
        result = self.live_meshgen.create_mesh_from_points(test_points) # shouldn't raise
        self.assertIsInstance(result, FiniteVoronoi)


class TestVoronoiMeshGen(CommonVoronoiMeshGenTests,
                         unittest.TestCase):

    def setUp(self) -> None:
        self.seed_provider_mock = mock.Mock(spec = SeedCoordinateProvider)
        self.mocked_mesh_gen = VoronoiMeshGen(self.seed_provider_mock)
        self.live_meshgen = VoronoiMeshGen(seed_coordinate_provider=UniformRandomSeeds(ndim=2))

    @parameterized.expand([
            (True, ),
            (False, ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.incremental_mesh set to {p.args[-1]}")
    @mock.patch("hirise.voronoi.meshgen.FiniteVoronoi")
    def test_incremental_mesh_param_is_respected(self, is_incremental, fv_init):
        mg = VoronoiMeshGen(self.seed_provider_mock, incremental_mesh=is_incremental)
        # dummy_coords = np.random.rand(10, 2)
        coords_mock = mock.Mock(spec=np.ndarray)

        mg.create_mesh_from_points(coords_mock)

        fv_init.assert_called_once_with(coords_mock, incremental=is_incremental)

    @mock.patch("hirise.voronoi.meshgen.VoronoiMeshGen.create_mesh_from_points")
    def test_creation_from_N_calls_seed_provider_and_delegates_to_create_from_points(self, mesh_from_points_mock):
        dummy_N = 10
        self.seed_provider_mock.return_value = mock.Mock(spec=np.ndarray)
        self.seed_provider_mock.assert_not_called() # precondition

        self.mocked_mesh_gen.create_mesh(dummy_N)

        self.seed_provider_mock.assert_called_once_with(dummy_N)
        mesh_from_points_mock.assert_called_once_with(self.seed_provider_mock.return_value)

    @mock.patch("hirise.voronoi.meshgen.FiniteVoronoi")
    def test_creation_from_points(self, fv_init):
        incremental_setting_mock = mock.Mock()
        self.mocked_mesh_gen.incremental_mesh = incremental_setting_mock
        points_mock = mock.Mock()
        fv_init.return_value = mock.Mock(spec_set=FiniteVoronoi)

        # act
        mesh = self.mocked_mesh_gen.create_mesh_from_points(points_mock)

        # assert
        fv_init.assert_called_once_with(points_mock, incremental=incremental_setting_mock)
        self.assertIs(mesh, fv_init.return_value)
