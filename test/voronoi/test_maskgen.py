import itertools
import operator
from unittest import TestCase, mock
from unittest.mock import call, patch

import more_itertools
import numpy as np
import torch
from hirise.occlusions.gridgen import ThresholdGridGen
from parameterized import parameterized
from hirise.voronoi.maskgen import VoronoiMaskGen
from hirise.voronoi.meshgen import VoronoiMeshGen
from hirise.voronoi.renderer import PillowOcclusionRenderer
from hirise.voronoi.structures import FiniteVoronoi


class TestVoronoiMaskGen(TestCase):

    def setUp(self):
        self.cpu = torch.device("cpu")

        self.mesh_gen = VoronoiMeshGen()
        self.grid_gen = ThresholdGridGen(self.cpu)
        self.renderer = PillowOcclusionRenderer(self.cpu)

        fake_renderer_logic = lambda occlusions, shape: torch.rand(*shape)
        fake_batch_renderer_logic = lambda occlusions, selectors, shape: torch.rand(*selectors.shape[:-1], *shape)
        self.renderer_mock = mock.Mock(
            spec=VoronoiMaskGen,
            **dict(
                render=mock.Mock(side_effect=fake_renderer_logic),
                batch_render=mock.Mock(side_effect=fake_batch_renderer_logic),
                _one_hot_select=mock.Mock(side_effect=PillowOcclusionRenderer._one_hot_select)
            )
        )

        self.maskgen = VoronoiMaskGen(mesh_gen=self.mesh_gen,
                                      occlusion_selector=self.grid_gen,
                                      renderer=self.renderer)

    def test_batch_render_shape(self):
        N = 10
        target_shape = (100, 100)
        masks = self.maskgen.generate_masks(N=N, M=2, s=49, p1=0.3, target_shape=target_shape)
        
        self.assertTupleEqual(masks.shape, (N, *target_shape))

    def test_masks_from_mesh(self):
        N, p1, target_shape = 7, .5, (20, 20)
        voronoi = FiniteVoronoi(points=np.random.rand(10, 2))
        render_result_mock = mock.Mock()
        self.renderer_mock.batch_render.side_effect = [render_result_mock]
        maskgen = VoronoiMaskGen(self.mesh_gen, self.grid_gen, self.renderer_mock)

        # act
        masks = maskgen.masks_from_mesh(N, voronoi=voronoi, p1=p1, target_shape=target_shape)

        # assert
        self.renderer_mock.batch_render.assert_called_once()
        self.assertEqual(masks, render_result_mock)

        (polygons, selectors, target_shape), _ = self.renderer_mock.batch_render.call_args_list[0]
        self.assertEqual(polygons, voronoi.polygon_coords)
        self.assertTupleEqual(selectors.shape, (N, len(voronoi.polygon_coords)))
        self.assertTupleEqual(target_shape, target_shape)

    @parameterized.expand(
        [
            (3, 3, (1, 1, 1), ""),
            (5, 3, (2, 2, 1), ""),
            (6, 3, (2, 2, 2), ""),

            (3, 1, (3,), ""),
            (12, 2, (6, 6), ""),

            (3, 4, (1, 1, 1, 0), "(last will not be used)"),
        ],
        testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[0]} masks from {p.args[1]} meshes"
                                         f" = {p.args[2]} masks,"
                                         f" {sum(p.args[2])} in total"
                                         f" {p.args[-1]}"
    )
    def test_multimesh_logic(self, N, M, expected_masks_per_mesh, desc):
        with patch.object(self.mesh_gen, 'create_mesh', wraps=self.mesh_gen.create_mesh) as create_mesh_mock:
            self.maskgen = VoronoiMaskGen(mesh_gen=self.mesh_gen,
                                          occlusion_selector=self.grid_gen,
                                          renderer=self.renderer_mock)

            # act
            occl_count = 49
            self.maskgen.generate_masks(N=N, M=M, s=occl_count, p1=0.5, target_shape=(10, 10))

            # assert
            # Assert that meshgen has created M unique meshes
            create_mesh_mock.assert_has_calls([call(occl_count)] * M)

            # Assert that renderer has used each mesh once
            renderer_calls = self.renderer_mock.batch_render.call_args_list
            used_meshes = tuple(polygons for (polygons, *_), *_ in renderer_calls)
            self.assertEqual(len(renderer_calls), M)
            self.assertTrue(
                all(
                    all(
                        not any(id(a) == id(b) for a, b in zip(used_meshes[i], other_mesh))
                        for other_mesh
                        in used_meshes[i + 1:]
                    )
                    for i
                    in range(len(used_meshes) - 2)
                )
            )

            # Assert that each mesh requested an expected number of masks
            mask_counts = tuple(len(selector)
                                for (_, selector, _), *_
                                in renderer_calls)
            self.assertTrue(
                    all(n == expected_n for n, expected_n in zip(mask_counts, expected_masks_per_mesh)),
                    msg=f"Expected {expected_masks_per_mesh} masks per mesh, got {mask_counts}"
            )

    def test_yield_masks(self):
        maskgen = VoronoiMaskGen(self.mesh_gen, self.grid_gen, self.renderer_mock)

        M = 3
        n_polys = 9
        target_shape = (20, 20)
        masks_iterator = maskgen.yield_masks(M=M, s=n_polys, p1=.3, target_shape=target_shape)
        expected_selector_shape = (1, n_polys)

        # act
        masks = more_itertools.take(7, masks_iterator)

        # assert
        self.assertTrue(all(mask.shape == target_shape for mask in masks))

        calls_chronological = self.renderer_mock.batch_render.call_args_list
        self.assertTrue(all(shape == target_shape for (_, _, shape), _ in calls_chronological))
        self.assertTrue(all(selector.shape == expected_selector_shape for (_, selector, _), _ in calls_chronological))
        # check that polygon sets in subsequent calls always differ
        self.assertTrue(all(
            not all(
                any((poly1.shape == poly2.shape) and np.allclose(poly1, poly2) for poly1 in ps1)
                for poly2
                in ps2
            )
            for ps1, ps2
            in more_itertools.pairwise(
                mesh
                for (mesh, *_), _
                in calls_chronological
            )
        ))

        # assert if meshes are used in proper round-robin order
        meshes_used = [[mesh for (mesh, *_), _ in call_group]
                       for call_group
                       in more_itertools.distribute(3, calls_chronological)
                       ]

        self.assertTrue(
                all(
                        all(itertools.starmap(operator.eq, more_itertools.pairwise(mesh_group)))
                        for mesh_group
                        in meshes_used
                )
        )


class VoronoiMaskGenIntegrationTests(TestCase):
    """
    These tests are final acceptance tests meant to catch e.g. changes in interfaces or default behaviour,
    which could slip past mocked unittests.
    """
    def setUp(self) -> None:
        self.meshgen = VoronoiMeshGen()
        self.device_cpu = torch.device('cpu')
        self.selector = ThresholdGridGen(self.device_cpu)
        self.renderer = PillowOcclusionRenderer(self.device_cpu)

        self.maskgen = VoronoiMaskGen(mesh_gen=self.meshgen, occlusion_selector=self.selector, renderer=self.renderer)

    @parameterized.expand([
        (0, lambda masks: torch.all(masks.flatten() == 0), "all empty when p1 == 0"),
        (1, lambda masks: torch.all(masks.flatten() == 1), "all full when p1 == 1"),
        (.5, lambda masks: all((mask.mean() > 0) and (mask.mean() < 1) for mask in masks), "all neither empty nor full when (0 < p1 < 1)"),
    ], testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_of_generate_masks(self, p1, validator, desc):
        masks = self.maskgen.generate_masks(N=100, M=10, s=50, p1=p1, target_shape=(100, 100))
        self.assertTrue(validator(masks))

    @parameterized.expand([
        (0, lambda masks: torch.all(masks.flatten() == 0), "all empty when p1 == 0"),
        (1, lambda masks: torch.all(masks.flatten() == 1), "all full when p1 == 1"),
        (.5, lambda masks: all((mask.mean() > 0) and (mask.mean() < 1) for mask in masks), "all neither empty nor full when (0 < p1 < 1)"),
    ], testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_of_yield_masks(self, p1, validator, desc):
        mask_iter = self.maskgen.yield_masks(10, 50, p1, target_shape=(100, 100))
        masks = torch.stack(more_itertools.take(100, mask_iter))
        self.assertTrue(validator(masks))

    @parameterized.expand([
        (0, lambda masks: torch.all(masks.flatten() == 0), "all empty when p1 == 0"),
        (1, lambda masks: torch.all(masks.flatten() == 1), "all full when p1 == 1"),
        (.5, lambda masks: all((mask.mean() > 0) and (mask.mean() < 1) for mask in masks),
         "all neither empty nor full when (0 < p1 < 1)"),
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_results_of_masks_from_mesh(self, p1, validator, desc):
        mesh = FiniteVoronoi(np.random.rand(50, 2))
        masks = self.maskgen.masks_from_mesh(N=100, voronoi=mesh, p1=p1, target_shape=(100, 100))
        self.assertTrue(validator(masks))

