import itertools
import unittest
from unittest import mock, skip

from hirise.experiment.utils import *
from parameterized import parameterized_class, parameterized


def positive_num_gen():
    counter = 0
    while True:
        yield counter
        counter += 1

class CommonNormalizeTests():

    def test_normalize_does_not_change_shape(self):
        shape = (2, 5)
        input = np.random.rand(10).reshape(shape)

        self.assertTupleEqual(self.normalize_func(input).shape, shape)

    def test_normalize_default_target_range_is_0_to_1(self):
        expected_result = np.array([0,1])
        input = np.array([0.2, 1.8])

        self.assertTrue(np.allclose(self.normalize_func(input), expected_result))

    def test_normalize_stretches_input_to_target_range(self):
        input = np.array([0.2, 0.4, 0.6, 0.8])
        expected_result = np.array([0, 1/3, 2/3, 1])

        self.assertTrue(np.allclose(self.normalize_func(input), expected_result))

    def test_normalize_with_target_range(self):
        input = np.array([0.2, 0.4, 0.6, 0.8])
        target_range = (0.1, 1.3)
        expected_result = np.array([0.1, 0.5, 0.9, 1.3])

        self.assertTrue(np.allclose(self.normalize_func(input, new_range=target_range), expected_result))

    def test_original_range(self):
        input = np.array([0.2, 0.4, 0.6, 0.8])
        original_range = (0, 1)
        target_range = (1, 2)
        expected_result = np.array([1.2, 1.4, 1.6, 1.8])

        self.assertTrue(np.allclose(self.normalize_func(input, new_range=target_range, original_range=original_range),
                                    expected_result))

    def test_on_mix_of_positive_and_negative(self):
        input = np.array([0.2, 0.4, 0.6, 0.8])
        original_range = (0, 1)
        target_range = (-5, 5)
        expected_result = np.array([-3, -1, 1, 3])

        self.assertTrue(np.allclose(self.normalize_func(input, new_range=target_range, original_range=original_range),
                                    expected_result))

    @parameterized.expand([
        (np.arange(10000) / (10000 - 1), "fully streched"),
        (np.arange(10000) / (10000), "almost fully stretched")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_normalize_already_normalized_array(self, array, desc):
        expected_result = np.arange(10000) / (10000 - 1)

        self.assertTrue(np.allclose(self.normalize_func(array),
                                    expected_result))



class TestNumpyNormalize(CommonNormalizeTests,
                         unittest.TestCase):

    def setUp(self) -> None:
        self.normalize_func = normalize

    @skip("Not implemented yet")
    def test_works_with_axes(self):
        pass

class TestTorchNormalize(CommonNormalizeTests,
                         unittest.TestCase):

    def setUp(self) -> None:
        self.normalize_func = self.normalize_func_for_tests
        self.expected_result = torch.tensor([[0, 0.5, 1],
                                             [0.25, 0.5, 0.75]])
        self.input = torch.tensor([[0.2, 0.5, 0.8],
                                   [0.35, 0.5, 0.65]])


    def normalize_func_for_tests(self, x, new_range=(0,1), original_range=None):
        return torch_normalize(torch.as_tensor(x),
                                new_range=new_range,
                                original_range=original_range)


    def test_supports_numpy_arrays(self):
        n = 10
        array = np.random.rand(n)
        expected_result = (array - array.min()) / (array.max() - array.min())

        result = torch_normalize(array)
        self.assertTrue(np.allclose(result, expected_result),
                        msg=f"Expected: {expected_result},\n got {result}")


class ConfusionTriuTests():

    def setUp(self) -> None:
        self.n_maps = 5
        self.metric_mock = mock.Mock(side_effect=positive_num_gen())

    def test_ranks_are_calculated_in_correct_order(self):
        expected_indices = list(zip(*np.triu_indices(self.n_maps, k=1)))
        coords_in_calls = [coords for coords, kwargs in self.metric_mock.call_args_list]

        self.assertListEqual(expected_indices, coords_in_calls)

    def test_triu_is_built_correctly(self):
        expected_triu = np.arange(sum(range(self.n_maps)))

        self.assertTrue(np.allclose(self.confusion_triu, expected_triu))

    def test_on_2d_inputs(self):
        self.n_maps = 5
        self.metric_mock = mock.Mock(side_effect=positive_num_gen())

        input = np.arange(self.n_maps).repeat(4).reshape(self.n_maps, 2, 2)
        self.confusion_triu = self.method(input, lambda x, y: np.sum(x) / np.sum(y), op_dims=2)

        self.assertTupleEqual(self.confusion_triu.shape, (10,))


    def test_on_ndim_tensor(self):
        self.n_maps = 5
        self.n_others = 4
        self.metric_mock = mock.Mock(side_effect=positive_num_gen())

        input = np.arange(self.n_maps * self.n_others).repeat(4).reshape(self.n_others, self.n_maps, 2, 2)
        self.confusion_triu = self.method(input, lambda x, y: np.sum(x) / np.sum(y), op_dims=2)

        self.assertTupleEqual(self.confusion_triu.shape, (self.n_others, 10))

    @parameterized.expand([
        (         [4, 5], 1, 1, [6],       "0 hyperdims, 1D element, k=1"),
        (      [4, 5, 5], 2, 1, [6],       "0 hyperdims, 2D element, k=1"),
        (   [2, 4, 5, 5], 2, 1, [2, 6],    "1 hyperdim,  2D element, k=1"),
        ([3, 2, 4, 5, 5], 2, 1, [3, 2, 6], "2 hyperdims, 2D element, k=1"),

        (         [4, 5], 1, 2, [3],       "0 hyperdims, 1D element, k=2"),
        (      [4, 5, 5], 2, 2, [3],       "0 hyperdims, 2D element, k=2"),
        (   [2, 4, 5, 5], 2, 2, [2, 3],    "1 hyperdim,  2D element, k=2"),
        ([3, 2, 4, 5, 5], 2, 2, [3, 2, 3], "2 hyperdims, 2D element, k=2")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_correctness_of_result(self, shape, op_dims, k, expected_shape, desc):
        array = np.random.rand(*shape)
        metric = mock.Mock(side_effect=lambda x,y : np.sum(x*y, axis=None))

        base_dim = -(op_dims+1)
        expected_triu_ndidx = list(zip(*np.triu_indices(shape[base_dim], k=k)))
        expected_hyperdim_ndidx = list(np.ndindex(*shape[:base_dim]))
        expected_ndidx = list(itertools.product(expected_hyperdim_ndidx, expected_triu_ndidx))

        # act
        result = self.method(array, metric=metric, op_dims=op_dims, k=k)

        # assert
        self.assertSequenceEqual(result.shape, expected_shape)
        self.assertEqual(len(metric.call_args_list), len(expected_ndidx))
        for hyperidx in expected_hyperdim_ndidx:
            for i, (a_idx, b_idx) in enumerate(expected_triu_ndidx):
                self.assertAlmostEqual(result[(*hyperidx, i)].item(),
                                       metric(array[(*hyperidx, a_idx)], array[(*hyperidx, b_idx)]))


class TestSequentialConfusionTriu(ConfusionTriuTests,
                                  unittest.TestCase):
    def setUp(self) -> None:
        super().setUp()
        self.method = sequential_confusion_triu
        self.confusion_triu = self.method(np.arange(self.n_maps), self.metric_mock, op_dims=0)


class TestParallelConfusionTriu(ConfusionTriuTests,
                                unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.method = parallel_confusion_triu
        self.confusion_triu = self.method(np.arange(self.n_maps), self.metric_mock, op_dims=0)

    def test_is_equivalent_to_sequential_version(self):
        self.n_maps = 5
        self.n_others = 4
        self.metric_mock = mock.Mock(side_effect=positive_num_gen())

        input_tensor = np.arange(self.n_maps * self.n_others).repeat(4).reshape(self.n_others, self.n_maps, 2, 2)
        args = {"maps": input_tensor,
                "metric": lambda x,y: np.sum(x)/ np.sum(y),
                "op_dims": 2}

        parallel_result = parallel_confusion_triu(**args)
        sequential_result = sequential_confusion_triu(**args)

        self.assertTrue(np.allclose(parallel_result, sequential_result))

TEST_CONFUSION_MATRIX_CASES = [
        ([3, 4],       [3, 4],       1, [9],        "no hypershape, matching application shapes"),
        ([3, 4],       [5, 4],       1, [15],       "no hypershape, mismatched application shapes"),
        ([2, 3, 4],    [2, 3, 4],    1, [2, 9],     "1D hypershape, matching application shapes"),
        ([2, 3, 4],    [2, 5, 4],    1, [2, 15],    "1D hypershape, mismatched application shapes"),
        ([2, 8, 3, 4], [2, 8, 3, 4], 1, [2, 8, 9],  "nD hypershape, matching application shapes"),
        ([2, 8, 3, 4], [2, 8, 5, 4], 1, [2, 8, 15], "nD hypershape, matching application shapes"),
        ([2, 8, 3, 4], [2, 8, 3, 4], 2, [2, 64],    "nD hypershape, matching application shapes, 2D op"),
    ]

class TestConfusionMatrix(unittest.TestCase):

    def setUp(self) -> None:
        self.dummy_metric = lambda x, y: np.sum(x + y, axis=None)

    @parameterized.expand(
            TEST_CONFUSION_MATRIX_CASES,
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_result(self, a_shape, b_shape, op_dims, expected_shape, desc):
        a, b = map(lambda shape: np.random.rand(*shape), (a_shape, b_shape))

        result = confusion_matrix(a, b, metric=self.dummy_metric, op_dims=op_dims)

        self.assertSequenceEqual(result.shape, expected_shape, msg=f"result shape ({result.shape}) dosen't match the expected shape {expected_shape}")

    @parameterized.expand(
            TEST_CONFUSION_MATRIX_CASES,
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_correctness_of_result(self, a_shape, b_shape, op_dims, expected_shape, desc):
        # arrange
        a, b = map(lambda shape: np.random.rand(*shape), (a_shape, b_shape))
        metric = mock.Mock(return_value=np.random.rand(1).item())

        base_dim = -(op_dims + 1)
        expected_hyperdim_ndidx = list(np.ndindex(*a_shape[:base_dim]))
        expected_operation_ndidx = list(np.ndindex(a_shape[base_dim], b_shape[base_dim]))
        expected_ndidx = list(itertools.product(expected_hyperdim_ndidx, expected_operation_ndidx))

        # act
        result = confusion_matrix(a, b, metric=metric, op_dims=op_dims)

        # assert
        self.assertEqual(len(metric.call_args_list), len(expected_ndidx))
        self.assertSequenceEqual(result.shape, expected_shape)
        for hyperidx in expected_hyperdim_ndidx:
            for i, (a_idx, b_idx) in enumerate(expected_operation_ndidx):
                self.assertAlmostEqual(result[(*hyperidx, i)].item(),
                                       metric(a[(*hyperidx, a_idx)], b[(*hyperidx, b_idx)]))


    def test_raises_on_mismatched_hypershape(self):
        a = np.random.rand(2, 3, 5, 4, 4)
        b = np.random.rand(4, 5, 5, 4, 4)

        with self.assertRaises(ValueError):
            confusion_matrix(a, b, metric=self.dummy_metric, op_dims=2)


class TestSpearmanUpperMatrix(unittest.TestCase):

    def setUp(self):
        self.spearman_patcher = mock.patch('hirise.experiment.utils.scipy.stats.spearmanr')
        self.spearman_mock = self.spearman_patcher.start()
        self.spearman_mock.side_effect = positive_num_gen()

        self.n_maps = 5
        self.generated_triu = spearman_upper_matrix(np.arange(self.n_maps).repeat(4).reshape(self.n_maps, 2,2))

    def test_spearman_rank_is_used_on_flattened_inputs(self):
        self.assertTrue(all(kwargs['axis'] is None for coords, kwargs in self.spearman_mock.call_args_list))

    def tearDown(self) -> None:
        self.spearman_mock.stop()


@parameterized_class(('matrix_size', 'triu_length'),
                     [
                         (2, 1),
                         (3, 3),
                         (4, 6),
                         (5, 10),
                         (6, 15),
                         (7, 21)
                     ])
class TestTriuToMatrix(unittest.TestCase):

    def setUp(self) -> None:
        self.raveled_triu = np.arange(self.triu_length, dtype=np.int) + 1

        self.expected_matrix_shape = (self.matrix_size, self.matrix_size)
        self.rebuilt_matrix = triu_to_matrix(self.raveled_triu)

    def test_main_diagonal_is_zero(self):
        main_diagonal = np.diagonal(self.rebuilt_matrix)
        self.assertTrue(np.allclose(main_diagonal, np.zeros_like(main_diagonal)))

    def test_rebuilt_matrix_shape_matches(self):
        self.assertEqual(self.rebuilt_matrix.shape, self.expected_matrix_shape)

    def test_matrix_contents_by_extracting_triu_again(self):
        generated_triu = self.rebuilt_matrix[np.triu_indices_from(self.rebuilt_matrix, k=1)]

        self.assertTrue(np.allclose(self.raveled_triu, generated_triu),
                        msg="Raveled upper triangulars don't match. Expected %s, got %s" % (self.raveled_triu, generated_triu))

    def test_upper_triangle_has_the_same_content_as_lower(self):
        transposed_matrix = self.rebuilt_matrix.T  # now the lower triangle is in pale of upper triangle
        lower_triangle = transposed_matrix[np.triu_indices_from(self.rebuilt_matrix, k=1)]

        self.assertTrue(np.all(np.equal(self.raveled_triu, lower_triangle)),
                        msg="Raveled lower triangle doesn't match upper. Expected %s, got %s" % (
                        self.raveled_triu, lower_triangle))


class TestReplaceInitialConsecutiveZeros(unittest.TestCase):

    @parameterized.expand([
        ([0, 0, 0, 0, 4, 5, 6], [0, 1, 2, 3, 4, 5, 6], "array of integers"),
        ([0, 0, 0, 0.75, 1], [0, 0.25, 0.5, 0.75, 1], "array of floats")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_array_is_altered_with_linear_interpolation(self, array, expected_result, desc):
        array, expected_result = map(np.array, (array, expected_result))
        result = replace_initial_consecutive_zeros(array)

        self.assertTrue(np.allclose(result, expected_result),
                        msg=f"Expected: {expected_result},\n got {result}")

    @parameterized.expand([
        (np.random.rand(10), "non-monotonic"),
        (np.zeros(10, dtype=float), "all-zeros")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_array_is_not_altered_when_it_is(self, array, desc):
        result = replace_initial_consecutive_zeros(array)

        self.assertTrue(np.allclose(array, result),
                        msg=f"Expected: {array},  got {result}")

    def test_support_for_torch_tensor(self):
        array = torch.as_tensor([0, 0, 0, 0.75, 1])
        expected_result = torch.as_tensor([0, 0.25, 0.5, 0.75, 1])

        result = replace_initial_consecutive_zeros(array)

        self.assertTrue(np.allclose(result, expected_result),
                        msg=f"Expected: {expected_result},\n got {result}")


if __name__ == '__main__':
    unittest.main()
