import unittest

import numpy as np

from hirise.experiment.digitization import top_energy, top_quantile
from parameterized import parameterized


class TestTopQuantile(unittest.TestCase):

    @parameterized.expand([
        ([1, 1, 1, 1], 0.26, [0, 1, 1, 1], "stability test 1"),
        ([1, 1, 1, 1], 0.51, [0, 0, 1, 1], "stability test 2"),
        ([1, 1, 1, 1], 0.76, [0, 0, 0, 1], "stability test 3"),

        ([1, 2, 3, 4], 0.00, [1, 2, 3, 4], "q=0_0 returns input data"),
        ([1, 2, 3, 4], 0.24, [1, 2, 3, 4], ""),
        ([1, 2, 3, 4], 0.25, [0, 2, 3, 4], ""),
        ([1, 2, 3, 4], 0.49, [0, 2, 3, 4], ""),
        ([1, 2, 3, 4], 0.50, [0, 0, 3, 4], ""),
        ([1, 2, 3, 4], 0.51, [0, 0, 3, 4], ""),
        ([1, 2, 3, 4], 0.76, [0, 0, 0, 4], ""),
        ([1, 2, 3, 4], 1.00, [0, 0, 0, 0], "q=1_0 zeroes out the entire array"),

        ([4, 1, 3, 2], 0.25, [4, 0, 3, 2], ""),
        ([4, 1, 3, 2], 0.50, [4, 0, 3, 0], ""),
        ([4, 1, 3, 2], 0.75, [4, 0, 0, 0], ""),

        ([4, 1, 3, 2], [0.25, 0.75], [[4, 0, 3, 2], [4, 0, 0, 0]], ""),
        ([4, 1, 3, 2], [0.75, 0.25], [[4, 0, 0, 0], [4, 0, 3, 2]], ""),
        ([4, 1, 3, 2], [0.00, 1.00], [[4, 1, 3, 2], [0, 0, 0, 0]], ""),

        ([[4, 1, 3, 2],
          [5, 8, 7, 6]],

         [0.25, 0.75],

         [[[4, 0, 3, 0],
           [5, 8, 7, 6]],
          [[0, 0, 0, 0],
           [0, 8, 7, 0]]], "")
    ],
    testcase_func_name=lambda f, n, p: f.__name__ + "."+(p.args[-1] if p.args[-1] else str(p.args[1]).replace('.', '_')))
    def test_quantization(self, input_matrix, quantile, expected_result, description):
        input_matrix, expected_result = map(np.array, (input_matrix, expected_result))
        result = top_quantile(input_matrix, quantile)

        self.assertTupleEqual(result.shape, expected_result.shape)
        self.assertTrue(np.allclose(result, expected_result),
                        msg="Expected\n %s,\n got\n %s" % (expected_result, result))


class TestTopEnergy(unittest.TestCase):

    @parameterized.expand([
        ([1, 1, 1, 1], 0.20, [0, 0, 0, 1], "stability test 1"),
        ([1, 1, 1, 1], 0.45, [0, 0, 1, 1], "stability test 2"),
        ([1, 1, 1, 1], 0.72, [0, 1, 1, 1], "stability test 3"),

        ([1, 2, 3, 4], 0.00, [0, 0, 0, 0], "e=0_0 returns zeros"),
        ([1, 2, 3, 4], 0.39, [0, 0, 0, 4], ""),
        ([1, 2, 3, 4], 0.41, [0, 0, 3, 4], ""),
        ([1, 2, 3, 4], 0.69, [0, 0, 3, 4], ""),
        ([1, 2, 3, 4], 0.71, [0, 2, 3, 4], ""),
        ([1, 2, 3, 4], 0.89, [0, 2, 3, 4], ""),
        ([1, 2, 3, 4], 0.91, [1, 2, 3, 4], ""),
        ([1, 2, 3, 4], 1.00, [1, 2, 3, 4], "q=1_0 zeroes out the entire array"),

        ([4, 1, 3, 2], 0.41, [4, 0, 3, 0], "40% non-monotonic"),
        ([4, 1, 3, 2], 0.71, [4, 0, 3, 2], "70% non-monotonic"),
        ([4, 1, 3, 2], 0.91, [4, 1, 3, 2], "90% non-monotonic"),

        ([4, 1, 3, 2], [0.39, 0.89], [[4, 0, 0, 0], [4, 0, 3, 2]], ""),
        ([4, 1, 3, 2], [0.89, 0.39], [[4, 0, 3, 2], [4, 0, 0, 0]], ""),
        ([4, 1, 3, 2], [0.00, 1.00], [[0, 0, 0, 0], [4, 1, 3, 2]], ""),

        ([[4, 1, 3, 2],
          [5, 8, 7, 6]],

         [0.30, 0.90],

         [[[0, 0, 0, 0],
           [0, 8, 7, 0]],
          [[4, 0, 3, 0],
           [5, 8, 7, 6]]], "")
    ],
    testcase_func_name=lambda f, n, p: f.__name__ + "."+(p.args[-1] if p.args[-1] else str(p.args[1]).replace('.', '_')))
    def test_top_energy(self, input_matrix, quantile, expected_result, description):
        input_matrix, expected_result = map(np.array, (input_matrix, expected_result))
        result = top_energy(input_matrix, quantile)

        self.assertTupleEqual(result.shape, expected_result.shape)
        self.assertTrue(np.allclose(result, expected_result),
                        msg="Expected\n %s,\n got\n %s" % (expected_result, result))


if __name__ == '__main__':
    unittest.main()
