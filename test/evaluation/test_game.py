import copy
import random
from unittest import TestCase, mock

import numpy as np
import torch
from hirise.evaluation.game import *
from parameterized import parameterized
from torch import nn


class GameInitializationTests:

    def test_image_size_is_the_product_of_the_last_two_dimensions(self):
        self.assertEqual(self.game.img_px_count, 16)

    def test_game_starts_with_step_counter_at_zero(self):
        self.assertEquals(self.game.current_step, 0)

    def test_internally_stored_images_are_all_torch_tensors(self):
        self.assertIsInstance(self.game._initial_img, torch.Tensor)
        self.assertIsInstance(self.game._current_img, torch.Tensor)
        self.assertIsInstance(self.game._final_img, torch.Tensor)

    def test_default_observed_class_is_the_highest_scoring_one_in_the_original_image(self):
        self.model_mock.side_effect = lambda _: self.dummy_prediction
        expected_class = np.argmax(self.dummy_prediction).item()

        test_game = AlterationGame(model=self.model_mock, initial_state=self.dummy_image, final_state=None,
                                   explanation=self.dummy_explanation, step_size=self.step_size, observed_classes=None)

        self.assertIsInstance(test_game.observed_classes, np.ndarray)
        self.assertEqual(test_game.observed_classes, expected_class)

    def test_ellipsis_is_accepted_as_observed_classes_value(self):
        test_game = AlterationGame(model=self.model_mock, initial_state=self.dummy_image, final_state=None,
                                   explanation=self.dummy_explanation, step_size=self.step_size,
                                   observed_classes=Ellipsis)

        self.assertEqual(test_game.observed_classes, Ellipsis)


    @parameterized.expand([
        (0, 2, False),
        (1, 2, False),
        (2, 2, True),
        (3, 2, True),
    ],
    testcase_func_name=lambda f, n, p: f"test_is_over_returns.{p.args[-1]}_at_{p.args[0]}_steps_out_of_{p.args[1]}")
    def test_is_over(self, current_step, n_steps, expected_result):
        self.game.n_steps = n_steps
        self.game.current_step = current_step

        self.assertEqual(self.game.is_over(), expected_result)

    @parameterized.expand([
        ((3, 3), 1, 9),
        ((3, 3), 2, 5),
        ((3, 3), 3, 3),
        ((3, 3), 4, 3),
        ((3, 3), 5, 2),
        ((3, 3), 8, 2),
        ((3, 3), 9, 1),
    ],
    testcase_func_name=lambda func, n, p: f"test_step_count_calculation.expect{p.args[-1]} steps for image shape {p.args[0]} and step_size={p.args[1]}"
    )
    def test_n_steps_calcuation(self, xy_shape, step_size, expected_steps):
        sample_img = torch.Tensor(np.random.rand(1, self.num_channels, *xy_shape))
        sample_expl = torch.Tensor(np.random.rand(*xy_shape))

        game = AlterationGame(model=self.model_mock, initial_state=sample_img, final_state=None,
                              explanation=sample_expl, step_size=step_size, observed_classes=None)

        self.assertEquals(game.n_steps, expected_steps)

    def test_current_image_at_first_step_is_equal_to_initial_image(self):
        self.assertEqual(self.game.current_step, 0)
        self.assertTrue(np.allclose(self.game._current_img, self.game._initial_img))

    def test_scores_are_initialized_with_pre_game_score(self):
        # Assert
        self.assertTrue(np.allclose(self.model_mock.call_args[0][0], self.game._initial_img))
        self.assertEqual(self.game.current_step, 0)
        self.assertEqual(len(self.game.scores), 1)

    @parameterized.expand([
        (0, "single int"),
        ([0, 1, 3], "list of classes"),
        ((0, 1, 3), "tuple of classes"),
        (np.array(0), "ndarray with single class"),
        (np.array([0, 1, 3]), "ndarray of multiple classes")
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_observed_classes_are_uses_as_provided_when(self, classes, description):
        game = AlterationGame(model=self.model_mock, initial_state=self.dummy_image, final_state=None,
                              explanation=self.dummy_explanation, step_size=self.step_size, observed_classes=classes)

        self.assertTrue(np.array_equal(game.observed_classes, np.array(classes)))

    def test_negative_observed_classes_value_causes_top_N_classes_to_be_observed(self):
        # Arrange
        dummy_scores = np.random.rand(1, 1000)
        model_mock = copy.deepcopy(self.model_mock)
        model_mock.side_effect = lambda _: torch.from_numpy(dummy_scores)

        observed_classes_value = -4
        expected_classes = np.flip(np.argsort(dummy_scores.ravel(), axis=-1))[:-observed_classes_value]

        # Act
        game = AlterationGame(model=model_mock, initial_state=self.dummy_image, final_state=None,
                              explanation=self.dummy_explanation, step_size=self.step_size,
                              observed_classes=observed_classes_value)

        # Assert
        self.assertTrue(np.array_equal(game.observed_classes, expected_classes))



class GameStepBehaviourTests:

    def test_step_alters_the_original_image(self):
        pre_step_image = self.game._current_img.clone().detach()
        self.game.step()

        self.assertFalse(np.allclose(pre_step_image, self.game._current_img))

    def test_step_alters_all_channels_of_image(self):
        pre_step_image = self.game._current_img.clone().detach()
        self.game.step()

        for channel in range(pre_step_image.shape[1]):
            self.assertFalse(np.allclose(pre_step_image[:, channel], self.game._current_img[:, channel]))

    def test_step_does_not_alter_initial_and_final_img(self):
        pre_step_initial_img = np.copy(self.game._initial_img)
        pre_step_final_img = np.copy(self.game._final_img)

        self.game.step()

        self.assertTrue(np.allclose(self.game._initial_img, pre_step_initial_img))
        self.assertTrue(np.allclose(self.game._final_img, pre_step_final_img))

    def test_step_appends_scores_for_all_observed_classes(self):
        observed_classes = [2, 3]
        self.game.observed_classes = np.array(observed_classes)

        # Act
        returned_confidence = self.game.step()

        # Assert
        self.assertTrue(np.array_equal(self.game.current_predictions[..., observed_classes].cpu().numpy(),
                                       returned_confidence))


class SequentialGameStepBehaviourTests(GameStepBehaviourTests):

    def test_step_appends_next_score_to_score_list(self):
        pre_step_game_score_list = len(self.game.scores)

        last_score = self.game.step()

        self.assertEqual(len(self.game.scores), pre_step_game_score_list+1)
        self.assertEqual(self.game.scores[-1], last_score)

    def test_step_increments_step_counter(self):
        pre_step_counter = self.game.current_step
        self.game.step()

        self.assertEquals(self.game.current_step, pre_step_counter + 1)

    def test_step_returns_float_if_single_observed_class(self):
        observed_class = np.array(0)
        self.game.observed_classes = observed_class
        score = self.game.step()
        self.assertIsInstance(score, (float, np.float32, np.float64))
        self.assertAlmostEqual(self.game.current_predictions[observed_class].cpu().item(),
                               score,
                               places=6)

    def test_step_copies_pixels_from_final_image_into_current_image(self):
        expected_num_changed_pixels = self.game.step_size * self.num_channels       # 3 is the number of image channels
        self.game.step()

        pixel_state = dict(zip(*np.unique(np.equal(self.game._final_img, self.game._current_img), return_counts=True)))
        num_pixels_from_final_img = pixel_state[True]

        self.assertEqual(num_pixels_from_final_img, expected_num_changed_pixels)


    @parameterized.expand([
        (np.array([0,1]), 2, "selected classes"),
        (Ellipsis, 1000, "all classes are observed when Ellipsis is given")
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}"
    )
    def test_step_returns_1d_array_if_multiple_observed_classes(self, observed_classes, num_expected_scores, desc):
        self.game.observed_classes = observed_classes
        score = self.game.step()

        self.assertTupleEqual(score.shape, (num_expected_scores,))

    def test_step_returns_a_score_calculated_based_on_the_version_of_the_image_altered_by_step(self):
        self.model_mock.reset_mock() # remove the single call made during __init__

        # Act
        self.game.step()

        # Assert
        # using call_args here, because argument matching doesn't work for some reason
        img_altered_by_step = self.game._current_img
        call_args = self.model_mock.call_args
        self.assertEqual(self.model_mock.call_count, 1)
        self.assertTrue(np.allclose(call_args[0][0], img_altered_by_step))

    @parameterized.expand([
        (lambda img_size, step_size: 0, "first"),
        (lambda img_size, step_size: 1, "second"),
        (lambda img_size, step_size: (np.product(img_size[-2:]) // step_size) - 1, "last")
    ],
    testcase_func_name=lambda f, n, p: f"test_Nth_step_alters_appropriate_pixels.{p.args[-1]}")
    def test_step_changes_appropriate_pixels(self, step_number_provider, test_name):
        # Arrange
        flattened_explanation = self.dummy_explanation.reshape(-1)
        saliency_sorted_pixels = np.flip(np.argsort(flattened_explanation.numpy()), axis=-1)

        # Arrange - calculate the coordinates of pixels to examine
        n = self.game.step_size
        step_to_examine = step_number_provider(self.img_size, n)
        step_boundaries = [n * step_to_examine, n * (step_to_examine + 1)]
        already_changed_px, px_changed_now, unchanged_pix = map(
            lambda x: np.unravel_index(x, self.dummy_explanation.shape),
            np.split(saliency_sorted_pixels, step_boundaries))

        # Arrange - prepare masks which select only the subset of pixels relevant to the each of the assertions
        already_changed_mask, changed_now_mask, unchanged_mask = np.zeros((3, *self.dummy_explanation.shape))
        already_changed_mask[already_changed_px] = 1
        changed_now_mask[px_changed_now] = 1
        unchanged_mask[unchanged_pix] = 1

        # Arrange - perform preliminary steps
        for i in range(step_to_examine):
            self.game.step()

        # Arrange - capture the state before examined step
        img_before_step = self.game._current_img.clone().detach()

        # Act
        self.game.step()

        # Assert
        # pixels changed in prior steps are the same as in final picture
        self.assertTrue(np.allclose(self.game._current_img * already_changed_mask,
                                    self.game._final_img * already_changed_mask))

        # pixels changed now are the same as in the final picture
        self.assertTrue(np.allclose(self.game._current_img * changed_now_mask,
                                    self.game._final_img * changed_now_mask))

        # pixels changed just now, were the same as in the initial picture, before this step
        self.assertTrue(np.allclose(img_before_step * changed_now_mask,
                                    self.game._initial_img * changed_now_mask))

        # pixels to be changed in further steps are still the same as in the initial picture
        self.assertTrue(np.allclose(self.game._current_img * unchanged_mask,
                                    self.game._initial_img * unchanged_mask))

class BatchedGameStepBehaviourTests(GameStepBehaviourTests):

    def test_step_copies_pixels_from_final_image_into_current_image(self):
        expected_num_changed_pixels = self.game.step_size * self.num_channels * self.batch_size
        self.game.step()

        pixel_state = dict(zip(*np.unique(np.equal(self.game._final_img, self.game._current_img), return_counts=True)))
        num_pixels_from_final_img = pixel_state[True]

        self.assertEqual(num_pixels_from_final_img, expected_num_changed_pixels)

    @parameterized.expand([
        (np.array([0,1]), 2, "selected classes"),
        (Ellipsis, 1000, "all classes are observed when Ellipsis is given")
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_step_returns_1d_array_if_multiple_observed_classes(self, observed_classes, num_expected_scores, desc):
        self.game.observed_classes = observed_classes
        score = self.game.step()

        self.assertTupleEqual(score.shape, (self.batch_size, num_expected_scores))

    def test_batch_is_equivalent_to_n_steps(self):
        expected_batch_images = []
        for i in range(self.batch_size):
            self.sequential_game.step()
            expected_batch_images.append(self.sequential_game._current_img.clone().detach())

        self.game.step()

        expected_batch = torch.cat(expected_batch_images)
        self.assertTrue(torch.allclose(self.game.batch, expected_batch))

    def test_step_returns_a_score_calculated_based_on_the_version_of_the_image_altered_by_step(self):
        self.model_mock.reset_mock() # remove the single call made during __init__

        # Act
        self.game.step()

        # Assert
        # using call_args here, because argument matching doesn't work for some reason
        call_args = self.model_mock.call_args
        self.assertEqual(self.model_mock.call_count, 1)
        input_batch = call_args[0][0]
        self.assertTupleEqual(input_batch.shape, (self.batch_size, *self.img_size))

    def test_step_increments_step_counter(self):
        pre_step_counter = self.game.current_step
        self.game.step()

        self.assertEquals(self.game.current_step, pre_step_counter + self.batch_size)

    def test_step_appends_scores_for_entire_batch_to_score_list(self):
        pre_step_game_score_list = len(self.game.scores)

        self.game.step()

        self.assertEqual(len(self.game.scores), pre_step_game_score_list + self.batch_size)
        self.assertTrue(np.allclose(self.game.scores[-self.batch_size:],
                                    self.game.current_predictions[:, self.game.observed_classes]))

    @parameterized.expand([
        (np.array([0]), "single_observed_class"),
        (np.array([2, 5, 4]), "multiple_observed_classes"),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}"
    )
    def test_step_returns_2d_tensor_of_floats_float_when(self, observed_classes, description):
        # Arrange
        self.game.observed_classes = observed_classes
        expected_tensor_shape = (self.batch_size, len(self.game.observed_classes))

        # Act
        scores = self.game.step()

        # Assert
        self.assertIsInstance(scores, torch.Tensor)
        self.assertIn(scores.dtype, (float, torch.float32, torch.float64))
        self.assertTupleEqual(scores.shape, expected_tensor_shape)

class GameResetTests:

    def run_game_until_end(self):
        while not self.game.is_over():
            self.game.step()

    def test_reset_leaves_only_the_score_on_initial_image(self):
        self.game.step()
        self.game.reset()

        self.assertEqual(len(self.game.scores), 1)

    def test_reset_sets_step_counter_to_zero(self):
        self.game.step()
        self.game.reset()

        self.assertEqual(self.game.current_step, 0)

    def test_reset_makes_current_img_identical_to_initial_img(self):
        self.game.step()
        self.game.reset()

        self.assertTrue(np.allclose(self.game._current_img, self.game._initial_img))

    def test_step_doesnt_raise_after_reset(self):
        # Arrange
        self.run_game_until_end()

        # Act
        self.game.reset()

        # Assert
        try:
            self.game.step()
        except StopIteration:
            self.fail(".step() right after reset shouldn't raise")

class FinalGameStateTests:

    def test_scores_list_is_one_element_larger_than_the_number_of_steps(self):
        # That's because the first element of scores list is the score on unmodified image
        while not self.game.is_over():
            self.game.step()

        self.assertEquals(len(self.game.scores), self.game.current_step + 1)

    def test_scores_list_is_a_numpy_array_of_floats(self):
        while not self.game.is_over():
            self.game.step()

        scores = self.game.scores
        self.assertIsInstance(scores, np.ndarray)
        self.assertIn(scores.dtype, (float, np.float32, np.float64))

    def test_scores_list_is_1d_if_one_observed_class(self):
        self.game.observed_classes = np.array(0)
        self.game.reset()

        while not self.game.is_over():
            self.game.step()

        self.assertTupleEqual(self.game.scores.shape, (self.game.n_steps + 1,))

    def test_scores_list_is_2d_if_multiple_observed_classes(self):
        self.game.observed_classes = np.array([0,1])
        self.game.reset()

        while not self.game.is_over():
            self.game.step()

        self.assertTupleEqual(self.game.scores.shape,
                              (self.game.n_steps + 1, len(self.game.observed_classes)))

    def test_current_image_is_equal_to_final_image_when_game_is_over(self):
        while not self.game.is_over():
            self.game.step()

        self.assertTrue(np.allclose(self.game._current_img, self.game._final_img))

    def test_game_raises_when_step_is_called_after_game_is_finished(self):
        while not self.game.is_over():
            self.game.step()

        with self.assertRaises(StopIteration):
            self.game.step()

class SequentialGameFinalStateTests(FinalGameStateTests):
    pass

class BatchedGameFinalStateTest(FinalGameStateTests):
    pass

class GameGetterTests:

    @parameterized.expand([
        (lambda game: game.current_state, lambda game: game._current_img, "current"),
        (lambda game: game.initial_state, lambda game: game._initial_img, "initial"),
        (lambda game: game.final_state, lambda game: game._final_img, "final"),
    ],
    testcase_func_name=lambda f, n, p: f"test_state_getter_returns_a_CPU_version_of_tensor.{p.args[-1]}_state")
    def test_state_getter_returns_the_CPU_version_of_tensor(self, getter_ref, internal_object_extractor, test_case_name):
        returned_img = getter_ref(self.game)
        self.assertEqual(returned_img.device.type, "cpu")

    @parameterized.expand([
        (lambda game: game.current_state, lambda game: game._current_img, "current"),
        (lambda game: game.initial_state, lambda game: game._initial_img, "initial"),
        (lambda game: game.final_state, lambda game: game._final_img, "final"),
    ],
    testcase_func_name=lambda f, n, p: f"test_state_getter_does_not_return_a_copy_of_the_object.{p.args[-1]}_state")
    def test_getter_does_not_return_a_copy_of_the_object(self, getter_ref, internal_object_extractor, test_case_name):
        returned_img = getter_ref(self.game)
        some_random_value = random.random()

        returned_img.view(-1)[0] = some_random_value

        self.assertEqual(internal_object_extractor(self.game).view(-1)[0], some_random_value)

    def test_scores_getter_returns_a_copy_of_the_internal_scores_list(self):
        scores = self.game.scores
        some_random_value = "4431"

        scores[0] = some_random_value
        self.assertNotEqual(self.game.scores_list[0], some_random_value)


class GameTestFixture:

    def setUp(self) -> None:
        self.step_size = 2
        self.num_channels = 3
        self.img_size = (self.num_channels, 4, 4)
        self.dummy_image = torch.from_numpy(np.random.rand(1, *self.img_size))
        self.dummy_final_state = torch.from_numpy(np.random.rand(1, *self.img_size))

        self.dummy_explanation = torch.from_numpy(np.arange(16).reshape(4, 4) / 16)
        self.dummy_prediction = torch.from_numpy(np.random.rand(1,1000))

        self.model_mock = mock.Mock(
            spec=nn.Module,
            side_effect=lambda tensor: torch.rand((tensor.shape[0], 1000)),
            **{
                "parameters": lambda: [mock.Mock(device="cpu:0", dtype=torch.float32)].__iter__()
            }
        )


class GameTestBase(GameTestFixture,
                   GameInitializationTests,
                   GameGetterTests,
                   GameResetTests):
    pass

class SequentialGameTestBase(GameTestBase,
                             SequentialGameStepBehaviourTests,
                             SequentialGameFinalStateTests):
    pass

class BatchedGameTestBase(GameTestBase,
                          BatchedGameStepBehaviourTests,
                          BatchedGameFinalStateTest):
    pass

class TestAlterationGame(GameTestFixture,
                         TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.game = AlterationGame(model=self.model_mock,
                                   initial_state=self.dummy_image,
                                   final_state=self.dummy_final_state,
                                   explanation=self.dummy_explanation,
                                   step_size=self.step_size,
                                   observed_classes=None)
    
    def test_initialization(self):
        self.assertTrue(torch.allclose(self.game._initial_img, self.dummy_image))
        self.assertTrue(torch.allclose(self.game._final_img, self.dummy_final_state))
        self.assertTrue(torch.allclose(self.game._current_img, self.dummy_image))

        self.assertIsNotNone(self.game.scores_list)
        self.assertTrue(np.allclose(self.game.current_predictions[self.game.observed_classes], self.game.scores_list[0]))

    def test_game_is_ready_to_go(self):
        self.game.step() # doesn't raise

class ConcreteGameImplementationTests:
    def test_game_state_is_initialized(self):
        self.assertIsNotNone(self.game._initial_img)
        self.assertIsNotNone(self.game._current_img)
        self.assertIsNotNone(self.game._final_img)
        self.assertIsNotNone(self.game.scores)
        self.assertIsNotNone(self.game.current_predictions)

    def test_initial_score_is_generated_with_initial_img(self):
        initial_score_calc_call = self.model_mock.mock_calls[1]
        self.assertTrue(np.allclose(self.game._initial_img, initial_score_calc_call[1][0]))

class TestInsertionGame(SequentialGameTestBase,
                        ConcreteGameImplementationTests,
                        TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.blurred_image_mock = torch.from_numpy(np.random.rand(1, *self.img_size))
        self.blur_kernel_mock = mock.Mock(return_value=self.blurred_image_mock)

        self.game = InsertionGame(model=self.model_mock,
                                  image=self.dummy_image,
                                  explanation=self.dummy_explanation,
                                  step_size=2,
                                  substrate_fn=self.blur_kernel_mock)

    def test_initial_image_is_the_altered_one(self):
        self.blur_kernel_mock.assert_called_once()
        self.assertTrue(np.array_equal(self.blur_kernel_mock.call_args[0][0], self.dummy_image))
        self.assertTrue(np.allclose(self.game._initial_img.numpy(), self.blurred_image_mock.numpy()))

    def test_final_image_is_the_original_one(self):
        self.assertTrue(np.allclose(self.game._final_img.numpy(), self.dummy_image.numpy()))

    @parameterized.expand([
        (np.zeros_like, "numpy.ndarray"),
        (lambda x: torch.from_numpy(np.zeros_like(x)), "torch.Tensor")
    ])
    def test_substrate_modified_image_is_converted_to_torch_tensor(self, kernel, kernel_return_type):
        self.game = DeletionGame(model=self.model_mock,
                                 image=self.dummy_image,
                                 explanation=self.dummy_explanation,
                                 step_size=2,
                                 substrate_fn=kernel)

        self.assertIsInstance(self.game._initial_img,
                              torch.Tensor,
                              msg="kernel output of type %s wasn't converted to Tensor" % kernel_return_type)

class TestDeletionGame(SequentialGameTestBase,
                       ConcreteGameImplementationTests,
                       TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.expected_final_image = torch.from_numpy(np.zeros((1, *self.img_size)))
        self.deletion_kernel = mock.Mock(side_effect=lambda x: torch.from_numpy(np.zeros_like(x)))

        self.game = DeletionGame(model=self.model_mock,
                                 image=self.dummy_image,
                                 explanation=self.dummy_explanation,
                                 step_size=2,
                                 substrate_fn=self.deletion_kernel)

    def test_initial_image_is_the_original_one(self):
        self.assertTrue(np.array_equal(self.deletion_kernel.call_args[0][0], self.dummy_image))
        self.assertTrue(np.allclose(self.game._initial_img, self.dummy_image))

    def test_final_image_is_the_altered_one(self):
        self.deletion_kernel.assert_called()
        self.assertTrue(np.allclose(self.game._final_img, self.expected_final_image))

    @parameterized.expand([
        (np.zeros_like, "numpy.ndarray"),
        (lambda x: torch.from_numpy(np.zeros_like(x)), "torch.Tensor")
    ])
    def test_substrate_modified_image_is_converted_to_torch_tensor(self, kernel, kernel_return_type):
        self.game = DeletionGame(model=self.model_mock,
                                 image=self.dummy_image,
                                 explanation=self.dummy_explanation,
                                 step_size=2,
                                 substrate_fn=kernel)

        self.assertIsInstance(self.game._final_img,
                              torch.Tensor,
                              msg="kernel output of type %s wasn't converted to Tensor" % kernel_return_type)


class TestBatchedDeletionGame(BatchedGameTestBase,
                               ConcreteGameImplementationTests,
                               TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.expected_final_image = torch.from_numpy(np.zeros((1, *self.img_size)))
        self.deletion_kernel = mock.Mock(side_effect=lambda x: torch.from_numpy(np.zeros_like(x)))

        self.default_game_params = {"model": self.model_mock,
                                    "image": self.dummy_image,
                                    "explanation": self.dummy_explanation,
                                    "step_size": 2,
                                    "substrate_fn": self.deletion_kernel}

        self.batch_size = 4
        self.game = BatchedDeletionGame(**self.default_game_params,
                                        batch_size=self.batch_size)

        self.sequential_game = DeletionGame(**self.default_game_params)

    @parameterized.expand([
        (False, "not reversed - Deletion Game"),
        (True, "reversed - (new-style) Insertion Game"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_reverse(self, is_reversed, desc):
        expected_initial_state = self.dummy_image
        expected_final_state = torch.rand_like(self.dummy_image)
        self.default_game_params["substrate_fn"] = lambda _: expected_final_state

        # act
        game = BatchedDeletionGame(**self.default_game_params,
                                   batch_size=self.batch_size,
                                   reverse=is_reversed)

        # assert
        self.assertEqual(game.reversed, is_reversed)
        if is_reversed:
            self.assertTrue(np.allclose(game.initial_state, expected_final_state))
            self.assertTrue(np.allclose(game.final_state, expected_initial_state))
        else:
            self.assertTrue(np.allclose(game.final_state, expected_final_state))
            self.assertTrue(np.allclose(game.initial_state, expected_initial_state))
        self.assertTrue(np.allclose(game.initial_state, game.current_state))


class TestBatchedInsertionGame(BatchedGameTestBase,
                               ConcreteGameImplementationTests,
                               TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.blurred_image_mock = torch.from_numpy(np.random.rand(1, *self.img_size))
        self.blur_kernel_mock = mock.Mock(return_value=self.blurred_image_mock)

        self.default_game_params = {"model": self.model_mock,
                                    "image": self.dummy_image,
                                    "explanation": self.dummy_explanation,
                                    "step_size": 2,
                                    "substrate_fn": self.blur_kernel_mock}
        self.batch_size = 4
        self.game = BatchedInsertionGame(**self.default_game_params,
                                         batch_size=self.batch_size)

        self.sequential_game = InsertionGame(**self.default_game_params)

    @parameterized.expand([
        (False, "not reversed - De-blurring game"),
        (True, "reversed - Blurring game"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_reverse(self, is_reversed, desc):
        expected_initial_state = torch.rand_like(self.dummy_image)
        expected_final_state = self.dummy_image
        self.default_game_params["substrate_fn"] = lambda _: expected_initial_state

        # act
        game = BatchedInsertionGame(**self.default_game_params,
                                    batch_size = self.batch_size,
                                    reverse=is_reversed)

        # assert
        self.assertEqual(game.reversed, is_reversed)
        if is_reversed:
            self.assertTrue(np.allclose(game.initial_state, expected_final_state))
            self.assertTrue(np.allclose(game.final_state, expected_initial_state))
        else:
            self.assertTrue(np.allclose(game.final_state, expected_final_state))
            self.assertTrue(np.allclose(game.initial_state, expected_initial_state))
        self.assertTrue(np.allclose(game.initial_state, game.current_state))

#region kernel-tests
class CommonKernelTests():

    @parameterized.expand([
        (torch.rand(3, 50, 50), "3-channel img"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape_is_maintained(self, dummy_tensor, desc):
        self.assertTupleEqual(self.kernel(dummy_tensor).shape, dummy_tensor.shape)

    @parameterized.expand([
        (np.random.rand(3, 50, 50), "numpy.N-channel 3D array"),
        (torch.rand(3, 50, 50), "torch.N-channel 3D tensor"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_accepts(self, input_tensor, desc):
        self.kernel(input_tensor) # doesn't raise
         

class DefaultDeletionKernelTests(CommonKernelTests, TestCase):

    def setUp(self) -> None:
        self.kernel = default_deletion_kernel()

    @parameterized.expand([
        ((10, 10),    "0-channel img"),
        ((1, 10, 10), "1-channel img"),
        ((3, 10, 10), "3-channel img"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_returned_tensor_is_all_zeros(self, shape, desc):
        dummy_tensor = torch.rand(shape)

        self.assertTrue(np.allclose(self.kernel(dummy_tensor), 0))

    @parameterized.expand([
        (torch.rand(50, 50),    "0-channel img"),
        (torch.rand(1, 50, 50), "1-channel img"),
        (torch.rand(2, 50, 50), "2-channel img"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape_is_maintained(self, dummy_tensor, desc):
        self.assertTupleEqual(self.kernel(dummy_tensor).shape, dummy_tensor.shape)

    @parameterized.expand([
        (np.random.rand(50, 50),    "numpy.2D array"),
        (np.random.rand(1, 50, 50), "numpy.1-channel 3D array"),

        (torch.rand(50, 50),    "torch.2D tensor"),
        (torch.rand(1, 50, 50), "torch.1-channel 3D tensor"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_accepts(self, input_tensor, desc):
        self.kernel(input_tensor) # doesn't raise


class DefaultBlurKernelTests(CommonKernelTests, TestCase):

    def setUp(self) -> None:
        self.kernel = default_blur_kernel()


class CustomValuedKernelTests(TestCase):

    @parameterized.expand([
        ([0.1], torch.rand(10, 10), "1 value but 0 channels"),
        ([0.1], torch.rand(3, 10, 10), "1 value but 3 channels"),
        ([0.1, 0.2], torch.rand(3, 10, 10), "2 values but 3 channels"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_on_shape_mismatch(self, channel_values, dummy_tensor, desc):
        kernel = custom_valued_deletion_kernel(channel_values)

        with self.assertRaises(RuntimeError):
            self.assertTupleEqual(kernel(dummy_tensor).shape, dummy_tensor.shape)

    @parameterized.expand([
        ([0.1], torch.rand(1, 10, 10), "1 channel and 1-channel img"),
        ([0.1, 0.5], torch.rand(2, 10, 10), "2 channels and 2-channel img"),
        ([0.1, 0.9, 0.3], torch.rand(3, 10, 10), "3 channels and 3-channel img"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_shape_is_maintained(self, channel_values, dummy_tensor, desc):
        kernel = custom_valued_deletion_kernel(channel_values)

        # act & assert
        self.assertTupleEqual(kernel(dummy_tensor).shape, dummy_tensor.shape)

    @parameterized.expand([
        ([0.1, 0.2, 0.3], "list"),
        (np.array([0.1, 0.2, 0.3]), "numpy array"),
        (torch.as_tensor([0.1, 0.2, 0.3]), "torch tensor"),
    ], 
    testcase_func_name=lambda f,n,p: f"{f.__name__}.for {p.args[-1]}")
    def test_result_is_as_expected(self, channel_values, desc):
        shape = (4, 4)
        dummy_tensor = torch.rand(len(channel_values), *shape)
        expected_result = torch.stack([torch.empty(*shape).fill_(v) for v in channel_values])

        kernel = custom_valued_deletion_kernel(channel_values)

        self.assertTrue(np.allclose(kernel(dummy_tensor), expected_result))
#endregion kernel-tests