from unittest import TestCase

import more_itertools
import numpy as np

from hirise.evaluation.pointing_game import pointing_game
from parameterized import parameterized

class TestPointingGame(TestCase):


    @parameterized.expand([
        ((5, 5), (1, 2, 2), 2, (1,), "single 2D map.as 2D tensor"),
        ((1, 5, 5), (1, 2, 2), 2, (1,), "single 2D map.as 3D tensor"),
        ((3, 5, 5), (3, 2, 2), 2, (3, ), "multiple 2D maps.3D tensor"),

        ((4, 3, 5, 5), (3, 2, 2), 2, (4, 3), "multiple 2D maps.3D tensor"),
        ((6, 4, 3, 5, 5), (3, 2, 2), 2, (6, 4, 3), "multiple 2D maps.ND tensor"),

        ((3, 5, 5, 5), (3, 3, 2), 3, (3,), "multiple 3D maps"),

        ((5, 5), (3, 2, 2), 2, (3,), "single map (as 2D), multiple boxes"),
        ((1, 5, 5), (3, 2, 2), 2, (3,), "single map (as 3D), multiple boxes"),
        ((3, 5, 5), (1, 2, 2), 2, (3,), "multiple maps, single bbox"),

        ((2, 1, 5, 5), (3, 2, 2), 2, (2, 3), "broadcasting.2 maps with extra 'broadcastable' dim, 3 bboxes"),
        ((2, 1, 5, 5), (1, 3, 2, 2), 2, (2, 3), "broadcasting.2 maps with extra 'broadcastable' dim, 3 bboxes with matching broadcast dim"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_shape(self, smap_shape, bbox_shape, single_map_ndim, expected_shape, desc):
        # arrange
        dummy_smaps = np.random.rand(*smap_shape)
        dummy_bbox = np.random.rand(*bbox_shape)

        # act
        result = pointing_game(dummy_smaps, dummy_bbox, single_map_ndim)

        # assert
        self.assertTupleEqual(result.shape, expected_shape)


    @parameterized.expand([
        ((10, 10), ((4, 6),), (((1, 4), (6, 9)),), [False], "no match when max point on edge"),

        ((10, 10), ((8, 8),), (((1, 4), (6, 9)),), [False], "single map.match along X axis, but not Y"),
        ((10, 10), ((3, 3),), (((1, 4), (6, 9)),), [False], "single map.match along Y axis, but not X"),
        ((10, 10), ((8, 3),), (((1, 4), (6, 9)),), [False], "single map.doesn't match along any axis"),
        ((10, 10), ((3, 8),), (((1, 4), (6, 9)),), [True], "single map.match along both axes"),

        (
            (10, 10),
            (
                (3, 4), (7, 8), (4, 3), (8, 7),
            ),
            (
                ((1, 5), (1, 5)),
                ((1, 5), (1, 5)),
                ((5, 9), (5, 9)),
                ((5, 9), (5, 9)),
            ),
            [True, False, False, True],
            "multiple maps.stacked along one dim"
        ),

        (
            (10, 10),
            (
                ((3, 4), (7, 8), (4, 3), (8, 7)),
                ((8, 7), (4, 3), (7, 8), (4, 3)),
            ),
            (
                ((1, 5), (1, 5)),
                ((1, 5), (1, 5)),
                ((5, 9), (5, 9)),
                ((5, 9), (5, 9)),
            ),
            [
                [True, False, False, True],
                [False, True, True, False],
            ],
            "multiple maps.stacked along multiple dims"
        ),

        (
            (10, 10),
            (
                # hypershape: (4, 2, 1)
                [[[2, 8]], # matches 1st bbox
                 [[4, 6]]], # matches 1st and 2nd

                [[[4, 4]], # matches 2nd
                 [[6, 4]]], # matches 2nd and 3rd

                [[[7, 2]], # matches 3rd
                 [[3, 7]]], # matches 1st

                [[[0, 0]], # matches none
                 [[9, 9]]], # matches none
            ),
            (
                # bounding boxes lie on the x != y diagonal
                # (ymin, ymax), (xmin, xmax)
                # hypershape: (3,)
                ((1, 5), (5, 9)),
                ((3, 7), (3, 7)),
                ((5, 9), (1, 5)),
            ),
            [
                # the (3,) hypershape of bbox is broadcast against (4,2,1) hypershape of maps
                # resulting in (4,2,3) tensor of results
                [[True, False, False], # map (0,0) vs each of the three bounding boxes
                 [True, True, False]], # map (0,1) vs each of the three bounding boxes
                [[False, True, False], # map (1,0)
                 [False, True, True]], # and so on
                [[False, False, True],
                 [True, False, False]],
                [[False, False, False],
                 [False, False, False]],
            ],
            "broadcasting.N-map (explicit broadast) M-box (implicit) scenario (case of gridsearch on multiinstance img)"
        ),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result(self, smap_shape, max_coords, bboxes, expected_result, desc):
        map_ndim = len(smap_shape)
        max_coords = np.array(max_coords)
        bboxes = np.array(bboxes)
        expected_result = np.array(expected_result)

        smaps = np.random.rand(*max_coords.shape[:-1], *smap_shape)
        for smap, max_coord in more_itertools.zip_equal(
            smaps.reshape(-1, *smaps.shape[-map_ndim:]),
            max_coords.reshape(-1, map_ndim)
        ):
            smap[tuple(max_coord)] = 1

        # act
        result = pointing_game(smaps, bboxes, map_ndim)

        # assert
        self.assertTrue(np.array_equal(result, np.array(expected_result)))

    @parameterized.expand([
        (
            (10, 10),
            (3, 4),
            (
                ((1, 5), (1, 5)),
                ((5, 9), (5, 9)),
            ),
            [True, False],
            "multiple bbox, single map (as 2D tensor)"
        ),
        (
            (1, 10, 10),
            (0, 3, 4),
            (
                ((1, 5), (1, 5)),
                ((5, 9), (5, 9)),
            ),
            [True, False],
            "multiple bbox, single map (as 3D tensor)"
        ),
        (
            (1, 10, 10),
            (0, 3, 4),
            (
                ((1, 5), (1, 5)),
                ((5, 9), (5, 9)),
            ),
            [True, False],
            "multiple bbox, multiple maps (as 3D tensor)"
        )
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_result_single_map_multiple_bbox(self, smap_tensor_shape, max_coord, bboxes, expected_result, desc):
        map_ndim = 2
        max_coord = np.array(max_coord)
        bboxes = np.array(bboxes)
        expected_result = np.array(expected_result)

        smap = np.random.rand(*smap_tensor_shape)
        smap[tuple(max_coord)] = 1

        # act
        result = pointing_game(smap, bboxes, map_ndim)

        # assert
        self.assertTrue(np.array_equal(result, np.array(expected_result)))

    @parameterized.expand([
        ((2, 5, 5), (2, 3, 3), 2, "bbox_ndim param.bbox for 3D map, but map_ndim declared to be 2D"),
        # technically not an error, but in gray area (the map tensor could be interpreted as 3D, after all)
        # ((2, 5, 5), (2, 3, 3), 3, "bbox_ndim param.bbox for 3D map, map_ndim 3D, but actual smap is 2D"),

        ((2, 5, 5), (3, 2, 2), 2, "2 maps, 3 bboxes"),
        ((2, 1, 5, 5), (3, 1, 2, 2), 2, "2 maps with broadcasting dim, 3 bboxes with broadcasting dim"),
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_on_dimension_mismatch(self, smap_shape, bbox_shape, single_map_ndim, desc):
        # arrange
        dummy_smaps = np.random.rand(*smap_shape)
        dummy_bbox = np.random.rand(*bbox_shape)

        with self.assertRaisesRegex(ValueError, expected_regex=r'could not be broadcast'):
            pointing_game(dummy_smaps, dummy_bbox, single_map_ndim)