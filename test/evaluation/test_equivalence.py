import unittest
from hirise.evaluation import CausalMetric
from hirise.evaluation.game import InsertionGame, DeletionGame
from hirise.evaluation.runner import GameRunner

import numpy as np

import torch
import torch.functional

from unittest import mock


class TestFunctionalEquivalence(unittest.TestCase):
    """
    Assures that the refactored AlterationGame still works like the original CausalMetric
    """
    @mock.patch("hirise.evaluation.game.device_from_torch_model")
    def setUp(self, device_getter_patch) -> None:
        super().setUp()

        device_getter_patch.return_value = torch.device("cpu")


        # The fake model "simulates" the scores returned by ResNet by aggregating the input and multiplying
        # the result by pre-generated array of class confidences
        # It does simulate the change in confidence scores as game progresses, but not the changes in TopK classes list,
        # as the latter is pre-determined and static
        self.fake_class_likelihoods = torch.rand((1, 1000))
        self.fake_model = mock.Mock(side_effect=lambda input: torch.sum(input) * self.fake_class_likelihoods.to(input.device))
        self.fake_model.parameters = mock.Mock(side_effect=[iter([torch.zeros([2, 2], dtype=torch.float32)])])


        self.fake_image = torch.from_numpy(np.random.rand(3, 224, 224))
        self.fake_explanation = np.random.rand(224, 224)
        self.substrate_fn = lambda x: torch.zeros(x.shape)

        self.step_size = 224 * 28

        self.refactored_game = DeletionGame(self.fake_model,
                                            self.fake_image,
                                            self.fake_explanation,
                                            step_size=self.step_size,
                                            substrate_fn=self.substrate_fn)
        # original game implementation always observes the score of the top class
        self.old_game = CausalMetric(self.fake_model,
                                     mode='del',
                                     step=self.step_size,
                                     substrate_fn=self.substrate_fn,
                                     class_names=mock.Mock())



        self.refactored_game_scores = GameRunner().run(self.refactored_game)
        self.old_game_scores = self.old_game.single_run(img_tensor=self.fake_image,
                                                        explanation=self.fake_explanation,
                                                        verbose=0)

    @unittest.skip("No longer true. Old game didn't support calculating scores for multiple classes simultaneously")
    def test_score_arrays_are_of_the_same_shape(self):
        self.assertTupleEqual(self.old_game_scores.shape, self.refactored_game_scores.shape)

    def test_number_of_scores_per_class_is_equal(self):
        self.assertEqual(self.old_game_scores.shape[0], self.refactored_game_scores.shape[0])

    def test_scores_are_equivalent(self):
        self.assertTrue(np.allclose(self.old_game_scores.flatten(), self.refactored_game_scores.flatten()))


if __name__ == '__main__':
    unittest.main()
