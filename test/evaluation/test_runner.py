import random
from unittest import mock, skip
from unittest.case import TestCase

import numpy as np
from hirise.evaluation.game import AlterationGame
from hirise.evaluation.runner import GameRunner, VisualizingRunner, TqdmRunner
from hirise.evaluation.visualization import Visualizer
from parameterized import parameterized


class TestGameRunner(TestCase):

    def setUp(self):
        super().setUp()

        self.initial_score = 0.999

        self.game_mock = mock.create_autospec(AlterationGame)
        self.game_mock.step = mock.Mock(side_effect=random.random)
        self.game_mock.is_over = mock.Mock(side_effect=[False] * 8 + [True] * 3)
        self.game_mock.scores = np.array([self.initial_score])

        self.runner = GameRunner()

    def test_game_runner_gets_initial_score_from_game(self):
        scores = self.runner.run(self.game_mock)

        self.assertAlmostEqual(scores[0], self.initial_score)

    def test_game_runner_returns_scores_as_numpy_array(self):
        scores = self.runner.run(self.game_mock)

        self.assertIsInstance(scores, np.ndarray)

    def test_game_runner_collects_all_scores_returned_by_game(self):
        scores = self.runner.run(self.game_mock)
        steps_taken = self.game_mock.step.call_count

        self.assertEqual(len(scores[1:]), steps_taken)

    def test_game_runner_runs_the_game_until_the_end(self):
        try:
            self.runner.run(self.game_mock)
            self.assertTrue(self.game_mock.is_over())
        except NotImplementedError as e:
            self.fail("GameRunner should not attempt to continue the game after it had finished")

    def test_game_runner_does_not_reset_the_game_if_it_hasnt_finished_yet(self):
        self.game_mock.reset.assert_not_called()
        self.assertFalse(self.game_mock.is_over())

        self.runner.run(self.game_mock)

        self.assertTrue(self.game_mock.is_over())
        self.game_mock.reset.assert_not_called()

    def test_game_runner_resets_the_game_if_its_already_finished(self):
        self.game_mock.reset.assert_not_called()
        self.game_mock.is_over = mock.Mock(side_effect=[True] + ([False]*3) + [True]*3)
        self.runner.run(self.game_mock)

        self.game_mock.reset.assert_called_once()

    @skip("This behaviour is undefined")
    def test_game_runner_does_not_reset_a_game_in_progress(self):
        raise NotImplementedError("Actually, this behaviour is undefined")

class TestVisualizingRunner(TestGameRunner):

    def setUp(self):
        super().setUp()

        self.visualizer_mock = mock.create_autospec(Visualizer)
        self.plt_patch = mock.patch("hirise.evaluation.runner.plt")
        self.plt_mock = self.plt_patch.start()
        self.runner = VisualizingRunner(self.visualizer_mock)

    @parameterized.expand([
        (1, 7),
        (2, 4),
        (3, 3),
        (7, 1)
    ])
    def test_calls_visualize_on_every_nth_step(self, n, expected_num_calls):
        self.runner.run(self.game_mock, show_every_nth_step=n)

        calls_to_visualize = self.visualizer_mock.visualize.call_count

        self.visualizer_mock.visualize.assert_called()
        self.assertEqual(calls_to_visualize, expected_num_calls)

    def test_new_figure_is_created_at_each_step(self):
        self.runner.run(self.game_mock)

        figures_created = self.plt_mock.figure.call_count
        game_steps = self.game_mock.step.call_count

        self.assertGreater(game_steps, 0)
        self.assertEqual(game_steps, figures_created)


    def tearDown(self) -> None:
        self.plt_patch.stop()

class TestTqdmRunner(TestGameRunner):

    def setUp(self):
        super().setUp()
        self.game_mock.n_steps = 3
        self.game_mock.current_step = 0
        self.runner = TqdmRunner()

    @mock.patch('hirise.evaluation.runner.tqdm')
    def test_progess_bar_is_updated_on_each_step(self, tqdm_patch):
        tqdm_mock = tqdm_patch.return_value

        self.runner.run(self.game_mock)
        update_calls = len(list(filter(lambda call: 'update' in call[0], tqdm_mock.mock_calls)))

        self.assertEqual(update_calls, self.game_mock.step.call_count)

    @mock.patch('hirise.evaluation.runner.tqdm')
    def test_initial_is_equal_to_current_step(self, tqdm_patch):
        current_step = 42
        self.game_mock.current_step = current_step

        self.runner.run(self.game_mock)
        self.assertEqual(tqdm_patch.call_args[1]['initial'], current_step)

    @mock.patch('hirise.evaluation.runner.tqdm')
    def test_total_is_equal_to_n_steps(self, tqdm_patch):
        n_steps = 42
        self.game_mock.n_steps = n_steps

        self.runner.run(self.game_mock)
        self.assertEqual(tqdm_patch.call_args[1]['total'], n_steps)

    @mock.patch('hirise.evaluation.runner.tqdm')
    def test_leave_bar_is_honored(self, tqdm_patch):
        leave_bar = True
        self.runner.run(self.game_mock, leave_bar)
        self.assertEqual(tqdm_patch.call_args[1]['leave'], leave_bar)

    @mock.patch('hirise.evaluation.runner.tqdm')
    def test_leave_bar_is_false_by_default(self, tqdm_patch):
        self.runner.run(self.game_mock)
        self.assertEqual(tqdm_patch.call_args[1]['leave'], False)