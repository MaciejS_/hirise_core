from unittest import TestCase
from unittest.mock import Mock

import torch
from hirise.structures import ClassFilter, CombinedMask


class TestClassFilter(TestCase):

    def setUp(self) -> None:
        self.retainer_mock = Mock(spec=CombinedMask)
        self.observed_class = 1

        self.masks_mock = Mock()
        self.dummy_scores = torch.rand(5, 10)

        self.class_filter = ClassFilter(self.retainer_mock, self.observed_class)

    def test_add_one(self):
        self.class_filter.add_one(self.masks_mock, self.dummy_scores)

        self.assertEqual(self.retainer_mock.add_one.call_count, 1)

        actual_masks, actual_scores = self.retainer_mock.add_one.call_args_list[0][0]
        self.assertEqual(actual_masks, self.masks_mock)
        self.assertTrue(torch.allclose(actual_scores, self.dummy_scores[:, self.observed_class]))


    def test_add_many(self):
        self.class_filter.add_many(self.masks_mock, self.dummy_scores)

        self.assertEqual(self.retainer_mock.add_many.call_count, 1)

        actual_masks, actual_scores = self.retainer_mock.add_many.call_args_list[0][0]
        self.assertEqual(actual_masks, self.masks_mock)
        self.assertTrue(torch.allclose(actual_scores, self.dummy_scores[:, self.observed_class]))


