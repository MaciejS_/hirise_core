from unittest import TestCase
from unittest.mock import Mock

import torch
from parameterized import parameterized
from hirise.structures import CombinedMask, BatchingCombinedMask


class TestBatchingCombinedMask(TestCase):

    def setUp(self) -> None:
        self.operation_size_limit = 100*(224 * 224)
        self.combined_mask_mock = Mock(spec=CombinedMask)
        self.batching_combined_mask = BatchingCombinedMask(self.combined_mask_mock,
                                                           single_op_size_limit=self.operation_size_limit,
                                                           single_mask_ndim=2,
                                                           use_tqdm=False)

    def test_add_one_ignores_the_operation_size_limit(self):
        self.masks = torch.rand(1000, 224, 224)
        self.scores = torch.rand(1000)

        self.batching_combined_mask.add_one(self.masks, self.scores)
        self.combined_mask_mock.add_one.assert_called_once_with(self.masks, self.scores)

    @parameterized.expand([
        ((20, 224, 224), (20, 1), 1, "single-op"),
        ((20, 224, 224), (20, 11), 3, "multi-op, classes cause batching"),
        ((150, 224, 224), (150, 1), 2, "multi-op, number of images causes batching")
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_operation_splitting_on_add_many(self, masks_shape, scores_shape, expected_batch_count, desc):
        masks = torch.rand(*masks_shape)
        scores = torch.rand(*scores_shape)

        self.batching_combined_mask.add_many(masks, scores)
        calls_to_add_many = self.combined_mask_mock.add_many.call_args_list

        self.assertEqual(len(calls_to_add_many), expected_batch_count)

    @parameterized.expand([
        ((112, 112), (1000,), "single sub-size image, too many classes"),
        ((224, 224), (101,), "single standard image, too many classes"),
        ((20, 224, 224), (20, 101), "multiple standard image, too many classes"),
        ((2250, 2250), (1,), "image too large"),
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_raises_when_operation_cannot_be_batched(self, masks_shape, scores_shape, desc):
        masks = torch.rand(*masks_shape)
        scores = torch.rand(*scores_shape)

        with self.assertRaises(Exception):
            self.batching_combined_mask.add_many(masks, scores)

    @parameterized.expand([
        ((24, 24),      (1),      (24, 24), "single 2D map, single class"),
        ((24, 24),      (1, 1),   (1, 24, 24), "single 2D map, single class 2D"),
        ((24, 24),      (7),      (7, 24, 24), "single 2D map, multiple classes"),
        ((24, 24),      (1, 7),   (7, 24, 24), "single 2D map, multiple classes 2D"),
        ((1, 24, 24),   (7),      (1, 7, 24, 24), "3D tensor with single 2D map, multiple classes"),
        ((1, 24, 24),   (1, 7),   (1, 7, 24, 24), "3D tensor with single 2D map, multiple classes 2D"),
        ((3, 24, 24),   (3),      (3, 24, 24), "multiple 2D maps, single class"),
        ((3, 24, 24),   (3, 1),   (3, 1, 24, 24), "multiple 2D maps, single class 2D"),
        ((3, 24, 24),   (3, 7),   (3, 7, 24, 24), "multiple 2D maps, multiple classes"),


    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_post_op_dim_calculation(self, masks_shape, scores_shape, expected_op_shape, desc):
        masks = torch.rand(masks_shape)
        scores = torch.rand(scores_shape)
        expected_op_size = torch.prod(torch.as_tensor(expected_op_shape)).item()

        post_op_size = self.batching_combined_mask.calculate_op_size(masks, scores)
        self.assertEqual(post_op_size, expected_op_size,
                        msg=f"Expected {expected_op_size}, got {post_op_size}")