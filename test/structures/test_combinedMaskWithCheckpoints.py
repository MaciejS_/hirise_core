import random
import unittest
from unittest import mock
from unittest.mock import Mock

import torch
from parameterized import parameterized

from hirise.structures import IntervalCheckpoints, CombinedMaskWithCheckpoints, ArbitraryCheckpoints
from hirise.structures import LambdaCheckpoints, CombinedMask

BOUNDED_INSERTION_TESTCASES = [
    (0, 24, 100, [(0, 24)]),
    (0, 100, 100, [(0, 100)]),
    (0, 101, 100, [(0, 100), (100, 101)]),
    (99, 1, 100, [(0, 1)]),
    (99, 2, 100, [(0, 1), (1, 2)]),
    (100, 1, 100, [(0, 1)]),
    (176, 24, 100, [(0, 24)]),
    (176, 30, 100, [(0, 24), (24, 30)]),
    (176, 176, 100, [(0, 24), (24, 124), (124, 176)]),
    (176, 227, 100, [(0, 24), (24, 124), (124, 224), (224, 227)]),
]


def test_name_gen(testcase_func, param_num, params):
    existing_masks, new_masks, interval = params.args[:3]
    test_name = testcase_func.__name__
    interval__format = "{test_name}_when_append_{new_masks}_to_{existing_masks}_interval_{interval}".format(
        **locals())
    return interval__format


class CombinedMaskWithCheckpointsFixture():

    def setUp(self) -> None:
        self.mask_shape = (1, 1)

        self.combined_mask = CombinedMask(self.mask_shape, 0.1)
        self.mask_mock = mock.MagicMock(wraps=self.combined_mask)

        self.mask_count_mock = mock.PropertyMock()
        self.mask_count_mock.__get__ = lambda s, i, o: self.combined_mask.mask_count
        type(self.mask_mock).mask_count = self.mask_count_mock


class TestCombinedMaskWithCheckpoints(unittest.TestCase, CombinedMaskWithCheckpointsFixture):

    def setUp(self) -> None:
        unittest.TestCase.setUp(self)
        CombinedMaskWithCheckpointsFixture.setUp(self)
        self.mask_with_checkpoints = CombinedMaskWithCheckpoints(self.mask_mock)

    def test_add_one_adds_to_underlying_mask(self):
        mask = torch.ones(self.mask_shape)
        score = random.random()
        self.mask_with_checkpoints.add_one(mask, score)

        self.mask_mock.add_one.assert_called_once_with(mask, score)

    def test_add_many_adds_to_underlying_mask(self):
        n_masks = 5
        masks = torch.ones((n_masks, *self.mask_shape))
        score = torch.rand(n_masks)
        self.mask_with_checkpoints.add_many(masks, score)

        self.mask_mock.add_many.assert_called_once_with(masks, score)

    def test_save_checkpoint_adds_a_new_checkpoint(self):
        masks_stored = 3
        self.mask_count_mock.__get__ = lambda s,i,o: masks_stored
        checkpoint_state_mock = torch.rand(5, 5)
        self.mask_mock.get_mask.return_value = checkpoint_state_mock

        self.mask_with_checkpoints.save_checkpoint()

        checkpoints = self.mask_with_checkpoints.get_checkpoints()
        self.assertEqual(len(checkpoints), 1)

        checkpoint = checkpoints[0]
        mask_count, mask_state = checkpoint
        self.assertIsInstance(checkpoint, tuple)
        self.assertEqual(len(checkpoint), 2)
        self.assertEqual(mask_count, masks_stored)
        self.assertTrue(torch.allclose(mask_state, checkpoint_state_mock))

    def test_mask_counter_returns_the_counter_of_underlying_combined_mask(self):
        self.mask_count_mock = mock.PropertyMock()
        type(self.mask_mock).mask_count = self.mask_count_mock

        x = self.mask_with_checkpoints.mask_count
        self.mask_count_mock.assert_called_once_with()


class TestLambdaCheckpoints(unittest.TestCase, CombinedMaskWithCheckpointsFixture):

    def setUp(self) -> None:
        CombinedMaskWithCheckpointsFixture.setUp(self)
        unittest.TestCase.setUp(self)

        self.mask_with_checkpoints = LambdaCheckpoints(self.mask_mock, lambda i,m,s:True)


    @parameterized.expand([
         (0, [[0, 1, 2, 3]],     [],        lambda i, m, s: False, "no checkpoints"),
         (0, [[0, 1, 2, 3]],     [],        lambda i, m, s: (i == 0), "checkpoint at 0 masks is not possible"),
         (0, [[0], [1, 2, 3]],   [1],       lambda i, m, s: (i == 1), "single checkpoint at the beginning"),
         (0, [[0, 1, 2], [3]],   [3],       lambda i, m, s: (i == 3), "single checkpoint in the middle"),
         (0, [[0, 1, 2, 3]],     [4],       lambda i, m, s: (i == 4), "single checkpoint at the end"),
         (0, [[0], [1, 2], [3]], [1, 3],    lambda i, m, s: i % 2 == 1, "two checkpoints, one at the beginning"),
         (0, [[0, 1], [2], [3]], [2, 3],    lambda i, m, s: i in (2, 3), "two checkpoints next to each other"),
         (0, [[0], [1, 2, 3]],   [1, 4],    lambda i, m, s: i in (1, 4), "two checkpoints at the edges"),
         (0, [[0, 1], [2, 3]],   [2, 4],    lambda i, m, s: i in (2, 4), "two checkpoints, one at the end"),
         (1, [[0], [1, 2], [3]], [2, 4],    lambda i, m, s: not i % 2, "1 mask inserted earlier, no checkpoint at the end"),
         (1, [[0, 1], [2, 3]],   [1, 3, 5], lambda i, m, s: i % 2, "1 mask inserted earlier, checkpoint at the end"),
    ], testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_insertions_on_add_many(self, preinserted_masks, expected_batches, expected_checkpoints, checkpoint_lambda, msg):
        n_masks = 4

        self.mask_with_checkpoints = LambdaCheckpoints(self.mask_mock, checkpoint_lambda)
        for i in range(preinserted_masks):
            self.mask_with_checkpoints.add_one(torch.rand(*self.mask_shape), random.random())

        # exclude pre-inserted masks, for insertion batch checks at the end
        self.mask_mock.reset_mock()

        fake_masks = torch.ones((n_masks, *self.mask_shape)) * torch.arange(n_masks).reshape(-1, 1, 1)
        fake_scores = torch.rand(n_masks)

        # Act
        self.mask_with_checkpoints.add_many(fake_masks, fake_scores)
        actual_checkpoints = [idx for idx, state in self.mask_with_checkpoints.get_checkpoints()]

        # Assert
        # assert that checkpoints were created at expected mask counts
        self.assertListEqual(actual_checkpoints, expected_checkpoints,
                             msg="Expected checkpoints at %s, got %s" % (expected_checkpoints, actual_checkpoints))

        # assert that masks were inserted in expected batches
        for idx, args in enumerate(self.mask_mock.add_many.call_args_list):
            masks, scores = args[0]
            self.assertTrue(torch.allclose(masks.squeeze(), torch.as_tensor(expected_batches[idx], dtype=torch.float)))


    def test_add_many_creates_a_checkpoint_iff_lambda_returns_true(self):
        every_other_mask = lambda i, m, s: not i % 2
        self.mask_with_checkpoints = LambdaCheckpoints(self.mask_mock, every_other_mask)

        n_masks = 5
        fake_masks = torch.rand(n_masks, *self.mask_shape)
        fake_scores = torch.rand(n_masks)

        self.mask_with_checkpoints.add_many(fake_masks, fake_scores)
        checkpoints = self.mask_with_checkpoints.get_checkpoints()

        expected_checkpoint_indices = [idx
                                       for idx, _, _
                                       in filter(lambda x: every_other_mask(*x), zip(range(1, n_masks+1), fake_masks, fake_scores))]
        actual_checkpoint_indices = [idx for idx, state in checkpoints]
        self.assertListEqual(expected_checkpoint_indices, actual_checkpoint_indices)


    def test_add_many_when_no_checkpoints_need_to_be_made(self):
        self.mask_with_checkpoints = LambdaCheckpoints(self.mask_mock, lambda i,m,s: False)

        n_masks = 3
        fake_masks = torch.rand(n_masks, *self.mask_shape)
        fake_scores = torch.rand(n_masks)

        # Act
        self.mask_with_checkpoints.add_many(fake_masks, fake_scores)
        checkpoints = self.mask_with_checkpoints.get_checkpoints()

        # Assert
        self.mask_mock.add_many.assert_called_once_with(fake_masks, fake_scores)
        self.assertEqual(len(checkpoints), 0)


    @parameterized.expand([
        (1, lambda i, m, s: True, "creates a checkpoint when lambda returns true"),
        (0, lambda i, m, s: False, "does not create when lambda returns false")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_add_one(self, expected_checkpoints, checkpoint_lambda, msg):
        self.mask_with_checkpoints = LambdaCheckpoints(self.mask_mock, checkpoint_lambda)

        self.mask_with_checkpoints.add_one(torch.rand(*self.mask_shape), random.random())
        checkpoints = self.mask_with_checkpoints.get_checkpoints()

        self.mask_mock.add_one.assert_called_once()
        self.assertEqual(len(checkpoints), expected_checkpoints)



class TestIntervalCheckpoints(unittest.TestCase, CombinedMaskWithCheckpointsFixture):

    def setUp(self) -> None:
        CombinedMaskWithCheckpointsFixture.setUp(self)
        unittest.TestCase.setUp(self)

        self.mask_occlusion_prob = 0.1
        self.checkpoint_interval = 5
        self.num_masks = 11
        self.masks = torch.ones((self.num_masks, *self.mask_shape))
        self.scores = torch.ones(self.num_masks)

        self.combined_mask = CombinedMask(self.mask_shape, self.mask_occlusion_prob)
        self.mask_with_checkpoints = IntervalCheckpoints(self.combined_mask, self.checkpoint_interval)

        self.mask_with_checkpoints.add_many(self.masks, self.scores)

        self.expected_mask_at_first_checkpoint = CombinedMask.from_masks(self.masks[:5],
                                                                         self.scores[:5],
                                                                         self.mask_occlusion_prob).get_mask()
        self.expected_mask_at_second_checkpoint = CombinedMask.from_masks(self.masks[:10],
                                                                          self.scores[:10],
                                                                          self.mask_occlusion_prob).get_mask()

    def test_checkpoint_retrieval(self):
        checkpoints = self.mask_with_checkpoints.get_checkpoints()

        self.assertEquals(len(checkpoints), 2)
        self.assertTrue(torch.allclose(checkpoints[0][1], self.expected_mask_at_first_checkpoint))
        self.assertTrue(torch.allclose(checkpoints[1][1], self.expected_mask_at_second_checkpoint))

        self.assertEquals(self.combined_mask.mask_counter, self.num_masks)

    def test_checkpoint_is_a_tuple_of_number_masks_used_and_the_combined_mask(self):
        checkpoints = self.mask_with_checkpoints.get_checkpoints()

        first_mask_count, first_combined_mask = checkpoints[0]
        second_mask_count, second_combined_mask = checkpoints[1]

        self.assertIsInstance(first_mask_count, int)
        self.assertIsInstance(first_combined_mask, torch.Tensor)

        self.assertEquals(first_mask_count, self.checkpoint_interval)
        self.assertEquals(second_mask_count, self.checkpoint_interval * 2)

    @parameterized.expand(BOUNDED_INSERTION_TESTCASES, testcase_func_name=test_name_gen)
    def test_calculate_insertion_boundaries(self, existing_masks, new_masks, interval, expected_boundaries):
        boundaries = IntervalCheckpoints.calculate_insertion_boundaries(existing_masks, new_masks, interval)
        error_msg = "When adding {new_masks} to {existing_masks} masks with interval={interval}, expected boundaries at {expected_boundaries}, but got {boundaries}"
        self.assertListEqual(boundaries,
                             expected_boundaries,
                             msg=error_msg.format(**locals()))


class TestArbitraryCheckpoints(CombinedMaskWithCheckpointsFixture,
                               unittest.TestCase):

    def setUp(self):
        self.dummy_checkpoint_idxs = [1, 3, 8]
        self.mask_shape = (5, 5)
        self.combiner = CombinedMask(self.mask_shape, 0.2)
        self.checkpoints = ArbitraryCheckpoints(self.combiner, self.dummy_checkpoint_idxs)

        n_masks = max(self.dummy_checkpoint_idxs) + 2
        self.checkpoints.add_many(torch.ones(n_masks, *self.mask_shape),
                                  scores=torch.ones(n_masks))
        self.actual_checkpoints = self.checkpoints.get_checkpoints()

    def test_only_maps_at_checkpoint_idx_are_retained(self):
        self.assertSequenceEqual([n for n, smap in self.actual_checkpoints],
                                 self.dummy_checkpoint_idxs)

    @parameterized.expand([
        (0, 3, [1], [(0,1), (1,3)], "single checkpoint"),
        (0, 5, [2, 4], [(0,2), (2, 4), (4,5)], "more masks in batch then checkpoints"),
        (0, 5, [2, 4, 10], [(0,2), (2, 4), (4,5)], "does not "),
        (0, 5, [2, 5], [(0,2), (2, 5)], "checkpoint on last mask in batch"),
        (2, 4, [1, 10], [(0, 4)], "no checkpoint crossed by this batch"),
        (4, 2, [4, 5], [(0,1), (1,2)], "boundaries are correctly adjusted to the number of existing masks")
    ])
    def test_calculate_insertion_boundaries(self, existing_masks, new_masks, checkpoint_idx, expected_result, desc):
        self.assertSequenceEqual(ArbitraryCheckpoints.calculate_insertion_boundaries(existing_masks, new_masks, checkpoint_idx),
                                 expected_result)