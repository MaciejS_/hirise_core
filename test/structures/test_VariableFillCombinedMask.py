import functools
import operator as op
from unittest import TestCase, mock

import torch
from hirise.structures import VariableFillCombinedMask, CombinedMask
from parameterized import parameterized


class TestVariableFillCombinedMask(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.mask_shape = (10, 10)
        self.mask_occlusion_prob = 0.5

        self.num_test_masks = 3

        self.test_masks = (torch.rand(5, *self.mask_shape) < 0.4).to(torch.float32)
        self.test_scores = torch.rand(5)
        self.actual_fill_rates = self.test_masks.mean(dim=(-1, -2))

        self.combined_mask = VariableFillCombinedMask(self.mask_shape)

    def test_sets_occlusion_prob_to_1(self):
        # The purpose of setting occlusion_prob to 1 is to prevent it from altering the final result when get_mask() is called
        # The VariableFillCombinedMask has already accounted for the fill rate and that final division would falsify the result

        self.assertEqual(self.combined_mask.mask_occlusion_prob, 1)

    @mock.patch("hirise.structures.CombinedMask.from_masks", spec=CombinedMask.from_masks)
    def test_sets_occlusion_to_1_when_instantiated_from_set_of_masks(self, from_masks_mock):
        fill_rates = self.test_masks.mean(dim=(-1, -2))
        random_occlusion_prob = 0.314

        VariableFillCombinedMask.from_masks(self.test_masks, self.test_scores, random_occlusion_prob)

        self.assertNotAlmostEqual(fill_rates.mean().item(), 1)
        from_masks_mock.assert_called_once()
        args, kwargs = from_masks_mock.call_args_list[0]

        # assert that occlusion_probability is always set to 1
        self.assertEqual(kwargs["occlusion_probability"], 1)

        # assert that first two args are indeed the masks and scores
        self.assertTrue(torch.allclose(self.test_masks, args[0]))
        self.assertTrue(torch.allclose(self.test_scores, args[1]))

    @mock.patch("hirise.structures.CombinedMask.from_map", spec=CombinedMask.from_map)
    def test_sets_occlusion_to_1_when_instantiated_from_existing_map(self, from_masks_mock):
        test_map = torch.rand(*self.mask_shape)
        test_number_of_masks = 10
        random_occlusion_prob = 0.314

        # act
        VariableFillCombinedMask.from_map(test_map, test_number_of_masks, random_occlusion_prob)

        # assert
        self.assertNotAlmostEqual(self.actual_fill_rates.mean().item(), 1)
        from_masks_mock.assert_called_once()
        args, kwargs = from_masks_mock.call_args_list[0]

        # assert that occlusion_probability is always set to 1
        self.assertEqual(kwargs["occlusion_probability"], 1)

        # assert that first two args are indeed the masks and scores
        self.assertTrue(torch.allclose(test_map, args[0]))
        self.assertEqual(test_number_of_masks, args[1])


    @parameterized.expand([
            (0.45, "score as float"),
            (torch.as_tensor([0.45]), "score as 1-elem tensor"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    @mock.patch("hirise.structures.CombinedMask.add_one", spec=True)
    def test_add_one_calls_super_with_score_adjusted_by_fill_rate(self, score, desc, add_one_mock):
        test_mask = self.test_masks[0]
        fill_rate = test_mask.mean()

        # act
        self.combined_mask.add_one(self.test_masks[0], score)

        # assert
        add_one_mock.assert_called_once()
        args, kwargs = add_one_mock.call_args_list[0]
        self.assertTrue(torch.allclose(test_mask, args[0]))
        self.assertTrue(torch.allclose(score / fill_rate, args[1]))

    @mock.patch("hirise.structures.CombinedMask.add_many", spec=True)
    def test_add_many_calls_super_with_score_adjusted_by_fill_rate(self, add_many_mock):
        test_masks = (torch.rand(5, *self.mask_shape) < 0.4).to(torch.float32)
        fill_rates = test_masks.mean(dim=(-1, -2))
        test_scores = torch.rand(5)

        # act
        self.combined_mask.add_many(test_masks, test_scores)

        # assert
        add_many_mock.assert_called_once()
        args, kwargs = add_many_mock.call_args_list[0]
        self.assertTrue(torch.allclose(test_masks, args[0]))
        self.assertTrue(torch.allclose(test_scores / fill_rates, args[1]))


    def test_result_mask_state_is_correct(self):
        hyperdim = (3, 5)
        mask_count = functools.reduce(op.mul, hyperdim, 1)

        test_masks = (torch.rand(*hyperdim, *self.mask_shape) < 0.4).to(torch.float32)
        fill_rates = test_masks.mean(dim=(-1,-2))
        test_scores = torch.rand(*hyperdim)

        expected_result_mask = (test_masks * (test_scores/fill_rates)[:, :, None, None]).sum(dim=(0, 1))

        # act
        for mask_set, score_set in zip(test_masks, test_scores):
            self.combined_mask.add_many(mask_set, score_set)
        result = self.combined_mask.get_mask()

        # assert
        self.assertTupleEqual(result.shape, expected_result_mask.shape)
        self.assertTrue(torch.allclose(
            result,
            expected_result_mask / mask_count
        ))

    @mock.patch("hirise.structures.CombinedMask.add_one", spec=True)
    def test_add_one__score_of_empty_mask_is_always_overridden_by_zero(self, add_one_mock):
        test_mask = torch.zeros(*self.mask_shape, dtype=torch.float32)
        score = torch.rand(1).item()
        expected_score = 0

        # act
        self.combined_mask.add_one(test_mask, score)

        # assert
        add_one_mock.assert_called_once()
        args, kwargs = add_one_mock.call_args_list[0]
        self.assertTrue(torch.allclose(test_mask, args[0]))
        self.assertEqual(expected_score, args[1])


    @mock.patch("hirise.structures.CombinedMask.add_many", spec=True)
    def test_add_many__empty_masks_always_get_a_score_of_zero(self, add_many_mock):
        normal_masks = (torch.rand(4, *self.mask_shape) < 0.4).to(torch.float32)

        test_masks = torch.cat([
            normal_masks[:2],
            torch.zeros(1, *self.mask_shape, dtype=torch.float32),
            normal_masks[-2:],
        ])
        fill_rates = test_masks.mean(dim=(-1,-2))
        test_scores = torch.rand(len(test_masks))
        expected_scores_tensor = torch.cat(
            [test_scores[:2] / fill_rates[:2],
             torch.zeros((1,)),
             test_scores[-2:] / fill_rates[-2:]]
        )

        # act
        self.combined_mask.add_many(test_masks, test_scores)

        # assert
        add_many_mock.assert_called_once()
        args, kwargs = add_many_mock.call_args_list[0]
        self.assertTrue(torch.allclose(test_masks, args[0]))
        self.assertTrue(torch.allclose(expected_scores_tensor, args[1]))
