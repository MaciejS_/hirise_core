import unittest

import torch
from parameterized import parameterized

from hirise.structures import CombinedMask


class TestCombinedMask(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.mask_shape = (2, 2)
        self.mask_occlusion_prob = 0.5

        self.num_test_masks = 3
        self.all_ones_masks = torch.ones((self.num_test_masks, *self.mask_shape))
        self.all_ones_scores = torch.ones((self.num_test_masks))
        self.combined_mask = CombinedMask(self.mask_shape, self.mask_occlusion_prob)


    @parameterized.expand([
        (torch.rand(2, 2), torch.ones((1,)), (2, 2), "single map, single score"),
        (torch.rand(1, 2, 2), torch.ones((1,)), (2, 2), "one map slice of multiple maps"),
        (torch.rand(3, 2, 2), torch.ones((3,)), (2, 2), "multiple maps")
    ])
    def test_creation_from_masks(self, masks_tensor, scores, expected_map_shape, msg):
        combined_mask = CombinedMask.from_masks(masks_tensor, scores, 0.2)
        self.assertTupleEqual(combined_mask.get_mask().shape, expected_map_shape)

    def test_creation_from_checkpoint(self):
        saliency_map = torch.rand(3,3)
        n = 100
        p1 = 1

        restored_mask = CombinedMask.from_map(saliency_map, n, p1)
        self.assertEqual(restored_mask.mask_count, n)
        self.assertTrue(torch.allclose(restored_mask.get_mask(), saliency_map/n/p1))

    @parameterized.expand([
        (torch.ones((3, 2, 2)), torch.ones(3), 3 * torch.ones((2, 2)) / 3, "3 2D masks"),
        (torch.ones((3, 1, 2, 2)), torch.ones((3,1)), 3 * torch.ones((2, 2)) / 3, "3 slices of a 3D mask stack"),
        (torch.ones((3, 3, 2, 2)), torch.ones((3,3)), 9 * torch.ones((2, 2)) / 9, "3 batches of 3 masks each"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_add_many(self, masks, scores, expected_mask, msg):
        for mask_batch, score in zip(masks, scores):
            self.combined_mask.add_many(mask_batch, score)
        self.assertTrue(torch.allclose(self.combined_mask.get_mask(), expected_mask / self.mask_occlusion_prob),
                        msg="Got %s" % self.combined_mask.get_mask())
        self.assertEqual(self.combined_mask.mask_count, torch.prod(torch.as_tensor(masks.shape[:-2])))

    @parameterized.expand([
        (torch.ones(1), "1D mask batch"),
        (torch.ones((1, 1, 1, 1)), "4D+ mask batch")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_add_many_raises_on(self, mask, msg):
        with self.assertRaises(ValueError):
            self.combined_mask.add_one(mask, 1.0)

        mask_state = self.combined_mask.get_mask()
        self.assertEqual(self.combined_mask.mask_count, 0,
                         msg="Expected no masks to have been added, but the counter shows %d" % self.combined_mask.mask_count)
        self.assertTrue(torch.allclose(mask_state, torch.zeros_like(mask_state)), msg="expected mask to be all zeros, got\n %s" % mask_state)


    @parameterized.expand([
        (torch.ones((3,2,2)), torch.ones(3), 3 * torch.ones((2,2)) / 3, "3 2D masks"),
        (torch.ones((3,2,2)), torch.arange(3), sum(range(3)) * torch.ones((2,2)) / 3, "3 2D masks with varying scores"),
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_add_one(self, masks, scores, expected_mask, msg):
        for mask, score in zip(masks, scores):
            self.combined_mask.add_one(mask, score)
        self.assertTrue(torch.allclose(self.combined_mask.get_mask(), expected_mask / self.mask_occlusion_prob))
        self.assertEqual(self.combined_mask.mask_count, torch.prod(torch.as_tensor(masks.shape[:-2])))

    @parameterized.expand([
        (torch.ones(1), "1D mask"),
        (torch.ones((1,1,1)), "3D mask"),
        (torch.ones((1,1,1,1)), "4D mask")
    ],
    testcase_func_name=lambda f, n, p: "%s.%s" % (f.__name__, p.args[-1]))
    def test_add_one_raises_value_error_on(self, mask, msg):
        with self.assertRaises(ValueError):
            self.combined_mask.add_one(mask, 1.0)

        mask_state = self.combined_mask.get_mask()
        self.assertEqual(self.combined_mask.mask_count, 0,
                         msg="Expected no masks to have been added, but the counter shows %d" % self.combined_mask.mask_count)
        self.assertTrue(torch.allclose(mask_state, torch.zeros_like(mask_state)), msg="expected mask to be all zeros, got\n %s" % mask_state)

    @parameterized.expand([
        (torch.rand(3, 2, 2), torch.rand(3), CombinedMask.add_one),
        (torch.rand(4, 3, 2, 2), torch.rand(4, 3), CombinedMask.add_many),
    ])
    def test_on_random_data(self, masks, scores, insertion_method):
        for i in range(len(masks)):
            insertion_method(self.combined_mask, masks[i], scores[i])

        result_mask = self.combined_mask.get_mask()
        expected_mask = torch.mean(masks.reshape(-1, 2, 2) * scores.reshape(-1, 1, 1), dim=0) / self.mask_occlusion_prob
        self.assertTrue(torch.allclose(result_mask, expected_mask))

    def test_get_mask_returns_zero_when_no_masks_were_added(self):
        self.assertEqual(self.combined_mask.mask_count, 0)
        mask_state = self.combined_mask.get_mask()
        self.assertTrue(torch.allclose(mask_state, torch.zeros_like(mask_state)),
                        msg="expected mask to be all zeros, got\n %s" % mask_state)


if __name__ == '__main__':
    unittest.main()
