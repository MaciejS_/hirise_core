from unittest import TestCase
from unittest.mock import Mock

from hirise.structures import TopMasksRetainer, CombinedMask
import random

class TopMasksRetainerTestBase(TestCase):

    def setUp(self) -> None:
        super().setUp()
        self.underlying_combined_mask_mock = Mock(spec=CombinedMask)

        self.sample_masks = [("Mask1", 0.5), ("Mask2", 0.3), ("Mask3", 0.7)]
        self.top_masks = TopMasksRetainer(self.underlying_combined_mask_mock, 3)
        self.top_masks.add_many(*zip(*self.sample_masks))

    def create_prefilled_retainer(self, max_size, n_masks_to_insert):
        masks_to_insert = [("Mask%d" % i, (random.random() + 0.2)/3) for i in range(n_masks_to_insert)]

        retainer = TopMasksRetainer(self.underlying_combined_mask_mock, max_size)
        retainer.add_many(*zip(*masks_to_insert))

        return retainer

class TestMaskRetainerAddOne(TopMasksRetainerTestBase):

    def test_add_when_empty(self):
        self.top_masks = TopMasksRetainer(self.underlying_combined_mask_mock, 3)
        self.top_masks.add_one(*self.sample_masks[0])

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), 1)

    def test_add_another(self):
        self.top_masks = self.create_prefilled_retainer(3, 1)

        self.top_masks.add_one("Foo", 0.4)

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), 2)

    def test_add_one_passes_the_mask_down(self):
        mask_mock, score = Mock(), 1

        self.top_masks.add_one(mask_mock, score)
        self.underlying_combined_mask_mock.add_one.assert_called_once_with(mask_mock, score)

    def test_when_full_then_new_mask_pushes_out_the_smallest_mask(self):
        stack_size = 3
        self.top_masks = self.create_prefilled_retainer(stack_size, stack_size)

        new_mask, new_score = Mock(), 0.99999
        self.top_masks.add_one(new_mask, new_score)

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), stack_size)
        self.assertIn((new_score, new_mask), saved_masks)

    def test_when_full_then_new_mask_smaller_than_all_other_on_stack_is_ignored(self):
        stack_size = 3
        self.top_masks = self.create_prefilled_retainer(stack_size, stack_size)

        new_mask, new_score = Mock(), 0.0001
        self.top_masks.add_one(new_mask, new_score)

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), stack_size)
        self.assertNotIn((new_score, new_mask), saved_masks)


class TestMaskRetainerAddMany(TopMasksRetainerTestBase):

    def test_add_many(self):
        stack_size = 4
        self.top_masks = TopMasksRetainer(self.underlying_combined_mask_mock, stack_size)

        masks_to_insert = self.sample_masks[:3]
        self.top_masks.add_many(*zip(*masks_to_insert))

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), 3)
        self.assertTrue(all(tuple(reversed(mask)) in saved_masks
                            for mask
                            in masks_to_insert))

    def test_when_full_inserting_only_better_masks_replaces_entire_stack(self):
        stack_size = 3
        self.top_masks = self.create_prefilled_retainer(stack_size, stack_size)

        masks_to_insert = [("Mask", score) for score in (0.8, 0.9, 0.99)]
        self.top_masks.add_many(*zip(*masks_to_insert))

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), stack_size)
        self.assertListEqual(saved_masks, [(score, mask) for mask, score in reversed(masks_to_insert)])

    def test_when_full_then_inserting_only_worse_masks_rejects_them(self):
        stack_size = 3
        self.top_masks = self.create_prefilled_retainer(stack_size, stack_size)

        masks_to_insert = [("Mask", score) for score in (0.01, 0.1, 0.2)]
        self.top_masks.add_many(*zip(*masks_to_insert))

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), stack_size)
        self.assertFalse(all((score, mask) in saved_masks
                             for mask, score
                             in masks_to_insert))

    def test_add_many_some_pushed_out_some_ignored(self):
        stack_size = 4
        pre_inserted_masks = [("Mask5", 0.5), ("Mask3", 0.3), ("Mask7", 0.7)]
        masks_to_insert = [("Mask4", 0.4), ("Mask1", 0.1), ("Mask6", 0.6)]
        expected_masks = [("Mask7", 0.7), ("Mask6", 0.6), ("Mask5", 0.5), ("Mask4", 0.4)]
        self.top_masks = TopMasksRetainer(self.underlying_combined_mask_mock, stack_size)
        self.top_masks.add_many(*zip(*pre_inserted_masks))

        self.top_masks.add_many(*zip(*masks_to_insert))

        saved_masks = self.top_masks.get_entries()
        self.assertEqual(len(saved_masks), stack_size)
        self.assertListEqual(saved_masks, [(score, mask) for mask, score in expected_masks])

    def test_add_many_does_not_trigger_calls_to_add_one_in_underlying_mask(self):
        masks, scores = zip(*self.sample_masks)
        self.underlying_combined_mask_mock.reset_mock()

        self.top_masks.add_many(masks, scores)

        self.underlying_combined_mask_mock.add_one.assert_not_called()
        self.underlying_combined_mask_mock.add_many.assert_called_once_with(masks, scores)



class TestMaskRetainerGetters(TopMasksRetainerTestBase):

    def setUp(self) -> None:
        super().setUp()
        self.top_masks = self.create_prefilled_retainer(3, 3)

    def test_get_entries_returns_score_first_then_mask(self):
        entries = self.top_masks.get_entries()
        score, mask = random.choice(entries)

        self.assertIsInstance(score, float)
        self.assertIsInstance(mask, str)

    def test_entries_are_sorted_in_descending_order(self):
        entries = self.top_masks.get_entries()

        retrieved_scores = [score for score, mask in entries]
        self.assertListEqual(retrieved_scores, sorted(retrieved_scores, reverse=True))

    def test_get_top_n_entries_returns_top_n(self):
        requested_n = 2

        n_entries = self.top_masks.get_entries(requested_n)
        all_entries = self.top_masks.get_entries()

        self.assertEqual(len(n_entries), requested_n)
        self.assertListEqual(n_entries, all_entries[:requested_n])

    def test_get_masks(self):
        all_entries = self.top_masks.get_entries()
        masks = self.top_masks.get_masks()

        self.assertListEqual(masks, [mask for score, mask in all_entries])

    def test_get_scores(self):
        all_entries = self.top_masks.get_entries()
        scores = self.top_masks.get_scores()

        self.assertListEqual(scores, [score for score, mask in all_entries])

    def test_get_n_larger_than_stack_size_defaults_to_stack_size(self):
        stack_size = 20
        self.top_masks = self.create_prefilled_retainer(stack_size, stack_size)

        self.assertEqual(len(self.top_masks.get_entries(1000)), len(self.top_masks.get_entries()))

    def test_get_n_with_negative_value_throws(self):
        with self.assertRaises(ValueError):
            self.top_masks.get_entries(-1)

