from unittest import TestCase, skip
import numpy as np
import torch

from parameterized import parameterized_class

from hirise.structures_utils import hash_tensor

@skip # prevent unparameterized instance from being ran
@parameterized_class([
    {"tensor_instantiator": lambda self, shape: np.random.rand(*shape),
     "copier": lambda self, x: x.copy(),
     "desc": "Numpy"},

    {"tensor_instantiator": lambda self, shape: torch.rand(shape),
     "copier": lambda self, x: x.clone(),
     "desc": "Torch"},
],
class_name_func=lambda c,n,d: f"{c.__name__}.{d['desc']}")
class TestHashTensor(TestCase):

    def setUp(self):
        self.default_shape = (100, 10, 10)
        self.test_tensor = self.tensor_instantiator(self.default_shape)

    def test_hash_is_identical_for_identical_tensors(self):#, tensor, copy_func, desc):
        clone = self.copier(self.test_tensor)

        self.assertNotEqual(id(self.test_tensor), id(clone))
        self.assertTrue((self.test_tensor == clone).all())
        self.assertEqual(hash_tensor(self.test_tensor), hash_tensor(clone), msg="Hashes of identical tensors should be equal")

    def test_hash_differs_for_different_tensors(self):
        other = self.tensor_instantiator(self.default_shape)

        self.assertFalse((self.test_tensor == other).all())
        self.assertNotEqual(hash_tensor(self.test_tensor), hash_tensor(other), msg="Hashes of differing tensors should differ")
