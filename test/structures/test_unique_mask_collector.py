import numpy as np
import random

from parameterized import parameterized
from unittest import TestCase, mock
from unittest.mock import Mock

from hirise.structures import UniqueMaskCollector


class TestUniqueMaskCollector(TestCase):

    def setUp(self) -> None:
        self.hash_function_mock = Mock(side_effect=lambda x: random.randint(0, 0xffff_ffff))
        self.nested_retainer_mock = Mock()
        self.collector = UniqueMaskCollector(retainer=self.nested_retainer_mock,
                                             mask_hash_function=self.hash_function_mock)

        self.N_masks = 10
        self.dummy_masks = np.random.rand(self.N_masks, 3, 3)
        self.dummy_scores = np.random.rand(self.N_masks)

    @parameterized.expand([
            (1, "called once"),
            (5, "called multiple times")
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.{p.args[-1]}")
    def test_add_one(self, n_calls, desc):
        masks = np.random.rand(n_calls, 3, 3)

        for single_mask in masks:
            self.collector.add_one(single_mask, Mock())

        self.assertEqual(len(self.collector.masks), n_calls)
        for mask in masks:
            self.assertTrue(any(np.allclose(m, mask) for m in self.collector.masks))

        self.assertEqual(self.hash_function_mock.call_count, n_calls)
        for i, ((intercepted_mask,), _) in enumerate(self.hash_function_mock.call_args_list):
            self.assertTrue(np.array_equal(intercepted_mask, masks[i]))

    @mock.patch.object(UniqueMaskCollector, "_add_one")
    def test_add_many_calls_add_one_on_each_mask(self, internal_add_one_mock):
        self.collector.add_many(self.dummy_masks, self.dummy_scores)

        self.assertEqual(internal_add_one_mock.call_count, self.N_masks)
        for i, passed_mask in enumerate([x[0][0] for x in internal_add_one_mock.call_args_list]):
            self.assertTrue(np.array_equal(passed_mask, self.dummy_masks[i]),
                            msg=f"Mask passed to call #{i} differs from expected")

    def test_add_many_passes_masks_down_to_nested_retainer(self):
        test_masks = np.random.rand(4, 3, 3)
        test_scores = np.random.rand(4)

        self.collector.add_many(test_masks, test_scores)

        self.nested_retainer_mock.add_one.assert_not_called()

        self.nested_retainer_mock.add_many.assert_called_once()
        intercepted_masks, intercepted_scores = self.nested_retainer_mock.add_many.call_args_list[0][0]
        self.assertTrue(np.array_equal(intercepted_masks, test_masks))
        self.assertTrue(np.array_equal(intercepted_scores, test_scores))

    def test_add_one_passes_masks_down_to_nested_retainer(self):
        test_mask = np.random.rand(3, 3)
        test_scores = np.random.rand(1).item()

        self.collector.add_one(test_mask, test_scores)

        self.nested_retainer_mock.add_many.assert_not_called()

        self.nested_retainer_mock.add_one.assert_called_once()
        intercepted_masks, intercepted_scores = self.nested_retainer_mock.add_one.call_args_list[0][0]
        self.assertTrue(np.array_equal(intercepted_masks, test_mask))
        self.assertAlmostEqual(intercepted_scores, test_scores)

    @mock.patch.object(UniqueMaskCollector, "_add_one")
    def test_add_many_ravels_hyperdimensions(self, internal_add_one_mock):
        hyperdims = (4, 3, 2)
        mask_ndim = 2

        masks = np.random.rand(*hyperdims, *((5,) * mask_ndim))
        scores = np.random.rand(*hyperdims)
        collector = UniqueMaskCollector(mask_ndim=mask_ndim, mask_hash_function=self.hash_function_mock)
        expected_number_of_added_masks = np.product(hyperdims)

        # act
        collector.add_many(masks, scores)

        # assert
        self.assertEqual(internal_add_one_mock.call_count, expected_number_of_added_masks)
        for i, (ndidx, passed_mask) in enumerate(zip(np.ndindex(*hyperdims),
                                                     ([x[0][0] for x in internal_add_one_mock.call_args_list]))):
            self.assertTrue(np.array_equal(passed_mask, masks[ndidx]),
                            msg=f"Mask passed to call #{i} differs from expected")

    @parameterized.expand([
            (1, 2),
            (1, 3),
            (2, 1),
            (2, 3),
            (3, 1),
            (3, 2),
            (3, 4),
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.expected={p.args[-2]}, actual={p.args[-1]}")
    def test_add_one_raises_value_error_when_mask_dimensionality_does_not_match(self, expected_mask_ndim, actual_ndim):
        single_mask = np.random.rand(*((np.random.randint(3, 8),) * actual_ndim))
        collector = UniqueMaskCollector(mask_ndim=expected_mask_ndim, mask_hash_function=self.hash_function_mock)

        with self.assertRaises(ValueError):
            collector.add_one(single_mask, Mock())

    @parameterized.expand([
            (2, 1),
            (2, 2),
            (3, 2),
            (3, 3),
    ], testcase_func_name=lambda f, n, p: f"{f.__name__}.expected={p.args[-2]}, actual={p.args[-1]}")
    def test_add_many_raises_value_error_when_dimensionality_lower_or_equal_to_declared(self, declared_ndim,
                                                                                        actual_ndim):
        # raises when ndim equal to declared, because, by definition, masks tensor passed to add *many* should have at least one extra dimension
        test_mask_shape = (np.random.randint(3, 8),) * actual_ndim
        masks = np.random.rand(*test_mask_shape)
        collector = UniqueMaskCollector(mask_ndim=declared_ndim, mask_hash_function=self.hash_function_mock)

        with self.assertRaises(ValueError):
            collector.add_many(masks, Mock())
