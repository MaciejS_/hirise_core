import math
import unittest
from unittest import TestCase, mock, skip
from unittest.mock import Mock

import torch
from hirise.mask_evaluator import MaskEvaluator
from parameterized import parameterized, parameterized_class


@skip # prevent unparameterized version from running
@parameterized_class([
    {"device": torch.device("cpu"), "description": "Cpu"},
    {"device": torch.device("cuda:0"), "description": "Gpu"}
], class_name_func=lambda cls, idx, paramdict: f"{cls.__name__}{paramdict['description']}")
class TestEvaluator(TestCase):

    def setUp(self) -> None:
        # self.device is provided by parameterized
        if self.device.type == "cuda" and torch.cuda.device_count() == 0:
            raise unittest.SkipTest("CUDA devices not available")

        self.n_classes = 100
        self.model_mock = Mock(side_effect=lambda tensor: torch.rand((tensor.shape[0], self.n_classes)))
        self.model_mock.half = Mock(return_value=self.model_mock) # called when fp16 mode is selected
        self.model_mock.float = Mock(return_value=self.model_mock) # called when fp32 mode is selected
        self.batch_size = 3

        self.evaluator = MaskEvaluator(self.model_mock, self.device, self.batch_size, False)

        self.n_masks = 10
        self.input_size = (3, 3)
        self.img_mock = torch.rand(2, *self.input_size)
        self.masks = torch.rand((self.n_masks, 1, *self.input_size))

        self.result = self.evaluator.evaluate(self.img_mock, self.masks, 0)

    def test_masks_are_processed_in_batches(self):
        expected_number_of_calls = math.ceil(self.n_masks / self.batch_size)
        call_args = self.model_mock.call_args_list

        self.assertEqual(len(call_args), expected_number_of_calls)

    def test_evaluate_with_fp16(self):
        if self.device.type == 'cpu':
            self.skipTest(reason="Test disabled because PyTorch doesn't support FP16 on CPU")

        self.model_mock.reset_mock()
        evaluator = MaskEvaluator(self.model_mock, self.device, self.batch_size, False, use_fp16=True)

        # act
        result = evaluator.evaluate(self.img_mock, self.masks, 0)

        # assert
        self.assertEqual(result.dtype, torch.float32)
        self.model_mock.half.assert_called_once() # model is converted to float on initialization
        self.model_mock.assert_called()
        (masked_imgs, ), _ = self.model_mock.call_args_list[0]
        self.assertEqual(masked_imgs.dtype, torch.float16)

    def test_evaluate_with_fp32(self):
        self.model_mock.reset_mock()
        evaluator = MaskEvaluator(self.model_mock, self.device, self.batch_size, False, use_fp16=False)

        # act
        result = evaluator.evaluate(self.img_mock, self.masks, 0)

        # assert
        self.assertEqual(result.dtype, torch.float32)
        self.model_mock.float.assert_called_once() # model is converted to float on initialization
        self.model_mock.assert_called()
        (masked_imgs, ), _ = self.model_mock.call_args_list[0]
        self.assertEqual(masked_imgs.dtype, torch.float32)

    def test_raises_runtime_error_on_attempt_to_use_fp16_on_cpu(self):
        if self.device.type != "cpu":
            self.skipTest(reason="Applicable only for test parameterized with CPU device")

        with self.assertRaises(RuntimeError):
            MaskEvaluator(self.model_mock, self.device, self.batch_size, False, use_fp16=True)

    @parameterized.expand([
        (3, (10,) , "single class"),
        ([1, 3], (10, 2), "list of classes"),
        ((1,3), (10, 2), "tuple of classes"),
        (Ellipsis, (10, 100), "all classes"),
        (None, (10, 100), "all classes if None")
    ],
    testcase_func_name=lambda f,n,p: f"{f.__name__}.{p.args[-1]}")
    def test_evaluate_result_shape(self, observed_classes, expected_shape, msg):
        result = self.evaluator.evaluate(self.img_mock, self.masks, observed_classes)
        self.assertTupleEqual(result.shape, expected_shape)

    @parameterized.expand([
        (100, 30, [(0, 30), (30, 60), (60, 90), (90, 100)]),
        (100, 50, [(0, 50), (50, 100)]),
        (3, 1, [(0,1), (1,2), (2,3)]),
        (250, 500, [(0,250)])
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.N{p.args[0]}_batch{p.args[1]}")
    def test_batch_boundaries(self, N, batch_size, expected_boundaries):
        boundaries = list(MaskEvaluator.batch_boundaries(N, batch_size))
        self.assertListEqual(boundaries, expected_boundaries)

    @parameterized.expand([
        (0, 0),
        (-1, 1),
        (1, -1),
        (-1, -1),
        (-1, 0),
        (0, -1)
    ],
    testcase_func_name=lambda f, n, p: f"{f.__name__}.N({p.args[0]})_batch({p.args[1]})")
    def test_raises_on_invalid_values(self, N, batch_size):
        with self.assertRaises(ValueError):
            MaskEvaluator.batch_boundaries(N, batch_size)


class TestEvaluatorBanded(TestCase):

    def setUp(self) -> None:
        self.model_mock = Mock()
        self.batch_size = 10

        self.img_mock = Mock()
        self.mask_ns = [5, 8, 17]
        self.masks = [torch.rand(x, 10, 10) for x in self.mask_ns]

        self.evaluator = MaskEvaluator(self.model_mock, Mock(), self.batch_size, False)
        self.evaluate_patcher = mock.patch.object(self.evaluator, "evaluate",
                                               side_effect=lambda i, m, c: torch.rand(m.shape[0], 1000))

        self.evaluate_mock = self.evaluate_patcher.start()
        self.scores = self.evaluator.evaluate_banded(self.img_mock, self.masks)

    def test_evaluate_banded_returns_a_list_of_score_tensors_corresponding_to_mask_tensors(self):
        self.assertIsInstance(self.scores, (list, tuple))
        self.assertEqual(len(self.scores), len(self.masks))
        self.assertListEqual([int(m.shape[0]) for m in self.masks], [int(s.shape[0]) for s in self.scores])

    def test_evaluate_banded_calls_regular_evaluate_with_raveled_list_of_masks(self):
        ravelled_masks = torch.cat(self.masks)

        self.evaluate_mock.assert_called_once()
        img, masks, observed_classes = self.evaluate_mock.call_args_list[0][0]

        self.assertTrue(torch.allclose(masks, ravelled_masks))

    def tearDown(self) -> None:
        self.evaluate_patcher.stop()