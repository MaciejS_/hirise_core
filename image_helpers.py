import math
import numpy as np

from matplotlib import pyplot as plt

def torch_make_displayable(inp):
    inp = inp.numpy().transpose((1, 2, 0))
    # Mean and std for ImageNet
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    inp = std * inp + mean
    return np.clip(inp, 0, 1)

def grid_size_for_images(n_images) -> (int, int):
    """ 
    Returns (x,y) size of the image grid.
    The grid will never be wider than MAX_GRID_X.
    Large collection of images (more than MAX_GRID_X**2), will extend along the Y axis
    
    """
    MAX_GRID_X = 4
    sqrt_n = math.ceil(math.sqrt(n_images))
    x_dim = min(MAX_GRID_X, sqrt_n)
    y_dim = math.ceil(n_images/x_dim)
    return (x_dim, y_dim)


def grid_plot_images(images, salience_maps=None, titles=None):
    xdim, ydim = grid_size_for_images(len(images))
    f, axarr = plt.subplots(ydim, xdim)
    f.set_size_inches(*map(lambda x: x*5, (xdim, ydim)))
    for y in range(ydim):
        for x in range(xdim):
            img_id = y * xdim + x
            if img_id < len(images):
                axarr[y,x].axis('off')
                axarr[y,x].imshow(images[img_id])
                if salience_maps is not None:
                    axarr[y,x].imshow(salience_maps[img_id], cmap='jet', alpha=0.5)
#                     axarr[y,x].colorbar(fraction=0.046, pad=0.04)
                if titles is not None: 
                    axarr[y,x].set_title(titles[img_id])
    f.show()